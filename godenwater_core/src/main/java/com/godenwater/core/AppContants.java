/**
 * $RCSfile$
 * $Revision: 13437 $
 * $Date: 2013-02-04 07:34:30 -0600 (Mon, 04 Feb 2013) $
 *
 * Copyright (C) 2004-2008 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.godenwater.core;

import java.io.File;

/**
 * Contains constant values representing various objects in Jive.
 */
public class AppContants {

	public static final int SYSTEM = 17;
	public static final int ROSTER = 18;
	public static final int OFFLINE = 19;
	public static final int MUC_ROOM = 23;
	public static final int SECURITY_AUDIT = 25;
	public static final int MUC_SERVICE = 26;

	public static final long SECOND = 1000;
	public static final long MINUTE = 60 * SECOND;
	public static final long HOUR = 60 * MINUTE;
	public static final long DAY = 24 * HOUR;
	public static final long WEEK = 7 * DAY;

	public static final String SEPARATE = File.separator;
	/**
	 * 分隔符
	 */
	public static String SEPARATER = ",";
	public static String COMMA = ",";
	public static String COLON = ":";
	public static String SEMICOLON = ";";
	
	public static final String SSO_USER = "RTU_SSO_USER";
	public static final String USER_SESSION = "RTU_USER";
	public static final String USER_ROLES = "RTU_ROLE";

	/**
	 * COOKIE
	 */
	public static final String COOKIE_USER_ID = "cookie.user.id";
	public static final String COOKIE_USER_NAME = "cookie.user.name";
	public static final String COOKIE_USER_AUTOLOGIN = "cookie.user.autologin";
	public static final String COOKIE_USER_HASH = "cookie.user.hash";

	public static final String ANONYMOUS_USER_ID = "anonymousUserId";

	/**
	 * 模板目录
	 */
	public static final String TEMPLATES_MAPPING = "templates.mapping";
	public static final String TEMPLATE_DIRECTORY = "template.directory";
	public static final String TEMPLATE_NAME = "template.name";


	/**
	 * 证书信息
	 *
	 */
	public static String CERT_PWD = "CERT_PWD";

	/**
	 * 验证码配置部分
	 */
	public static final String CAPTCHA_IGNORE_CASE = "captcha.ignore.case";
	public static final String CAPTCHA_REGISTRATION = "captcha.registration";
	public static final String CAPTCHA_POSTS = "captcha.posts";
	public static final String CAPTCHA_WIDTH = "captcha.width";
	public static final String CAPTCHA_HEIGHT = "captcha.height";
	public static final String CAPTCHA_MIN_FONT_SIZE = "captcha.min.font.size";
	public static final String CAPTCHA_MAX_FONT_SIZE = "captcha.max.font.size";
	public static final String CAPTCHA_MIN_WORDS = "captcha.min.words";
	public static final String CAPTCHA_MAX_WORDS = "captcha.max.words";

	/**
	 * 邮件部分
	 */
	public static final String MAIL_BATCH_SIZE = "mail.batch.size";
	public static final String MAIL_LOST_PASSWORD_MESSAGE_FILE = "mail.lostPassword.messageFile";
	public static final String MAIL_LOST_PASSWORD_SUBJECT = "mail.lostPassword.subject";
	public static final String MAIL_NOTIFY_ANSWERS = "mail.notify.answers";
	public static final String MAIL_SENDER = "mail.sender";
	public static final String MAIL_CHARSET = "mail.charset";
	public static final String MAIL_TEMPLATE_ENCODING = "mail.template.encoding";
	public static final String MAIL_NEW_ANSWER_MESSAGE_FILE = "mail.newAnswer.messageFile";
	public static final String MAIL_NEW_ANSWER_SUBJECT = "mail.newAnswer.subject";
	public static final String MAIL_NEW_PM_SUBJECT = "mail.newPm.subject";
	public static final String MAIL_NEW_PM_MESSAGE_FILE = "mail.newPm.messageFile";
	public static final String MAIL_MESSSAGE_FORMAT = "mail.messageFormat";

	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_LOCALHOST = "mail.smtp.localhost";
	public static final String MAIL_SMTP_USERNAME = "mail.smtp.username";
	public static final String MAIL_SMTP_PASSWORD = "mail.smtp.password";
	public static final String MAIL_SMTP_DELAY = "mail.smtp.delay";

	public static final String MAIL_SMTP_SSL = "mail.smtp.ssl";

	public static final String MAIL_SMTP_SSL_AUTH = "mail.smtps.auth";
	public static final String MAIL_SMTP_SSL_HOST = "mail.smtps.host";
	public static final String MAIL_SMTP_SSL_PORT = "mail.smtps.port";
	public static final String MAIL_SMTP_SSL_LOCALHOST = "mail.smtps.localhost";

	public static final String MAIL_USER_EMAIL_AUTH = "mail.user.email.auth";
	public static final String MAIL_ACTIVATION_KEY_MESSAGE_FILE = "mail.activationKey.messageFile";
	public static final String MAIL_ACTIVATION_KEY_SUBJECT = "mail.activationKey.subject";

	public static final String MAIL_NEW_TOPIC_MESSAGE_FILE = "mail.newTopic.messageFile";
	public static final String MAIL_NEW_TOPIC_SUBJECT = "mail.newTopic.subject";

	/**
	 * 头像信息部分
	 */
	public static final String AVATAR_GALLERY_DIR = "avatar.gallery.dir";
	public static final String AVATAR_UPLOAD_DIR = "avatar.upload.dir";
	public static final String AVATAR_MAX_SIZE = "avatar.maxSize";
	public static final String AVATAR_MIN_WIDTH = "avatar.minWidth";
	public static final String AVATAR_MIN_HEIGHT = "avatar.minHeight";
	public static final String AVATAR_MAX_WIDTH = "avatar.maxWidth";
	public static final String AVATAR_MAX_HEIGHT = "avatar.maxHeight";
	public static final String AVATAR_ALLOW_UPLOAD = "avatar.allow.upload";
	public static final String AVATAR_ALLOW_GALLERY = "avatar.allow.gallery";
	public static final String AVATAR_STORE_PATH = "avatar.store.path";

	/**
	 * 系统基础信息部分
	 */
	public static String base_name = "base_name";
	public static String base_title = "base_title";
	public static String base_url = "base_url";
	public static String base_keyword = "base_keyword";
	public static String base_descript = "base_descript";
	public static String base_illegal_word = "base_illegal_word";
	public static String base_domain_flag = "base_domain_flag";

	public static String base_www_domain = "base_www_domain";
	public static String base_res_domain = "base_res_domain";
	public static String base_cdn_domain = "base_cdn_domain";

	public static String base_logo = "base_logo";
	public static String base_copyright = "base_copyright";
	public static String base_qrcode = "base_qrcode";
	public static String base_wapcode = "base_wapcode";
	public static String base_icp = "base_icp";
	public static String base_status = "base_status";
	public static String base_status_note = "base_status_note";
	public static String base_style = "base_style";
	public static String localstorage_interval = "localstorage_interval";
	public static String localstorage_flag = "localstorage_flag";

	/**
	 * 系统联系人
	 */
	public static String link_email = "link_email";
	public static String link_tel = "link_tel";
	public static String link_qq = "link_qq";
	public static String link_address = "link_address";
	public static String link_onlineQQ = "link_onlineQQ";
	public static String link_hotline = "link_hotline";

	public static String wap_domain = "wap_domain";
	public static String wap_name = "wap_name";

	/**
	 * 系统水印
	 */
	public static String watermark_width = "watermark_width";
	public static String watermark_height = "watermark_height";
	public static String watermark_path = "watermark_path";
	public static String watermark_position = "watermark_position";
	public static String watermark_positionX = "watermark_positionX";
	public static String watermark_positionY = "watermark_positionY";

	/**
	 * 奖励部分
	 */
	public static String bonus_units = "bonus_units";
	public static String bonus_name = "bonus_name";
	public static String bonus_minWithdraw = "bonus_minWithdraw";
	public static String bonus_intWithdraw = "bonus_intWithdraw";

	/**
	 * 用户注册部分
	 */
	public static String user_reg_flag = "user_reg_flag";
	public static String user_reg_code = "user_reg_code";
	public static String user_reg_close = "user_reg_close";
	public static String user_reg_codeflag = "user_reg_codeflag";
	public static String user_reg_sendflag = "user_reg_sendflag";
	public static String user_reg_ftlEmail = "user_reg_ftlEmail";
	public static String user_reg_ftlSMS = "user_reg_ftlSMS";
	public static String user_username_type = "user_username_type";
	public static String user_username_prefix = "user_username_prefix";
	public static String user_username_length = "user_username_length";
	public static String user_login_trycount = "user_login_trycount";
	public static String user_login_award = "user_login_award";

	/**
	 * 用户找回密码部分
	 */
	public static String user_pwd_findtype = "user_pwd_findtype";
	public static String user_pwd_mail = "user_pwd_mail";
	public static String user_pwd_SMS = "user_pwd_SMS";
	public static String user_pwd_verifyCodeFormat = "user_pwd_verifyCodeFormat";
	public static String user_pwd_verifyCodeLength = "user_pwd_verifyCodeLength";

	/**
	 * 系统短信设置部分
	 */
	public static String sms_trade = "sms_trade";
	public static String sms_sdk = "sms_sdk";
	public static String sms_encode = "sms_encode";
	public static String sms_user = "sms_user";
	public static String sms_pass = "sms_pass";
	public static String sms_result = "sms_result";
	public static String sms_separated = "sms_separated";
	public static String sms_max = "sms_max";
	public static String sms_post = "sms_post";
	public static String sms_mobile_rule = "sms_mobile_rule";

	public static String sms_reg_valid_flag = "sms_reg_valid_flag";
	public static String sms_reg_valid_ftl = "sms_reg_valid_ftl";
	public static String sms_reg_success_flag = "sms_reg_success_flag";
	public static String sms_reg_success_ftl = "sms_reg_success_ftl";
	public static String sms_pwd_change_flag = "sms_pwd_change_flag";
	public static String sms_pwd_change_ftl = "sms_pwd_change_ftl";

	/**
	 * 系统邮件设置部分
	 */
	public static String mail_smtp_host = "mail_smtp_host";
	public static String mail_smtp_port = "mail_smtp_port";
	public static String mail_smtp_user = "mail_smtp_user";
	public static String mail_smtp_pass = "mail_smtp_pass";
	public static String mail_smtp_ssl = "mail.smtp.ssl";
	public static String mail_smtp_debug = "mail_smtp_debug";

	public static String mail_reg_valid_flag = "mail_reg_valid_flag";
	public static String mail_reg_valid_ftl = "mail_reg_valid_ftl";
	public static String mail_reg_success_flag = "mail_reg_success_flag";
	public static String mail_reg_success_ftl = "mail_reg_success_ftl";
	public static String mail_pwd_change_flag = "mail_pwd_change_flag";
	public static String mail_pwd_change_ftl = "mail_pwd_change_ftl";

	/**
	 * 支付接口
	 */
	public static String payment_alipay_flag = "payment_alipay_flag";
	public static String payment_alipay_type = "payment_alipay_type";
	public static String payment_alipay_account = "payment_alipay_account";
	public static String payment_alipay_id = "payment_alipay_id";
	public static String payment_alipay_key = "payment_alipay_key";

	public static String payment_tenpay_flag = "payment_tenpay_flag";
	public static String payment_tenpay_id = "payment_tenpay_id";
	public static String payment_tenpay_key = "payment_tenpay_key";

	public static String payment_chinabank_flag = "payment_chinabank_flag";
	public static String payment_chinabank_id = "payment_chinabank_id";
	public static String payment_chinabank_key = "payment_chinabank_key";

	public static String payment_alipayfast_flag = "payment_alipayfast_flag";
	public static String payment_alipayfast_account = "payment_alipayfast_account";
	public static String payment_alipayfast_id = "payment_alipayfast_id";
	public static String payment_alipayfast_key = "payment_alipayfast_key";

	public static String payment_weixin_flag = "payment_weixin_flag";
	public static String payment_weixin_id = "payment_weixin_id";
	public static String payment_weixin_key = "payment_weixin_key";

	/**
	 * 微信接口
	 */
	public static String weixin_name = "weixin_name";
	public static String weixin_appid = "weixin_appid";
	public static String weixin_appsecret = "weixin_appsecret";
	public static String weixin_qrcode = "weixin_qrcode";
	public static String weixin_token = "weixin_token";
	public static String weixin_welcome = "weixin_welcome";

	/**
	 * 第三方登录接口
	 */
	public static String sign_sina_flag = "sign_sina_flag";
	public static String sign_sina_appid = "sign_sina_appid";
	public static String sign_sina_appkey = "sign_sina_appkey";

	public static String sign_ali_flag = "sign_ali_flag";
	public static String sign_ali_appid = "sign_ali_appid";
	public static String sign_ali_appkey = "sign_ali_appkey";

	public static String sign_qq_flag = "sign_qq_flag";
	public static String sign_qq_appid = "sign_qq_appid";
	public static String sign_qq_appkey = "sign_qq_appkey";
	
	/**
	 * 页面元素
	 */
	public static String page_index_banner = "page_index_banner";
	public static String page_index_banner_width = "page_index_banner_width";
	public static String page_index_banner_height = "page_index_banner_height";
	public static String page_index_static = "page_index_static";
	public static String page_index_interval = "page_index_interval"; 
	public static String page_index_bg = "page_index_bg"; 

}