package com.godenwater.core.json;

public class JsonResult {

	//返回值状态的定义码
	public static String STATUS = "status";
	public static String MESSAGE = "message";
	
	public static String SUCCESS = "success";
	public static String FAILURE = "failure";
	
	//返回值的数据定义码
	public static String ROWS = "rows";
	public static String TOTAL = "total";
	public static String PAGE = "page";
	public static String RECORDS = "records";
	public static String MODEL = "model";
	
}
