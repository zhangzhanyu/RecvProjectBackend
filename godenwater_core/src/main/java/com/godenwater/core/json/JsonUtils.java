package com.godenwater.core.json;

import java.util.List;
import java.util.Map;

public class JsonUtils {

	/**
	 * public static void putSuccess(JSONObject jsonObject) {
	 * jsonObject.put(JsonResult.STATUS, JsonResult.SUCCESS); }
	 * 
	 * public static void putFailure(JSONObject jsonObject, String message) {
	 * jsonObject.put(JsonResult.STATUS, JsonResult.FAILURE);
	 * jsonObject.put(JsonResult.MESSAGE, message); }
	 * 
	 * public static void putDataList(JSONObject jsonObject, List dataList) {
	 * jsonObject.put(JsonResult.TOTAL, dataList.size());
	 * jsonObject.put(JsonResult.ROWS, dataList); }
	 * 
	 * public static void putSuccess(Model model) {
	 * model.addAttribute(JsonResult.STATUS, JsonResult.SUCCESS); }
	 * 
	 * public static void putFailure(Model model, String message) {
	 * model.addAttribute(JsonResult.STATUS, JsonResult.FAILURE);
	 * model.addAttribute(JsonResult.MESSAGE, message); }
	 * 
	 * public static void putDataList(Model model, List dataList) {
	 * model.addAttribute(JsonResult.TOTAL, dataList.size());
	 * model.addAttribute(JsonResult.ROWS, dataList); }
	 */
	public static void putSuccess(Map map) {
		map.put(JsonResult.SUCCESS, true);
		map.put(JsonResult.STATUS, JsonResult.SUCCESS);
	}
	
	public static void putSuccess(Map map, String message) {
		map.put(JsonResult.STATUS, JsonResult.SUCCESS);
		map.put(JsonResult.MESSAGE, message);
		map.put(JsonResult.SUCCESS, true);
	}

	public static void putMessage(Map map, String message) {
		map.put(JsonResult.MESSAGE, message);
	}
	
	public static void putValue(Map map, String key, Object value) {
		map.put(key, value);
	}

	public static void putFailure(Map map, String message) {
		map.put(JsonResult.STATUS, JsonResult.FAILURE);
		map.put(JsonResult.MESSAGE, message);
		map.put(JsonResult.SUCCESS, false);
	}

	public static void putDataList(Map map, List dataList) {
		map.put(JsonResult.TOTAL, dataList.size());
		map.put(JsonResult.ROWS, dataList);
	}
	
	public static void putModel(Map map, Object model) {
		map.put(JsonResult.MODEL, model);
	}

}
