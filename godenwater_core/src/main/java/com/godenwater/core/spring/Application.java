/*
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.godenwater.core.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoader;

/**
 * @author
 */
public class Application {

	// private ApplicationContext ctx;
	private ApplicationContext ctx;
	private static Application app = null;

	private Application() {

		// ctx =
		// WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext());
		ctx = ContextLoader.getCurrentWebApplicationContext();
		if (ctx == null) {
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>spring getCurrentWebApplicationContext is null");
			String source = "applicationContext.xml";
			ctx = new ClassPathXmlApplicationContext(source);
			// ctx = new
			// ClassPathXmlApplicationContext("com/wdf/base/eoa_spring_common.xml");
			// ctx = new
			// FileSystemXmlApplicationContext("D:/eclipse/workspace/wdf/eoa_spring_common.xml");
		}

	}

	public static synchronized Application getInstance() {
		if (app == null) {
			app = new Application();
		}
		return app;
	}

	/**
	 * @return Returns the ctx.
	 * @uml.property name="ctx"
	 */
	public ApplicationContext getCtx() {
		return ctx;
	}

	/**
	 * @param ctx
	 *            The ctx to set.
	 * @uml.property name="ctx"
	 */
	public void setCtx(ApplicationContext ctx) {
		this.ctx = ctx;
	}

	/**
	 * @return Returns the ctx.
	 * @uml.property name="ctx"
	 */
	public Object getBean(String beanName) {
		return ctx.getBean(beanName);
	}

	public static void main(String[] args) {
		// org.springframework.data.redis.cache.RedisCacheManager
		// redisCacheManager =
		// (org.springframework.data.redis.cache.RedisCacheManager) Application
		// .getInstance().getCtx().getBean("redisCacheManager");
		// System.out.println(Configurer.getContextProperty("center_addvcd"));
		// System.out.println(redisCacheManager);
		//
		// Cache aCache = redisCacheManager.getCache("aaaa");
		//
		// aCache.evict("a1");
		//
		// // aCache.put("a1", "aaaaaaaaaa1111111111");
		// // aCache.put("a1", "aaaaaaaaaa000000000000000");
		// // aCache.put("a2", "aaaaaaaaaa2222222222");
		// org.springframework.data.redis.cache.RedisCacheManager
		// redisCacheManager2 =
		// (org.springframework.data.redis.cache.RedisCacheManager) Application
		// .getInstance().getCtx().getBean("redisCacheManager");
		// System.out.println(redisCacheManager2.getCache("aaaa").get("a1").get());

		org.springframework.data.redis.core.RedisTemplate redisTemplate = (org.springframework.data.redis.core.RedisTemplate) Application
				.getInstance().getCtx().getBean("redisTemplate");
		String msg = "Hello, Redis!";
		redisTemplate.convertAndSend("monitor", msg);
		// 发布信息：可以通过RedisConnection或者RedisTemplate来实现。
		// redisConnection.publish(byte[] msg,byte[] channel);
		// template.convertAndSend("hello!","world");
	}
}
