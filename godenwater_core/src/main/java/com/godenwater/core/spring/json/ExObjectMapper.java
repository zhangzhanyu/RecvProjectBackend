package com.godenwater.core.spring.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.core.JsonParser;

/*
 * JSON对象转换器
 */
public class ExObjectMapper extends ObjectMapper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8751323507053436270L;

	public ExObjectMapper(){
		
		
		super();
		System.out.println("===============================================");
		System.out.println("=== com.pq.core.spring.json.ExObjectMapper  ===");
		System.out.println("===============================================");
		//允许单引号
		this.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		//字段和值都加引号
		this.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		//数字也加引号
		this.configure(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		//空值处理为空串
		
		this.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>(){

			@Override
			public void serialize(Object arg0, JsonGenerator jg,
					SerializerProvider arg2) throws IOException,
					JsonProcessingException {
				// TODO Auto-generated method stub
				jg.writeString("");
			}
			
		});
		
//		this.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>(){
//
//			@Override
//			public void serialize(Object value, JsonGenerator jg,
//					SerializerProvider sp) throws IOException,
//					JsonProcessingException {
//				// TODO Auto-generated method stub
//				System.out.println("-------JSON MAPPING------------");
//				jg.writeString("");
//			}
//			
//		});
		
	}
	
	
	
}
