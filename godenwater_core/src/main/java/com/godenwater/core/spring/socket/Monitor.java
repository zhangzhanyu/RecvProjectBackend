package com.godenwater.core.spring.socket;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

public class Monitor {

	private final int id;
	private final WebSocketSession session;

	public Monitor(int id, WebSocketSession session) {
		this.id = id;
		this.session = session;
	}

	protected void sendMessage(String msg) throws Exception {
		session.sendMessage(new TextMessage(msg));
	}

}
