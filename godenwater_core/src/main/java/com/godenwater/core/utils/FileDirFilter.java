package com.godenwater.core.utils;

import java.io.File;
import java.io.FileFilter;

public class FileDirFilter implements FileFilter {
	private boolean isDirectory;

	public FileDirFilter(boolean isDir) {
		this.isDirectory = isDir;
	}

	public boolean accept(File pathname) {
		if (pathname.isDirectory() == isDirectory) {
			return true;
		} else
			return false;
	}

}
