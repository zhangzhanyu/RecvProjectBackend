package com.godenwater.core.utils;

import java.io.File;
import java.io.FilenameFilter;

public class FileNameFilter implements FilenameFilter {

	private String type;

	public FileNameFilter(String tp) {
		this.type = tp;
	}

	public boolean accept(File fl, String path) {
		return path.endsWith(type);
	}
}