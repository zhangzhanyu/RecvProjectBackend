package com.godenwater.core.utils;

import java.security.SecureRandom;

public class RandomUtils {

	private static final SecureRandom sr = new SecureRandom();
	
	public static String getRandomIntNum(int iLength) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < iLength; i++) {
			sb.append(Math.abs(sr.nextInt(10)));
		}
		return sb.toString();
	}

	/**
     * 生成给定长度的字符
     * 
     * @param iLength
     *            产生随机字符的位数
     * @return
     */
    public static char[] getRandomCharArray(int iLength)
    {
        char[] ca = new char[iLength];
        for (int i = 0; i < ca.length; i++)
        {
            ca[i] = (char) (((Math.abs(sr.nextInt())) % 26) + (sr.nextBoolean() ? 65
                    : 97));
            // 65;//(byte)(Math.abs(ba[i]) % 72);
        }
        
        return ca;
    }
	
}
