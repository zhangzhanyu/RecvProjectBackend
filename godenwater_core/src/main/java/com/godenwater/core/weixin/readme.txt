
使用说明：

1、如果要开发公众号（订阅号、服务号）应用，请您使用com.easywater.core.weixin.mp

2、如果要开发企业号应用，请您使用com.easywater.core.weixin.cp




#########################################################################
	如果要开发公众号（订阅号、服务号）应用，在你的maven项目中添加：
	<dependency>
	  <groupId>me.chanjar</groupId>
	  <artifactId>weixin-java-mp</artifactId>
	  <version>1.3.1</version>
	</dependency>

	如果要开发企业号应用，在你的maven项目中添加：
	<dependency>
	  <groupId>me.chanjar</groupId>
	  <artifactId>weixin-java-cp</artifactId>
	  <version>1.3.1</version>
	</dependency>
#########################################################################