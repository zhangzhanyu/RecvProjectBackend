package com.godenwater.recv;

public class RecvConstant {

	public static String SL651 = "SL651";//水文协议标识

	public static String SZY206 = "SZY206";//水资源协议标识

	public static String YY = "YY";//燕禹协议标识

	public static String HD = "HD";//宏电协议标识

	public static String YLN = "YLN";//亿立能协议标识

	public static String YF= "YF";//湖北一方

	public static String YF722= "YF722";//亿立能协议标识

	public static String SUMMIT= "SUMMIT";//西安山脉

	public static String CACHE_MESSAGE = "MESSAGE";

	public static String CACHE_MONITOR = "MONITOR";

	public static String CACHE_STATION = "STATION";

	public static String CACHE_CALLER = "CALLER";
	
}
