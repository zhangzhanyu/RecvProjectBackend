package com.godenwater.recv.decode;

import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.hd.HdMessageHeader;
import org.apache.commons.lang3.StringUtils;
import org.apache.mina.core.buffer.IoBuffer;

/**
 * 宏电协议解析器
 *
 * @author admin
 */
public class HdMessageDecoder {

    public static CommonMessage decode(String channel, IoBuffer buffer) {
        // --------------------------------------------
        CommonMessage message = new CommonMessage();

        //宏电动态库协议
        HdMessageHeader header = new HdMessageHeader();

        // //包类型
        byte[] cmdType = new byte[1];
        buffer.get(cmdType);
        header.setCMD_TYPE(cmdType[0]);

        //包长度，2位
        byte[] bodyLen = new byte[2];
        buffer.get(bodyLen);
        header.setBODY_LEN(bodyLen);

        // DTU身份识别码,11位
        byte[] dtuId = new byte[11];
        buffer.get(dtuId);
        header.setDTU_ID(dtuId);

        if (header.getCMD_TYPE() == 0x01) {
            //本地移动IP，4位
            byte[] ip = new byte[4];
            buffer.get(ip);
            header.setIP(ip);

            //本地移动端口，2位
            byte[] port = new byte[2];
            buffer.get(port);
            header.setPORT(port);
        }
        //结束位，这个是跟传输方式有关，如果是UDP方式，则直接读取结束位，如果是TCP方式，则需要先读取数据
        byte[] endBit = new byte[1];

        if (StringUtils.equalsIgnoreCase(channel, "udp")) {
            buffer.get(endBit);//UDP方式，先读取结束位

            int contentLen = buffer.remaining();
            if (buffer.remaining() > 0) {
                byte[] content = new byte[contentLen];
                buffer.get(content);
                message.setContent(content);
                message.setBodySize(content.length);
            }
        } else {
            int contentLen = buffer.remaining();
            if (contentLen > 1) {
                byte[] content = new byte[contentLen - 1];
                buffer.get(content);
                message.setContent(content);
                message.setBodySize(content.length);

                buffer.get(endBit);
            }
        }

        // message.setHeader(message.getHeader());
        message.setHeader(header);
        message.setEOF((byte) 0x00);
        message.setCrclen(0);
        message.setCRC(null);

        return message;
    }

    public static CommonMessage decode(byte[] buffer) {
        CommonMessage message = new CommonMessage();
        int pos = 1;
        //宏电动态库协议
        HdMessageHeader header = new HdMessageHeader();
        //包类型
        byte[] cmdType = new byte[1];
        System.arraycopy(buffer, pos, cmdType, 0, cmdType.length);
        header.setCMD_TYPE(cmdType[0]);
        pos = pos + cmdType.length;
        //包长度，2位
        byte[] bodyLen = new byte[1];
        System.arraycopy(buffer, pos, bodyLen, 0, bodyLen.length);
        header.setBODY_LEN(bodyLen);
        pos = pos + bodyLen.length;
        // DTU身份识别码,11位
        byte[] dtuId = new byte[11];
        System.arraycopy(buffer, pos, dtuId, 0, dtuId.length);
        header.setDTU_ID(dtuId);
        pos = pos + dtuId.length;

        pos = pos + 2;
        byte[] content = new byte[buffer.length - 16];
        System.arraycopy(buffer, pos, content, 0, content.length);

        message.setContent(content);
        message.setHeader(header);
        message.setEOF((byte) 0x00);
        message.setCrclen(0);
        message.setCRC(null);
        return message;
    }

}
