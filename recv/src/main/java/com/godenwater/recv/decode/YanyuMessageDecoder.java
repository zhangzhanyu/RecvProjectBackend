package com.godenwater.recv.decode;

import org.apache.mina.core.buffer.IoBuffer;

import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.utils.ByteUtil;

import com.godenwater.recv.model.CommonMessage;

/**
 * 燕禹协议解析器
 * 
 * @author admin
 *
 */
public class YanyuMessageDecoder {

	public static CommonMessage decode(IoBuffer buffer, byte[] secondBit,byte[] threedBit) {
		// 燕禹协议
		com.godenwater.yanyu.YYMessageHeader header = new com.godenwater.yanyu.YYMessageHeader();

		// 协议内容：7E 目的地址 源地址 特征 长度 时间1《数据》 ETX CRC16H CRC16L
		
		//起始符
		header.setStartBit(new byte[] { Symbol.SOH_HEX });
		// 上行的，中心站地址在前
		header.setCenterAddr(secondBit);

		// 源地址,测站地址

		header.setStationAddr(threedBit);

		// 特征码
		byte[] funccode = new byte[header.getFuncCodeLen()];
		buffer.get(funccode);
		header.setFuncCode(funccode);

		// 报文长度
		byte[] bodySize = new byte[header.getBodySizeLen()];
		buffer.get(bodySize);
		header.setBodySize(bodySize);

		// 转换报文中的上下行标识及报文长度
		int bodySizeLen = ByteUtil.bytesToUbyte(bodySize);// 需要考虑拆分字节;

		int crclen = 2;

		// --------------------------------
		CommonMessage temp = new CommonMessage();
		temp.setHeader(header);
		temp.setCrclen(crclen);
		temp.setBodySize(bodySizeLen - 3);// 需要减去三个字节，一位结束符，两位CRC校验符

		return temp;
	}

	public static CommonMessage decode(byte[] buffer) {
		// 燕禹协议
		com.godenwater.yanyu.YYMessageHeader header = new com.godenwater.yanyu.YYMessageHeader();

		int pos = 1;

		// 7E 目的地址 源地址 特征 长度 时间1《数据》 ETX CRC16H CRC16L
		header.setStartBit(new byte[] { Symbol.SOH_HEX });
		// 上行的，中心站地址在前
		byte[] centerAddr = new byte[header.getCenterAddrLen()];
		System.arraycopy(buffer, pos, centerAddr, 0, centerAddr.length);
		pos = pos + header.getCenterAddrLen();

		header.setCenterAddr(centerAddr);

		// 源地址,测站地址
		byte[] stationAddr = new byte[header.getStationAddrLen()];
		System.arraycopy(buffer, pos, stationAddr, 0, stationAddr.length);
		pos = pos + header.getStationAddrLen();
		header.setStationAddr(stationAddr);

		// 特征码
		byte[] funccode = new byte[header.getFuncCodeLen()];
		System.arraycopy(buffer, pos, funccode, 0, funccode.length);
		pos = pos + header.getFuncCodeLen();
		header.setFuncCode(funccode);

		// 报文长度
		byte[] bodySize = new byte[header.getBodySizeLen()];
		System.arraycopy(buffer, pos, bodySize, 0, bodySize.length);
		pos = pos + header.getBodySizeLen();
		header.setBodySize(bodySize);

		// 转换报文中的上下行标识及报文长度
		int bodySizeLen = ByteUtil.bytesToUbyte(bodySize);// 需要考虑拆分字节;

		
		
		byte[] bodyContent = new byte[bodySizeLen - 3];// 根据前面header的内容来获取长度,需要减去后面的3个字节
		System.arraycopy(buffer, pos, bodyContent, 0, bodyContent.length);
		pos = pos + bodyContent.length;

		// 终止符
		byte[] eof = new byte[1];
		System.arraycopy(buffer, pos, eof, 0, eof.length);
		pos = pos + eof.length;

		// 校验码
		int crclen = 2;
		byte[] crc = new byte[crclen];
		System.arraycopy(buffer, pos, crc, 0, crc.length);
		pos = pos + crc.length;

		// 组装消息
		// --------------------------------
		CommonMessage message = new CommonMessage();
		// message.setHeader(message.getHeader());
		message.setHeader(header);
		message.setCrclen(crclen);
		message.setBodySize(bodySizeLen - 3);// 需要减去三个字节，一位结束符，两位CRC校验符
		message.setContents(bodyContent);
		message.setEOF(eof[0]);
		message.setCRC(crc);

		return message;
	}

}
