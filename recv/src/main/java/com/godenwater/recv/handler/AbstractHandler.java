package com.godenwater.recv.handler;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.godenwater.recv.manager.RedisManager;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.all.RtuConfig;
import com.godenwater.recv.server.all.RtuServer;
import com.godenwater.recv.utils.FileUtil;
import org.apache.commons.lang.StringUtils;

public abstract class AbstractHandler implements IMessageHandler {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmssS");
    RedisManager redisManager = new RedisManager();

    /**
     * 记录报文到文件，文件命名规则：测站编码+时间+suffix
     */
    public void saveMessage(String channel, String protocol, String stcd, boolean crcFlag, String message, String flagHd, String replayMsg, String ip, Integer port, String funcCode, int recvPort) {
        redisManager.append(channel, protocol, message, flagHd, replayMsg, ip, port, funcCode, recvPort);
    }

    /**
     * 检测是否有下发命令，如果有，则需要将标识切换为等待状态，如果无，则正常状态
     *
     * @return
     */
    public int checkCommand(String protocol, String stcd) {
        String msg = redisManager.read(stcd);
        if (msg.equals("")) {
            return 0;
        } else {
            return 1;
        }

    }

    /**
     * 检测是否有下发命令，如果有，则需要将标识切换为等待状态，如果无，则正常状态
     *
     * @return
     */
    public int checkYYCommand(String protocol, String stcd) {
        String msg = redisManager.find(stcd + "YYCallder");
        if (msg.equals("")) {
            return 0;
        } else {
            return 1;
        }

    }

    /**
     * 检测是否有下发命令，如果有，则需要将标识切换为等待状态，如果无，则正常状态
     *
     * @return
     */
    public File[] getCommand(String protocol, String stcd, String centerAddr,
                             String stationAddr) {
        String path = RtuConfig.getMsgCmdPath();

        if (StringUtils.equalsIgnoreCase(protocol, "yy")) {
            path = path + "/msg/" + protocol + "/" + centerAddr + "/" + stationAddr + "/";
        } else {
            path = path + "/msg/" + protocol + "/" + stcd + "/";
        }

        File fullPathDir = new File(path);
        if (fullPathDir != null && !fullPathDir.exists()) {
            fullPathDir.mkdirs();
        }

        return fullPathDir.listFiles();

    }

    /**
     * 记录报文到文件，文件命名规则：测站编码+时间+suffix
     */
    public void saveMessage(String channel, String stcd, CommonMessage message, int recvPort) {
        String path = RtuConfig.getMsgRcvPath();
        File fullPathDir = new File(path);
        if (fullPathDir != null && !fullPathDir.exists()) {
            fullPathDir.mkdirs();
        }
        // 2、写入报文文件
        String date = sdf.format(new Date());
        String fileName = channel + "_" + stcd + "_" + date + ".msg";
        FileUtil.writeObject(message, fileName);

    }

    /**
     * 前台监测通知
     */
    public void monitorMessage(String channel, String stcd, String message) {
        try {
            RtuServer.getInstance().getMonitorManager().append("RCV: channel " + channel + " stcd " + stcd + " hex " + message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
