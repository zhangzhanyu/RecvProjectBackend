package com.godenwater.recv.handler;

import com.godenwater.recv.model.CommonMessage;
import org.apache.mina.core.session.IoSession;

/**
 * 消息处理handler
 *
 * @author admin
 */
public interface IMessageHandler {

    public void perform(String channel, IoSession session, CommonMessage message, int port);

    public boolean checkCRC(CommonMessage message);

    public byte[] replyMessage(IoSession session, CommonMessage message);

    public byte[] repeatMessage(CommonMessage message);

    public void monitorMessage(String channel, String stcd, String message);

    public void saveMessage(String channel, String protocol, String stcd, boolean crcFlag, String message, String flagHd, String replayMsg, String ip, Integer port, String funcCode, int recvPort);
}
