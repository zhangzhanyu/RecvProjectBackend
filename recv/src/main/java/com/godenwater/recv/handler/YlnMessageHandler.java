package com.godenwater.recv.handler;

import cn.gov.mwr.sl651.utils.ByteUtil;
import com.godenwater.recv.RecvConstant;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.yln.YlnDownCommand;
import com.godenwater.recv.server.yln.YlnMessageHeader;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lipujun
 */
public class YlnMessageHandler extends AbstractHandler {

    private static Logger logger = LoggerFactory.getLogger(YlnMessageHandler.class);

    /**
     * Returns a singleton HdMessageHandler instance.
     *
     * @return a HdMessageHandler instance.
     */
    public static YlnMessageHandler getInstance() {
        return YlnMessageHandlerContainer.instance;
    }

    // Wrap this guy up so we can mock out the UserManager class.
    private static class YlnMessageHandlerContainer {
        private static YlnMessageHandler instance = new YlnMessageHandler();
    }

    private YlnMessageHandler() {
    }

    public void perform(String channel, IoSession session, CommonMessage message,int recvPort) {
        byte[] funcCode;
        boolean crcFlag = true;
        YlnMessageHeader header = (YlnMessageHeader) message.getHeader();
        funcCode = header.getSTART_BIT();

        //logger.info(" YLN  " + new String(funcCode) + new String(message.getContent()));

        // 3、应答回复或重发报文请求
        byte[] replyMsg = null;

        replyMsg = replyMessage(session, message);

        //回复数据
        if (replyMsg != null && (channel.equalsIgnoreCase("GPRS") || channel.equalsIgnoreCase("TCP") || channel.equalsIgnoreCase("UDP"))) {//
            logger.debug("回复报文，YLN " + ByteUtil.toHexString(replyMsg) + "");
            // logger.debug("S>  YLN " + ByteUtil.toHexString(replyMsg) + "");
            session.write(replyMsg);
        }
//
//        // 4、将测站与session关联起来，此处需注意关联为宏电的电话号码
//        RtuServer.getInstance().getSessionManager()
//                .bindSession(session, RecvConstant.HD, stcd);
//
        // 6、监测器显示
        String logMsg = viewMessage(channel, message);
        //monitorMessage(channel, "", logMsg);

        // 2.2 写入报文记录
        saveMessage(channel, RecvConstant.YLN, "", crcFlag, logMsg, "0", "", "", 0, "", 0);

    }


    /**
     * 亿立能模块中，无crc校验,默认为true
     *
     * @param message
     * @return
     */
    public boolean checkCRC(CommonMessage message) {
        return true;
    }

    public byte[] replyMessage(IoSession session, CommonMessage message) {
        return YlnDownCommand.reply();
    }

    /**
     * 构造重发报文
     *
     * @return
     */
    public byte[] repeatMessage(CommonMessage message) {
        return null;
    }

    public String viewMessage(String channel, CommonMessage message) {
        String log = "";
        try {

            YlnMessageHeader header = (YlnMessageHeader) message.getHeader();
            byte[] funcCode = header.getSTART_BIT();
            logger.debug(" YLN  " + new String(funcCode) + new String(message.getContent()));
            log = ByteUtil.toHexString("$".getBytes()) + ByteUtil.byteToHexString(funcCode) + ByteUtil.byteToHexString(message.getContent());
            //logger.info(log);
            return log;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return log;
    }

}
