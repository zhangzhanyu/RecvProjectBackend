package com.godenwater.recv.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.godenwater.core.container.BasicModule;
import com.godenwater.recv.model.CommonMessage;

/**
 * 原始报文管理器，将报文以写的方式写入到队列中
 * 
 * @ClassName: MessageManager
 * @Description: TODO
 * @author lipujun
 * @date Apr 28, 2015
 * 
 */
public class MessageManager extends BasicModule {

	protected static final Logger log = LoggerFactory
			.getLogger(MessageManager.class);

	private static String KEY = "messages";

	public MessageManager() {
		super("Message Manager");
	}

	public void initialize() {

	}

	/**
	 * 监控日志，只需要管理往缓存中写入，由缓存管理端进行分发
	 */
	public void append(String type, CommonMessage msg) {

		if (msg != null) {
			// redisTemplate.opsForList().leftPush(type + "_" + KEY, msg);
		}

	}

}
