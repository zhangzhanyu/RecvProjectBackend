package com.godenwater.recv.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.godenwater.core.container.BasicModule;
import com.godenwater.core.spring.Application;

/**
 * 监控管理器，是通过获取缓存，将日志数据放入到缓存消息队列中，以PUB/SUB的方式进行分发
 * 
 * @ClassName: MonitorManager
 * @Description: TODO
 * @author lipujun
 * @date Apr 28, 2013
 * 
 */
public class MonitorManager extends BasicModule {

	protected static final Logger log = LoggerFactory
			.getLogger(MonitorManager.class);

	private RedisTemplate redisTemplate;

	public MonitorManager() {
		super("ServerLog Manager");
	}

	public void initialize() {
 		redisTemplate = (RedisTemplate) Application.getInstance().getCtx()
 				.getBean("redisTemplate");
	}

	/**
	 * 监控日志，只需要管理往缓存中写入，由缓存管理端进行分发
	 */
	public void append(String msg) {
		
		if (redisTemplate == null) {
			System.out.println(">> 无监控终端！");
		} else {
			//log.debug(msg);
			if (msg != null){
				 redisTemplate.convertAndSend("monitor", msg);
			}
				
		}

	}

}
