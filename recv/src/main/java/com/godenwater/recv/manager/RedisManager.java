package com.godenwater.recv.manager;

import com.godenwater.core.container.BasicModule;
import com.godenwater.core.spring.Application;
import com.godenwater.recv.RecvConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Random;

/**
 * redis 报文管理  lhc
 */
public class RedisManager extends BasicModule {

    protected static final Logger log = LoggerFactory.getLogger(RedisManager.class);

    private RedisTemplate redisTemplate;

    public RedisManager() {
        super("ServerLog Manager");
    }

    public void initialize() {
        redisTemplate = (RedisTemplate) Application.getInstance().getCtx()
                .getBean("redisTemplate");
    }

    /**
     * 设置燕禹的电话号码，保存到数据库
     *
     * @param msg
     */
    public void setYyTel(String msg) {
        try {
            redisTemplate = (RedisTemplate) Application.getInstance().getCtx().getBean("redisTemplate");
            if (redisTemplate == null) {
                System.out.println(">> 无监控终端！");
            } else {
                log.debug("入YyTel报文信息：", msg);
                if (msg != null) {
                    redisTemplate.opsForList().leftPush("YyTel", msg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
        }
    }

    public void setRedisPwd(String stcd, String f01, String f03) {
        try {
            redisTemplate = (RedisTemplate) Application.getInstance().getCtx().getBean("redisTemplate");
            if (redisTemplate == null) {
                log.info(">> 无监控终端！");
            } else {
                ValueOperations station = redisTemplate.opsForValue();
                station.set(stcd + "pwd", f01 + "#" + f03);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
        }
    }

    public void writer(String key, String val) {
        try {
            redisTemplate = (RedisTemplate) Application.getInstance().getCtx().getBean("redisTemplate");
            if (redisTemplate == null) {
                log.info(">> 无监控终端！");
            } else {
                //log.info(">> 召测下发redis key" + key + ":" + val);
                redisTemplate.opsForList().leftPush(key, val);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
        }
    }

    /**
     * 监控日志，只需要管理往缓存中写入，由缓存管理端进行分发
     */
    public void append(String chanel, String protocol, String msg, String flagHd, String replayMsg, String ip, Integer port, String funcCode, int recvPort) {
        try {
            redisTemplate = (RedisTemplate) Application.getInstance().getCtx().getBean("redisTemplate");
            Random random = new Random();
            if (redisTemplate == null) {
                log.info(">> 无监控终端！");
            } else {
                if (msg != null) {
                    String content = null;
                    if (protocol.equals(RecvConstant.YY)) {
                        content = msg + "#" + replayMsg + "#" + ip + "#" + port;
                    } else if (protocol.equals(RecvConstant.SL651)) {
                        content = msg + "#" + replayMsg + "#" + ip + "#" + port + "#" + recvPort;
                        //log.info("--------------------------sl651 in redis before----------------------------------------");
                        if (funcCode.equals("36")) {
                            redisTemplate.opsForList().leftPush(protocol + "F36", content);
                            //log.info("--------------------------sl651 in redis F36----------------------------------------");
                        } else {
                            redisTemplate.opsForList().leftPush(protocol, content);
                            //log.info("--------------------------sl651 in redis----------------------------------------");
                        }
                    } else {
                        content = msg + "#" + replayMsg + "#" + ip + "#" + port + "#" + recvPort;
                        redisTemplate.opsForList().leftPush(protocol, content);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
        }
    }

    /**
     * 根据KEY读取redis里的内容
     *
     * @param key
     * @return
     */

    public String read(String key) {
        String val = "";
        try {

            redisTemplate = (RedisTemplate) Application.getInstance().getCtx().getBean("redisTemplate");
            if (redisTemplate == null) {
                log.info(">> 无监控终端！");
            } else {
                if (redisTemplate.opsForList().size(key) > 0) {
                    val = redisTemplate.opsForList().rightPop(key).toString();
                } else {
                    return "";
                }
            }
            return val;
        } catch (
                Exception e) {
            e.printStackTrace();
        } finally {
            RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
            return val;
        }

    }

    /**
     * 根据KEY检索redis里的内容
     *
     * @param key
     * @return
     */
    public String find(String key) {
        String val = "";
        try {
            redisTemplate = (RedisTemplate) Application.getInstance().getCtx().getBean("redisTemplate");
            if (redisTemplate == null) {
                log.info(">> 无监控终端！");
            } else {
                if (redisTemplate.opsForList().size(key) > 0) {
                    return "key值存在";
                } else {
                    return "";
                }
            }
            return val;
        } catch (
                Exception e) {
            e.printStackTrace();
        } finally {
            RedisConnectionUtils.unbindConnection(redisTemplate.getConnectionFactory());
            return val;
        }
    }
}
