package com.godenwater.recv.manager;

import org.apache.mina.core.future.IoFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionIoFutureListener implements IoFutureListener {

    private static Logger logger = LoggerFactory
            .getLogger(SessionIoFutureListener.class);

    private String protocol;
    private String id;
    private String stcd;

    public SessionIoFutureListener(String _protocol, String _id, String _stcd) {
        this.protocol = _protocol;
        this.id = _id;
        this.stcd = _stcd;
    }

    @Override
    public void operationComplete(IoFuture future) {
        /*
        WriteFuture wfuture = (WriteFuture) future;
        // 写入成功,需将文件移动到下发成功的目录
        if (wfuture.isWritten()) {
            logger.debug(">> 下发【召测消息】到客户端完成！消息ID：" + this.id);

            String path = RtuConfig.getMsgCmdPath();

            path = path + "/msg/" + protocol + "/" + stcd;

            File srcFile = new File(path + "/" + id);
            File destDir = new File(RtuConfig.getMsgCmdPath() + "/suc/" + protocol + "/" + stcd);

            try {
                if (destDir != null && !destDir.exists()) {
                    destDir.mkdirs();
                }
                String movePath = destDir + "//" + srcFile.getName();
                File moveFile = new File(movePath);
                if (moveFile.exists()) {
                    moveFile.delete();
                }
                logger.debug(">> 移动文件到成功目录 " + srcFile.getPath() + "----->" + destDir.getPath());
                FileUtils.moveFileToDirectory(srcFile, destDir, true);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // 写入失败，等待下次处理
            logger.debug(">> 下发【召测消息】到客户端失败,等待下次处理....");

        }
        */
    }


}
