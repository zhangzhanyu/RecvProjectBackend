package com.godenwater.recv.manager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.godenwater.core.container.BasicModule;
import com.godenwater.core.spring.Application;


public class StationManager extends BasicModule {

	private static final Logger Log = LoggerFactory
			.getLogger(StationManager.class);

	// Wrap this guy up so we can mock out the UserManager class.
	private static class UserManagerContainer {
		private static StationManager instance = new StationManager();
	}

	/**
	 * Returns a singleton UserManager instance.
	 * 
	 * @return a UserManager instance.
	 */
	public static StationManager getInstance() {
		return UserManagerContainer.instance;
	}

	/** Cache of local station. */
//	private static Map<String, Station> caches = new ConcurrentHashMap<String, Station>();
//
//	private static Map<String, StationFetchRate> rateCaches = new ConcurrentHashMap<String, StationFetchRate>();

//	BaseDao baseDao = (BaseDao) Application.getInstance().getCtx()
//			.getBean("baseDao");
//
//	
//	private RedisTemplate redisTemplate = (RedisTemplate) Application.getInstance().getCtx()
//			.getBean("redisTemplate");
//	
	  

	public StationManager() {
		super("Station Manager");
	}

	/**
	 * Returns an unmodifiable Collection of all users in the system.
	 * 
	 * @return an unmodifiable Collection of all users.
	 */
	public void initialize() {

		// String sql =
		// "SELECT * FROM rtu_station a LEFT JOIN rtu_sl651_40 b ON a.stcd = b.stcd ";
		Application.getInstance();
		
//		String sql = "SELECT * FROM rtu_station a   LEFT JOIN rtu_sl651_40 b ON a.stcd = b.stcd ";
//
//		List<Station> stations = baseDao.getJdbcTemplate().query(sql,
//				new RowMapper<Station>() {
//					@Override
//					public Station mapRow(ResultSet rs, int arg1)
//							throws SQLException {
//						Station st = new Station();
//						st.setStcd(rs.getString("stcd"));
//						st.setStnm(rs.getString("stnm"));
//						st.setF01(rs.getString("f01"));
//						st.setF02(rs.getString("f02"));
//						st.setF03(rs.getString("f03"));
//						st.setF04(rs.getString("f04"));
//						st.setProtocol(rs.getString("protocol"));
//						st.setMsgmode(rs.getString("msgmode"));
//						return st;
//					}
//				});
//		// Fire event.
//		for (Station station : stations) {
//			caches.put(station.getStcd(), station);
//			System.out.println(">> 测站： " + station.getStcd() + "\t"
//					+ station.getStnm());
//		}

	}

	 
	public static void main(String[] args) {
		// StationManager.getInstance().createStation();
 
		Map<String, String> clients = new ConcurrentHashMap<String, String>();
		String stcd = "aaaa";
		clients.put(stcd, "111");
		clients.put(stcd, "222");

		System.out.println(">> " + clients.get(stcd));
	}

}
