package com.godenwater.recv.model;

public class CommonMessage { 

	public Object header;

	private byte[] content;

	public byte eof;

	public byte[] crc;

	public int bodySize;
	public int crclen;
	
	public void setHeader(Object header) {
		this.header = header;
	}

	public Object getHeader() {
		return this.header;
	}

	
	public void setContents(byte[] content) {
		this.content = content;
	}
	
	public byte[] getContent() {
		if (this.content != null)
			return content;

		return content;
	}

	public void setEOF(byte eof) {
		this.eof = eof;
	}

	public byte getEOF() {
		return this.eof;
	}

	public byte getEof() {
		return eof;
	}

	public void setEof(byte eof) {
		this.eof = eof;
	}
	

	public void setCRC(byte[] crc) {
		this.crc = crc;
	}

	public byte[] getCRC() {
		return this.crc;
	}

	public byte[] getCrc() {
		return crc;
	}

	public void setCrc(byte[] crc) {
		this.crc = crc;
	}
	
	public int getCrclen() {
		return crclen;
	}

	public void setCrclen(int crclen) {
		this.crclen = crclen;
	}

	public int getBodySize() {
		return bodySize;
	}

	public void setBodySize(int bodySize) {
		this.bodySize = bodySize;
	}
	
	public void setContent(byte[] content) {
		this.content = content;
	}

	
}
