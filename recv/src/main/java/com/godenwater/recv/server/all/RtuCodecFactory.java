package com.godenwater.recv.server.all;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class RtuCodecFactory implements ProtocolCodecFactory {
	private ProtocolEncoder encoder;
    private ProtocolDecoder decoder;

    public RtuCodecFactory(boolean server,String channel) {
        if (server) {
            encoder = new ServerDataEncoder();
            decoder = new ServerDataDecoder(channel);
        } else {
            encoder = new ClientDataEncoder();
            decoder = new ClientDataDecoder();
        }
    }

    public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
        return encoder;
    }

    public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
        return decoder;
    }
}
