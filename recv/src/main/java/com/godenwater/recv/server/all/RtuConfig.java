package com.godenwater.recv.server.all;

import com.godenwater.recv.spring.Configurer;

import java.io.File;


public class RtuConfig {

	/**
	 * 中心站址，代表发送数据源地址中高四位
	 * 
	 * @return
	 */
	public static String getCenterAddr() {
		return (String) Configurer.getProperty("center_address");
	}

	/**
	 * 当前节点的行政区划
	 * 
	 * @return
	 */
	public static String getCenterAddvcd() {
		return (String) Configurer.getProperty("center_addvcd");
	}

	/**
	 * 当前节点的行政级别 4为市级 6为县级
	 * 
	 * @return
	 */
	public static String getCenterLevel() {
		return (String) Configurer.getProperty("center_level");
	}

	// ----------------------------------------------------

	/**
	 * 线程及缓冲区大小
	 * 
	 * @return
	 */
	public static int getBufferSize() {

		return Configurer.getIntProperty("buffer_size", 4096);
	}

	public static int getThreadSize() {
		return Configurer.getIntProperty("thread_size", 10);
	}

	public static int getQueueSize() {
		return Configurer.getIntProperty("queue_size", 300);
	}

	// ----------------------------------------------------

	/**
	 * GRPS端口
	 * 
	 * @return
	 */
	public static String getHydroGprsTcpIp() {
		return Configurer.getStringProperty("hydro_gprs_tcp_ip", "0");
	}

	public static int getHydroGprsTcpPort() {
		return Configurer.getIntProperty("hydro_gprs_tcp_port", 20001);
	}
	public static boolean isHydroGprsTcpEnabled() {

		String port = Configurer.getProperty("hydro_gprs_tcp_port");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}


	public static int getHydroGprsTcpPort2() {
		return Configurer.getIntProperty("hydro_gprs_tcp_port2", 20002);
	}

	public static boolean isHydroGprsTcp2Enabled() {
		String port = Configurer.getProperty("hydro_gprs_tcp_port2");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	public static String getHydroGprsUdpIp() {
		return Configurer.getStringProperty("hydro_gprs_udp_ip", "0");
	}
	public static int getHydroGprsUdpPort() {
		return Configurer.getIntProperty("hydro_gprs_udp_port", 20002);
	}

	public static boolean isHydroGprsUdpEnabled() {
		String port = Configurer.getProperty("hydro_gprs_udp_port");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	public static int getHydroGprsUdpPort2() {
		return Configurer.getIntProperty("hydro_gprs_udp_port2", 20002);
	}
	public static int getHydroGprsUdpPort3() {
		return Configurer.getIntProperty("hydro_gprs_udp_port3", 20005);
	}
	public static boolean isHydroGprsUdp2Enabled() {
		String port = Configurer.getProperty("hydro_gprs_udp_port2");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}
	public static boolean isHydroGprsUdp3Enabled() {
		String port = Configurer.getProperty("hydro_gprs_udp_port3");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}
	/**
	 * GSM设置
	 * 
	 * @return
	 */
	public static int getHydroGsmFlag() {
		return Configurer.getIntProperty("hydro_gsm_flag",0);
	}
	
	public static String getHydroGsmSerial() {
		return Configurer.getProperty("hydro_gsm_serial");
	}

	public static int getHydroGsmBaudRate() {
		return Configurer.getIntProperty("hydro_gsm_baudrate");
	}

	public static int getHydroGsmByteSize() {
		return Configurer.getIntProperty("hydro_gsm_bytesize",8);
	}
	
	public static String getHydroGsmParity() {
		return Configurer.getProperty("hydro_gsm_parity");
	}
	
	public static String getHydroGsmStopbits() {
		return Configurer.getProperty("hydro_gsm_stopbits");
	}
	
	public static String getHydroGsmMobile() {
		return Configurer.getProperty("hydro_gsm_mobile");
	}

	public static boolean isHydroGsmEnabled() {
		int port = getHydroGsmFlag();
		if (port == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * PSTN设置
	 * 
	 * @return
	 */

	public static String getHydroPstnSerial() {
		return Configurer.getProperty("hydro_pstn_serial");
	}

	public static int getHydroPstnBaudRate() {
		return Configurer.getIntProperty("hydro_pstn_baudrate");
	}

	public static String getHydroPstnMobile() {
		return Configurer.getProperty("hydro_pstn_mobile");
	}

	public static boolean isHydroPstnEnabled() {
		String port = getHydroPstnSerial();
		if (port == null || port.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	
	/**
	 * BD设置
	 * 
	 * @return
	 */
	public static int getHydroBdFlag() {
		return Configurer.getIntProperty("hydro_bd_flag",0);
	}
	
	public static String getHydroBdSerial() {
		return Configurer.getProperty("hydro_bd_serial");
	}

	public static int getHydroBdBaudRate() {
		return Configurer.getIntProperty("hydro_bd_baudrate");
	}
 
	public static int getHydroBdByteSize() {
		return Configurer.getIntProperty("hydro_bd_bytesize",8);
	}
	
	public static String getHydroBdParity() {
		return Configurer.getProperty("hydro_bd_parity");
	}
	
	public static String getHydroBdStopbits() {
		return Configurer.getProperty("hydro_bd_stopbits");
	}
	
	public static boolean isHydroBdEnabled() {
		int port = getHydroBdFlag();
		if (port == 0 ) {
			return false;
		} else {
			return true;
		}
	}
	// ----------------------------------------------------
	/**
	 * 图片文件临时目录
	 * 
	 * @return
	 */
	public static String getPicTempPath() {
		return (String) Configurer.getProperty("msg.img.path");
	}

	/**
	 * 图片保存路径
	 * 
	 * @return
	 */
	public static String getPicSavePath() {
		return (String) Configurer.getProperty("msg.img.path");
	}

	/**
	 * 图片格式，如jpg,gif,png,bmp
	 * 
	 * @return
	 */
	public static String getPicFormat() {
		return (String) Configurer.getProperty("pic_format");
	}

	/**
	 * 图片命名规则，如：站名+观测时间
	 * 
	 * @return
	 */
	public static String getPicNameRule() {
		return (String) Configurer.getProperty("pic_namerule");
	}

	/**
	 * 报文文件存储目录
	 * 
	 * @return
	 */
	public static String getMsgRcvPath() {
		return (String) Configurer.getProperty("msg.rcv.path");
	}
 
	/**
	 * 报文文件固态文件目录
	 * 
	 * @return
	 */
	public static String getMsgSsrPath() {
		return (String) Configurer.getProperty("msg.ssr.path");
	}
 
	
	/**
	 * 报文处理异常的文件存储目录
	 * 
	 * @return
	 */
	public static String getMsgErrPath() {
		return (String) Configurer.getProperty("msg.err.path");
	}
	

	/**
	 * 报文图像文件目录
	 * 
	 * @return
	 */
	public static String getMsgImgPath() {
		return (String) Configurer.getProperty("msg.img.path");
	}
	
	/**
	 * 报文处理成功后的存储目录
	 * 
	 * @return
	 */
	public static String getMsgSucPath() {
		return (String) Configurer.getProperty("msg.suc.path");
	}
	

	/**
	 * 报文处理成功后的存储目录
	 * 
	 * @return
	 */
	public static String getMsgCmdPath() {
		return (String) Configurer.getProperty("msg.cmd.path");
	}
	
	/**
	 * 报文程序的升级目录
	 * 
	 * @return
	 */
	public static String getMsgBinPath() {
		return (String) Configurer.getProperty("msg.bin.path");
	}
	
	/**
	 * 下发报文的存储目录 
	 * 
	 * @return
	 */
	public static String getMsgSndPath() {
		return (String) Configurer.getProperty("msg.snd.path");
	}

	/**
	 * 报文临时文件的存储目录 
	 * 
	 * @return
	 */
	public static String getMsgTmpPath() {
		return (String) Configurer.getProperty("msg.tmp.path");
	}


	public static File getBinMessage(String stcd) {

		String path = RtuConfig.getMsgBinPath();

		path = path + "/" + stcd + ".sbin";

		File fullPathDir = new File(path);
		if (fullPathDir != null && !fullPathDir.exists()) {
			fullPathDir.mkdirs();
		}

		File binFile = new File(path);

		if (binFile.exists()) {
			return binFile;
		} else {
			return null;
		}
	}
	/**
	 * #--------------------------------------------------
	 * #___________水文监测数据通信规约___________________
	 * #--------------------------------------------------
	 * 
	 */

}
