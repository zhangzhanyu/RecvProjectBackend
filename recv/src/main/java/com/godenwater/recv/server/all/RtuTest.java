package com.godenwater.recv.server.all;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.gov.mwr.sl651.utils.ByteUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class RtuTest {

	
	public void readData(){
		String dataFile = "d:/RGcom31609051500RTU.txt";
		
		try {
			List<String> lines =FileUtils.readLines(new File(dataFile), "UTF-8");
			
			int i = 0;
			String preDate ="";
			String preLine = "";
			
			List<String> newlines = new ArrayList<String>();
			
			for(String line:lines){
				
				String date = line.substring(1,20);   
				
				System.out.println(">> data " + i + " " + date + " preDate " + preDate);
				
				if(StringUtils.equalsIgnoreCase(date, preDate)){ 
					
					preLine = preLine + line.substring(21);
					 
				}else{  
					newlines.add(preLine);
					preLine = line.substring(21); 
				}
				
				preDate = date;
				
				//System.out.println(">> data " + i + " " + date + " msg " + msg7e7e);
				
			}
//			
			/**
			for(String line:newlines){
//				 
				i++;
				System.out.println(">> line " + i + " " + line); 
			}*/
			
			FileUtils.writeLines(new File("d:\\newRtu.txt"), newlines);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	
	public void readData2(){
		String dataFile = "d:/newRtu.txt";
		
		try {
			List<String> lines =FileUtils.readLines(new File(dataFile), "UTF-8");
			
			int i = 0;
			String preDate ="";
			String preLine = "";
			
			List<String> newlines = new ArrayList<String>();
			
			for(String line:lines){
				i++;
				String date = line.substring(0,5);   
				
				System.out.println(">> data " + i + " " + date + " preDate " + preDate);
				
				if(StringUtils.equalsIgnoreCase(date, "7E 7E")){ 
					
					newlines.add(line );
					 
				}else{  
					newlines.remove(newlines.size()-1);
					newlines.add(preLine + line  ); 
				}
				
				preDate = date;
				preLine = line;   
				//System.out.println(">> data " + i + " " + date + " msg " + msg7e7e);
				
			}
//			
			/**
			for(String line:newlines){
//				 
				i++;
				System.out.println(">> line " + i + " " + line); 
			}*/
			
			FileUtils.writeLines(new File("d:\\newRtu2.txt"), newlines);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	public void readData3(){
		String dataFile = "d:/newRtu2.txt";
		
		try {
			List<String> lines =FileUtils.readLines(new File(dataFile), "UTF-8");
			 
			
			List<String> newlines = new ArrayList<String>();
			
			for(String line:lines){ 
				String date = line.substring(30,32);   
				 
				if(StringUtils.equalsIgnoreCase(date, "2f")){ 
					  
				}else{   
					newlines.add(line ); 
				}
				 
			}
 
			FileUtils.writeLines(new File("d:\\newRtu3.txt"), newlines);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//RtuTest test = new RtuTest();
		//test.readData3();

		byte[] b = "$".getBytes();//0x24
		byte[] b2 = "6".getBytes();
		byte[] A = "A".getBytes();//0X41
		byte[] B = "B".getBytes();//0X42
		if(b[0] == (byte) 0x24){
			System.out.println("ok");
		}


//		String hhex="BCF5C28F";
//		Float  value=Float.intBitsToFloat(Integer.valueOf(hhex, 16));

		Float f = new Float("2.50f");

     /* returns the floating-point value with the same bit pattern */
		System.out.println(f.intBitsToFloat(123));
		System.out.println(f.intBitsToFloat(0x7f800000));
		System.out.println(f.intBitsToFloat(0xff800000));
		System.out.println(f.intBitsToFloat(0xBF9E0419));

		System.out.println(Integer.toHexString(-5) );
		//System.out.println(Integer.valueOf("FFFFFFFB",16));

		//---------采用大数据类型来处理--------------------------
		//int value = -3;
		BigInteger bi = new BigInteger("FFFFFFFB", 16);
		System.out.println(bi.intValue());


		//---------采用浮点型数据类型来处理--------------------------
		 String hhex="BD4CCCCD";
		Float  value=Float.intBitsToFloat( new BigInteger("BD4CCCCD", 16).intValue());
		System.out.println(value);

		System.out.println("--------------------------------------------");
		System.out.println(new String(b) + " : " + ByteUtil.toBinaryString(b));
		System.out.println(new String(new byte[]{0x36})+ " : " + ByteUtil.toBinaryString(b2));

		System.out.println(new String(A)+ " : " + ByteUtil.toBinaryString(A));
		System.out.println(new String(B)+ " : " + ByteUtil.toBinaryString(B));
		//----------------------
		String hex = "244141413B31313030303030303B31313030303030303BD2DAC1A2C4DCBFC6BCBC3BC4A3C4E2525455D5BE3B323031372D30312D31372031303A34323A33363B33302E3537352C32342E352C31322E322C33382E382C302E36383BCBAECEBB286D292CD3EAC1BF286D6D292CB5E7D1B92856292CCEC2B6C82843292CC1F7CBD9286D2F73293B24454E44";
		byte[] msg = ByteUtil.HexStringToBinary(hex);
		try {
			System.out.println(new String(msg, "GB2312"));


		String hdstr = "43484C5B305D2D3E6765742061206E657720736D73200D0A";
		System.out.println(">> " + new String(ByteUtil.HexStringToBinary(hdstr)));
		hdstr = "43484C5B305D2D3E6765742061206E657720736D73200A";
		System.out.println(">> " + new String(ByteUtil.HexStringToBinary(hdstr)));

		hdstr = "7B01001631353932373730333230370AA6D6070FA27B";
		System.out.println(">> " + new String(ByteUtil.HexStringToBinary(hdstr),"GB2312"));


			String dstr = "3133393132333435363738";
			System.out.println("tel " +new String(ByteUtil.HexStringToBinary(dstr)));

			String ddstr = "24";
			System.out.println("tel " +new String(ByteUtil.HexStringToBinary(ddstr)));

			String xasmstr = "68";
			System.out.println("tel " +new String(ByteUtil.HexStringToBinary(xasmstr)));

			String ylnstr = "AA";
			System.out.println("tel " +new String(ByteUtil.HexStringToBinary(ylnstr)));


			System.out.println(ByteOrder.nativeOrder());
			/**
			ArrayList<String> lines = (ArrayList<String>) FileUtils.readLines(new File("d:/TEL.TXT"));
		for(String l :lines){
			System.out.println(">> " + new String(ByteUtil.HexStringToBinary(l),"GB2312"));
		}
			 */
		}catch(Exception e){
			e.printStackTrace();
		}
/**

		int x = 0x01020304;

		ByteBuffer bb = ByteBuffer.wrap(new byte[4]);
		bb.asIntBuffer().put(x);
		String ss_before = Arrays.toString(bb.array());

		System.out.println("默认字节序 " +  bb.order().toString() +  ","  +  " 内存数据 " +  ss_before);

		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.asIntBuffer().put(x);
		String ss_after = Arrays.toString(bb.array());

		System.out.println("修改字节序 " + bb.order().toString() +  ","  +  " 内存数据 " +  ss_after);

**/

	}

}
