package com.godenwater.recv.server.hd;


import cn.gov.mwr.sl651.utils.ByteUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Li on 2017/1/18.
 */
public class HdDownCommand {

    //宏电模块协议头和尾
    public static byte HD_START = 0X7B;
    public static byte HD_END = 0X7B;


    /**
     * 返回DTU报文
     */
    public static byte[] cmdDTU(byte cmdtype, byte[] dtuId) {
        byte[] msg = new byte[16];
        msg[0] = HD_START;
        msg[1] = cmdtype;
        msg[2] = 0x00;
        msg[3] = 0x10;
        System.arraycopy(dtuId, 0, msg, 4, 11); //将DTU身份识别码返回
        msg[15] = HD_END;

        return msg;
    }


    /**
     * 2.1.2 注册应答（DSC->DTU）
     * 1)注册成功，返回81号报文
     *
     * @param dtuId DTU身份识别码
     * @return
     */
    public static byte[] cmd81DTU(byte[] dtuId) {
        return cmdDTU((byte) 0x81, dtuId);
    }
    
    /**
     * 2.1.2 注册应答（DSC->DTU）
     * 1)一方收到数据返回89号报文
     *
     * @param dtuId DTU身份识别码
     * @return
     */
    public static byte[] cmd89DTU(byte[] dtuId) {
    	return cmdDTU((byte) 0x89, dtuId);
    }

    /**
     * 2.1.2 注册应答（DSC->DTU）
     * 1)无效的命令或数据，返回84号报文
     *
     * @param dtuId DTU身份识别码
     * @return
     */
    public static byte[] cmd84DTU(byte[] dtuId) {
        return cmdDTU((byte) 0x84, dtuId);
    }

    /**
     * 2.2.2注销应答包 DSC->DTU
     * 1) 注销成功，返回82号报文
     *
     * @param dtuId DTU身份识别码
     * @return
     */
    public static byte[] cmd82DTU(byte[] dtuId) {
        return cmdDTU((byte) 0x82, dtuId);
    }


    /**
     * 发送用户数据包后，给DTU的回复
     * 2.3.2 DSC 应答收到正确数据包(一般不用)，返回85号报文
     *
     * @param dtuId
     * @return
     */
    public static byte[] cmd85DTU(String channel, byte[] dtuId, byte[] content) {
        //return cmdDTU((byte) 0x85, dtuId);

        int len = 0;
        if (content != null)
            len = content.length;

        byte[] msg = new byte[16 + len];
        msg[0] = HD_START;
        msg[1] = (byte) 0x85;

        if (StringUtils.endsWithIgnoreCase(channel, "udp")) {
            msg[2] = 0x00;      //需要根据通道类型来判断包的长度及大小
            msg[3] = 0x10;
        } else {
            int xlen = msg.length;
            byte[] lb = ByteUtil.shortToBytes((short) xlen);
            msg[2] = lb[0];      //需要根据通道类型来判断包的长度及大小
            msg[3] = lb[1];
        }

        System.arraycopy(dtuId, 0, msg, 4, 11); //将DTU身份识别码返回

        if (StringUtils.endsWithIgnoreCase(channel, "udp")) {
            msg[15] = HD_END;
            System.arraycopy(content, 0, msg, 16, len);
        } else {
            System.arraycopy(content, 0, msg, 15, len); //将DTU身份识别码返回
            msg[msg.length - 1] = HD_END;
        }

        return msg;

    }


    /**
     * 2.3.3 DSC 发送给DTU 的数据包 DSC->DTU
     *
     * @param dtuId
     * @return
     */
    public static byte[] cmd89DTU(String channel, byte[] dtuId, byte[] content) {

        int len = 0;
        if (content != null)
            len = content.length;

        byte[] msg = new byte[16 + len];
        msg[0] = HD_START;
        msg[1] = (byte) 0x89;

        if (StringUtils.endsWithIgnoreCase(channel, "udp")) {
            msg[2] = 0x00;      //需要根据通道类型来判断包的长度及大小
            msg[3] = 0x10;
        } else {
            int xlen = msg.length;
            byte[] lb = ByteUtil.shortToBytes((short) xlen);
            msg[2] = lb[0];      //需要根据通道类型来判断包的长度及大小
            msg[3] = lb[1];
        }

        System.arraycopy(dtuId, 0, msg, 4, 11); //将DTU身份识别码返回

        if (StringUtils.endsWithIgnoreCase(channel, "udp")) {
            msg[15] = HD_END;
            System.arraycopy(content, 0, msg, 16, len);
        } else {
            System.arraycopy(content, 0, msg, 15, len); //将DTU身份识别码返回
            msg[msg.length - 1] = HD_END;
        }

        return msg;
    }

    /**
     * 2.8 远程唤醒(DSC->DTU)
     * 协议包格式
     *
     * @param dtuId
     * @return
     */
    public static byte[] cmd13DTU(byte[] dtuId) {
        return cmdDTU((byte) 0x13, dtuId);
    }


    /**
     * @param dtuId
     * @param type    0x00 查询所有参数
     *                0xN1 查询移动服务参数
     *                0xN2 查询RTU参数
     *                0xN3 查询SMS设置参数
     *                0xN4 查询运行参数
     *                0xN5 查询系统参数
     *                0xN6 查询IP通道参数
     *                N7~0xFF 保留
     * @param content
     * @return
     */
    public static byte[] cmd8BDTU(byte[] dtuId, byte type, byte[] content) {

        int len = 0;
        if (content != null)
            len = content.length;

        byte[] msg = new byte[16 + 1 + len];
        msg[0] = HD_START;
        msg[1] = (byte) 0x8B;


        byte[] lenbytes = ByteUtil.shortToBytes((short) msg.length);
        //msg[2] = 0x00;//此字节需为调整为实际字节长度
        //msg[3] = 0x10;
        System.arraycopy(lenbytes, 0, msg, 2, 2); //将DTU身份识别码返回
        System.arraycopy(dtuId, 0, msg, 4, 11); //将DTU身份识别码返回

        msg[15] = type;//查询类型
        System.arraycopy(content, 0, msg, 16, len); //请求的数据
        msg[msg.length - 1] = HD_END;

        return msg;
    }

    /**
     * 2.5.1 设置DTU 参数 DSC->DTU
     *
     * @param dtuId
     * @param content
     * @return
     */
    public static byte[] cmd8DDTU(byte[] dtuId, byte[] content) {

        int len = 0;
        if (content != null)
            len = content.length;

        byte[] msg = new byte[16 + len];
        msg[0] = HD_START;
        msg[1] = (byte) 0x8D;
        msg[2] = 0x00;
        msg[3] = 0x10;
        System.arraycopy(dtuId, 0, msg, 4, 11); //将DTU身份识别码返回

        System.arraycopy(content, 0, msg, 15, len); //请求的数据
        msg[msg.length - 1] = HD_END;

        return msg;
    }
}
