package com.godenwater.recv.server.hd;

/**
 * Created by Li on 2017/1/18.
 */
public class HdMessageBody {


    public byte[] content;

    public void setContents(byte[] content) {
        this.content = content;
    }

    public byte[] getContent() {
        if (this.content != null)
            return content;
        return null;
    }

}
