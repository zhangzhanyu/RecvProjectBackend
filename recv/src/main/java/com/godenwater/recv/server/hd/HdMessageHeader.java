package com.godenwater.recv.server.hd;

/**
 * Created by Li on 2017/1/18.
 */
public class HdMessageHeader {

    public static int LEN_START_BIT = 1;
    public static int LEN_BODY_SIZE = 1;
    public static int LEN_BODY_START_BIT = 1;

    //起始标志
    private byte START_BIT = (byte) 0x7B ;

    //包类型
    private byte CMD_TYPE ;

    //包长度，2位
    private byte[] BODY_LEN;

    // DTU身份识别码,11位
    private byte[] DTU_ID;

    //本地移动IP，4位
    private byte[] IP;

    //本地移动端口，2位
    private byte[] PORT;

    //结束标志
    private byte END_BIT =   (byte) 0x7B ;

    public byte getCMD_TYPE() {
        return CMD_TYPE;
    }

    public void setCMD_TYPE(byte CMD_TYPE) {
        this.CMD_TYPE = CMD_TYPE;
    }

    public byte[] getBODY_LEN() {
        return BODY_LEN;
    }

    public void setBODY_LEN(byte[] BODY_LEN) {
        this.BODY_LEN = BODY_LEN;
    }

    public byte[] getDTU_ID() {
        return DTU_ID;
    }

    public void setDTU_ID(byte[] DTU_ID) {
        this.DTU_ID = DTU_ID;
    }

    public byte[] getIP() {
        return IP;
    }

    public void setIP(byte[] IP) {
        this.IP = IP;
    }

    public byte[] getPORT() {
        return PORT;
    }

    public void setPORT(byte[] PORT) {
        this.PORT = PORT;
    }

    public int  getLength(){
        int len = 16;
        if(CMD_TYPE == 0x01){
            len = len + 6;
        }
        return len;
    }
}
