package com.godenwater.recv.server.qx;

import java.util.List;

/**
 * Created by Li on 2017/4/14.
 */
public class QxCommand {

    public static String BEGIN = "BG";
    public static String END = "ED";
    public static String COMMA = ",";

    public static String cmdQuery(String command){
        StringBuffer sb = new StringBuffer();
        sb.append(BEGIN).append(COMMA);
        sb.append(command).append(COMMA);
        sb.append(END);
        return sb.toString();
    }

    public static String cmdConfig(String command,String value){
        StringBuffer sb = new StringBuffer();
        sb.append(BEGIN).append(COMMA);
        sb.append(command).append(COMMA).append(value).append(COMMA);
        sb.append(END);
        return sb.toString();
    }


}
