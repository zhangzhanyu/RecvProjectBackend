package com.godenwater.recv.server.qx;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Li on 2017/4/14.
 */
public class QxFileBuilder {

    public static void generate(String areacode, String type, String value, String tm, String content) {
        String filename = areacode + "_" + type + "_" + value + "_" + tm + ".txt";
        try {
            File file = new File(filename);
            if (file.exists()) {
                FileUtils.write(file, System.getProperty("line.separator"), true);//写入换行符"\r\n"+System.getProperty("line.separator");
            }
            FileUtils.write(file, content, true);//追加写入文件
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        QxFileBuilder parser = new QxFileBuilder();
        String msg = "BG,12345, 321420 ,1163418 ,01000,18,YNAI,001, 20120912131000,005,009,03,ASA,010001, AAA5,-102, ADA5,080, ASB,0300, ASC,1000, ASD,-100, ASE,080, ASF,10000, ASG,000200,000000000,z,1,y_AAA,2,wA,4,9574,ED";
        parser.generate("123","1","1","20170410112200","111");
        parser.generate("123","1","1","20170410112200","22");
    }

}
