package com.godenwater.recv.server.qx;

import java.util.List;

/**
 * Created by Li on 2017/4/14.
 */
public class QxMessage {

   private  QxMessageHeader header;
    private List<QxMessageItem> vitems;
    private String control;
    private List<QxMessageItem> vstatus;

    private String mcrc;//校验码

    public QxMessageHeader getHeader() {
        return header;
    }

    public void setHeader(QxMessageHeader header) {
        this.header = header;
    }

    public List<QxMessageItem> getVitems() {
        return vitems;
    }

    public void setVitems(List<QxMessageItem> vitems) {
        this.vitems = vitems;
    }

    public List<QxMessageItem> getVstatus() {
        return vstatus;
    }

    public void setVstatus(List<QxMessageItem> vstats) {
        this.vstatus = vstats;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    public String getMcrc() {
        return mcrc;
    }

    public void setMcrc(String mcrc) {
        this.mcrc = mcrc;
    }
}
