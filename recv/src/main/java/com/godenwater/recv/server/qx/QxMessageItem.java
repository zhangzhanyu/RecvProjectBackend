package com.godenwater.recv.server.qx;

/**
 * Created by Li on 2017/4/14.
 */
public class QxMessageItem {
    private String text;
    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
