package com.godenwater.recv.server.qx;

import java.util.List;

/**
 * Created by Li on 2017/4/14.
 */
public class QxMessageLabel {
    String code;
    String title;
    String unit;
    String radio;
    int len;
    String note;

    //ASA     K≥0.4 负离子浓度     个·cm -3  0      6      5分钟负离子
    //AAA5    5分钟气温             ℃         1       4   5分钟空气温度
    //ADA5    5分钟相对湿度         %        0       3   5分钟相对湿度

}
