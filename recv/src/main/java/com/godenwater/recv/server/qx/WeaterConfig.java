package com.godenwater.recv.server.qx;

import com.godenwater.recv.spring.Configurer;

/**
 summit_gprs_tcp_flag=1
 summit_gprs_tcp_port=5280
 summit_gprs_udp_flag=0
 summit_gprs_udp_port=5290
 summit_gsm_flag = 1
 summit_gsm_serial = COM1
 summit_gsm_baudrate =9600
 summit_gsm_stopbits =1
 summit_gsm_bytesize = 8
 summit_gsm_parity = None
 */
public class WeaterConfig {


	/**
	 * GRPS端口
	 *
	 * @return
	 */
	public static int getHydroGprsTcpPort() {
		return Configurer.getIntProperty("weater_gprs_tcp_port", 5280);
	}
	public static boolean isHydroGprsTcpEnabled() {
		String port = Configurer.getProperty("weater_gprs_tcp_flag");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	public static int getHydroGprsUdpPort() {
		return Configurer.getIntProperty("weater_gprs_udp_port", 5290);
	}

	public static boolean isHydroGprsUdpEnabled() {
		String port = Configurer.getProperty("weater_gprs_udp_flag");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}


	/**
	 * GSM设置
	 *
	 * @return
	 */
	public static int getHydroGsmFlag() {
		return Configurer.getIntProperty("weater_gsm_flag",0);
	}

	public static String getHydroGsmSerial() {
		return Configurer.getProperty("weater_gsm_serial");
	}

	public static int getHydroGsmBaudRate() {
		return Configurer.getIntProperty("weater_gsm_baudrate");
	}

	public static int getHydroGsmByteSize() {
		return Configurer.getIntProperty("weater_gsm_bytesize",8);
	}

	public static String getHydroGsmParity() {
		return Configurer.getProperty("weater_gsm_parity");
	}

	public static String getHydroGsmStopbits() {
		return Configurer.getProperty("weater_gsm_stopbits");
	}

	public static boolean isHydroGsmEnabled() {
		int port = getHydroGsmFlag();
		if (port == 0) {
			return false;
		} else {
			return true;
		}
	}
}
