package com.godenwater.recv.server.qx;

import cn.gov.mwr.szy206.IMessage;
import cn.gov.mwr.szy206.command.DownCommand;
import cn.gov.mwr.szy206.utils.ByteUtil;
import com.godenwater.core.container.Module;
import com.godenwater.core.spring.Application;
import com.godenwater.core.spring.BaseDao;
import com.godenwater.recv.manager.SessionManager;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.szy206.ConnectionManager;
import com.godenwater.recv.server.szy206.SzyHelp;
import com.godenwater.recv.utils.TaskEngine;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 西安山脉协议接收服务器，此为单独运行时所用！
 */
public class WeaterServer {

    private static Logger logger = LoggerFactory.getLogger(WeaterServer.class);

    private long startDate = System.currentTimeMillis();
    private String host;
    private boolean initialized = false;
    private boolean started = false;
    // ----------------------------------------------------------------------


    /**
     * All modules loaded by this server
     */
    private Map<Class, Module> modules = new LinkedHashMap<Class, Module>();

    private ClassLoader loader;

    // 创建一个报文队列, 应根据遥测机的数量而定，默认给定300
    private BlockingQueue<CommonMessage> queueOriginal = new LinkedBlockingQueue<CommonMessage>(
            300);

    private BlockingQueue<IMessage> queueCombin = new LinkedBlockingQueue<IMessage>(
            300);

    // 创建10个生产线程，使用线程池的方式，可以保障线程的安全性。
    ExecutorService service = Executors.newFixedThreadPool(10);

    // 计数
    public final AtomicInteger wc = new AtomicInteger();

    /**
     * 传输消息最大数量
     */
    public static final int MAX_SERIAL = 65535;

    // 定义所管理的测站列表
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");

    private static WeaterServer instance;

    private boolean RUNNING = false;

    /**
     * Returns a singleton instance of XMPPServer.
     *
     * @return an instance.
     */
    public static WeaterServer getInstance() {
        if (instance == null) {
            instance = new WeaterServer();
        }
        return instance;
    }

    /**
     * Creates a server and starts it.
     */
    private WeaterServer() {
        if (instance != null) {
            throw new IllegalStateException("RTU QX server is already running");
        }
        instance = this;
    }

    /**
     * 初始化数据
     */
    public void initialize() {

        startDate = System.currentTimeMillis();

        loader = Thread.currentThread().getContextClassLoader();

        initialized = true;

    }

    /**
     * 加载模块
     *
     * @param module
     */
    private void loadModule(String module) {
        System.out.println(">> Load " + module + "...");
        try {
            Class modClass = loader.loadClass(module);
            Module mod = (Module) modClass.newInstance();
            this.modules.put(modClass, mod);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("admin.error", e);
        }
    }

    public SessionManager getSessionManager() {
        return (SessionManager) modules.get(SessionManager.class);
    }

    public ConnectionManager getConnectionManager() {
        return (ConnectionManager) modules.get(ConnectionManager.class);
    }

    /**
     * 加载内部模块
     */
    private void loadModules() {

        loadModule(WeaterConnectionManager.class.getName()); // 加载内部连接管理
        //loadModule(SessionManager.class.getName()); // 加载内部连接管理

        //loadModule(StationCacheManager.class.getName()); // 加载所有测站信息
        //loadModule(MessageManager.class.getName()); // 加载报文管理
        //loadModule(TaskManager.class.getName()); // 加载内部连接管理
        // loadModule(LogManager.class.getName()); //加载内部连接管理

        //loadModule(MonitorManager.class.getName()); // 加载监控终端管理器
    }

    private void initModules() {
        for (Module module : modules.values()) {
            boolean isInitialized = false;
            try {
                module.initialize();
                isInitialized = true;
            } catch (Exception e) {
                e.printStackTrace();
                this.modules.remove(module.getClass());
                if (isInitialized) {
                    module.stop();
                    module.destroy();
                }
                logger.error("admin.error", e);
            }
        }
    }

    private void startModules() {
        for (Module module : modules.values()) {
            boolean started = false;
            try {
                module.start();
            } catch (Exception e) {
                if (started && module != null) {
                    module.stop();
                    module.destroy();
                }
                logger.error("admin.error", e);
            }
        }
    }

    /**
     * 启动服务实例
     */
    public void start() {

        try {
            initialize();

            // load all the modules
            loadModules();
            // Initize all the modules
            initModules();
            // Start all the modules
            startModules();

            // /---------------------------

            RUNNING = true;

            // service.execute(new MessageConsumer(this));
            // service.execute(new CombinConsumer(this));
            // service.execute(new RtuDatabaseConsumer(this));
            // service.execute(new BizDatabaseConsumer(this));

            System.out.println("RTU SERVER >> Startup.");

            startDate = System.currentTimeMillis();

            // /------------------------------

            started = true;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            System.out.println("startup.error");
            // shutdownServer();
        }

    }

    /**
     * 关闭服务实例
     */
    public void stop() {

        RUNNING = false;
        try {
            Thread.sleep(3 * 1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        service.shutdown();
        // service.shutdownNow();
        System.out.println("正在关闭处理服务，请等待....");
        try {
            Thread.sleep(3 * 1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // If we don't have modules then the server has already been shutdown
        if (modules.isEmpty()) {
            return;
        }
        // Get all modules and stop and destroy them
        for (Module module : modules.values()) {
            module.stop();
            module.destroy();
        }
        modules.clear();

        TaskEngine.getInstance().shutdown();

        System.out.println("RTU SERVER >> Shutting down...");
    }

    public boolean running() {
        return RUNNING;
    }

    /**
     * 检查数据库是否异常
     *
     * @return
     */
    public boolean verifyDb() {
        BaseDao dao = (BaseDao) Application.getInstance().getBean("baseDao");
        java.sql.Connection conn;
        try {
            conn = dao.getDataSource().getConnection();

            if (conn.isClosed()) {
                return false;
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    public long getStartDate() {
        return startDate;
    }

    // private synchronized void createListeners() {
    // createTcpListener();
    // createUdpListener();
    // createGsmListener();
    // createPstnListener();
    // createCommListener();
    // }

    /**
     * 原始报文，消息入队
     *
     * @param IMessage
     * @return
     */
    public boolean queuePush(CommonMessage message) {
        return this.queueOriginal.offer(message);
    }

    /**
     * 原始报文，消息出队
     *
     * @return
     */
    public CommonMessage queuePoll() {
        CommonMessage result = null;
        try {
            result = this.queueOriginal.take();
        } catch (InterruptedException e) {
            logger.error("", e);
        }
        return result;
    }

    /**
     * 原始报文，获取队列大小
     *
     * @return
     */
    public int queueSize() {
        return this.queueOriginal.size();
    }

    /**
     * 合并报文，消息入队
     *
     * @param IMessage
     * @return
     */
    public boolean queueCombinPush(IMessage message) {
        return this.queueCombin.offer(message);
    }

    /**
     * 合并报文，消息出队
     *
     * @return
     */
    public IMessage queueCombinPoll() {
        IMessage result = null;
        try {
            result = this.queueCombin.take();
        } catch (InterruptedException e) {
            logger.error("", e);
        }
        return result;
    }

    /**
     * 合并报文，获取队列大小
     *
     * @return
     */
    public int queueCombinSize() {
        return this.queueCombin.size();
    }

    /**
     * 获取消息总数
     *
     * @return
     */
    public int messageSize() {
        if (wc.get() == Integer.MAX_VALUE) {
            wc.set(0);
        }
        return wc.incrementAndGet();
    }

    /**
     * 保存错误的消息
     */
    protected void saveErrMessage(IMessage message) {

    }

    protected void sendCommand(String command) {
        DownCommand cmd = new DownCommand();

        IMessage message = null;
        // ------------------------------
        if (StringUtils.isNotEmpty(StringUtils.trim(command))) {

            byte[] funcCode = ByteUtil.HexStringToBinary(command);

            switch (funcCode[0]) {
                case 0x02: // 链路检测

                    break;
                case 0x10:// 设置遥测终端、中继站地址

                    break;
                case 0x11:// 设置遥测终端、中继站时钟

                    break;
                case 0x12:// 设置遥测终端工作模式

                    break;
                case 0x15:// 设置遥测终端本次充值量

                    break;
                case 0x16:// 设置遥测终端剩余水量报警值

                    break;
                case 0x17:// 设置遥测终端水位基值、水位上下限

                    break;
                case 0x18:// 设置遥测终端水压上、下限

                    break;
                case 0x19:// 设置遥测终端水质参数种类、上限值

                    break;
                case 0x1A:// 设置遥测终端水质参数种类、下限值

                    break;
                case 0x1B:// 设置遥测终端站水量的表底（初始）值

                    break;
                case 0x1C:// 设置遥测终端转发中继引导码长

                    break;
                case 0x1D:// 设置中继站转发终端地址

                    break;
                case 0x1E:// 设置中继站工作机自动切换，自报状态

                    break;
                case 0x1F:// 设置遥测终端流量参数上限值

                    break;
                case 0x20:// 设置遥测终端检测参数启报阈值及固态存储时间段间隔

                    break;
                case 0x30:// 设置遥测终端IC卡功能有效

                    break;
                case 0x31:// 取消遥测终端IC卡功能

                    break;
                case 0x32:// 定值控制投入

                    break;
                case 0x33:// 定值控制退出

                    break;
                case 0x34:// 定值量设定
                    // sendMessage(command);

                    break;
                default:
                    System.out.println("未知指令，请重新发送正确指令或输入HELP进行指令查看.");
            }

            if (message != null) {

                System.out.println("---------中心站查询遥测站-------功能码：0x" + command
                        + "---------");
                //cmd.printHexString(message);
                System.out
                        .println("----------------------------------------------------------");
            }
        }

    }

    public static void main(String[] args) {
        WeaterServer server = WeaterServer.getInstance();
        server.start();
        InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        try {
            String cmd = "";

            boolean flag = true;
            System.out.print("CMD>>");
            while (flag) {
                cmd = br.readLine();
                if (cmd.equalsIgnoreCase("exit")
                        || cmd.equalsIgnoreCase("quit")) {
                    server.stop();
                    flag = false;
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    System.exit(0);

                } else if (cmd.equalsIgnoreCase("help")) {
                    SzyHelp.printHelp();
                } else if (cmd.equalsIgnoreCase("client")) {

                } else if (cmd.equalsIgnoreCase("data")) {
                    SzyHelp.printData(server.queueSize());
                } else {
                    server.sendCommand(cmd);
                }

                System.out.print("CMD>>");
            }

        } catch (IOException e) {
            System.out.println("系统错误！");
            e.printStackTrace();
        } finally {
            try {
                is.close();
                br.close();
            } catch (IOException e) {
                System.out.println("关闭流发生错误！");
                e.printStackTrace();
            }
        }
    }

}
