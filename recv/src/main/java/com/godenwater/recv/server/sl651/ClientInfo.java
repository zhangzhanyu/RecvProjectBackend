package com.godenwater.recv.server.sl651;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.mina.core.session.IoSession;

/**
 * 客户端机器的状态，包括对客户端机器的最后收到消息的时间，收到的消息总数
 * 
 * @ClassName: ClientStatus
 * @Description: TODO
 * @author lipujun
 * @date Mar 2, 2013
 * 
 */
public class ClientInfo {

	private String stcd;
	
	/**
	 * 最后的更新时间
	 */
	private Date lastRecvTime = new Date(System.currentTimeMillis());

	/**
	 * 收到的消息总数
	 */
	private AtomicInteger recvNum = new AtomicInteger(0);

	/**
	 * 连接的session
	 */
	private IoSession session = null;
	
	public ClientInfo() {
		super();
	}

	public String getStcd() {
		return stcd;
	}

	public void setStcd(String stcd) {
		this.stcd = stcd;
	}

	public Date getLastRecvTime() {
		return lastRecvTime;
	}

	public void setLastRecvTime(Date lastRecvTime) {
		this.lastRecvTime = lastRecvTime;
	}

	public AtomicInteger getRecvNum() {
		return recvNum;
	}

	public void setRecvNum(AtomicInteger recvNum) {
		this.recvNum = recvNum;
	}

	public IoSession getSession() {
		return session;
	}

	public void setSession(IoSession session) {
		this.session = session;
	}

	public void updateLastTime() {
		lastRecvTime = new Date(System.currentTimeMillis());
	}

	public void updateRecvMsgs() {
		recvNum.incrementAndGet();
	}
	
	public int viewRecvMsgs(){
		return recvNum.get();
	}
	
	public Date viewLastTime(){
		return lastRecvTime;
	}
	
	
}
