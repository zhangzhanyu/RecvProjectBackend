package com.godenwater.recv.server.sl651;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class HydroClientTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HydroClient client = new HydroClient();
		
		String filePath = "C:/Users/goldenwater/Desktop/Log/水文网络数据日志/20130917.txt";
		
		try {
			String encoding = "GBK";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					System.out.println(lineTxt);
					String log  = lineTxt;
					String[] loglist = log.split(" ");
					if(loglist.length >2){
						String msg = loglist[3];
						
						client.sendCommand(msg);
					
					}
					
					Thread.sleep(1000);
				}
				bufferedReader.close();
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
		}

	}

}
