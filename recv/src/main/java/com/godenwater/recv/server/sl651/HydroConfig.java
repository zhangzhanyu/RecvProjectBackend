package com.godenwater.recv.server.sl651;

import com.godenwater.core.spring.Configurer;


public class HydroConfig {

	/**
	 * 中心站址，代表发送数据源地址中高四位
	 * 
	 * @return
	 */
	public static String getCenterAddr() {
		return (String) Configurer.getProperty("center_address");
	}

	/**
	 * 当前节点的行政区划
	 * 
	 * @return
	 */
	public static String getCenterAddvcd() {
		return (String) Configurer.getProperty("center_addvcd");
	}

	/**
	 * 当前节点的行政级别 4为市级 6为县级
	 * 
	 * @return
	 */
	public static String getCenterLevel() {
		return (String) Configurer.getProperty("center_level");
	}

	// ----------------------------------------------------

	/**
	 * 线程及缓冲区大小
	 * 
	 * @return
	 */
	public static int getBufferSize() {

		return Configurer.getIntProperty("buffer_size", 4096);
	}

	public static int getThreadSize() {
		return Configurer.getIntProperty("thread_size", 10);
	}

	public static int getQueueSize() {
		return Configurer.getIntProperty("queue_size", 300);
	}

	// ----------------------------------------------------

	/**
	 * GRPS端口
	 * 
	 * @return
	 */
	public static boolean isHydroGprsTcpEnabled() {
		String port = Configurer.getProperty("hydro_gprs_tcp_port");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	public static int getHydroGprsTcpPort() {
		return Configurer.getIntProperty("hydro_gprs_tcp_port", 20001);
	}

	
	public static boolean isHydroGprsUdpEnabled() {
		String port = Configurer.getProperty("hydro_gprs_udp_port");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	public static int getHydroGprsUdpPort() {
		return Configurer.getIntProperty("hydro_gprs_udp_port", 20002);
	}

	/**
	 * GSM设置
	 * 
	 * @return
	 */
	public static String getHydroGsmSerial() {
		return Configurer.getProperty("hydro_gsm_serial");
	}

	public static int getHydroGsmBaudRate() {
		return Configurer.getIntProperty("hydro_gsm_baudrate");
	}

	public static String getHydroGsmMobile() {
		return Configurer.getProperty("hydro_gsm_mobile");
	}

	public static boolean isHydroGsmEnabled() {
		String port = getHydroGsmSerial();
		if (port == null || port.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * PSTN设置
	 * 
	 * @return
	 */

	public static String getHydroPstnSerial() {
		return Configurer.getProperty("hydro_pstn_serial");
	}

	public static int getHydroPstnBaudRate() {
		return Configurer.getIntProperty("hydro_pstn_baudrate");
	}

	public static String getHydroPstnMobile() {
		return Configurer.getProperty("hydro_pstn_mobile");
	}

	public static boolean isHydroPstnEnabled() {
		String port = getHydroGsmSerial();
		if (port == null || port.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	// ----------------------------------------------------
	/**
	 * 图片文件临时目录
	 * 
	 * @return
	 */
	public static String getPicTempPath() {
		return (String) Configurer.getProperty("msg.img.path");
	}

	/**
	 * 图片保存路径
	 * 
	 * @return
	 */
	public static String getPicSavePath() {
		return (String) Configurer.getProperty("pic_savepath");
	}

	/**
	 * 图片格式，如jpg,gif,png,bmp
	 * 
	 * @return
	 */
	public static String getPicFormat() {
		return (String) Configurer.getProperty("pic_format");
	}

	/**
	 * 图片命名规则，如：站名+观测时间
	 * 
	 * @return
	 */
	public static String getPicNameRule() {
		return (String) Configurer.getProperty("pic_namerule");
	}

	/**
	 * 图片目录存储方式，按年划分存储，按月划分存储，按天划分存储
	 * 
	 * @return
	 */
	public static String getPicDirRule() {
		return (String) Configurer.getProperty("pic_dirrule");
	}

	/**
	 * #--------------------------------------------------
	 * #___________水文监测数据通信规约___________________
	 * #--------------------------------------------------
	 * 
	 */

}
