package com.godenwater.recv.server.sl651;

public class HydroHelp {

	public static void printHelp() {

		StringBuffer sb = new StringBuffer();
		sb.append("------ RTU 中心站下发命令列表 ------\n");
		sb.append(">> 36H	遥测站图片报或中心站查询遥测站图片采集信息\n");
		sb.append(">> 37H	中心站查询遥测站实时数据\n");
		sb.append(">> 38H	中心站查询遥测站时段数据\n");
		sb.append(">> 39H	中心站查询遥测站人工置数\n");
		sb.append(">> 3AH	中心站查询遥测站指定要素数据\n");
		sb.append(">> 3B～3FH	保留\n");
		sb.append(">> 40H	中心站修改遥测站基本配置表\n");
		sb.append(">> 41H	中心站读取遥测站基本配置表/遥测站自报基本配置表\n");
		sb.append(">> 42H	中心站修改遥测站运行参数配置表\n");
		sb.append(">> 43H	中心站读取遥测站运行参数配置表/遥测站自报运行参数配置表\n");
		sb.append(">> 44H	查询水泵电机实时工作数据\n");
		sb.append(">> 45H	查询遥测终端软件版本\n");
		sb.append(">> 46H	查询遥测站状态和报警信息\n");
		sb.append(">> 47H	初始化固态存储数据\n");
		sb.append(">> 48H	恢复终端出厂设置\n");
		sb.append(">> 49H	修改密码\n");
		sb.append(">> 4AH	设置遥测站时钟\n");
		sb.append(">> 4BH	设置遥测终端IC卡状态\n");
		sb.append(">> 4CH	控制水泵开关命令/水泵状态信息自报\n");
		sb.append(">> 4DH	控制阀门开关命令/阀门状态信息自报\n");
		sb.append(">> 4EH	控制闸门开关命令/闸门状态信息自报\n");
		sb.append(">> 4FH	水量定值控制命令\n");
		sb.append(">> 50H	中心站查询遥测站事件记录\n");
		sb.append(">> 51H	中心站查询遥测站时钟\n");
		sb.append("----------------------------------\n");
		sb.append("------ 系统控制命令  ------\n");
		sb.append(">> quit	退出系统\n");
		sb.append(">> client  查看遥测机状态\n");
		sb.append(">> data  查看未处理的数据列表\n");
		sb.append(">> help  系统帮助\n");
		System.out.println(sb.toString());
	}

	public static void printData(int count) {
		StringBuffer sb = new StringBuffer();
		sb.append("------系统未处理的报文数量为：" + count + " ------\n");
		System.out.println(sb.toString());
	}

}
