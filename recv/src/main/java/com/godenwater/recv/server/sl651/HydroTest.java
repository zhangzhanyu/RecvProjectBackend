package com.godenwater.recv.server.sl651;

import cn.gov.mwr.sl651.IMessage;
import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.command.DownCommand;
import cn.gov.mwr.sl651.utils.ByteUtil;

public class HydroTest {

	public static void main(String[] args) {

		DownCommand cmd = new DownCommand();

		cmd.setStartBit(Symbol.SOH_HEX);
		cmd.setCenterAddr(ByteUtil.HexStringToBinary("00"));
		cmd.setStationAddr(ByteUtil.HexStringToBinary("1234567800"));
		cmd.setPassword(ByteUtil.HexStringToBinary("1234"));

		cmd.setBodyStartBit(Symbol.STX);
		cmd.setEof(Symbol.ENQ);
		cmd.setFuncCode(new byte[] { (byte) 0x38 });
		IMessage message = cmd.send38Message(1, null, "14081708",
				"14081720", "000000", "F4");

		// 保存进数据库
		System.out.println(cmd.printHexString(message));

		// String hexString =
		// "7E7EFF0000000003FFFF38001F0200A0130505152450F1F1000000000348F0F002010605000418000005F46000036EBE";
		// hexString =
		// "7E7E30556677889900003800DE02000A121116164136F1F1556677889950F0F012111600000418000000F4F400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001E000000000000000000000000000000000000000014000000000000000000173E1A";
		// byte[] message = ByteUtil.HexStringToBinary(hexString);

	}

}
