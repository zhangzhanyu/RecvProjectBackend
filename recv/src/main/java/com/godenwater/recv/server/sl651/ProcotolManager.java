package com.godenwater.recv.server.sl651;

import org.springframework.cache.Cache;
import org.springframework.data.redis.cache.RedisCacheManager;

import cn.gov.mwr.sl651.appendix.FC;

import com.godenwater.core.container.BasicModule;
import com.godenwater.core.spring.Application;

public class ProcotolManager extends BasicModule {

	/** Cache of local station. */
	private Cache FAHexCache;
	private Cache FAAscCache;

	/** Cache of local station. */
	private Cache FBCache;

	/** Cache of local station. */
	private Cache FCHexCache;
	private Cache FCAscCache;

	public ProcotolManager() {
		super("Procotol Manager");

	}

	/**
	 * Returns an unmodifiable Collection of all users in the system.
	 * 
	 * @return an unmodifiable Collection of all users.
	 */
	public void initialize() {
		// Initialize caches.
		RedisCacheManager redisCacheManager = (RedisCacheManager) Application
				.getInstance().getCtx().getBean("redisCacheManager");

		FAHexCache = redisCacheManager.getCache("FA");
		FAAscCache = redisCacheManager.getCache("FA");

		FBCache = redisCacheManager.getCache("FB");

		FCHexCache = redisCacheManager.getCache("FCHEX");
		FCAscCache = redisCacheManager.getCache("FCASC");
		// System.out.println(redisCacheManager.getCache("aaaa").get("a1").get());
	}

	public FC getFcHex(String hex) throws Exception {

		if (hex == null) {
			throw new Exception("FC hex cannot be null");
		}
		// Make sure that the username is valid.
		hex = hex.trim().toLowerCase();
		FC fc = (FC) FCHexCache.get(hex).get();
		if (fc == null) {
			fc = new FC();
			fc.setHex(hex);
			fc.setLen(0);
			fc.setDecimal(0);
		}
		return fc;
	}

	public FC getFcAsc(String asc) throws Exception {
		if (asc == null) {
			throw new Exception("FC hex cannot be null");
		}
		// Make sure that the username is valid.
		asc = asc.trim().toLowerCase();
		FC fc = (FC) FCAscCache.get(asc).get();
		if (fc == null) {
			fc = new FC();
			fc.setHex(asc);
			fc.setLen(0);
			fc.setDecimal(0);
		}
		return fc;
	}
}
