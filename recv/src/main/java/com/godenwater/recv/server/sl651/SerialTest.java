package com.godenwater.recv.server.sl651;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.serial.SerialAddress;
import org.apache.mina.transport.serial.SerialConnector;

public class SerialTest extends IoHandlerAdapter {

	@SuppressWarnings("deprecation")
	public static void main(String [] args){

	    SerialConnector connector = new SerialConnector();      
//	    
//	    connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(
//	            new TextLineCodecFactory()));
////	    connector.setHandler(new SerialTest());
	    
	    DefaultIoFilterChainBuilder chain = connector.getFilterChain();
//		 chain.addLast("logger", new LoggingFilter());
		chain.addLast("protocol", new ProtocolCodecFilter(
				new HydroCodecFactory(true)));

	    
	    connector.setHandler(new CommDataHandler("COMM"));
	    String comPort = "COM2";
	    int bauds = 19200;
	    ConnectFuture future = connector.connect(
	        new SerialAddress(comPort, bauds,   SerialAddress.DataBits.DATABITS_8,
	            SerialAddress.StopBits.BITS_1, SerialAddress.Parity.NONE,
	            SerialAddress.FlowControl.NONE));       

//	    future.join();      
	    String rawcmd = "7E7EFF0000000003FFFF51000F020020130503152852F1F10000000003033792";
         
	    future.getSession().write(rawcmd.getBytes());
	    
//	    future.getSession().close();
	    connector.dispose();
	}
	public void sessionClosed(IoSession session) {     
	    System.err.println("Total " + session.getReadBytes() + " byte(s)");
	}
	public void messageSent(IoSession session, Object message) {
	    System.out.println("message has been sent: " + message);
	}
	public void messageReceived(IoSession session, Object message) {
	    System.out.println("message received:[" + message + "]");
	}   
	}