package com.godenwater.recv.server.sl651;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.gov.mwr.sl651.HydroMessage;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.IMessageHeader;
import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.body.Up2FBody;
import cn.gov.mwr.sl651.body.UpBaseBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 报文解码类，作为上行报文的解码类，此类必须实现对所有消息体的封闭细节
 * 
 * @ClassName: YfServerDataDecoder
 * @Description: 处理断包和粘包的实现类
 * @author lipujun
 * @date Mar 2, 2013
 * 
 */
public class ServerDataDecoder2Bak extends CumulativeProtocolDecoder {
	private static final AttributeKey BUF_BYTE = new AttributeKey(
			ServerDataDecoder2.class, "BUF_KEY");

	private final AttributeKey CONTEXT = new AttributeKey(getClass(), "context");

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public Context getContext(IoSession session) {
		Context ctx = (Context) session.getAttribute(CONTEXT);
		if (ctx == null) {
			ctx = new Context();
			session.setAttribute(CONTEXT, ctx);
		}
		return ctx;
	}

	private class Context {
		// 状态变量
		private final IoBuffer innerBuffer;

		public Context() {
			innerBuffer = IoBuffer.allocate(100).setAutoExpand(true);
		}
	}

	/**
	 * 这个办法的返回值是重点：
	 * 
	 * 1、当内容正好时，返回false，告诉父类可以进行下一批消息的处理
	 * 
	 * 2、内容不符时，需要下一批消息的内容，此时返回false
	 * ，如许父类会将内容放进IoSession中，等下次数据来后就主动拼装再交给本类的doDecode
	 * 
	 * 3、当内容多时，返回true，因为需要再将本批数据进行读取，父类会将残剩的数据再次推送本类的doDecode
	 */
	@Override
	protected boolean doDecode(IoSession session, IoBuffer buffer,
			ProtocolDecoderOutput out) throws Exception {
		// TODO Auto-generated method stub
		logger.info(">> HEX : " + ByteUtil.toHexString(buffer.array()));

		if (buffer.remaining() > 0) {// 表示缓冲区中有数据
			// System.out.println("------buffer.remaining() ---------"
			// + buffer.remaining() );

			buffer.mark();// 标记当前位置，以便reset

			byte[] mode = new byte[1];
			buffer.get(mode);

			IMessageHeader header;
			if (mode[0] == (byte)Symbol.SOH_ASC) {
				header = new cn.gov.mwr.sl651.header.AscHeader();
				header.setStartBit(new byte[] { mode[0] });
			} else if (mode[0] == (byte)Symbol.SOH_HEX ) {
				header = new cn.gov.mwr.sl651.header.HexHeader();
				byte[] startBit = new byte[1];
				buffer.get(startBit);
				header
						.setStartBit(new byte[] { Symbol.SOH_HEX,
								Symbol.SOH_HEX });
			} else {
				// 处理DTU设备上线时的登录信息，发的是DTU编号和手机号
				// buffer.get(buffer.array().length);
				buffer.get();
				return true;
			}

			
			// 上行的，中心站地址在前
			byte[] centerAddr = new byte[header.getCenterAddrLen()];
			buffer.get(centerAddr);
			header.setCenterAddr(centerAddr);

			// 上行的，遥测站地址在前
			byte[] stationAddr = new byte[header.getStationAddrLen()];
			buffer.get(stationAddr);
			header.setStationAddr(stationAddr);

			byte[] password = new byte[header.getPasswordLen()];
			buffer.get(password);
			header.setPassword(password);
			//
			byte[] funcCode = new byte[header.getFuncCodeLen()];
			buffer.get(funcCode);
			header.setFuncCode(funcCode);

			byte[] bodySize = new byte[header.getBodySizeLen()];
			buffer.get(bodySize);
			header.setBodySize(bodySize);

			byte[] bodyStartBit = new byte[header.getBodyStartBitLen()];
			buffer.get(bodyStartBit);
			header.setBodyStartBit(bodyStartBit);

			if (bodyStartBit[0] == Symbol.SYN) {
				byte[] bodyCount = new byte[header.getBodyCountLen()];
				buffer.get(bodyCount);
				header.setBodyCount(bodyCount);
			}

			// 转换报文中的上下行标识及报文长度
			int bodyLengthLen = parseLength(bodySize);// 需要考虑拆分字节;
			if (bodyStartBit[0] == Symbol.SYN) {
				bodyLengthLen = bodyLengthLen - header.getBodyCountLen();
			}
			int rcrlen = (mode[0] == Symbol.SOH_ASC) ? 4 : 2;
			int size = bodyLengthLen + 1 + rcrlen;

			// 若是消息内容的长度不敷则直接返回true
			if (size > buffer.remaining()) {// 若是消息大小与缓冲区中的内容大小不匹配，则重置，相当于不读取size
//				System.out.println("------size > buffer.remaining() ---------"
//						+ size + "  " + buffer.remaining());
				buffer.reset();
				return false;// 接管新数据，以拼凑成完全数据
			} else {
//				System.out
//						.println("------size < buffer.remaining() ---------bodyLengthLen "
//								+ bodyLengthLen
//								+ " size "
//								+ size
//								+ "  "
//								+ buffer.remaining());
				byte[] bodyContent = new byte[bodyLengthLen];// 根据前面header的内容来获取长度
				buffer.get(bodyContent);

				// 终止符
				byte[] eof = new byte[1];
				buffer.get(eof);

				// 校验码
				byte[] crc;
				if (mode[0] == Symbol.SOH_ASC) {
					crc = new byte[4];
				} else {
					crc = new byte[2];

				}
				buffer.get(crc);

				// 组装消息
				HydroMessage message = new HydroMessage();
				message.setHeader(header);

				// 根据功能码，构建body体
				IMessageBody body = new UpBaseBody();
				body.setContents(bodyContent);
				message.setBody(body);
				message.setEOF(eof[0]);
				message.setCRC(crc);

				out.write(message);

				if (buffer.remaining() > 0) {// 若是读取内容后还粘了包，就让父类再给解析一次，返回true进行下一次解析

					return true;
				}
			}
		}
		return false;// 处理惩罚成功，让父类进行接管下个包
	}

	private int parseLength(byte[] bodyLen) {
		byte A1 = (byte) (bodyLen[0] & 0x0f);
		byte A2 = bodyLen[1];
		return ByteUtil.bytesToUshort(new byte[] { A1, A2 });
	}

	public void dispose(IoSession session) throws Exception {
		// TODO Auto-generated method stub

	}

	public void finishDecode(IoSession session, ProtocolDecoderOutput out)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
