package com.godenwater.recv.server.summit;

import cn.gov.mwr.szy206.utils.ByteUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * CRC校验工具
 * @author admin
 *
 */
public class CrcUtil {
	// X7+X6+X5+X2+1 = 0*X8+1*X7+1*X6+1*X5+0*X4+0*X3+1*X2+0*X+1*X0；
	// 生成的多项式为：X7+X6+X5+X2+1。 0 1110 0101
	public static int[] crctable76520 = new int[] { 0, 229, 47, 202, 94, 187,
			113, 148, 188, 89, 147, 118, 226, 7, 205, 40, 157, 120, 178, 87,
			195, 38, 236, 9, 33, 196, 14, 235, 127, 154, 80, 181, 223, 58, 240,
			21, 129, 100, 174, 75, 99, 134, 76, 169, 61, 216, 18, 247, 66, 167,
			109, 136, 28, 249, 51, 214, 254, 27, 209, 52, 160, 69, 143, 106,
			91, 190, 116, 145, 5, 224, 42, 207, 231, 2, 200, 45, 185, 92, 150,
			115, 198, 35, 233, 12, 152, 125, 183, 82, 122, 159, 85, 176, 36,
			193, 11, 238, 132, 97, 171, 78, 218, 63, 245, 16, 56, 221, 23, 242,
			102, 131, 73, 172, 25, 252, 54, 211, 71, 162, 104, 141, 165, 64,
			138, 111, 251, 30, 212, 49, 182, 83, 153, 124, 232, 13, 199, 34,
			10, 239, 37, 192, 84, 177, 123, 158, 43, 206, 4, 225, 117, 144, 90,
			191, 151, 114, 184, 93, 201, 44, 230, 3, 105, 140, 70, 163, 55,
			210, 24, 253, 213, 48, 250, 31, 139, 110, 164, 65, 244, 17, 219,
			62, 170, 79, 133, 96, 72, 173, 103, 130, 22, 243, 57, 220, 237, 8,
			194, 39, 179, 86, 156, 121, 81, 180, 126, 155, 15, 234, 32, 197,
			112, 149, 95, 186, 46, 203, 1, 228, 204, 41, 227, 6, 146, 119, 189,
			88, 50, 215, 29, 248, 108, 137, 67, 166, 142, 107, 161, 68, 208,
			53, 255, 26, 175, 74, 128, 101, 241, 20, 222, 59, 19, 246, 60, 217,
			77, 168, 98, 135 };

	// ----------------------------------------------------------------

	/**
	 * 这个生成校验码的方法应该没有问题。
	 */
	public static int makeCrc8Table(int data) {
		int crc, i, j;
		crc = 0;
		crc = crc ^ data;
		for (j = 1; j <= 8; j++) {
			if ((crc & 0x80) == 0x80)
				crc = (crc << 1) ^ 0xE5; // 多项式值为E5,被校验值左移
			else
				crc = crc << 1;
		}
		crc = crc & 0xff;
		 System.out.println("check \t" + Integer.toHexString(crc));
		return crc;
	}

	/**
	 * 这个校验方法感觉有问题，可结果好像是正确的
	 * 
	 * @param data
	 * @return
	 */
	public static int check(byte[] data) {
		int crc, i, j;
		crc = 0;
		int len = data.length;
		for (i = 0; i < len; i++) {
			crc = crc ^ data[i];
			for (j = 1; j <= 8; j++) {
				if ((crc & 0x80) == 0x80)
					crc = (crc << 1) ^ 0xE5; // 多项式值为E5,被校验值左移
				else
					crc = crc << 1;
			}
		}
		crc = crc & 0xff;
//		System.out.println("check " + Integer.toHexString(crc));
		return crc;
	}

	// (生成多项式X^8+X^6+X^4+X^3+X^2+X^1)
	public static int[] crctable864321 = new int[] { 0, 94, 188, 226, 97, 63,
			221, 131, 194, 156, 126, 32, 163, 253, 31, 65, 157, 195, 33, 127,
			252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220, 35, 125, 159,
			193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98, 190, 224,
			2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255, 70,
			24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89,
			7, 219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38,
			196, 154, 101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69,
			198, 152, 122, 36, 248, 166, 68, 26, 153, 199, 37, 123, 58, 100,
			134, 216, 91, 5, 231, 185, 140, 210, 48, 110, 237, 179, 81, 15, 78,
			16, 242, 172, 47, 113, 147, 205, 17, 79, 173, 243, 112, 46, 204,
			146, 211, 141, 111, 49, 178, 236, 14, 80, 175, 241, 19, 77, 206,
			144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238, 50, 108, 142,
			208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115, 202,
			148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
			87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170,
			72, 22, 233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74,
			20, 246, 168, 116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10,
			84, 215, 137, 107, 53 };

	public static int CRC8(int IN, int CRC) {
		IN ^= CRC;
		return crctable76520[IN];

	}

	/**
	 * 用查表法进行计算
	 * @param bytes
	 * @return
	 */
	public static byte[] crc8Check(byte[] bytes) {

		int crc = 0x00;
		for (byte b : bytes) {
			// crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
			crc = crctable76520[(crc ^ b) & 0xff];
		}

		return ByteUtil.ubyteToBytes(crc);

	}

	public static void main(String[] args) throws IOException {

		String hexString = "B33201040200C08937120000131700000005005C000245132200";
		// hexString = "910000001234C0000000040000005330100300";
		hexString="3012341503405C120312130312"	;
		hexString = "BB 12 34 15 03 40 86 01 35 02 00 00 35 00 00 00 00 41 15 15 12 03 12 68 25 10 35 12 00 00 70 00";
		hexString = StringUtils.replace(hexString," ","");
		System.out.println("hexString " + hexString);
		// 第一种方式：
		int checkValue = CrcUtil.check(ByteUtil.HexStringToBinary(hexString));
		System.out.println("xxxx " + checkValue);
		// 第二种方式：
		byte[] aaa = CrcUtil.crc8Check(ByteUtil	.HexStringToBinary(hexString));
		System.out.println("xxxx " + ByteUtil.toHexString(aaa));
		
		
//		 for (int i = 0; i < 256; i++) {
//		 CrcChecker.makeCrc8Table(i);
//		 }

		int CRC = 0;

		// for (int i = 0; i < 100; i++) {
		// CRC = CrcChecker.CRC8(CrcChecker.crctable76520[i], CRC);
		// System.out.println("CRC " + i + " = " + CRC);
		// }

	}

}
