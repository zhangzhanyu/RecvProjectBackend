package com.godenwater.recv.server.summit;


import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * Created by Li on 2017/3/30.
 * 下发命令
 */
public class SummitCommand {


    /**
     * 查询历史记录：
     服务器向RTU发送此命令后，RTU则会将存储的历史数据逐条发送给服务器
     例：查询12年3月12日 – 12年3月13日历史记录
     68 0D 68 30 12 34 15 03 40 5C 12 03 12 13 03 12 3A 16

     行政区划和终端机站号要和RTU匹配，5c是固定功能码，12 03 12表示起始日期12年3月12日，13 03 12表示12年3月13日
     * @param addv 行政区划
     * @param rtucd 终端机站号
     * @param startDate
     * @param endDate
     * @return
     */
    public static byte[] queryHistory(String addv,int rtucd,String startDate,String endDate) {
        byte[] message = new byte[18];
        message[0] = 0x68;
        message[1] = 0x0D;
        message[2] = 0x68;
        message[3] = 0x30;

        //addv、rtucd，得转换为字节数组
        System.arraycopy(ByteUtil.str2Bcd(addv),0,message,4,2);

        byte[] rtucdbyte = ByteUtil.shortToBytes((short)rtucd);
        //System.arraycopy(ByteUtil.shortToBytes((short)rtucd),0,message,6,2);
        message[6] = rtucdbyte[1];//低字节在前
        message[7] = rtucdbyte[0];
        message[8] = 0x40;
        message[9] = 0x5C;

        //startDate、endDate，得转换为字节数组
        System.arraycopy(ByteUtil.str2Bcd(startDate),0,message,10,3);
        System.arraycopy(ByteUtil.str2Bcd(endDate),0,message,13,3);


        byte[] tmp = new byte[13];
        System.arraycopy(message,3,tmp,0,13);
        byte[] crc = CrcUtil.crc8Check(tmp);

        message[16] = crc[0];//crc
        message[17] = 0x16;

        return message;
    }

    public String config(String stcd,String cmdcode,String cmdparam){
//        String commandCode = (String)obj.get("commandcode");
//        String commandParm = (String)obj.get("commandparm");
//        String afterParam = "<cfg>";
//        String commandText = "<" + commandCode + ">" + commandParm + afterParam;
        return "";
    }

    public  static void main(String[] args){

        byte[] message = SummitCommand.queryHistory("1234",789,"120312","130312");
        System.out.println(cn.gov.mwr.szy206.utils.ByteUtil.toHexString(message));
    }

}
