package com.godenwater.recv.server.summit;


import com.godenwater.recv.spring.Configurer;

/**
 * summit_gprs_tcp_flag=1
 * summit_gprs_tcp_port=5280
 * summit_gprs_udp_flag=0
 * summit_gprs_udp_port=5290
 * summit_gsm_flag = 1
 * summit_gsm_serial = COM1
 * summit_gsm_baudrate =9600
 * summit_gsm_stopbits =1
 * summit_gsm_bytesize = 8
 * summit_gsm_parity = None
 */
public class SummitConfig {


    /**
     * GRPS端口
     *
     * @return
     */
    public static int getHydroGprsTcpPort() {
        return Configurer.getIntProperty("summit_gprs_tcp_port", 5280);
    }

    public static boolean isHydroGprsTcpEnabled() {

        String port = Configurer.getProperty("summit_gprs_tcp_flag");
        if (port == null || port.equals("0")) {
            return false;
        } else {
            return true;
        }
    }

    public static String getHydroGprsTcpIp() {
        return Configurer.getStringProperty("summit_gprs_tcp_ip", "0");
    }


    public static int getHydroGprsUdpPort() {
        return Configurer.getIntProperty("summit_gprs_udp_port", 5290);
    }

    public static boolean isHydroGprsUdpEnabled() {
        String port = Configurer.getProperty("summit_gprs_udp_flag");
        if (port == null || port.equals("0")) {
            return false;
        } else {
            return true;
        }
    }

    public static String getHydroGprsUdpIp() {
        return Configurer.getStringProperty("summit_gprs_udp_ip", "0");
    }

    /**
     * GSM设置
     *
     * @return
     */
    public static int getHydroGsmFlag() {
        return Configurer.getIntProperty("summit_gsm_flag", 0);
    }

    public static String getHydroGsmSerial() {
        return Configurer.getProperty("summit_gsm_serial");
    }

    public static int getHydroGsmBaudRate() {
        return Configurer.getIntProperty("summit_gsm_baudrate");
    }

    public static int getHydroGsmByteSize() {
        return Configurer.getIntProperty("summit_gsm_bytesize", 8);
    }

    public static String getHydroGsmParity() {
        return Configurer.getProperty("summit_gsm_parity");
    }

    public static String getHydroGsmStopbits() {
        return Configurer.getProperty("summit_gsm_stopbits");
    }

    public static boolean isHydroGsmEnabled() {
        int port = getHydroGsmFlag();
        if (port == 0) {
            return false;
        } else {
            return true;
        }
    }
}
