package com.godenwater.recv.server.summit;

/**
 * Created by Li on 2017/3/30.
 */
public class SummitMessage {

    private String stcd ;
    private byte type ;
    private byte funccode ;
    private String sttp ;
    private String viewdate ;

    private byte[] content ;

    private double tempature;
    private String voltage1;
    private String voltage2;
    private int err;


    private String river = "0";
    private String dayRain = "0";
    private String dayTotal = "0";
    private String qTotal = "0";
    private String qCurrent = "0";


    public String getStcd() {
        return stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public byte getFunccode() {
        return funccode;
    }

    public void setFunccode(byte funccode) {
        this.funccode = funccode;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public double getTempature() {
        return tempature;
    }

    public void setTempature(double tempature) {
        this.tempature = tempature;
    }

    public String getVoltage1() {
        return voltage1;
    }

    public void setVoltage1(String voltage1) {
        this.voltage1 = voltage1;
    }

    public String getVoltage2() {
        return voltage2;
    }

    public void setVoltage2(String voltage2) {
        this.voltage2 = voltage2;
    }

    public String getSttp() {
        return sttp;
    }

    public void setSttp(String sttp) {
        this.sttp = sttp;
    }

    public String getViewdate() {
        return viewdate;
    }

    public void setViewdate(String viewdate) {
        this.viewdate = viewdate;
    }

    public String getRiver() {
        return river;
    }

    public void setRiver(String river) {
        this.river = river;
    }

    public String getDayRain() {
        return dayRain;
    }

    public void setDayRain(String dayRain) {
        this.dayRain = dayRain;
    }

    public String getDayTotal() {
        return dayTotal;
    }

    public void setDayTotal(String dayTotal) {
        this.dayTotal = dayTotal;
    }

    public String getqTotal() {
        return qTotal;
    }

    public void setqTotal(String qTotal) {
        this.qTotal = qTotal;
    }

    public String getqCurrent() {
        return qCurrent;
    }

    public void setqCurrent(String qCurrent) {
        this.qCurrent = qCurrent;
    }
}
