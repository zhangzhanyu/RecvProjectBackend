package com.godenwater.recv.server.summit;

/**
 * Created by lhc on 2017-7-17.
 */
public class SummitMessage96 {
    private String stcd;//测站编码
    private byte type;//数据类型 此处都是96
    private byte funccode;//测站功能码
    private String viewdate;//测站遥测时间
    private byte[] viewdata;//雨量和水位数据

    public String getStcd() {
        return stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public byte getFunccode() {
        return funccode;
    }

    public void setFunccode(byte funccode) {
        this.funccode = funccode;
    }

    public String getViewdate() {
        return viewdate;
    }

    public void setViewdate(String viewdate) {
        this.viewdate = viewdate;
    }

    public byte[] getViewdata() {
        return viewdata;
    }

    public void setViewdata(byte[] viewdata) {
        this.viewdata = viewdata;
    }
}
