package com.godenwater.recv.server.summit;

import cn.gov.mwr.sl651.utils.ByteUtil;
import com.godenwater.recv.RecvConstant;
import com.godenwater.recv.handler.AbstractHandler;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.yf.YfDownCommand;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 西安山脉协议的处理器
 *
 * @author admin
 */
public class SummitMessageHandler extends AbstractHandler {

    private static Logger logger = LoggerFactory
            .getLogger(SummitMessageHandler.class);

    /**
     * Returns a singleton YfMessageHandler instance.
     *
     * @return a WeaterMessageHandler instance.
     */
    public static SummitMessageHandler getInstance() {
        return SummitMessageHandlerContainer.instance;
    }

    // Wrap this guy up so we can mock out the UserManager class.
    private static class SummitMessageHandlerContainer {
        private static SummitMessageHandler instance = new SummitMessageHandler();
    }

    private SummitMessageHandler() {
    }

    public void perform(String channel, IoSession session, CommonMessage message,int recvPort) {

        // 3、应答回复或重发报文请求
        byte[] replyMsg = null;

        // 确认应答
        logger.debug("收到【西安山脉】报文，发送\"应答\"回复....");
        //replyMsg = YfDownCommand.reply();//山脉报文无回复报文
/**
 if (replyMsg != null
 && (channel.equalsIgnoreCase("GPRS") || channel
 .equalsIgnoreCase("UDP"))) {
 logger.debug("回复报文，" + ByteUtil.toHexString(replyMsg) + "");
 logger.info("S> " + ByteUtil.toHexString(replyMsg) + "");
 session.write(replyMsg);
 }
 */
        byte[] bytes = message.getContent();

        if (bytes[3] != (byte) 0xB3) {//心跳包

            // 6、前台监测通知
            String logMsg = ByteUtil.toHexString(message.getContent());
            String stcd = "";
            String endChar = logMsg.substring(logMsg.length() - 2, logMsg.length());
            if (endChar.equals("16")) {
                byte[] btStcd = ByteUtil.HexStringToBinary(logMsg.substring(12, 16).toString());
                stcd = String.valueOf(ByteUtil.bytesToUshort(new byte[]{btStcd[1], btStcd[0]}));
                logger.info("西安山脉测站编号" + stcd);
                System.out.println(">>>添加session 到西安山脉测站编号..........." + stcd);
                //monitorMessage(channel, stcd, logMsg);
                SummitServer.getInstance().getSessionManager().bindSession(session, RecvConstant.SUMMIT, stcd);
            }
            // 7、 写入报文记录
            saveMessage(channel, RecvConstant.SUMMIT, stcd, true, logMsg, "0", "", "", 0, "", 0);
        } else {
            String stcd = "";//获取测站编号
            String logMsg = ByteUtil.toHexString(message.getContent());
            byte[] btStcd = ByteUtil.HexStringToBinary(logMsg.substring(12, 16).toString());
            stcd = String.valueOf(ByteUtil.bytesToUshort(new byte[]{btStcd[1], btStcd[0]}));
            logger.info("心跳包 西安山脉测站编号" + stcd);
            SummitServer.getInstance().getSessionManager().bindSession(session, RecvConstant.SUMMIT, stcd);
        }


    }

    /**
     * 前台监测通知
     */
    @Override
    public void monitorMessage(String channel, String stcd, String message) {
        try {
            SummitServer
                    .getInstance()
                    .getMonitorManager()
                    .append("RCV: channel " + channel + " stcd " + stcd + " hex "
                            + message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkCRC(CommonMessage message) {
        return false;
    }

    public byte[] replyMessage(IoSession session, CommonMessage message) {
        return YfDownCommand.reply();
    }

    /**
     * @return
     */
    public byte[] repeatMessage(CommonMessage message) {
        return null;
    }


    public String viewMessage(CommonMessage message) {
        return new String(message.getContent());
    }


}
