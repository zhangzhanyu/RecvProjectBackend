package com.godenwater.recv.server.summit;


import cn.gov.mwr.sl651.utils.ByteUtil;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;


public class SummitParser96 {
    public static SummitMessage96 parse96(byte[] buffer) {
        SummitMessage96 summitMessage96 = new SummitMessage96();
        return null;
    }

    public static SummitMessage96 parse(byte[] buffer) {
        SummitMessage96 message = new SummitMessage96();

        int pos = 1;//去掉第一个字节
        int len = buffer.length;

        byte[] blen = new byte[1];
        System.arraycopy(buffer, pos, blen, 0, blen.length);
        //message.setStcd(stcd);
        pos = pos + blen.length;
        pos = pos + 1;//多加后面一个字节

        byte[] dtype = new byte[1];//水位数据0xB5 雨量数据0xBB 流量数据0xB6
        System.arraycopy(buffer, pos, dtype, 0, dtype.length);
        message.setType(dtype[0]);
        pos = pos + dtype.length;

        byte[] addvcd = new byte[2];//行政区划	2BCD
        System.arraycopy(buffer, pos, addvcd, 0, addvcd.length);
        //message.setStcd(stcd);
        pos = pos + addvcd.length;

        byte[] rtucd = new byte[2];//终端机站号	2
        System.arraycopy(buffer, pos, rtucd, 0, rtucd.length);
        message.setStcd("" + ByteUtil.bytesToShort(new byte[]{rtucd[1], rtucd[0]}));
        pos = pos + rtucd.length;
        pos = pos + 1;//多加后面一个字节

        byte[] afn = new byte[1];//AFN定时上报0x80 阈值加报0x86 历史记录查询0x84
        System.arraycopy(buffer, pos, afn, 0, afn.length);
        message.setFunccode(afn[0]);
        pos = pos + afn.length;

        byte[] sensor = new byte[2];//特殊表示 BBAA
        System.arraycopy(buffer, pos, sensor, 0, sensor.length);
        //message.setStcd(stcd);
        pos = pos + sensor.length;

        byte[] viewdate = new byte[3];// 时间
        System.arraycopy(buffer, pos, viewdate, 0, viewdate.length);
        message.setViewdate(parseDate(new byte[]{viewdate[0], viewdate[1], viewdate[2]}));
        pos = pos + viewdate.length;

        byte[] viewData = new byte[864];
        System.arraycopy(buffer, pos, viewData, 0, viewData.length);
        message.setViewdata(viewData);
        pos = pos + viewData.length;

        byte[] crc = new byte[1];//CRC
        System.arraycopy(buffer, pos, crc, 0, crc.length);
        //message.setStcd(stcd);
        pos = pos + crc.length;

        byte[] eof = new byte[1];//eof
        System.arraycopy(buffer, pos, eof, 0, eof.length);
        //message.setStcd(stcd);
        pos = pos + eof.length;

        return message;
    }

    /**
     * 根据报文的类型和报文上报模式决定数据的长度
     * 根据定时报、加报报的类型来决定后面字节的长度，
     * 水位定时报，后面有28个字节（4个基值，24个水位）
     * 雨量定时报，后面有24个字节
     * 水位加报报，后面有3个字节
     * 雨量加报报，后面有7个字节
     *
     * @param type
     * @param mode
     * @return
     */
    public static int parseDataLen(byte type, byte mode) {
        //定时上报0x80 阈值加报0x86 历史记录查询0x84
        int nbodyLen = 0;
        //定时报
        if (mode == (byte) 0x80) {
            switch (type) {
                case (byte) 0xB5://水位长度为3
                case (byte) 0xBD://水位长度为3
                    nbodyLen = 28;
                    break;
                case (byte) 0xBB://雨量长度为7
                case (byte) 0xBE://雨量长度为7
                    nbodyLen = 24;
                    break;
                case (byte) 0xB6://这个不清楚
                    nbodyLen = 24;
                    break;
            }
        }

        //加报报
        if (mode == (byte) 0x86) {
            switch (type) {
                case (byte) 0xB5://水位长度为3
                case (byte) 0xBD://水位长度为3
                    nbodyLen = 3;
                    break;
                case (byte) 0xBB://雨量长度为7
                case (byte) 0xBE://雨量长度为7
                    nbodyLen = 7;
                    break;
                case (byte) 0xB6://流量长度为6
                    nbodyLen = 6;
                    break;
            }
        }

        return nbodyLen;
    }

    /**
     * 解析一小时雨量值
     *
     * @param rain
     * @return
     */
    public static int[] parseRain(byte[] rain) {
        int[] rv = new int[12];
        int pos = 0;
        for (int i = 0; i < 12; i++) {
            byte[] bv = new byte[2];
            System.arraycopy(rain, pos, bv, 0, 2);
            int value = ByteUtil.bytesToUshort(new byte[]{bv[1], bv[0]});
            rv[i] = value;
            pos = pos + 2;
        }
        return rv;
    }

    /**
     * 解析一小时水位
     *
     * @param river
     * @return
     */
    public static int[] parseRiver(byte[] river) {
        int[] rv = new int[12];
        int pos = 4;
        for (int i = 0; i < 12; i++) {
            byte[] bv = new byte[2];
            System.arraycopy(river, pos, bv, 0, 2);
            int value = ByteUtil.bytesToUshort(new byte[]{bv[1], bv[0]});
            rv[i] = value;

            pos = pos + 2;
        }
        return rv;
    }

    public static float[] parseBcdRiver(byte[] river) {
        float[] rv = new float[12];
        int pos = 4;
        for (int i = 0; i < 12; i++) {
            byte[] bv = new byte[2];
            System.arraycopy(river, pos, bv, 0, 2);

            String value = ByteUtil.bcd2Str(new byte[]{bv[1], bv[0]});
            rv[i] = Float.valueOf(value) / 100 - 1000;
            ;
            pos = pos + 2;
        }
        return rv;
    }

    public static String parseDate(byte[] data) {
        String value = ByteUtil.bcd2Str(data);
        return value;
    }

    public static double parseTemperature(byte[] data) {
        String value = ByteUtil.bcd2Str(data);
        BigDecimal b1 = new BigDecimal(value);
        BigDecimal b2 = new BigDecimal(1000);
        b1 = b1.divide(new BigDecimal(100));
        return b1.subtract(b2).doubleValue();
    }

    //BCD码，为测量到的水位+1000米，（为了应对地下负水位状况），45 23 11为1123.45，表示水位+123.45米
    public static float parseRiverValue(byte[] data) {
        String value = ByteUtil.bcd2Str(data);
        return Float.valueOf(value) / 100 - 1000;
    }

    //转换基值
    public static float parseRiverBaseValue(byte[] data) {
        byte[] rv = new byte[4];
        System.arraycopy(data, 0, rv, 0, 4);
        int value = ByteUtil.bytesToInt(new byte[]{rv[3], rv[2], rv[1], rv[0]});
        return (Float.valueOf(value) / 100);//- 1000
    }

    public static void main(String[] args) {
        String msg = "68 20 68 BB 12 34 15 03 40 80 01 15 02 00 00 15 00 00 00 00 00 07 15 12 03 12 43 24 10 36 12 00 00 70 00 6B 16";
        msg = StringUtils.replace(msg, " ", "");

        msg = "680d68b342136400408e13872854174fb916";

        SummitParser96 parser = new SummitParser96();

        byte[] aa = new byte[]{0x12, 0x34};
        int value = ByteUtil.bytesToUshort(aa);
        System.out.println(value);

        parser.parse(ByteUtil.HexStringToBinary(msg));

    }

}
