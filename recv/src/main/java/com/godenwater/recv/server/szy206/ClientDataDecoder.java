package com.godenwater.recv.server.szy206;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import cn.gov.mwr.szy206.IMessageBody;
import cn.gov.mwr.szy206.IMessageHeader;
import cn.gov.mwr.szy206.SzyMessage;
import cn.gov.mwr.szy206.SzyMessageBody;
import cn.gov.mwr.szy206.SzyMessageHeader;
import cn.gov.mwr.szy206.SzyParser;

public class ClientDataDecoder implements ProtocolDecoder {
	private static final AttributeKey BUF_BYTE = new AttributeKey(
			ClientDataDecoder.class, "BUF_KEY");

	public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
			throws Exception {
		// TODO Auto-generated method stub
		try {

			System.out.println(">>> YfClientDataDecoder decode ");

			IoBuffer buffer = null;
			byte[] buf = (byte[]) session.getAttribute(BUF_BYTE);
			if (buf == null) {
				// System.out.println("没有尚未处理的数据");
				buffer = in;
			} else {
				// System.out.println("合并尚未处理的数据");
				buffer = IoBuffer.allocate(buf.length + in.remaining());
				buffer.setAutoExpand(true);
				buffer.put(buf);
				buffer.put(in);
				buffer.flip();
			}

			// while (buffer.remaining() >= 0) {// 循环处理数据包
			// //
			// int dataLen = buffer.getInt(buffer.position());
			// byte[] b = new byte[dataLen];
			// buffer.get(b);
			// SMPPPacket pak = null;
			// int id = -1;
			// id = SMPPIO.bytesToInt(b, 4, 4);
			// pak = PacketFactory.newInstance(id);
			// if (pak != null) {
			// pak.readFrom(b, 0);
			// out.write(pak);
			// }
			byte[] mode = new byte[1];
			buffer.get(mode);

			IMessageHeader header;
			header = new SzyMessageHeader();

			header.setStartBit(mode);

			byte[] datalen = new byte[1];
			buffer.get(datalen);
			header.setBodySize(datalen);

			byte[] startBodyBit = new byte[1];
			buffer.get(startBodyBit);
			header.setBodyStartBit(startBodyBit);

			int rcrlen = 1;
			int bodyLengthLen = SzyParser.parseDataLen(datalen);

			byte[] bodyContent = new byte[bodyLengthLen];
			buffer.get(bodyContent);

			// 校验码
			byte[] crc = new byte[1];
			buffer.get(crc);
			// 终止符
			byte[] eof = new byte[1];
			buffer.get(eof);

			// 组装消息
			SzyMessage message = new SzyMessage();
			message.setHeader(header);

			// 根据功能码，构建body体
			IMessageBody body = new SzyMessageBody();
			body.setContents(bodyContent);
			message.setBody(body);
			message.setCRC(crc);
			message.setEOF(eof[0]);

			out.write(message);

			// }
			if (buffer.hasRemaining()) {// 如果有剩余的数据，则放入Session中
				byte[] tmpb = new byte[buffer.remaining()];
				buffer.get(tmpb);
				session.setAttribute(BUF_BYTE, tmpb);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
 
	public void dispose(IoSession session) throws Exception {
		// TODO Auto-generated method stub

	}

	public void finishDecode(IoSession session, ProtocolDecoderOutput out)
			throws Exception {
		// TODO Auto-generated method stub

	}

	public void parseMessage() {

	}

}
