package com.godenwater.recv.server.szy206;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class SzyCodecFactory implements ProtocolCodecFactory {
	private ProtocolEncoder encoder;
    private ProtocolDecoder decoder;

    public SzyCodecFactory(boolean server) {
        if (server) {
            encoder = new ServerDataEncoder();
            decoder = new ServerDataDecoder();
        } else {
            encoder = new ClientDataEncoder();
            decoder = new ClientDataDecoder();
        }
    }

    public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
        return encoder;
    }

    public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
        return decoder;
    }
}
