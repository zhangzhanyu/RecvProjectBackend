package com.godenwater.recv.server.yf;

/**
 * 湖北一方的报文结构
 * 报文内容包括：遥测站所带传感器当前数据（水位、雨量）、电池电压、包序号，数据格式定义如下：（可最多累计6小时数据，每小时中间以空格间隔，最后回车结束）
 * $30151G22010201120326001297065535323906553532390655353239065535323906553532390655353239065535323906553532390655353239065535323906553532390655353239
 * 说明	字节数	ASCII	备注
 * 起始符	1byte	$
 * 站号	4byte		3015
 * 类别	2byte	1G	1G
 * 报类	2byte	22 	定时报
 * 站类	2byte	01 02 03 12 13 	雨量 并行水位 并行水文 串行水位 串行水文
 * 包序号	4byte		01-05循环（天） 01-24循环（小时）
 * 数据时标	8byte		yymmddhh
 * 电压	4byte		12.97V
 * 水位	6byte		总共有12组水位雨量数据
 * 雨量	4byte
 * 水位	6byte
 * 雨量	4byte
 * 水位	6byte
 * 雨量	4byte
 * 水位	6byte
 * 雨量	4byte
 * 结束符	1byte	#13
 */
public class YFMessageBodyAsc {

    private String start;
    private String stcd ;
    private String type ;
    private String funccode ;
    private String sttp ;
    private String bnum ;
    private String viewdate ;

    private String voltage  ;
    private String[] river = new String[12];
    private String[] rain = new String[12];

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStcd() {
        return stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFunccode() {
        return funccode;
    }

    public void setFunccode(String funccode) {
        this.funccode = funccode;
    }

    public String getSttp() {
        return sttp;
    }

    public void setSttp(String sttp) {
        this.sttp = sttp;
    }

    public String getBnum() {
        return bnum;
    }

    public void setBnum(String bnum) {
        this.bnum = bnum;
    }

    public String getViewdate() {
        return viewdate;
    }

    public void setViewdate(String viewdate) {
        this.viewdate = viewdate;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String[] getRiver() {
        return river;
    }

    public void setRiver(String[] river) {
        this.river = river;
    }

    public String[] getRain() {
        return rain;
    }

    public void setRain(String[] rain) {
        this.rain = rain;
    }
}
