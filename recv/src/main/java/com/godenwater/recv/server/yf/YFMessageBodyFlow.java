package com.godenwater.recv.server.yf;
/**
 * 湖北一方的流速报文结构
 * 报文内容包括：遥测站所带传感器当前数据（流速数据）、电池电压，数据格式定义如下：（每分钟中间以空格间隔，最后回车结束）
 * 说明	        字节数	ASCII	备注
 * 起始符	1byte	$
 * 站号	    4byte	6004
 * 命令符	4byte	1G22         定时自报
 * 数据时标	12byte	yymmddhhmmss
 * 电压	    4byte	12.97V
 * 信息长度	4byte	0085
 * 流速数据	83byte  0.000    取02后数据其他字节无效
 * 结束符	1byte	#13
 */
public class YFMessageBodyFlow {

    private String start;
    private String stcd ;
    private String type ;
    private String funccode ;
    private String viewdate ;

    private String voltage  ;
    private String flow;

	public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStcd() {
        return stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFunccode() {
        return funccode;
    }

    public void setFunccode(String funccode) {
        this.funccode = funccode;
    }


    public String getViewdate() {
        return viewdate;
    }

    public void setViewdate(String viewdate) {
        this.viewdate = viewdate;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getFlow() {
		return flow;
	}

	public void setFlow(String flow) {
		this.flow = flow;
	}
}
