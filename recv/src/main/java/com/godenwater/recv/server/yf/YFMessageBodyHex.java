package com.godenwater.recv.server.yf;

/**
 * 湖北一方的报文结构
 * 报文内容包括：遥测站所带传感器当前数据（水位、雨量）、电池电压、包序号，数据格式定义如下：（可最多累计6小时数据，每小时中间以空格间隔，最后回车结束）
 * $30151G22010201120326001297065535323906553532390655353239065535323906553532390655353239065535323906553532390655353239065535323906553532390655353239
 * 说明	字节数	ASCII	备注
 * 起始符	1byte	$
 * 站号	4byte		3015
 * 类别	2byte	1G	1G
 * 报类	2byte	22 	定时报
 * 站类	2byte	01 02 03 12 13 	雨量 并行水位 并行水文 串行水位 串行水文
 * 包序号	4byte		01-05循环（天） 01-24循环（小时）
 * 数据时标	8byte		yymmddhh
 * 电压	4byte		12.97V
 * 水位	6byte		总共有12组水位雨量数据
 * 雨量	4byte
 * 水位	6byte
 * 雨量	4byte
 * 水位	6byte
 * 雨量	4byte
 * 水位	6byte
 * 雨量	4byte
 * 结束符	1byte	#13
 */
public class YFMessageBodyHex {

    private byte start;
    private byte[] stcd = new byte[4];
    private byte[] type = new byte[2];
    private byte[] funccode =  new byte[2];
    private byte[] sttp =  new byte[2];
    private byte[] bnum =  new byte[4];
    private byte[] viewdate = new byte[8];

    private byte[] voltage = new byte[4];
    private byte[] river = new byte[6];
    private byte[] rain = new byte[4];

    private byte end;

    public byte getStart() {
        return start;
    }

    public void setStart(byte start) {
        this.start = start;
    }

    public byte[] getStcd() {
        return stcd;
    }

    public void setStcd(byte[] stcd) {
        this.stcd = stcd;
    }

    public byte[] getType() {
        return type;
    }

    public void setType(byte[] type) {
        this.type = type;
    }

    public byte[] getFunccode() {
        return funccode;
    }

    public void setFunccode(byte[] funccode) {
        this.funccode = funccode;
    }

    public byte[] getSttp() {
        return sttp;
    }

    public void setSttp(byte[] sttp) {
        this.sttp = sttp;
    }

    public byte[] getBnum() {
        return bnum;
    }

    public void setBnum(byte[] bnum) {
        this.bnum = bnum;
    }

    public byte[] getViewdate() {
        return viewdate;
    }

    public void setViewdate(byte[] viewdate) {
        this.viewdate = viewdate;
    }

    public byte[] getVoltage() {
        return voltage;
    }

    public void setVoltage(byte[] voltage) {
        this.voltage = voltage;
    }

    public byte[] getRiver() {
        return river;
    }

    public void setRiver(byte[] river) {
        this.river = river;
    }

    public byte[] getRain() {
        return rain;
    }

    public void setRain(byte[] rain) {
        this.rain = rain;
    }

    public byte getEnd() {
        return end;
    }

    public void setEnd(byte end) {
        this.end = end;
    }
}
