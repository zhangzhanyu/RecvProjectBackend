package com.godenwater.recv.server.yf;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class YfClientCodecFactory implements ProtocolCodecFactory {
	private ProtocolEncoder encoder;
    private ProtocolDecoder decoder;

    public YfClientCodecFactory( ) {
            encoder = new YfClientDataEncoder();
            decoder = new YfClientDataDecoder();
    }

    public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
        return encoder;
    }

    public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
        return decoder;
    }
}
