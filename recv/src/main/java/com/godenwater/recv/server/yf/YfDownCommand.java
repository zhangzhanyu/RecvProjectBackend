package com.godenwater.recv.server.yf;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 湖北一方的传输规约
 * 传输规约：数据包采用ASCII编码
 * Created by Li on 2017/1/18.
 */
public class YfDownCommand {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
    //湖北一方模块协议头和尾
    public static byte YLN_START = 0X24;
    public static byte HD_END = 0X7B;

    /**
     * 中心站发送报文内容：
     * $30150S 03160721140000#13
     *
     * @return
     */
    public static byte[] down(String stcd) {
        //#(时间)OK
        String msg = "$" + stcd + "OS 03" + sdf.format(new Date()) + "\r";
        return msg.getBytes();
    }

    /**
     * 回复报文
     * @return
     */
    public static byte[] reply( ) {
        String msg = "TRU";
        return msg.getBytes();
    }


    public static void main(String[] args) {
        System.out.println(">> " + new String(YfDownCommand.down("1111")));
    }

}
