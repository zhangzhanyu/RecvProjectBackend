package com.godenwater.recv.server.yf;

import com.godenwater.recv.RecvConstant;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.hd.HdDownCommand;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.gov.mwr.sl651.utils.ByteUtil;

import com.godenwater.recv.handler.AbstractHandler;

/**
 * 湖北一方协议的处理器
 *
 * @author admin
 */
public class YfMessageHandler extends AbstractHandler {

    private static Logger logger = LoggerFactory
            .getLogger(YfMessageHandler.class);

    /**
     * Returns a singleton YfMessageHandler instance.
     *
     * @return a Sl651MessageHandler instance.
     */
    public static YfMessageHandler getInstance() {
        return YfMessageHandlerContainer.instance;
    }

    // Wrap this guy up so we can mock out the UserManager class.
    private static class YfMessageHandlerContainer {
        private static YfMessageHandler instance = new YfMessageHandler();
    }

    private YfMessageHandler() {
    }

    public void perform(String channel, IoSession session, CommonMessage message, int recvPort) {

        // 3、应答回复或重发报文请求
        byte[] replyMsg = null;

        // 确认应答
        logger.debug("收到一方报文，发送\"应答\"回复....");
//        replyMsg = YfDownCommand.reply();
        String logMsg = new String(message.getContent());
        logger.info("收到一方报文：" + logMsg);
        String tempMsg = logMsg;
        tempMsg = tempMsg.replaceAll("(?<!\\d)\\D", "");
        byte[] dtuCode = tempMsg.getBytes();
        if (tempMsg.indexOf("{") > 0) {
            dtuCode = tempMsg.substring(0, tempMsg.indexOf("{")).getBytes();
//        	replyMsg = ByteUtil.toHexString(HdDownCommand.cmd81DTU(dtuCode))+"5452550d";
            String strreplyMsg = ByteUtil.toHexString(HdDownCommand.cmd89DTU(dtuCode)) + "5452550d";
            replyMsg = ByteUtil.HexStringToBinary(strreplyMsg);
        } else {
            replyMsg = HdDownCommand.cmd81DTU(dtuCode);
            System.out.println(new String(ByteUtil.toHexString(replyMsg)));
        }
        if (replyMsg != null
                && (channel.equalsIgnoreCase("GPRS") || channel
                .equalsIgnoreCase("UDP"))) {
            logger.debug("回复报文，" + ByteUtil.toHexString(replyMsg) + "");
            logger.info("S> " + ByteUtil.toHexString(replyMsg) + "");
            session.write(replyMsg);
        }

        // 6、前台监测通知


        if (logMsg.length() > 20) {
            String stcd = logMsg.substring(17, 21);
            String msg = logMsg.substring(16);
            monitorMessage(channel, stcd, msg);
            logger.info("YF入redis，" + msg);
            // 7、 写入报文记录
            saveMessage(channel, RecvConstant.YF, stcd, true, msg, "0", "", "", 0, "", 0);
        }
    }

    public boolean checkCRC(CommonMessage message) {
        return false;
    }

    public byte[] replyMessage(IoSession session, CommonMessage message) {
        return YfDownCommand.reply();
    }

    /**
     * @return
     */
    public byte[] repeatMessage(CommonMessage message) {
        return null;
    }


    public String viewMessage(CommonMessage message) {
        return new String(message.getContent());
    }


}
