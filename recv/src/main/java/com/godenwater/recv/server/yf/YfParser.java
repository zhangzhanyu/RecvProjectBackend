package com.godenwater.recv.server.yf;

/**
 * Created by Li on 2017/3/11.
 * 报文内容包括：遥测站所带传感器当前数据（水位、雨量）、电池电压、包序号，数据格式定义如下：（可最多累计6小时数据，每小时中间以空格间隔，最后回车结束）
 * $30151G22010201120326001297065535323906553532390655353239065535323906553532390655353239065535323906553532390655353239065535323906553532390655353239
 */
public class YfParser {

    public static YFMessageBodyHex parseHex(byte[] buffer) {

        YFMessageBodyHex message = new YFMessageBodyHex();

        int pos = 1;//去掉第一个字节
        int len = buffer.length;

        //message.setStart();

        byte[] stcd = new byte[4];
        System.arraycopy(buffer, pos, stcd, 0, stcd.length);
        message.setStcd(stcd);
        pos = pos + stcd.length;

        byte[] type = new byte[2];
        System.arraycopy(buffer, pos, type, 0, type.length);
        message.setType(type);
        pos = pos + type.length;

        byte[] funccode = new byte[2];
        System.arraycopy(buffer, pos, funccode, 0, funccode.length);
        message.setFunccode(funccode);
        pos = pos + funccode.length;

        byte[] sttp = new byte[2];
        System.arraycopy(buffer, pos, sttp, 0, sttp.length);
        message.setSttp(sttp);
        pos = pos + sttp.length;

        byte[] bnum = new byte[4];
        System.arraycopy(buffer, pos, bnum, 0, bnum.length);
        message.setBnum(bnum);
        pos = pos + bnum.length;

        byte[] viewdate = new byte[8];
        System.arraycopy(buffer, pos, viewdate, 0, viewdate.length);
        message.setViewdate(viewdate);
        pos = pos + viewdate.length;

        byte[] voltage = new byte[4];
        System.arraycopy(buffer, pos, voltage, 0, voltage.length);
        message.setVoltage(voltage);
        pos = pos + voltage.length;

        System.out.println(">> " + new String(stcd) + " " + new String(type) + " " + new String(funccode) + " " + new String(sttp) + " " + new String(bnum) + " " + new String(viewdate) + " " + new String(voltage) + " ");

        for (int i = 0; i < 12; i++) {
            byte[] river = new byte[6];

            System.arraycopy(buffer, pos, river, 0, river.length);
            //message.setVoltage(river);
            pos = pos + river.length;

            byte[] rain = new byte[4];
            System.arraycopy(buffer, pos, rain, 0, rain.length);
            //message.setVoltage(river);
            pos = pos + rain.length;
            System.out.println(">> river " + new String(river) + " rain " + new String(rain));
        }

//        byte[] river = new byte[6];
//        byte[] rain = new byte[4];
//        message.setRiver();
//        message.setRain();

        //System.arraycopy();
        return message;
    }

    public static YFMessageBodyAsc parseAsc(String msg) {
        YFMessageBodyAsc message = new YFMessageBodyAsc();
        int pos = 0;
        String start = msg.substring(pos, pos + 1);
        message.setStart(start);
        pos = pos + 1;

        String stcd = msg.substring(pos, pos + 4);
        message.setStcd(stcd);
        pos = pos + 4;

        String type = msg.substring(pos, pos + 2);
        message.setType(type);
        pos = pos + 2;

        String funccode = msg.substring(pos, pos + 2);//22定时报,21加报
        message.setFunccode(funccode);
        pos = pos + 2;

        String sttp = msg.substring(pos, pos + 2);//01 02 12 03 13	雨量 并行水位 并行水文 串行水位 串行水文
        message.setSttp(sttp);
        pos = pos + 2;

        //如果为定时报
        String bnum = "";
        String viewdate = "";
        if (funccode.equals("22")) {
            bnum = msg.substring(pos, pos + 4);
            message.setBnum(bnum);
            pos = pos + 4;

            viewdate = msg.substring(pos, pos + 8);
            message.setViewdate(viewdate);
            pos = pos + 8;

            String voltage = msg.substring(pos, pos + 4);
            pos = pos + 4;
            message.setVoltage(voltage);
            System.out.println(">> " + stcd + " " + type + " " + funccode + " " + sttp + " " + bnum + " " + viewdate + " " + voltage + " ");

            String[] riverArr = new String[12];
            String[] rainArr = new String[12];
            for (int i = 0; i < 12; i++) {
                String river = msg.substring(pos, pos + 6);
                pos = pos + 6;
                riverArr[i] = river;

                String rain = msg.substring(pos, pos + 4);
                pos = pos + 4;
                rainArr[i] = rain;
                System.out.println(">> river " + river + " rain" + rain);
            }
            message.setRiver(riverArr);
            message.setRain(rainArr);

        }

        //如果为加报报
        if (funccode.equals("21")) {
            viewdate = msg.substring(pos, pos + 10);
            message.setViewdate(viewdate);
            pos = pos + 10;

            String[] riverArr = new String[1];
            String[] rainArr = new String[1];
            String river = msg.substring(pos, pos + 6);
            pos = pos + 6;
            riverArr[0] = river;

            String rain = msg.substring(pos, pos + 4);
            pos = pos + 4;
            rainArr[0] = rain;
            System.out.println(">> river " + river + " rain" + rain);
            message.setRiver(riverArr);
            message.setRain(rainArr);

            String voltage = msg.substring(pos, pos + 4);
            pos = pos + 4;
            message.setVoltage(voltage);
            System.out.println(">> " + stcd + " " + type + " " + funccode + " " + sttp + " " + bnum + " " + viewdate + " " + voltage + " ");
        }

        return message;
    }

    public static void main(String[] args) {
        //  $20041G2101110820072500774825021327
        //3015 1G 22 01 0201 12032600 1297
        String msg = "$40221G22120101170315001226003553000000355300000035530000003553000000355300000035530000003553000000355300000035530000003553000000355300000035530000";
        msg = "$40261G2103170315015500213227041262";
        YfParser.parseAsc(msg);
        System.out.println("=======================================================");
        YfParser.parseHex(msg.getBytes());
        System.out.println(">> " + new String(YfDownCommand.down("3015")));


//        加报
//        根据站类写相应加报值。
//        $20041G2101110820072500774825021327

    }
}
