package com.godenwater.recv.server.yf;

public class YfParserFlow {

	public static YFMessageBodyFlow parseAsc(String msg) {
		YFMessageBodyFlow message = new YFMessageBodyFlow();
		int pos = 0;
		String start = msg.substring(pos, pos + 1);
		message.setStart(start);
		pos = pos + 1;

		String stcd = msg.substring(pos, pos + 4);
		message.setStcd(stcd);
		pos = pos + 4;

		String type = msg.substring(pos, pos + 2);
		message.setType(type);
		pos = pos + 2;

		String funccode = msg.substring(pos, pos + 2);// 22定时报,21加报
		message.setFunccode(funccode);
		pos = pos + 2;

		String viewdate = msg.substring(pos, pos + 12);
		message.setViewdate(viewdate);
		pos = pos + 12;

		String voltage = msg.substring(pos, pos + 4);
		pos = pos + 4;
		message.setVoltage(voltage);	
		
		//取|03下标,往前5个字符为流速数据
		int flowFound=msg.indexOf("|03");
		String flow = msg.substring(flowFound-5, flowFound);
		message.setFlow(flow);
		System.out.println(">> " + stcd + " " + type + " " + funccode + " " + viewdate + " " + voltage + " "+flow);
		
		return message;
	}

	public static void main(String[] args) {
//		{	13477974947{$60041G2218072710000012560085 #M0001G00se00999 999.8|01 9999998 |02   0.528|03   752.29|0499999.98|0599999.98|4EEC;

		// $20041G2101110820072500774825021327
		// 3015 1G 22 01 0201 12032600 1297
		String msg = "$60041G2218073016250012590085#M0001G00se00999999.8|01 9999998|02   0.756|03  627.09|0499999.98|0599999.98|BFA0";
//		msg = "$40261G2103170315015500213227041262";
		YfParserFlow.parseAsc(msg);
		System.out.println("=======================================================");

		// 加报
		// 根据站类写相应加报值。
		// $20041G2101110820072500774825021327

	}

}
