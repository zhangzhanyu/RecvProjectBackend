package com.godenwater.recv.server.yf;

import com.godenwater.recv.spring.Configurer;

/**
 yf_gprs_tcp_flag=1
 yf_gprs_tcp_port=5280
 yf_gprs_udp_flag=0
 yf_gprs_udp_port=5290
 yf_gsm_flag = 1
 yf_gsm_serial = COM1
 yf_gsm_baudrate =9600
 yf_gsm_stopbits =1
 yf_gsm_bytesize = 8
 yf_gsm_parity = None
 */
public class YfRtuConfig {

	/**
	 * GRPS端口
	 * 
	 * @return
	 */
	public static int getHydroGprsTcpPort() {
		return Configurer.getIntProperty("yf_gprs_tcp_port", 5280);
	}
	public static boolean isHydroGprsTcpEnabled() {
		String port = Configurer.getProperty("yf_gprs_tcp_flag");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	public static int getHydroGprsUdpPort() {
		return Configurer.getIntProperty("yf_gprs_udp_port", 5290);
	}

	public static boolean isHydroGprsUdpEnabled() {
		String port = Configurer.getProperty("yf_gprs_udp_flag");
		if (port == null || port.equals("0")) {
			return false;
		} else {
			return true;
		}
	}


	/**
	 * GSM设置
	 * 
	 * @return
	 */
	public static int getHydroGsmFlag() {
		return Configurer.getIntProperty("yf_gsm_flag",0);
	}
	
	public static String getHydroGsmSerial() {
		return Configurer.getProperty("yf_gsm_serial");
	}

	public static int getHydroGsmBaudRate() {
		return Configurer.getIntProperty("yf_gsm_baudrate");
	}

	public static int getHydroGsmByteSize() {
		return Configurer.getIntProperty("yf_gsm_bytesize",8);
	}
	
	public static String getHydroGsmParity() {
		return Configurer.getProperty("yf_gsm_parity");
	}
	
	public static String getHydroGsmStopbits() {
		return Configurer.getProperty("yf_gsm_stopbits");
	}

	public static boolean isHydroGsmEnabled() {
		int port = getHydroGsmFlag();
		if (port == 0) {
			return false;
		} else {
			return true;
		}
	}


}
