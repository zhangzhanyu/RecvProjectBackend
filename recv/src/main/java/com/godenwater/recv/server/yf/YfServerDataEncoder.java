package com.godenwater.recv.server.yf;

import cn.gov.mwr.sl651.utils.ByteUtil;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据编码器
 * 
 * @ClassName: HexDataEncoder
 * @Description: TODO
 * @author lipujun
 * @date Feb 22, 2013
 * 
 */
public class YfServerDataEncoder extends ProtocolEncoderAdapter {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public void encode(IoSession session, Object message,
			ProtocolEncoderOutput out) throws Exception {

		if (message instanceof byte[]) {

			byte[] msg = (byte[]) message;

			logger.debug("下发【一方】报文，" + ByteUtil.toHexString(msg));

			int capacity = msg.length;
			IoBuffer buffer = IoBuffer.allocate(capacity, false); // 构造一个缓冲区
			buffer.setAutoExpand(true);
			buffer.put(msg);
			buffer.flip();
			out.write(buffer);
		} else {
			logger.debug("[下发报文] 报文类型不正确，不做下发处理！" + message.toString());
		}
	}

}
