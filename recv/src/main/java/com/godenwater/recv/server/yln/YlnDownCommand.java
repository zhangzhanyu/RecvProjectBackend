package com.godenwater.recv.server.yln;


import cn.gov.mwr.sl651.utils.ByteUtil;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Li on 2017/1/18.
 */
public class YlnDownCommand {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //亿立能模块协议头和尾
    public static byte YLN_START = 0X24;
    public static byte HD_END = 0X7B;

    public static byte[] reply(){
        //#(时间)OK
                String msg =   "#("+ sdf.format(new Date())+")OK";
                return msg.getBytes();
    }

}
