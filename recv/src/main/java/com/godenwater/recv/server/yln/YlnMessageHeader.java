package com.godenwater.recv.server.yln;

/**
 * Created by Li on 2017/3/2.
 */
public class YlnMessageHeader {

    //起始标志
    private int LEN_START_BIT = 3;

    private byte[] START_BIT  ;

    public byte[] getSTART_BIT() {
        return START_BIT;
    }

    public void setSTART_BIT(byte[] START_BIT) {
        this.START_BIT = START_BIT;
    }

    public int getLenStartBit() {
        return LEN_START_BIT;
    }
}
