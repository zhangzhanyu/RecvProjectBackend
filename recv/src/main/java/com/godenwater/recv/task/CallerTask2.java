package com.godenwater.recv.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.data.redis.cache.RedisCacheManager;

import com.godenwater.core.spring.Application;

/**
 * 测站召测任务。召测信息，仅通过读取缓存获取
 * 
 * @ClassName: CallerTask
 * @Description: TODO
 * @author lipujun
 * @date Apr 23, 2013
 * 
 */
public class CallerTask2 implements Runnable {

	private Logger logger = LoggerFactory.getLogger(CallerTask2.class);

	private static String CACHE_NAME = "Caller";

	private RedisCacheManager redisCacheManager;

	public void run() {
		redisCacheManager = (RedisCacheManager) Application.getInstance()
				.getBean("redisCacheManager");

		while (true) {
			try {
				process();
			} catch (Exception e) {
				logger.error("数据转入业务库线程出现异常：" + e.getMessage());
				e.printStackTrace();
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 处理业务逻辑
	 * 
	 * @param message
	 */
	private void process() throws Exception {

		if (redisCacheManager != null) {

			Cache cache = redisCacheManager.getCache(CACHE_NAME);
			 
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String message = "7E7E0001718700FF99994A800802000113112114241705";
		SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmmss");
		String nowtime = format.format(new Date());

	}

}
