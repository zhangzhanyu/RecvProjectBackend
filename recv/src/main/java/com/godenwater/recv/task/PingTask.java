package com.godenwater.recv.task;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.godenwater.core.spring.Application;
import com.godenwater.core.spring.BaseDao;

/**
 * 更新测站状态的任务
 * 
 * @ClassName: StationJob
 * @Description: TODO
 * @author lipujun
 * @date Apr 23, 2013
 * 
 */
public class PingTask extends TimerTask {

	private Logger logger = LoggerFactory.getLogger(PingTask.class);

	public void run() {

		BaseDao baseDao = (BaseDao) Application.getInstance().getCtx()
				.getBean("baseDao");
		String selectSql = "select * from rtu_server  ";
		String updateSql = "update rtu_server set flag = 1,sendtime = NOW()  where id = ? ";

		try {
			List<Server> serverList = baseDao.getJdbcTemplate().query(
					selectSql, new RowMapper<Server>() {

						@Override
						public Server mapRow(ResultSet rs, int arg1)
								throws SQLException {
							// TODO Auto-generated method stub
							Server server = new Server();
							server.setId(rs.getString("id"));
							server.setName(rs.getString("name"));
							server.setAddr(rs.getString("addr"));
							server.setStatus(rs.getString("status"));
							server.setUtm(rs.getDate("utm"));
							server.setRtm(rs.getDate("rtm"));
							return server;
						}
					});

			for (Server server : serverList) {

				// 查看是否有连通的客户端
				String id = server.getId();
				String addr = server.getAddr();

				// boolean flag = PingUtil.ping(addr);
				// if(flag){
				//
				// }else{
				//
				// }

			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	public class Server {
		private String id;
		private String name;
		private String addr;
		private String status;
		private Date utm;
		private Date rtm;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAddr() {
			return addr;
		}

		public void setAddr(String addr) {
			this.addr = addr;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Date getUtm() {
			return utm;
		}

		public void setUtm(Date utm) {
			this.utm = utm;
		}

		public Date getRtm() {
			return rtm;
		}

		public void setRtm(Date rtm) {
			this.rtm = rtm;
		}

	}

}
