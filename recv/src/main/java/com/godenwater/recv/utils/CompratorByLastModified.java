package com.godenwater.recv.utils;

import java.io.File;
import java.util.Comparator;

/**
 * 按文件的最后编辑时间进行排序
 * 
 * 使用用法：
 * Arrays.sort(fs, new Log.CompratorByLastModified());  
 * 
 * @author admin
 *
 */
public class CompratorByLastModified implements Comparator<File> {
	public int compare(File f1, File f2) {
		long diff = f1.lastModified() - f2.lastModified();
		if (diff > 0)
			return 1;
		else if (diff == 0)
			return 0;
		else
			return -1;
	}

	public boolean equals(Object obj) {
		return true;
	}

}
