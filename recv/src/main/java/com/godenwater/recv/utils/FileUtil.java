package com.godenwater.recv.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
 


public class FileUtil {

	/**
	 * The size of the buffer used by copy()
	 */
	private static final int BUFF_SIZE = 2048*1024;
 

	/**
	 * Reads input from System.in if data is avaiable.
	 *
	 * @return  the current (pending) input from System.in or null if nothing is there to read.
	 */
	public static String getSystemIn()
	{
		try
		{
			ByteArrayOutputStream out = new ByteArrayOutputStream(50);
			if (System.in.available() != 0)
			{
				int ch = System.in.read();
				while (ch > -1)
				{
					out.write(ch);
					ch = System.in.read();
				}
				String data = out.toString();
				return data;
			}
		}
		catch (Throwable th)
		{
		}
		return null;
	}

	
	
	/**
	 * 写序列化对象
	 */
	public static void writeObject(Object obj, String fileName) {
		FileOutputStream fo = null;
		ObjectOutputStream oos = null;
		try {
			fo = new FileOutputStream(fileName);

			oos = new ObjectOutputStream(fo);

			oos.writeObject(obj);

		} catch (IOException e) {
			System.out.println(e);
		} finally {
			if (oos != null)
				try {
					oos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (fo != null)
				try {
					fo.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * 读取序列化对象
	 * 
	 * @param fileName
	 * @return
	 */
	public static Object readObject(String fileName) {
		FileInputStream fi = null;
		ObjectInputStream si = null;

		try {
			fi = new FileInputStream(fileName);
			si = new ObjectInputStream(fi);

			return si.readObject();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
		} finally {
			if (si != null)
				try {
					si.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (fi != null)
				try {
					fi.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return null;
	} 
	public static void writeFile(String fileName, byte[] bytes) {

		FileOutputStream out = null;
		try {
			out = new FileOutputStream(new File(fileName), false);
			out.write(bytes);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static String splitStrToDir(String source) {

		String filePath = source;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmm");
		Date date;
		try {
			date = sdf.parse(source);

			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
			filePath = sdf2.format(date);
			File fp = new File(filePath);
			// 创建目录
			if (!fp.exists()) {
				fp.mkdirs();// 目录不存在的情况下，创建目录。
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filePath;
	}

	
	
}
