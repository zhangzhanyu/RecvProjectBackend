package com.godenwater.recv.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

public class StcdFilenameFilter implements FilenameFilter {

	private String stcd;

	public StcdFilenameFilter(String mStcd) {
		super();
		stcd = mStcd;
	}

	@Override
	public boolean accept(File file, String path) {
 
		if (path.startsWith(stcd)) {
			return true;
		} else {
			return false;
		}

	}

	public static void main(String[] args) {
		File file = new File("D:\\rcvApp\\cmd");
		
		File[] files = file.listFiles(new StcdFilenameFilter("12345678"));
		Arrays.sort(files, new CompratorByLastModified());

		for (int i = 0; i < files.length; i++) {

			System.out.println(">>-----" + files[i].getName() + "------");

		}
	}

}
