package com.ewaterchina.framework.config;

import com.godenwater.recv.spring.Configurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.ClassPathResource;

/**
 * @ClassName： XmlConfiguration
 * @Description: 装载xml配置
 * @CreateBy: 张占宇 2024年05月08日 下午15:49
 * @Version: 1.0
 */
@Configuration
@ImportResource(locations={"classpath:spring_redis.xml"})
public class XmlConfiguration {

    @Bean("propertyConfigurer")
    public Configurer getConfigurer() {
        Configurer configurer = new Configurer();
        configurer.setIgnoreResourceNotFound(true);
        configurer.setLocations(new ClassPathResource("config.properties"));
        return configurer;
    }

}
