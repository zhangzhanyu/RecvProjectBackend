package com.godenwater.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.godenwater.recv.server.all.RtuServer;
import com.godenwater.recv.server.summit.SummitServer;
import com.godenwater.recv.server.yf.YFRtuServer;
import javax.servlet.annotation.WebListener;
@WebListener
public class ADFContextListener implements ServletContextListener {

    protected String RTU_RCV_HYDRO_KEY = "RTU_RCV_HYDRO_KEY";
    protected String RTU_RCV_YF_KEY = "RTU_RCV_YF_KEY";
    protected String RTU_RCV_SUMMIT_KEY = "RTU_RCV_SUMMIT_KEY";

    public void contextInitialized(ServletContextEvent event) {

        System.out.println(">> " + RTU_RCV_HYDRO_KEY + "   " + event.getServletContext().getRealPath("/"));

        String homePath = event.getServletContext().getRealPath("/");
        if (homePath != null) {
            homePath = homePath.replaceAll("\\\\", "/");
        }

        // 初始化基础服务
        RtuServer m_hydroServer = RtuServer.getInstance();
        m_hydroServer.start();
        event.getServletContext().setAttribute(RTU_RCV_HYDRO_KEY, m_hydroServer);

        // 初始化【湖北一方】接收服务
        YFRtuServer m_yf_Server = YFRtuServer.getInstance();
        m_yf_Server.start();
        event.getServletContext().setAttribute(RTU_RCV_YF_KEY, m_yf_Server);


        // 初始化【西安山脉】接收服务
        SummitServer m_summit_Server = SummitServer.getInstance();
        m_summit_Server.start();
        event.getServletContext().setAttribute(RTU_RCV_SUMMIT_KEY, m_summit_Server);
        System.out.println(">>>RTU<<<  System RTU Platform Start Finish!");



    }

    public void contextDestroyed(ServletContextEvent event) {

        // 关闭基础服务
        RtuServer server = (RtuServer) event.getServletContext().getAttribute(
                RTU_RCV_HYDRO_KEY);
        if (null != server) {
            server.stop();
        }

        YFRtuServer yf_server = (YFRtuServer) event.getServletContext().getAttribute(
                RTU_RCV_YF_KEY);
        if (null != server) {
            yf_server.stop();
        }

        SummitServer summit_server = (SummitServer) event.getServletContext().getAttribute(
                RTU_RCV_SUMMIT_KEY);
        if (null != server) {
            summit_server.stop();
        }

        System.out.println(">>>RTU<<<  System RTU Platform Context Destroyed");

    }

}
