package com.godenwater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

/**
 * @ClassName： ReceiveHandleApplication
 * @Description: 启动程序
 * @CreateBy: 张占宇 2024年05月08日 下午14:19
 * @Version: 1.0
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@ServletComponentScan(value = "com.godenwater.web.listener")
public class ReceiveHandleApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReceiveHandleApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ReceiveHandleApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  水文水资源数据接收平台--后端接收程序启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                " o__ __o         o         o     \n" +
                " <|     v\\       <|>       <|>  \n" +
                " / \\     <\\      < >       < > \n" +
                " \\o/     o/       |         |   \n" +
                "  |__  _<|        o__/_ _\\__o   \n" +
                "  |       \\       |         |   \n" +
                " <o>       \\o    <o>       <o>  \n" +
                "  |         v\\    |         |   \n" +
                " / \\         <\\  / \\       / \\ ");
    }

}
