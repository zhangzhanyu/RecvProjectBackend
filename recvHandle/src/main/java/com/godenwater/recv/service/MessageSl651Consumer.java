package com.godenwater.recv.service;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import com.godenwater.framework.config.SpringUtils;
import com.godenwater.utils.WaterMarkUtils;
import com.godenwater.web.manager.StationManager;
import com.godenwater.web.rtu.model.RtuStationModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.gov.mwr.sl330.HydroParser;
import cn.gov.mwr.sl330.TypeDate;
import cn.gov.mwr.sl651.HydroBuilder;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.IMessageHeader;
import cn.gov.mwr.sl651.IParser;
import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.body.DataItem;
import cn.gov.mwr.sl651.body.Up30Body;
import cn.gov.mwr.sl651.body.Up31Body;
import cn.gov.mwr.sl651.body.Up35Body;
import cn.gov.mwr.sl651.body.Up36Body;
import cn.gov.mwr.sl651.body.Up42Body;
import cn.gov.mwr.sl651.body.Up44Body;
import cn.gov.mwr.sl651.body.Up45Body;
import cn.gov.mwr.sl651.body.Up46Body;
import cn.gov.mwr.sl651.body.Up47Body;
import cn.gov.mwr.sl651.body.Up48Body;
import cn.gov.mwr.sl651.body.Up49Body;
import cn.gov.mwr.sl651.body.Up4BBody;
import cn.gov.mwr.sl651.body.Up4CBody;
import cn.gov.mwr.sl651.body.Up4DBody;
import cn.gov.mwr.sl651.body.Up4EBody;
import cn.gov.mwr.sl651.body.Up4FBody;
import cn.gov.mwr.sl651.body.Up50Body;
import cn.gov.mwr.sl651.body.Up51Body;
import cn.gov.mwr.sl651.body.UpBaseBody;
import cn.gov.mwr.sl651.command.DownCommand;
import cn.gov.mwr.sl651.parser.AscParser;
import cn.gov.mwr.sl651.parser.HexParser;
import cn.gov.mwr.sl651.utils.ByteUtil;
import cn.gov.mwr.sl651.utils.DateUtil;

import com.godenwater.core.spring.Application;
import com.godenwater.core.spring.BaseDao;
import com.godenwater.recv.decode.Sl651MessageDecoder;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.all.RtuConfig;
import com.godenwater.recv.spring.Configurer;
import com.godenwater.utils.ByteUtils;


/**
 * 1.1报文消费者
 *
 * @author lipujun
 * @ClassName: MessageConsumer
 * @Description: 通过线程的方式将“报文”解析，然后入库，这是消息进行处理的第一个环节
 * <p>
 * 注意：只启动一个消费者
 * @date Mar 14, 2013
 */
public class MessageSl651Consumer extends AbstractMessageConsumer implements
        Runnable {

    private static Logger logger = LoggerFactory.getLogger(MessageSl651Consumer.class);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHH.mm");
    SimpleDateFormat sdfDay = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat sdffull = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
    SimpleDateFormat sdfMinte = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static long DAY = 24 * 60 * 60 * 1000;
    private static long HOUR = 60 * 60 * 1000;
    private static String KEY = "messages";
    SimpleDateFormat sdfMir = new SimpleDateFormat("yyyy/MM/dd");
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyMMddHHmm");

    public MessageSl651Consumer() {

    }


    @Override
    public void run() {

    }

    /**
     * 处理业务逻辑
     * <p>
     * 第一步：原始报文入库
     * <p>
     * 第二步：判断单报、多报。单报写“入库队列”；多报写文件，并根据最后一条报文写“合并队列”
     *
     * @param message
     */
    public void processMessage(String channel, CommonMessage message, String replayMsg, String ip, Integer port, int recvPort) throws Exception {
        logger.info("开始处理报文！");
        boolean crcFlag = true;
        IMessageHeader header = (IMessageHeader) message.getHeader();

        // 解析报文体
        IParser parser;
        if (header.getStartBit()[0] == Symbol.SOH_ASC) {
            parser = new AscParser();
        } else {
            parser = new HexParser();

        }
        String stcd = parser.parseStcd(header.getStationAddr());
        byte bodyStartBit = parser.parseBodyStartBit(header.getBodyStartBit());
        int amount = 1;// 包总数
        int seq = 1;// 当前序列数
        if (bodyStartBit == Symbol.SYN) {
            int[] bodyCount = parser.parseBodyCount(header.getBodyCount());
            amount = bodyCount[0];
            seq = bodyCount[1];
        }

        byte[] content = message.getContent();

        byte funcCode = parser.parseFuncCode(header.getFuncCode());
        if (funcCode != 0x2F) {
            // 因考虑到链接报数据频繁，则只将链路报以外的原始报文入库

            // 写入原始报文库
            String msgstr = HydroBuilder.toHexString((IMessageHeader) message.getHeader(), Symbol.UP) + ByteUtil.toHexString(content) + ByteUtil.toHexString(message.getEOF()) + ByteUtil.toHexString(message.getCRC());
            String id = UUID.randomUUID().toString();
            // 解析报文，并根据相对应的报文入库
            if (bodyStartBit == (byte) Symbol.STX) {
                // 单报文，先解析成记录数据，再放入到“入库报文消息队列”
                logger.info(">> 单报文解析，进入“入库报文消息队列”！");
                int serial = 0;
                Date sendtime = null;
                if (content != null) {
                    if (header.getStartBit()[0] == Symbol.SOH_HEX) {
                        byte[] serialid = new byte[2];
                        System.arraycopy(content, 0, serialid, 0, serialid.length);
                        serial = parser.parseSerial(new byte[]{content[0], content[1]});
                        byte[] sendtimeBytes = new byte[6];
                        System.arraycopy(content, serialid.length, sendtimeBytes, 0, sendtimeBytes.length);
                        sendtime = parser.parseSendTime(sendtimeBytes);
                    } else {
                        serial = parser.parseSerial(new byte[]{content[0], content[1], content[2], content[3]});
                        byte[] sendtimeBytes = new byte[12];
                        System.arraycopy(content, 4, sendtimeBytes, 0, sendtimeBytes.length);
                        sendtime = parser.parseSendTime(sendtimeBytes);

                    }

                }
                // 可以将原始报文后入库 设置钛能 81自定义功能码不入库，数据量太大
                if (funcCode != 0x81) {
                    this.writeRtuMessage(id, stcd, "SL651", channel, ByteUtil.toHexString(funcCode), serial, sendtime, amount, seq, msgstr, crcFlag, replayMsg, ip, port, recvPort);
                }
                //logger.info(">> 原始报文入库：" + msgstr);
                logger.info(">> 原始报文入库：测站号" + stcd);
                // 解析单报文
                IMessageBody body = null;
                try {
                    body = (IMessageBody) parser.parseBody(funcCode, content);
                } catch (Exception ex) {
                    System.out.println(msgstr + "------error");
                    return;
                }

                if (funcCode == 0x38) {
                    // 单报文的召测，需要添加处理 2016-12-08
                    processBody38Single(stcd, (Up31Body) body);
                } else {
                    // 针对普通报文的处理
                    process(id, channel, funcCode, body);
                }

            } else {
                // Symbol.SYN 多报文，需组合报文再解析 //
                // ----1.1查看是不是最后一条报文，如果是，则放入“合并报文消息队列”;如果不是，则进行文件缓存

                // 如果是召测报，并且是第一个报文
                if (funcCode == 0x38) {
                    // 单报文，先解析成记录数据，再放入到“入库报文消息队列”

                    logger.info(">> 多报文解析，进入“入库报文消息队列”！");

                    if (seq == 1) {
                        IMessageBody body = (IMessageBody) parser.parseBody(funcCode, content);
                        Up31Body body38 = (Up31Body) body;
                        processBody38Start(id, body38);
                    } else if (seq == amount) {
                        // 最后的数据
                        processBody38End(id, seq, content);
                    } else {
                        // 中间部分的数据
                        processBody38Middle(stcd, seq, content);
                    }
                }

                // 如果是召测报，并且是第一个报文
                if (funcCode == 0x36) {
                    IMessageBody body = (IMessageBody) parser.parseBody(funcCode, content);
                    Up36Body body36 = (Up36Body) body;
                    processBody36(id, "36", "GPRS", seq, body36);
                    /*
                    String dt = ((Up36Body) body).getViewDate();
                    String path = RtuConfig.getMsgImgPath();
                   String newFileName = "/" + sdfMir.format(new Date()) + "/" + stcd + "/" +dt +"-" + seq + ".log";
                    String tempFile = path + newFileName;
                    writePicToDir(tempFile, msgstr);*/
                    logger.info(" 图片报文 站号：" + stcd + "总报文" + amount + "  序号" + seq);
                    if (amount == seq) {
                        // 再放入“合并队列”
                        processBody36End(seq, body36);
                    }
                }
            }
        }
    }


    /**
     * 图片报文写入文件夹
     *
     * @param path
     * @param content
     */
    public void writePicToDir(String path, String content) {

        FileWriter writer;
        try {
            writer = new FileWriter(path);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 将校时消息写入召测库
     */
    private void writeDownMessage(String stcd, Date sendtime) throws Exception {
        try {
            Date now = new Date();
            long diff = now.getTime() - sendtime.getTime();
            // 判断时间如果大于5个小时，则产生下发时间召测报文
            if (Math.abs(diff) / HOUR > 5) {
                DownCommand cmd = new DownCommand();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * 处理业务逻辑
     *
     * @param message
     */
    private void process(String pid, String channel, byte funcCode,
                         IMessageBody body) throws Exception {
        try {
            logger.info(">> 入库报文处理！\t队列容量：<" + "" + ">");
            switch (funcCode) {
                case 0x2F:
                    break;
                case 0x30: // 测试报，不需要写入正式数据库，写入测试数据表
                    Up30Body body30 = (Up30Body) body;
                    this.processBody30(pid, channel, body30);
                    break;
                case 0x33: // 加报报
                    Up30Body body33 = (Up30Body) body;
                    this.processBody33(pid, ByteUtil.toHexString(funcCode), channel, body33);
                    break;
                case 0x32: // 定时报，目前以流量报为主（2016-12-09）
                    Up30Body body32 = (Up30Body) body;
                    this.processBody32(pid, ByteUtil.toHexString(funcCode), channel, body32);
                    break;
                case 0x34: // 小时报，以1小时为基本单位向中心站报送遥测站水文信息
                    Up30Body body34 = (Up30Body) body;
                    this.processBody34(pid, ByteUtil.toHexString(funcCode), channel, body34);
                    break;

                case 0x31: // 6.6.4.4　均匀时段报
                    Up31Body body31 = (Up31Body) body;
                    this.processBody31(pid, channel, body31);
                    break;
                case 0x35:// 人工置数报 报文内容为ASC码
                    Up35Body body35 = (Up35Body) body;
                    this.processBody35(pid, body35);
                    break;
                case 0x36: // 图片报
                    Up36Body body36 = (Up36Body) body;
                    this.processBody36(pid, ByteUtil.toHexString(funcCode), channel, body36);
                    break;
                case 0x37: // 6.6.4.10　中心站查询遥测站实时数据
                    Up30Body body37 = (Up30Body) body;
                    this.processBody37(pid, channel, body37);
                    break;
                case 0x38: // 6.6.4.11　中心站查询遥测站时段数据,返回数据与“均匀时段报数据”相同
                    Up31Body body38 = (Up31Body) body;
                    this.processBody38(pid, body38);
                    break;
                case 0x42:
                    // case 0x42: // 6.6.4.16　中心站修改遥测站运行参数配置表
                    Up42Body body42 = (Up42Body) body;
                    this.processBody42(pid, body42);
                    break;
                case 0x44: // case 0x44: // 6.6.4.19　 查询水泵电机实时工作数据
                    Up44Body body44 = (Up44Body) body;
                    this.processBody44(pid, body44);
                    break;
                case 0x45: // case 0x45: // 6.6.4.19　中心站查询遥测站软件版本
                    Up45Body body45 = (Up45Body) body;
                    this.processBody45(pid, body45);
                    break;
                case 0x46: // case 0x46: //6.6.4.20　中心站查询遥测站状态和报警信息
                    Up46Body body46 = (Up46Body) body;
                    this.processBody46(pid, body46);
                    break;
                case 0x49: // case 0x49: //6.6.4.23　修改密码
                    Up49Body body49 = (Up49Body) body;
                    this.processBody49(pid, body49);
                    break;
                case 0x4A: // case 0x4C: //6.6.4.26　控制水泵开关命令/水泵状态自报
                    UpBaseBody body4A = (UpBaseBody) body;
                    this.processBody4A(pid, body4A);
                    break;
                case 0x4B: // case 0x4C: //6.6.4.26　控制水泵开关命令/水泵状态自报
                    Up4BBody body4B = (Up4BBody) body;
                    this.processBody4B(pid, body4B);
                    break;
                case 0x4C:
                    Up4CBody body4c = (Up4CBody) body;
                    this.processBody4C(pid, body4c);
                    break;
                case 0x4D:
                    Up4DBody body4D = (Up4DBody) body;
                    this.processBody4D(pid, body4D);
                    break;
                case 0x4E: // case 0x4E: //6.6.4.28　控制闸门开关命令/闸门状态信息自报
                    Up4EBody body4E = (Up4EBody) body;
                    this.processBody4E(pid, body4E);
                    break;
                case 0x4F: // case 0x4E: //6.6.4.28　控制闸门开关命令/闸门状态信息自报
                    Up4FBody body4F = (Up4FBody) body;
                    this.processBody4F(pid, body4F);
                    break;
                case 0x50: // case 0x50: //6.6.4.30　中心站查询遥测站事件记录
                    Up50Body body50 = (Up50Body) body;
                    this.processBody50(pid, body50);
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            logger.info("---------------异常报文ID" + pid);
            ex.printStackTrace();
        }
    }

    /**
     * 6.6.4.14　中心站修改遥测站基本配置表
     *
     * @param body
     */
    private String processD1(byte[] data) {
        StringBuffer sb = new StringBuffer();
        int flag = ByteUtil.bytesToUbyte(new byte[]{data[0]});// 信道类型用1字节BCD码：1-短信，2-IPV4，3-北斗，4-海事卫星，5-PSTN，6-超短波。
        if (flag == 0) {
            sb.append("0");
        } else if (flag == 2) {
            StringBuffer sbdata = new StringBuffer();
            sbdata.append(flag).append(",");
            sbdata.append(ByteUtil.bcd2Str(new byte[]{data[1], data[2], data[3], data[4], data[5], data[6]})).append(",");
            sbdata.append(ByteUtil.bcd2Str(new byte[]{data[7], data[8], data[9]}));
            sb.append(sbdata.toString());
        } else {
            byte[] newvalue = new byte[data.length - 1];
            System.arraycopy(data, 1, newvalue, 0, newvalue.length);
            sb.append(ByteUtil.bcd2Str(newvalue));
        }
        return sb.toString();
    }

    /**
     * 处理一般报文，并将报文入库
     *
     * @param body
     */
    private void processBody30(String mid, String channel, Up30Body body)
            throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        // System.out.println("---------------------items.length " +
        // items.size());
        // 仅为找到电压
        String voltage = processSymbol38H(items);

        boolean pptnFlag = false;
        boolean riverFlag = false;
        String[] f4 = new String[12];
        String[] f5 = new String[12];
        String f26 = "", f20 = "", f1a = "", f37 = "", f39 = "";

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();

            // 有的公司，出现发送多次时间的现象
            if (label != null) {
                if (label[0] == (byte) 0xF0) {
                    // System.out.println("---xxxxxx--------xxxxxxxxxxxx------xxxxxxxxx---xxxxxx--------------------------");
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {
                        case (byte) 0xF4:
                            decimal = 1;
                            pptnFlag = true;
                            // 5分钟时段数据，添加时间段
                            f4 = processSymbolF4H(curViewDate, items);

                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            riverFlag = true;
                            decimal = 2;
                            // 5分钟水位数据，添加时间段，这个地方得显示声明，因为有的水文站，即包括雨量数据也包括水位数据。
                            f5 = processSymbolF5H(curViewDate, items);
                            break;
                        case (byte) 0x26:
                            f26 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x20:
                            f20 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x1A:
                            f1a = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x37:
                            f37 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x39:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3C:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x38:
                            // 写入电压数据到数据库
                            String voltagevalue = ByteUtil.bcd2Str(value);
                            if (decimal != 0) {
                                // 添加字符串
                                voltagevalue = HydroBuilder.buildDot(voltagevalue, decimal);
                            }
                            // this.writeRtuVoltage(stcd, sendDatetime, curViewDate,
                            // new Double(voltage).doubleValue());
                            break;
                        default:
                            String itemvalue = processSymbolValue(value, decimal);
                            //this.writeRtuSymbol(mid, stcd, sttp, sendDate, viewDate, ByteUtil.toHexString(label), itemvalue);

                    }
                }
            }
        }// end for
        this.writeRtuTest(mid, stcd, curViewDate, sendDatetime, new Double(voltage).doubleValue(), channel, f4, f5, f26, f20, f1a, f37, f39);

    }

    /**
     * 处理一般报文，并将报文入库
     *
     * @param body
     */
    private void processBody37(String mid, String channel, Up30Body body)
            throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        // System.out.println("---------------------items.length " +
        // items.size());
        // 仅为找到电压
        String voltage = processSymbol38H(items);

        boolean pptnFlag = false;
        boolean riverFlag = false;
        String[] f4 = new String[12];
        String[] f5 = new String[12];
        String f26 = "", f20 = "", f1a = "", f37 = "", f39 = "", f10 = "", f11 = "", f13 = "", f0d = "",
                f06 = "", f07 = "", fa3 = "", fa4 = "", f02 = "", f18 = "", f45 = "", f27 = "";
        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();

            // 有的公司，出现发送多次时间的现象
            if (label != null) {

                if (label[0] == (byte) 0xF0) {
                    // System.out.println("---xxxxxx--------xxxxxxxxxxxx------xxxxxxxxx---xxxxxx--------------------------");
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {

                        case (byte) 0xF4:
                            decimal = 1;
                            pptnFlag = true;
                            // 5分钟时段数据，添加时间段
                            f4 = processSymbolF4H(curViewDate, items);

                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            riverFlag = true;
                            decimal = 2;
                            // 5分钟水位数据，添加时间段，这个地方得显示声明，因为有的水文站，即包括雨量数据也包括水位数据。
                            f5 = processSymbolF5H(curViewDate, items);

                            break;
                        case (byte) 0x26:
                            f26 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x20:
                            f20 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x1A:
                            f1a = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x37:
                            f37 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x39:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3C:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x10:
                            f10 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x11:
                            f11 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x13:
                            f13 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x0D:
                            f0d = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x06:
                            f06 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x07:
                            f07 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA3:
                            fa3 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA4:
                            fa4 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x45:
                            f45 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x02:
                            f02 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x18:
                            f18 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x27:
                            f27 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x38:
                            // 写入电压数据到数据库
                            String voltagevalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                voltagevalue = HydroBuilder.buildDot(voltagevalue,
                                        decimal);
                            }
                            break;

                    }
                }
            }
        }
        writeRtuHour(mid, stcd, "37", curViewDate, sendDatetime, new Double(voltage).doubleValue(), channel, f4, f5, f26, f20, f1a, f37, f39, f10, f11, f13, f0d, f06, f07, fa3, fa4, f02, f18, f45, f27, null, null, null, null, null, null);
    }

    /**
     * 均匀时段报，原处理程序，并将报文入库
     *
     * @param body
     */
    private void processBody31(String mid, Up31Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        byte[] drxxx = body.getDrxxx();
        String dhm = ByteUtil.bcd2Str(drxxx);
        String day = dhm.substring(0, 2);
        String hour = dhm.substring(2, 4);
        String minute = dhm.substring(4, 6);
        String drx = TypeDate.N;
        String drnn = minute;
        if (!day.equals("00")) {
            drx = TypeDate.D;
            drnn = day;
        }
        if (!hour.equals("00")) {
            drx = TypeDate.H;
            drnn = hour;
        }
        if (!minute.equals("00")) {
            drx = TypeDate.N;
            drnn = minute;
        }

        // 有默认为000000的情况
        if (StringUtils.equalsIgnoreCase(drnn, "00")) {
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.D)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.H)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.N)) {
                drnn = "05";
            }
        }

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        int dataLength = 0;

        // 仅为找到电压
        String voltage = processSymbol38H(items);

        boolean pptnFlag = false;
        boolean riverFlag = false;
        String[] f4 = new String[12];
        String[] f5 = new String[12];
        String f26 = "", f20 = "", f1a = "", f37 = "", f39 = "", f10 = "", f11 = "", f13 = "", f0d = "", f06 = "", f07 = "", fa3 = "", fa4 = "", f02 = "", f18 = "", f45 = "", f27 = "", ff02 = "", ff03 = "", ff04 = "", ff05 = "", ff08 = "", ffc9 = "";

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();
            String extend = item.getExtend();
            // String itemvalue = ByteUtil.bcd2Str(value);
            // if (decimal != 0) {
            // // 添加小数点字符串
            // itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
            // }
            // this.writeDb(mid, serial, stcd, sttp, sendDatetime, curViewDate,
            // new String(label), itemvalue);
            // // 向下添加时间
            // curViewDate = HydroParser.processDate(curViewDate, drx, drnn);
            //
            // ==============================================
            // 有的公司，出现发送多次时间的现象

            if (label != null) {
                logger.info("field" + ByteUtil.toHexString(label) + "  value "
                        + ByteUtil.bcd2Str(value) + " byte "
                        + ByteUtil.bytesToUbyte(value) + " decimal " + decimal);
                if (label[0] == (byte) 0xF0) {
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {
                        case (byte) 0x22:

                            dataLength = value.length;

                            int tempDataLen = 0;
                            BigDecimal hourRain = new BigDecimal(0);

                            for (int ii = 0; ii < dataLength / length; ii++) {
                                byte[] temp = new byte[length];
                                System.arraycopy(value, ii * length, temp, 0, length);

                                int intvalue = ByteUtil.bytesToUbyte(temp);
                                if (intvalue == 255) {
                                    curViewDate = HydroParser.processDate(curViewDate, drx, drnn);
                                    continue;// 此种数据属于非法数据,协议中规定
                                }

                                String f22alue = ByteUtil.bcd2Str(temp);
                                f22alue = HydroBuilder.buildDot(f22alue, decimal);

                                //this.writeRtuSymbol(mid, stcd, sttp, sendDatetime, curViewDate, ByteUtil.toHexString(label), f22alue);

                                // 将每5分钟的雨量相加，得出一小时的雨量
                                curViewDate = HydroParser.processDate(curViewDate, drx, drnn);
                                hourRain = hourRain.add(new BigDecimal(f22alue));

                            }
                            // 多加了5分钟，应该给减回来
                            curViewDate = DateUtil.addMinutes(curViewDate, -5);
                            // this.writeRtuSymbol(mid, stcd, sttp, sendDatetime,
                            // curViewDate, "1A", hourRain.toString());

                            break;
                        case (byte) 0xF4:
                            //
                            decimal = 1;
                            // --需要根据时间步长码，将数据写入到相对应的实时数据表中----------
                            // 如果时间步长码为分钟，将分钟数据转换为小时，写入小时雨量
                            // 如果时间步长码为小时，将小时数据直接写入
                            // 如果时间步长码为天，这个好像不存在。
                            dataLength = value.length;
                            BigDecimal F4HourRain = new BigDecimal(0);

                            for (int ii = 0; ii < dataLength / length; ii++) {
                                byte[] temp = new byte[length];
                                System.arraycopy(value, ii * length, temp, 0,
                                        length);

                                int intvalue = ByteUtil.bytesToUbyte(temp);

                                if ((value.length == 1 && intvalue == 255)
                                        || (value.length == 2 && intvalue == 65535)) {
                                    curViewDate = HydroParser.processDate(
                                            curViewDate, drx, drnn);
                                    continue;// 此种数据属于非法数据,协议中规定
                                }

                                String f4Value = Integer.toString(intvalue);
                                f4Value = HydroBuilder.buildDot(f4Value, decimal);


                            }
                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            decimal = 2;
                            // 5分钟水位数据，添加时间段
                            String[] dh12 = processSymbolF5H(curViewDate, items);
                            break;
                        case (byte) 0x10:
                            f10 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x11:
                            f11 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x13:
                            f13 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x0D:
                            f0d = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x06:
                            f06 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x07:
                            f07 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA3:
                            fa3 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA4:
                            fa4 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x45:
                            f45 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x27:
                            f27 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x02:
                            f02 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x18:
                            f18 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xFF:
                            byte[] b1 = new byte[value.length - 1];
                            System.arraycopy(value, 1, b1, 0, value.length - 1);
                            logger.info("extend FF" + extend);
                            switch (extend) {
                                case "02":
                                    ff02 = processSymbolValue(b1, decimal);
                                    break;
                                case "03":
                                    ff03 = processSymbolValue(b1, decimal);
                                    break;
                                case "04":
                                    ff04 = processSymbolValue(b1, decimal);
                                    break;
                                case "05":
                                    ff05 = processSymbolValue(b1, decimal);
                                    break;
                                case "C9":
                                    ffc9 = processSymbolValue(b1, decimal);
                                    break;
                                case "08":
                                    ff08 = processSymbolValue(b1, decimal);
                                    break;
                            }
                            break;
                        default:
                            // ---------------2016-11-15修改的代码 -----------------
                            // 判断是否有非法数据的，如果数组中全为FF，则为非法数据
                            boolean flag = false;
                            boolean neg = false;

                            // 判断是否是正常数据，如果全为FF，则为非法数据
                            for (int i = 0, len = value.length; i < len; i++) {
                                int intvalue = ByteUtil
                                        .bytesToUbyte(new byte[]{value[i]});
                                if (intvalue != 255) {
                                    flag = true;
                                    break;
                                }
                            }

                            if (flag) {

                                if (value[0] == (byte) 0xFF) {
                                    neg = true;
                                    value[0] = (byte) 0x00;// 第一个值为FF表示为负数
                                }

                                String itemvalue = ByteUtil.bcd2Str(value);

                                if (decimal != 0) {
                                    // 添加字符串
                                    itemvalue = HydroBuilder.buildDot(itemvalue,
                                            decimal);
                                }

                                if (neg) {
                                    itemvalue = "-" + itemvalue;
                                }

                                logger.info("field" + ByteUtil.toHexString(label) + "  value " + itemvalue + " decimal " + decimal);
                            }
                    }
                }
            }
        }
        writeRtuHour(mid, stcd, "31", curViewDate, sendDatetime, new Double(voltage).doubleValue(), "", f4, f5, f26, f20, f1a, f37, f39, f10, f11, f13, f0d, f06, f07, fa3, fa4, f02, f18, f45, f27, ff02, ff03, ff04, ff05, ffc9, ff08);
    }

    /**
     * 新均匀时段段处理程序
     *
     * @param mid
     * @param channel
     * @param body
     * @throws Exception
     */
    private void processBody31(String mid, String channel, Up31Body body)
            throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        byte[] drxxx = body.getDrxxx();
        String dhm = ByteUtil.bcd2Str(drxxx);
        String day = dhm.substring(0, 2);
        String hour = dhm.substring(2, 4);
        String minute = dhm.substring(4, 6);
        String drx = TypeDate.N;
        String drnn = minute;
        if (!day.equals("00")) {
            drx = TypeDate.D;
            drnn = day;
        }
        if (!hour.equals("00")) {
            drx = TypeDate.H;
            drnn = hour;
        }
        if (!minute.equals("00")) {
            drx = TypeDate.N;
            drnn = minute;
        }

        // 有默认为000000的情况
        if (StringUtils.equalsIgnoreCase(drnn, "00")) {
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.D)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.H)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.N)) {
                drnn = "05";
            }
        }

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        int dataLength = 0;

        // 仅为找到电压
        String voltage = processSymbol38H(items);
        boolean pptnFlag = false;
        boolean riverFlag = false;
        String[] f4 = new String[12];
        String[] f5 = new String[12];
        String f26 = "", f20 = "", f1a = "", f37 = "", f39 = "", f10 = "", f11 = "", f13 = "", f0d = "",
                f06 = "", f07 = "", fa3 = "", fa4 = "", f02 = "", f18 = "", f45 = "", f27 = "", ff02 = "", ff03 = "", ff04 = "", ff05 = "", ff08 = "", ffc9 = "";

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();
            String extend = item.getExtend();
            // String itemvalue = ByteUtil.bcd2Str(value);
            // if (decimal != 0) {
            // // 添加小数点字符串
            // itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
            // }
            // this.writeDb(mid, serial, stcd, sttp, sendDatetime, curViewDate,
            // new String(label), itemvalue);
            // // 向下添加时间
            // curViewDate = HydroParser.processDate(curViewDate, drx, drnn);
            //
            // ==============================================
            // 有的公司，出现发送多次时间的现象

            if (label != null) {
                logger.info("field" + ByteUtil.toHexString(label) + "  value "
                        + ByteUtil.bcd2Str(value) + " byte "
                        + ByteUtil.bytesToUbyte(value) + " decimal " + decimal);
                if (label[0] == (byte) 0xF0) {
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {
                        case (byte) 0xF4:
                            decimal = 1;
                            pptnFlag = true;
                            // 5分钟时段数据，添加时间段
                            f4 = processSymbolF4H(curViewDate, items);

                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            riverFlag = true;
                            decimal = 2;
                            // 5分钟水位数据，添加时间段，这个地方得显示声明，因为有的水文站，即包括雨量数据也包括水位数据。
                            f5 = processSymbolF5H(curViewDate, items);

                            break;
                        case (byte) 0x26:
                            f26 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x20:
                            f20 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x1A:
                            f1a = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x37:
                            f37 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x39:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3C:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x10:
                            f10 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x11:
                            f11 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x13:
                            f13 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x0D:
                            f0d = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x06:
                            f06 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x07:
                            f07 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA3:
                            fa3 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA4:
                            fa4 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x45:
                            f45 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x02:
                            f02 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x18:
                            f18 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x27:
                            f27 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xFF:
                            byte[] b1 = new byte[value.length - 1];
                            System.arraycopy(value, 1, b1, 0, value.length - 1);
                            logger.info("extend FF" + extend);
                            switch (extend) {
                                case "02":
                                    ff02 = processSymbolValue(b1, decimal);
                                    break;
                                case "03":
                                    ff03 = processSymbolValue(b1, decimal);
                                    break;
                                case "04":
                                    ff04 = processSymbolValue(b1, decimal);
                                    break;
                                case "05":
                                    ff05 = processSymbolValue(b1, decimal);
                                    break;
                                case "C9":
                                    ffc9 = processSymbolValue(b1, decimal);
                                    break;
                                case "08":
                                    ff08 = processSymbolValue(b1, decimal);
                                    break;
                            }
                            break;
                        case (byte) 0x38:
                            // 写入电压数据到数据库
                            String voltagevalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                voltagevalue = HydroBuilder.buildDot(voltagevalue,
                                        decimal);
                            }

                            // this.writeRtuVoltage(stcd, sendDatetime, curViewDate,
                            // new Double(voltage).doubleValue());
                            break;
                        default:

                            String itemvalue = processSymbolValue(value, decimal);

                            //this.writeRtuSymbol(mid, stcd, sttp, sendDate, viewDate, ByteUtil.toHexString(label), itemvalue);

                    }
                }
            }
        }// end for
        String intoF1aF20 = (String) Configurer.getStringProperty("intoF1aF20", "0");
        if (f1a.equals("")) {
            double temp = 0;
            for (int i = 0; i < f4.length; i++) {
                if (f4[i] != null && !f4[i].equals("FF") && !f4[i].equals("")) {
                    temp += Double.parseDouble(f4[i]);
                }
            }
            //计算时段雨量
            if (intoF1aF20.equals("1")) {

                DecimalFormat df = new DecimalFormat("0.0");
                if (temp > 0) {
                    f1a = df.format(temp);
                } else {
                    f1a = String.valueOf(temp);
                }
            }
        }
        writeRtuHour(mid, stcd, "31", curViewDate, sendDatetime, new Double(voltage).doubleValue(), channel, f4, f5, f26, f20, f1a, f37, f39, f10, f11, f13, f0d, f06, f07, fa3, fa4, f02, f18, f45, f27, ff02, ff03, ff04, ff05, ffc9, ff08);

    }

    /**
     * 处理业务逻辑
     *
     * @param message
     */
    private void process(String pid, byte funcCode, Up31Body body)
            throws Exception {
        logger.info(">> 入库报文处理！\t队列容量：<" + "" + ">");

    }

    /**
     * 处理固态报文，主要是将固态报文进行处理
     *
     * @param body
     */
    private void processBody38(String mid, Up31Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        byte[] drxxx = body.getDrxxx();
        String dhm = ByteUtil.bcd2Str(drxxx);
        String day = dhm.substring(0, 2);
        String hour = dhm.substring(2, 4);
        String minute = dhm.substring(4, 6);
        String drx = TypeDate.N;
        String drnn = minute;
        if (!day.equals("00")) {
            drx = TypeDate.D;
            drnn = day;
        }
        if (!hour.equals("00")) {
            drx = TypeDate.H;
            drnn = hour;
        }
        if (!minute.equals("00")) {
            drx = TypeDate.N;
            drnn = minute;
        }

        // 有默认为000000的情况
        if (StringUtils.equalsIgnoreCase(drnn, "00")) {
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.D)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.H)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.N)) {
                drnn = "05";
            }
        }

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        int dataLength = 0;

        // 仅为找到电压
        String voltage = processSymbol38H(items);

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();
            // String itemvalue = ByteUtil.bcd2Str(value);
            // if (decimal != 0) {
            // // 添加小数点字符串
            // itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
            // }
            // this.writeDb(mid, serial, stcd, sttp, sendDatetime, curViewDate,
            // new String(label), itemvalue);
            // // 向下添加时间
            // curViewDate = HydroParser.processDate(curViewDate, drx, drnn);
            //
            // ==============================================
            // 有的公司，出现发送多次时间的现象

            if (label != null) {
                logger.info("field" + ByteUtil.toHexString(label) + "  value "
                        + ByteUtil.bcd2Str(value) + " byte "
                        + ByteUtil.bytesToUbyte(value) + " decimal " + decimal);
                if (label[0] == (byte) 0xF0) {
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {
                        case (byte) 0x22:

                            dataLength = value.length;

                            int tempDataLen = 0;
                            BigDecimal hourRain = new BigDecimal(0);

                            for (int ii = 0; ii < dataLength / length; ii++) {
                                byte[] temp = new byte[length];
                                System.arraycopy(value, ii * length, temp, 0,
                                        length);

                                int intvalue = ByteUtil.bytesToUbyte(temp);
                                if (intvalue == 255) {
                                    curViewDate = HydroParser.processDate(
                                            curViewDate, drx, drnn);
                                    continue;// 此种数据属于非法数据,协议中规定
                                }

                                String f22alue = ByteUtil.bcd2Str(temp);
                                f22alue = HydroBuilder.buildDot(f22alue, decimal);

                                //this.writeRtuSymbol(mid, stcd, sttp, sendDatetime, curViewDate, ByteUtil.toHexString(label), f22alue);

                                // 将每5分钟的雨量相加，得出一小时的雨量
                                curViewDate = HydroParser.processDate(curViewDate,
                                        drx, drnn);
                                hourRain = hourRain.add(new BigDecimal(f22alue));

                            }

                            // 多加了5分钟，应该给减回来
                            curViewDate = DateUtil.addMinutes(curViewDate, -5);
                            //this.writeRtuSymbol(mid, stcd, sttp, sendDatetime, curViewDate, "1A", hourRain.toString());

                            break;
                        case (byte) 0xF4:
                            //
                            // decimal = 1;

                            // --需要根据时间步长码，将数据写入到相对应的实时数据表中----------
                            // 如果时间步长码为分钟，将分钟数据转换为小时，写入小时雨量
                            // 如果时间步长码为小时，将小时数据直接写入
                            // 如果时间步长码为天，这个好像不存在。
                            dataLength = value.length;

                            // 此处取值应按照实际长度来取值，和判断小数点，
                            /**
                             * 如果是雨量，取一字节值，取一位小数 如果是水位，取两字节值，取两位小数
                             */
                            List<String> lines = new ArrayList<String>();
                            for (int ii = 0; ii < dataLength / length; ii++) {

                                curViewDate = HydroParser.processDate(curViewDate,
                                        drx, drnn);

                                byte[] temp = new byte[length];
                                System.arraycopy(value, ii * length, temp, 0,
                                        length);

                                int intvalue = ByteUtil.bytesToUbyte(temp);

                                // if ((temp.length == 1 && intvalue == 255)
                                // || (temp.length == 2 && intvalue == 65535)) {

                                // continue;// 此种数据属于非法数据,协议中规定
                                // }

                                String f4Value = Integer.toString(intvalue);
                                f4Value = HydroBuilder.buildDot(f4Value, decimal);

                                // 固态数据较多，不用写入数据库
                                // 需要将此追加到文件，如果是雨量，写入雨量文件，如果是水位，写入到水位文件
                                lines.add(sdf2.format(curViewDate) + " " + f4Value);

                            }
                            // 写入到文件
                            String callpath = RtuConfig.getMsgSsrPath();
                            String callFilename = callpath + "/" + stcd + "/"
                                    + viewDate + ".pzl";// ".zwl"
                            FileUtils.writeLines(new File(callFilename), lines,
                                    false);
                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            dataLength = value.length;

                            // 此处取值应按照实际长度来取值，和判断小数点，
                            /**
                             * 如果是雨量，取一字节值，取一位小数 如果是水位，取两字节值，取两位小数
                             */
                            List<String> xlines = new ArrayList<String>();
                            for (int ii = 0; ii < dataLength / length; ii++) {

                                curViewDate = HydroParser.processDate(curViewDate,
                                        drx, drnn);

                                byte[] temp = new byte[length];
                                System.arraycopy(value, ii * length, temp, 0,
                                        length);

                                int intvalue = ByteUtil.bytesToUbyte(temp);

                                // if ((temp.length == 1 && intvalue == 255)
                                // || (temp.length == 2 && intvalue == 65535)) {

                                // continue;// 此种数据属于非法数据,协议中规定
                                // }

                                String f5Value = Integer.toString(intvalue);
                                f5Value = HydroBuilder.buildDot(f5Value, decimal);

                                // 固态数据较多，不用写入数据库
                                // 需要将此追加到文件，如果是雨量，写入雨量文件，如果是水位，写入到水位文件
                                xlines.add(sdf2.format(curViewDate) + " " + f5Value);

                            }
                            // 写入到文件
                            String callpath2 = RtuConfig.getMsgSsrPath();
                            String callFilename2 = callpath2 + "/" + stcd + "/"
                                    + viewDate + ".zwl";// ".zwl"
                            FileUtils.writeLines(new File(callFilename2), xlines,
                                    false);
                            break;

                        default:

                            String ditemvalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                ditemvalue = HydroBuilder.buildDot(ditemvalue,
                                        decimal);
                            }

                            // 如果是“当前降雨量（20）”，也存在临时表中
                            if (ByteUtil.toHexString(label).equals("20")) {
                                // this.writeDbFor1H(mid, serial, stcd, sttp,
                                // sendDatetime, curViewDate,
                                // ByteUtil.toHexString(label), ditemvalue);
                            }

                    }
                }
            }
        }

    }

    /**
     * 处理图片数据，每一个图片报文数据写入到一个文件，最后再拼成图片
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processBody36(String mid, String funccode, String channel, int seq, Up36Body body) throws Exception {

        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();
        byte[] picByte = body.getPic();

        String path = RtuConfig.getMsgImgPath();
        String fileName = "/" + sdfMir.format(new Date()) + "/" + stcd + "/temp/" + viewDate + "_" + seq + ".tmp";
        fileName = path + fileName;

        if (picByte != null)
            FileUtils.writeByteArrayToFile(new File(fileName), picByte);

        /*可以将原始报文后入库
        this.writeRtuMessage(mid, stcd, "SL651", channel,
                funccode, serial, HydroParser.convertYYMMDDHHmm(viewDate),
                0, seq, ByteUtil.toHexString(body.getContent()), true);
                */
    }

    private void processBody36End(int seq, Up36Body body) throws Exception {
        try {
            int serial = body.getSerialId();
            String stcd = body.getStcd();
            String sttp = body.getSttp();
            String sendDate = body.getSendDate();
            String viewDate = body.getViewDate();
            String path = RtuConfig.getMsgImgPath();
            String newFileName = "/" + sdfMir.format(new Date()) + "/" + stcd + "/" + viewDate + ".jpg";
            String tempFile = path + "/" + sdfMir.format(new Date()) + "/" + stcd + "/temp";
            //判断是或否所有包都有
            if (revAllPic(seq, tempFile, viewDate)) {
                for (int i = 1; i <= seq; i++) {
                    String fileName = "/" + sdfMir.format(new Date()) + "/" + stcd + "/temp/" + viewDate + "_" + i + ".tmp";
                    fileName = path + fileName;
                    File xFile = new File(fileName);
                    if (xFile.exists()) {// 中间会有缺少的文件时怎么处理。
                        byte[] tmpbyte = FileUtils.readFileToByteArray(xFile);// 读临时文件，生成正式文件
                        // 合并到一个文件中
                        FileUtils.writeByteArrayToFile(new File(path + newFileName), tmpbyte, true);
                        // 将文件临时文件删除
                        FileUtils.deleteQuietly(xFile);
                    }
                }
                //添加水印
                new WaterMarkUtils().mark(path + newFileName, path + newFileName, Color.yellow, sdfMinte.format(sdfDate.parse(viewDate)));
                //图片路径入库 李海超 2017 0721
                RtuStationModel station = StationManager.getInstance().getStation(stcd);
                /* logger.info("测站号码：" + stcd);*/
                String stcd8 = "";
                if (station != null) {
                    stcd8 = station.getStcd8();
                }
                writePicPathToDB(stcd, sdfDate.parse(viewDate), newFileName, stcd8, viewDate + ".jpg");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    /**
     * 判断文件夹下的个数跟最后包的个数对应
     *
     * @param seq
     * @param path
     * @param viewDate
     * @return
     */
    public boolean revAllPic(int seq, String path, String viewDate) {
        int picCount = 0;
        File f = new File(path);
        if (!f.exists()) {
            System.out.println(path + " not exists");
            return false;
        }

        File fa[] = f.listFiles();
        for (int i = 0; i < fa.length; i++) {
            File fs = fa[i];
            if (fs.isDirectory()) {
                // System.out.println(fs.getName() + " [目录]");
            } else {
                String tempFileName = fs.getName();
                if (viewDate.equals(tempFileName.substring(0, 10))) {
                    picCount++;
                    //logger.info("pic:" + picCount + "---------------------------  seq" + seq);
                }

            }
        }
        logger.info("pic:" + picCount + "---------------------------  seq" + seq);
        if (picCount == seq) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 处理固态数据，将第一个数据写入到一个文件中
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processYYBody38Start(String mstcd, Up31Body body)
            throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        byte[] drxxx = body.getDrxxx();
        String dhm = ByteUtil.bcd2Str(drxxx);
        String day = dhm.substring(0, 2);
        String hour = dhm.substring(2, 4);
        String minute = dhm.substring(4, 6);
        String drx = TypeDate.N;
        String drnn = minute;
        if (!day.equals("00")) {
            drx = TypeDate.D;
            drnn = day;
        }
        if (!hour.equals("00")) {
            drx = TypeDate.H;
            drnn = hour;
        }
        if (!minute.equals("00")) {
            drx = TypeDate.N;
            drnn = minute;
        }

        // 有默认为000000的情况
        if (StringUtils.equalsIgnoreCase(drnn, "00")) {
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.D)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.H)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.N)) {
                drnn = "05";
            }
        }

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        int dataLength = 0;

        // 仅为找到电压
        String voltage = processSymbol38H(items);

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();

            if (label != null) {
                logger.info("field" + ByteUtil.toHexString(label) + "  value "
                        + ByteUtil.bcd2Str(value) + " byte "
                        + ByteUtil.bytesToUbyte(value) + " decimal " + decimal);
                if (label[0] == (byte) 0xF0) {
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {
                        case (byte) 0xF4:
                            //
                            // decimal = 1;

                            // --需要根据时间步长码，将数据写入到相对应的实时数据表中----------
                            // 如果时间步长码为分钟，将分钟数据转换为小时，写入小时雨量
                            // 如果时间步长码为小时，将小时数据直接写入
                            // 如果时间步长码为天，这个好像不存在。
                            dataLength = value.length;

                            // 此处取值应按照实际长度来取值，和判断小数点，
                            /**
                             * 如果是雨量，取一字节值，取一位小数 如果是水位，取两字节值，取两位小数
                             */
                            List<String> lines = new ArrayList<String>();
                            lines.add(stcd + " " + sdffull.format(curViewDate) + " " + ByteUtils.bytesToHexString(new byte[]{label[0]}) + " " + drx + " " + drnn + " " + "zwl");
                            lines.add(ByteUtils.bytesToHexString(value));

                            // 写固态数据写入到文件
                            MessageSynManager.getInstance().setStcd(stcd, viewDate);
                            MessageSynManager.getInstance().setStcd(stcd + "_serial", "" + serial);
                            // 写入到文件
                            String callpath = RtuConfig.getMsgSsrPath();
                            String callFilename = callpath + "/" + stcd + "/" + viewDate + "_1.tmp";
                            FileUtils.writeLines(new File(callFilename), lines, false);
                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            dataLength = value.length;

                            // 此处取值应按照实际长度来取值，和判断小数点，
                            /**
                             * 如果是雨量，取一字节值，取一位小数 如果是水位，取两字节值，取两位小数
                             */
                            List<String> xlines = new ArrayList<String>();

                            // 第一行写入：
                            xlines.add(stcd + " " + sdffull.format(curViewDate) + " " + ByteUtils.bytesToHexString(new byte[]{label[0]}) + " " + drx + " " + drnn + " " + "zwl");
                            xlines.add(ByteUtils.bytesToHexString(value));

                            // 写固态数据写入到文件
                            MessageSynManager.getInstance().setStcd(stcd, viewDate);
                            MessageSynManager.getInstance().setStcd(stcd + "_serial", "" + serial);
                            String callpath2 = RtuConfig.getMsgSsrPath();
                            String callFilename2 = callpath2 + "/" + stcd + "/" + viewDate + "_1.tmp";// ".zwl"
                            FileUtils.writeLines(new File(callFilename2), xlines, false);
                            break;

                        default:

                            String ditemvalue = ByteUtil.bcd2Str(value);
                            if (decimal != 0) {
                                // 添加字符串
                                ditemvalue = HydroBuilder.buildDot(ditemvalue, decimal);
                            }

                            // 如果是“当前降雨量（20）”，也存在临时表中
                            if (ByteUtil.toHexString(label).equals("20")) {
                                // this.writeDbFor1H(mid, serial, stcd, sttp,
                                // sendDatetime, curViewDate,
                                // ByteUtil.toHexString(label), ditemvalue);
                            }

                    }
                }
            }
        }

    }

    /**
     * 处理固态数据，将中间的数据写入到文件中
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processYYBody38Middle(String stcd, byte[] body)
            throws Exception {
        String callpath2 = RtuConfig.getMsgSsrPath();

        try {
            // 写固态数据写入到文件
            String viewDate = MessageSynManager.getInstance().getStcd(stcd);
            String viewSerial = MessageSynManager.getInstance().getStcd(
                    stcd + "_serial");

            // 前两个字节是序列号
            // arraycopy(Object src, int srcPos, Object dest, int destPos, int
            // length)
            byte[] curSerial = new byte[2];
            System.arraycopy(body, 0, curSerial, 0, 2);
            int curSeq = ByteUtil.bytesToUshort(curSerial);
            // 后面是是报文
            int curBodyLen = body.length - 2;
            byte[] curBody = new byte[curBodyLen];
            System.arraycopy(body, 2, curBody, 0, curBodyLen);

            if (viewDate != null) {
                String callFilename2 = callpath2 + "/" + stcd + "/" + viewDate
                        + "_" + (curSeq - Integer.parseInt(viewSerial) + 1)
                        + ".tmp";// ".zwl"
                FileUtils.writeStringToFile(new File(callFilename2),
                        ByteUtils.bytesToHexString(curBody));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 处理固态数据，根据最后一个数据合成最终固态数据
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processYYBody38End(String stcd, byte[] body) throws Exception {
        // 写入到文件
        String callpath = RtuConfig.getMsgSsrPath();

        // 写固态数据写入到文件
        String viewDate = MessageSynManager.getInstance().getStcd(stcd);
        String viewSerial = MessageSynManager.getInstance().getStcd(
                stcd + "_serial");

        // 前两个字节是序列号
        // arraycopy(Object src, int srcPos, Object dest, int destPos, int
        // length)
        byte[] curSerial = new byte[2];
        System.arraycopy(body, 0, curSerial, 0, 2);
        int curSeq = ByteUtil.bytesToUshort(curSerial);

        // 后面是是报文
        int curBodyLen = body.length - 2;
        byte[] curBody = new byte[curBodyLen];
        System.arraycopy(body, 2, curBody, 0, curBodyLen);

        if (viewDate != null) {
            String callFilename2 = callpath + "/" + stcd + "/" + viewDate + "_"
                    + (curSeq - Integer.parseInt(viewSerial) + 1) + ".tmp";// ".zwl"
            FileUtils.writeStringToFile(new File(callFilename2),
                    ByteUtils.bytesToHexString(curBody));

            // ----------------------------------------------------------------------

            // 写入到文件

            // ----------------------------
            // 开始合并文件
            String readFilename = callpath + "/" + stcd + "/" + viewDate
                    + "_1.tmp";
            List<String> lines = FileUtils.readLines(new File(readFilename));
            List<String> newlines = new ArrayList<String>();
            if (lines.size() > 1) {
                String line1 = lines.get(0);
                String[] data = line1.split(" "); // stcd curViewDate flag drx
                // // drnn信息由这五部分组成

                String mStcd = data[0];
                String mViewDate = data[1];
                String mFlag = data[2];
                String mDrx = data[3];
                String mDrnn = data[4];
                String mSuffix = data[5];
                int mLength = 1;
                Date mxViewDate = sdffull.parse(mViewDate);
                int mDecimal = 1;
                // 添加水位、雨量的长度判断
                if (StringUtils.equalsIgnoreCase(mFlag, "F4")) {
                    mLength = 1;
                    mDecimal = 1;
                }
                if (StringUtils.equalsIgnoreCase(mFlag, "F5")) {
                    mLength = 2;
                    mDecimal = 2;
                }

                // 读取每一行
                for (int i = 1; i < lines.size(); i++) {
                    String tmpLine = lines.get(i);
                    byte[] tmpByte = ByteUtils.hexStringToBytes(tmpLine);// 先将字符串转换为字节

                    int dLength = tmpByte.length;
                    for (int ii = 0; ii < dLength / mLength; ii++) {

                        mxViewDate = HydroParser.processDate(mxViewDate, mDrx,
                                mDrnn);

                        byte[] temp = new byte[mLength];
                        System.arraycopy(tmpByte, ii * mLength, temp, 0,
                                mLength);

                        int intvalue = ByteUtil.bytesToUbyte(temp);

                        String fValue = Integer.toString(intvalue);
                        fValue = HydroBuilder.buildDot(fValue, mDecimal);

                        newlines.add(sdf2.format(mxViewDate) + " " + fValue);

                    }
                }

                // 开始读第二个文件到最后的文件
                for (int i = 2; i <= (curSeq - Integer.parseInt(viewSerial)); i++) {
                    readFilename = callpath + "/" + stcd + "/" + viewDate + "_"
                            + i + ".tmp";

                    String tmpString = FileUtils.readFileToString(new File(
                            readFilename));
                    byte[] tmpByte = ByteUtils.hexStringToBytes(tmpString);// 先将字符串转换为字节

                    int dLength = tmpByte.length;
                    for (int ii = 0; ii < dLength / mLength; ii++) {

                        mxViewDate = HydroParser.processDate(mxViewDate, mDrx,
                                mDrnn);

                        byte[] temp = new byte[mLength];
                        System.arraycopy(tmpByte, ii * mLength, temp, 0,
                                mLength);

                        int intvalue = ByteUtil.bytesToUbyte(temp);

                        String fValue = Integer.toString(intvalue);
                        fValue = HydroBuilder.buildDot(fValue, mDecimal);

                        newlines.add(sdf2.format(mxViewDate) + " " + fValue);

                    }
                }// 读取完文件

                // 写入新的文件
                String newFilename = callpath + "/" + stcd + "/" + viewDate
                        + "." + mSuffix;
                FileUtils.writeLines(new File(newFilename), newlines);

                // 清空缓存
                MessageSynManager.getInstance().delStcd(stcd);
                MessageSynManager.getInstance().delStcd(stcd + "_serial");
            }

        }

    }

    /**
     * 处理固态数据，将第一个数据写入到一个文件中
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processBody38Start(String mid, Up31Body body) throws Exception {

        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        byte[] drxxx = body.getDrxxx();
        String dhm = ByteUtil.bcd2Str(drxxx);
        String day = dhm.substring(0, 2);
        String hour = dhm.substring(2, 4);
        String minute = dhm.substring(4, 6);
        String drx = TypeDate.N;
        String drnn = minute;
        if (!day.equals("00")) {
            drx = TypeDate.D;
            drnn = day;
        }
        if (!hour.equals("00")) {
            drx = TypeDate.H;
            drnn = hour;
        }
        if (!minute.equals("00")) {
            drx = TypeDate.N;
            drnn = minute;
        }

        // 有默认为000000的情况
        if (StringUtils.equalsIgnoreCase(drnn, "00")) {
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.D)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.H)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.N)) {
                drnn = "05";
            }
        }

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        int dataLength = 0;

        // 仅为找到电压
        String voltage = processSymbol38H(items);

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();

            if (label != null) {
                logger.info("field" + ByteUtil.toHexString(label) + "  value "
                        + ByteUtil.bcd2Str(value) + " byte "
                        + ByteUtil.bytesToUbyte(value) + " decimal " + decimal);
                if (label[0] == (byte) 0xF0) {
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {
                        case (byte) 0xF4:
                            //
                            // decimal = 1;

                            // --需要根据时间步长码，将数据写入到相对应的实时数据表中----------
                            // 如果时间步长码为分钟，将分钟数据转换为小时，写入小时雨量
                            // 如果时间步长码为小时，将小时数据直接写入
                            // 如果时间步长码为天，这个好像不存在。
                            dataLength = value.length;

                            // 此处取值应按照实际长度来取值，和判断小数点，
                            /**
                             * 如果是雨量，取一字节值，取一位小数 如果是水位，取两字节值，取两位小数
                             */
                            List<String> lines = new ArrayList<String>();
                            lines.add(stcd
                                    + " "
                                    + sdffull.format(curViewDate)
                                    + " "
                                    + ByteUtils
                                    .bytesToHexString(new byte[]{label[0]})
                                    + " " + drx + " " + drnn + " " + "pzl");
                            lines.add(ByteUtils.bytesToHexString(value));

                            String f4suffix = "pzl";
                            // 写固态数据写入到文件
                            MessageSynManager.getInstance().setStcd(stcd, viewDate);
                            MessageSynManager.getInstance().setStcd(
                                    stcd + "_suffix", f4suffix);
                            // 写入到文件
                            String callpath = RtuConfig.getMsgSsrPath();
                            String callFilename = callpath + "/" + stcd + "/"
                                    + f4suffix + "_" + viewDate + "_1.tmp";
                            FileUtils.writeLines(new File(callFilename), lines,
                                    false);
                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            dataLength = value.length;

                            // 此处取值应按照实际长度来取值，和判断小数点，
                            /**
                             * 如果是雨量，取一字节值，取一位小数 如果是水位，取两字节值，取两位小数
                             */
                            List<String> xlines = new ArrayList<String>();

                            // 第一行写入：
                            xlines.add(stcd
                                    + " "
                                    + sdffull.format(curViewDate)
                                    + " "
                                    + ByteUtils
                                    .bytesToHexString(new byte[]{label[0]})
                                    + " " + drx + " " + drnn + " " + "zwl");
                            xlines.add(ByteUtils.bytesToHexString(value));

                            // 写固态数据写入到文件
                            String f5suffix = "zwl";
                            MessageSynManager.getInstance().setStcd(stcd, viewDate);
                            MessageSynManager.getInstance().setStcd(
                                    stcd + "_suffix", f5suffix);
                            String callpath2 = RtuConfig.getMsgSsrPath();
                            String callFilename2 = callpath2 + "/" + stcd + "/"
                                    + f5suffix + "_" + viewDate + "_1.tmp";// ".zwl"
                            FileUtils.writeLines(new File(callFilename2), xlines,
                                    false);
                            break;

                        default:

                            String ditemvalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                ditemvalue = HydroBuilder.buildDot(ditemvalue,
                                        decimal);
                            }

                            // 如果是“当前降雨量（20）”，也存在临时表中
                            if (ByteUtil.toHexString(label).equals("20")) {
                                // this.writeDbFor1H(mid, serial, stcd, sttp,
                                // sendDatetime, curViewDate,
                                // ByteUtil.toHexString(label), ditemvalue);
                            }

                    }
                }
            }
        }

    }

    /**
     * 处理固态数据，将中间的数据写入到文件中
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processBody38Middle(String stcd, int seq, byte[] body)
            throws Exception {
        String callpath2 = RtuConfig.getMsgSsrPath();

        // 写固态数据写入到文件
        String viewDate = MessageSynManager.getInstance().getStcd(stcd);
        String suffix = MessageSynManager.getInstance().getStcd(
                stcd + "_suffix");

        if (viewDate != null) {
            String callFilename2 = callpath2 + "/" + stcd + "/" + suffix + "_" + viewDate
                    + "_" + seq + ".tmp";// ".zwl"
            FileUtils.writeStringToFile(new File(callFilename2),
                    ByteUtils.bytesToHexString(body));
        }

    }

    /**
     * 处理固态数据，根据最后一个数据合成最终固态数据
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processBody38End(String stcd, int seq, byte[] body)
            throws Exception {
        // 写入到文件
        String callpath = RtuConfig.getMsgSsrPath();

        // 写固态数据写入到文件
        String viewDate = MessageSynManager.getInstance().getStcd(stcd);
        String suffix = MessageSynManager.getInstance().getStcd(
                stcd + "_suffix");

        if (viewDate != null) {
            String saveFilename = callpath + "/" + stcd + "/" + suffix + "_" + viewDate + "_"
                    + seq + ".tmp";//
            FileUtils.writeStringToFile(new File(saveFilename),
                    ByteUtils.bytesToHexString(body));

            // ----------------------------------------------------------------------

            // 写入到文件

            // ----------------------------
            // 开始合并文件
            String readFilename = callpath + "/" + stcd + "/" + suffix + "_" + viewDate
                    + "_1.tmp";
            List<String> lines = FileUtils.readLines(new File(readFilename));
            List<String> newlines = new ArrayList<String>();
            if (lines.size() > 1) {
                String line1 = lines.get(0);
                String[] data = line1.split(" "); // stcd curViewDate flag drx
                // // drnn信息由这五部分组成

                String mStcd = data[0];
                String mViewDate = data[1];
                String mFlag = data[2];
                String mDrx = data[3];
                String mDrnn = data[4];
                String mSuffix = data[5];
                int mLength = 1;
                Date mxViewDate = sdffull.parse(mViewDate);
                int mDecimal = 1;
                // 添加水位、雨量的长度判断
                if (StringUtils.equalsIgnoreCase(mFlag, "F4")) {
                    mLength = 1;
                    mDecimal = 1;
                }
                if (StringUtils.equalsIgnoreCase(mFlag, "F5")) {
                    mLength = 2;
                    mDecimal = 2;
                }

                // 读取每一行
                for (int i = 1; i < lines.size(); i++) {
                    String tmpLine = lines.get(i);
                    byte[] tmpByte = ByteUtils.hexStringToBytes(tmpLine);// 先将字符串转换为字节

                    int dLength = tmpByte.length;
                    for (int ii = 0; ii < dLength / mLength; ii++) {

                        mxViewDate = HydroParser.processDate(mxViewDate, mDrx,
                                mDrnn);

                        byte[] temp = new byte[mLength];
                        System.arraycopy(tmpByte, ii * mLength, temp, 0,
                                mLength);

                        int intvalue = ByteUtil.bytesToUbyte(temp);

                        String fValue = Integer.toString(intvalue);
                        fValue = HydroBuilder.buildDot(fValue, mDecimal);

                        newlines.add(sdf2.format(mxViewDate) + " " + fValue);

                    }
                }

                // 开始读第二个文件到最后的文件
                for (int i = 2; i <= seq; i++) {
                    readFilename = callpath + "/" + stcd + "/" + suffix + "_" + viewDate + "_"
                            + i + ".tmp";

                    String tmpString = FileUtils.readFileToString(new File(
                            readFilename));
                    byte[] tmpByte = ByteUtils.hexStringToBytes(tmpString);// 先将字符串转换为字节

                    int dLength = tmpByte.length;
                    for (int ii = 0; ii < dLength / mLength; ii++) {

                        mxViewDate = HydroParser.processDate(mxViewDate, mDrx,
                                mDrnn);

                        byte[] temp = new byte[mLength];
                        System.arraycopy(tmpByte, ii * mLength, temp, 0,
                                mLength);

                        int intvalue = ByteUtil.bytesToUbyte(temp);

                        String fValue = Integer.toString(intvalue);
                        fValue = HydroBuilder.buildDot(fValue, mDecimal);

                        newlines.add(sdf2.format(mxViewDate) + " " + fValue);

                    }
                }// 读取完文件

                // 写入新的文件
                String newFilename = callpath + "/" + stcd + "/" + viewDate
                        + "." + mSuffix;
                FileUtils.writeLines(new File(newFilename), newlines);

                // 清空缓存
                MessageSynManager.getInstance().delStcd(stcd);
                MessageSynManager.getInstance().delStcd(stcd + "_serial");
                MessageSynManager.getInstance().delStcd(stcd + "_suffix");
            }

        }

    }

    /**
     * 处理固态数据，根据最后一个数据合成最终固态数据
     *
     * @param mid
     * @param body
     * @throws Exception
     */
    private void processBody38Single(String stcd, Up31Body body)
            throws Exception {
        // 写入到文件
        String callpath = RtuConfig.getMsgSsrPath();

        int serial = body.getSerialId();
        // String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        byte[] drxxx = body.getDrxxx();
        String dhm = ByteUtil.bcd2Str(drxxx);
        String day = dhm.substring(0, 2);
        String hour = dhm.substring(2, 4);
        String minute = dhm.substring(4, 6);
        String drx = TypeDate.N;
        String drnn = minute;
        if (!day.equals("00")) {
            drx = TypeDate.D;
            drnn = day;
        }
        if (!hour.equals("00")) {
            drx = TypeDate.H;
            drnn = hour;
        }
        if (!minute.equals("00")) {
            drx = TypeDate.N;
            drnn = minute;
        }

        // 有默认为000000的情况
        if (StringUtils.equalsIgnoreCase(drnn, "00")) {
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.D)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.H)) {
                drnn = "01";
            }
            if (StringUtils.equalsIgnoreCase(drx, TypeDate.N)) {
                drnn = "05";
            }
        }

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        // 仅为找到电压
        String voltage = processSymbol38H(items);

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();

            if (label != null) {
                logger.info("field" + ByteUtil.toHexString(label) + "  value "
                        + ByteUtil.bcd2Str(value) + " byte "
                        + ByteUtil.bytesToUbyte(value) + " decimal " + decimal);
                if (label[0] == (byte) 0xF0) {
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {
                        case (byte) 0xF4:

                            // --需要根据时间步长码，将数据写入到相对应的实时数据表中----------
                            // 如果时间步长码为分钟，将分钟数据转换为小时，写入小时雨量
                            // 如果时间步长码为小时，将小时数据直接写入
                            // 如果时间步长码为天，这个好像不存在。
                            // -------------------------------------------------
                            List<String> newlines = new ArrayList<String>();
                            String mFlag = ByteUtils
                                    .bytesToHexString(new byte[]{label[0]});
                            String mDrx = drx;
                            String mDrnn = drnn;
                            String mSuffix = "pzl";
                            int mLength = 1;
                            Date mxViewDate = curViewDate;
                            int mDecimal = 1;
                            // 添加水位、雨量的长度判断
                            if (StringUtils.equalsIgnoreCase(mFlag, "F4")) {
                                mLength = 1;
                                mDecimal = 1;
                            }
                            if (StringUtils.equalsIgnoreCase(mFlag, "F5")) {
                                mLength = 2;
                                mDecimal = 2;
                            }

                            // 读取每一行

                            int dLength = value.length;
                            for (int ii = 0; ii < dLength / mLength; ii++) {

                                mxViewDate = HydroParser.processDate(mxViewDate,
                                        mDrx, mDrnn);

                                byte[] temp = new byte[mLength];
                                System.arraycopy(value, ii * mLength, temp, 0,
                                        mLength);

                                int intvalue = ByteUtil.bytesToUbyte(temp);

                                String fValue = Integer.toString(intvalue);
                                fValue = HydroBuilder.buildDot(fValue, mDecimal);

                                newlines.add(sdf2.format(mxViewDate) + " " + fValue);

                            }
                            // 写入新的文件
                            String newFilename = callpath + "/" + stcd + "/"
                                    + viewDate + "." + mSuffix;
                            FileUtils.writeLines(new File(newFilename), newlines);
                            // -------------------------------------------------
                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            // 此处取值应按照实际长度来取值，和判断小数点，
                            List<String> f5lines = new ArrayList<String>();
                            String mf5Drx = drx;
                            String mf5Drnn = drnn;
                            String mf5Suffix = "zwl";
                            int mf5Length = 2;
                            Date mf5ViewDate = curViewDate;
                            int mf5Decimal = 2;

                            // 读取每一行

                            int df5Length = value.length;
                            for (int ii = 0; ii < df5Length / mf5Length; ii++) {

                                mf5ViewDate = HydroParser.processDate(mf5ViewDate,
                                        mf5Drx, mf5Drnn);

                                byte[] temp = new byte[mf5Length];
                                System.arraycopy(value, ii * mf5Length, temp, 0,
                                        mf5Length);

                                int intvalue = ByteUtil.bytesToUbyte(temp);

                                String fValue = Integer.toString(intvalue);
                                fValue = HydroBuilder.buildDot(fValue, mf5Decimal);

                                f5lines.add(sdf2.format(mf5ViewDate) + " " + fValue);

                            }
                            // 写入新的文件
                            String newf5Filename = callpath + "/" + stcd + "/"
                                    + viewDate + "." + mf5Suffix;
                            FileUtils.writeLines(new File(newf5Filename), f5lines);
                            break;

                        default:

                            String ditemvalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                ditemvalue = HydroBuilder.buildDot(ditemvalue,
                                        decimal);
                            }

                            // 如果是“当前降雨量（20）”，也存在临时表中
                            if (ByteUtil.toHexString(label).equals("20")) {
                                // this.writeDbFor1H(mid, serial, stcd, sttp,
                                // sendDatetime, curViewDate,
                                // ByteUtil.toHexString(label), ditemvalue);
                            }

                    }
                }
            }
        }

    }


    /**
     * 处理加报报数据,需要将加报报数据写入到加报报表中
     *
     * @param body
     */
    private void processBody33(String mid, String funccode, String channel,
                               Up30Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        // System.out.println("---------------------items.length " +
        // items.size());
        // 仅为找到电压
        String voltage = processSymbol38H(items);

        boolean pptnFlag = false;
        boolean riverFlag = false;
        String[] f4 = new String[12];
        String[] f5 = new String[12];
        String f26 = "", f20 = "", f1a = "", f37 = "", f39 = "", f10 = "", f11 = "", f13 = "", f0d = "",
                f06 = "", f07 = "", fa3 = "", fa4 = "", f02 = "", f18 = "", f45 = "", f27 = "", ff02 = "", ff03 = "", ff04 = "", ff05 = "", ff08 = "", ffc9 = "";
        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();
            String extend = item.getExtend();

            // 有的公司，出现发送多次时间的现象
            if (label != null) {

                if (label[0] == (byte) 0xF0) {
                    // System.out.println("---xxxxxx--------xxxxxxxxxxxx------xxxxxxxxx---xxxxxx--------------------------");
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {

                        case (byte) 0xF4:
                            decimal = 1;
                            pptnFlag = true;
                            // 5分钟时段数据，添加时间段
                            f4 = processSymbolF4H(curViewDate, items);

                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            riverFlag = true;
                            decimal = 2;
                            // 5分钟水位数据，添加时间段，这个地方得显示声明，因为有的水文站，即包括雨量数据也包括水位数据。
                            f5 = processSymbolF5H(curViewDate, items);

                            break;
                        case (byte) 0x26:
                            f26 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x20:
                            f20 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x1A:
                            f1a = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x37:
                            f37 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x39:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3C:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3B:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x10:
                            f10 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x11:
                            f11 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x13:
                            f13 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x0D:
                            f0d = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x06:
                            f06 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x07:
                            f07 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA3:
                            fa3 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA4:
                            fa4 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x45:
                            f45 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x02:
                            f02 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x18:
                            f18 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x27:
                            f27 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xFF:
                            byte[] b1 = new byte[value.length - 1];
                            System.arraycopy(value, 1, b1, 0, value.length - 1);
                            logger.info("extend FF" + extend);
                            switch (extend) {
                                case "02":
                                    ff02 = processSymbolValue(b1, decimal);
                                    break;
                                case "03":
                                    ff03 = processSymbolValue(b1, decimal);
                                    break;
                                case "04":
                                    ff04 = processSymbolValue(b1, decimal);
                                    break;
                                case "05":
                                    ff05 = processSymbolValue(b1, decimal);
                                    break;
                                case "C9":
                                    ffc9 = processSymbolValue(b1, decimal);
                                    break;
                                case "08":
                                    ff08 = processSymbolValue(b1, decimal);
                                    break;
                            }
                            break;
                        case (byte) 0x38:
                            // 写入电压数据到数据库
                            String voltagevalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                voltagevalue = HydroBuilder.buildDot(voltagevalue,
                                        decimal);
                            }

                            // this.writeRtuVoltage(stcd, sendDatetime, curViewDate,
                            // new Double(voltage).doubleValue());
                            break;
                    }
                }
            }

        }
        String intoF1aF20 = (String) Configurer.getStringProperty("intoF1aF20", "0");
        if (f1a.equals("")) {
            boolean elementAll = false;
            for (int i = 0; i < f4.length; i++) {
                if (f4[i] != null) {
                    elementAll = true;
                }
            }
            double temp = 0;
            for (int i = 0; i < f4.length; i++) {
                if (f4[i] != null && !f4[i].equals("FF") && !f4[i].equals("")) {
                    temp += Double.parseDouble(f4[i]);
                }
            }
            //计算时段雨量
            if (intoF1aF20.equals("1")) {
                if (elementAll) {
                    DecimalFormat df = new DecimalFormat("0.0");
                    if (temp > 0) {
                        f1a = df.format(temp);
                    } else {
                        f1a = String.valueOf(temp);
                    }
                }
            }
        }

        String intercept33 = (String) Configurer.getStringProperty("intercept33", "");
        if (StringUtils.isNotEmpty(intercept33)) {
            String[] intercept33Array = intercept33.split(";");
            for (String item : intercept33Array
            ) {
                String[] itemArray = item.split("-");
                Integer startStcd = Integer.parseInt(itemArray[0]);
                Integer endStcd = Integer.parseInt(itemArray[1]);
                Integer integerStcd = Integer.parseInt(stcd);
                if (integerStcd >= startStcd && integerStcd <= endStcd) {
                    return;
                }
            }
        }
        //加报存 分钟和秒都为零的情况 则设置 为下个小时的加报
        if (curViewDate.getMinutes() == 0 && curViewDate.getSeconds() == 0) {
            Calendar ca = Calendar.getInstance();
            Date tempDate = new Date(curViewDate.getYear(), curViewDate.getMonth(), curViewDate.getDate(), curViewDate.getHours(), 0, 0);
            ca.setTime(tempDate);
            ca.add(Calendar.HOUR_OF_DAY, 1);
            curViewDate = ca.getTime();
        }

        writeRtuHour(mid, stcd, funccode, curViewDate, sendDatetime, new Double(voltage).doubleValue(), channel, f4, f5, f26, f20, f1a, f37, f39, f10, f11, f13, f0d, f06, f07, fa3, fa4, f02, f18, f45, f27, ff02, ff03, ff04, ff05, ffc9, ff08);

    }

    /**
     * 处理定时报，目前以流量报数据为主（2016-12-09）
     *
     * @param body
     */
    private void processBody32(String mid, String funccode, String channel,
                               Up30Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List<DataItem> items = body.getItems();

        // System.out.println("---------------------items.length " +
        // items.size());
        // 仅为找到电压
        String voltage = processSymbol38H(items);

        boolean pptnFlag = false;
        boolean riverFlag = false;
        String[] f4 = new String[12];
        String[] f5 = new String[12];
        String f26 = "", f20 = "", f1a = "", f37 = "", f39 = "", f10 = "", f11 = "", f13 = "", f0d = "",
                f06 = "", f07 = "", fa3 = "", fa4 = "", f02 = "", f18 = "", f45 = "", f27 = "", ff02 = "", ff03 = "", ff04 = "", ff05 = "", ff08 = "", ffc9 = "";

        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();
            String extend = item.getExtend();

            // 有的公司，出现发送多次时间的现象
            if (label != null) {

                if (label[0] == (byte) 0xF0) {
                    // System.out.println("---xxxxxx--------xxxxxxxxxxxx------xxxxxxxxx---xxxxxx--------------------------");
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {

                        case (byte) 0xF4:
                            decimal = 1;
                            pptnFlag = true;
                            // 5分钟时段数据，添加时间段
                            f4 = processSymbolF4H(curViewDate, items);

                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            riverFlag = true;
                            decimal = 2;
                            // 5分钟水位数据，添加时间段，这个地方得显示声明，因为有的水文站，即包括雨量数据也包括水位数据。
                            f5 = processSymbolF5H(curViewDate, items);
                            break;
                        case (byte) 0x26:
                            f26 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x20:
                            f20 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x1A:
                            f1a = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x37:
                            f37 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x39:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3C:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3B:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x10:
                            f10 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x11:
                            f11 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x13:
                            f13 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x0D:
                            f0d = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x06:
                            f06 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x07:
                            f07 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA3:
                            fa3 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA4:
                            fa4 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x45:
                            f45 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x02:
                            f02 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x18:
                            f18 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x27:
                            f27 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xFF:
                            byte[] b1 = new byte[value.length - 1];
                            System.arraycopy(value, 1, b1, 0, value.length - 1);
                            logger.info("extend FF" + extend);
                            switch (extend) {
                                case "02":
                                    ff02 = processSymbolValue(b1, decimal);
                                    break;
                                case "03":
                                    ff03 = processSymbolValue(b1, decimal);
                                    break;
                                case "04":
                                    ff04 = processSymbolValue(b1, decimal);
                                    break;
                                case "05":
                                    ff05 = processSymbolValue(b1, decimal);
                                    break;
                                case "C9":
                                    ffc9 = processSymbolValue(b1, decimal);
                                    break;
                                case "08":
                                    ff08 = processSymbolValue(b1, decimal);
                                    break;
                            }
                            break;
                        case (byte) 0x38:
                            // 写入电压数据到数据库
                            String voltagevalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                voltagevalue = HydroBuilder.buildDot(voltagevalue,
                                        decimal);
                            }
                            // this.writeRtuVoltage(stcd, sendDatetime, curViewDate,
                            // new Double(voltage).doubleValue());
                            break;
                    }
                }
            }
        }

        String intoF1aF20 = (String) Configurer.getStringProperty("intoF1aF20", "0");
        if (f1a.equals("")) {
            boolean elementAll = false;
            for (int i = 0; i < f4.length; i++) {
                if (f4[i] != null) {
                    elementAll = true;
                }
            }
            double temp = 0;
            for (int i = 0; i < f4.length; i++) {
                if (f4[i] != null && !f4[i].equals("FF") && !f4[i].equals("")) {
                    temp += Double.parseDouble(f4[i]);
                }
            }
            //计算时段雨量
            if (intoF1aF20.equals("1")) {
                if (elementAll) {
                    DecimalFormat df = new DecimalFormat("0.0");
                    if (temp > 0) {
                        f1a = df.format(temp);
                    } else {
                        f1a = String.valueOf(temp);
                    }
                }
            }

        }
        logger.info("data F06:" + f06 + " F07:" + f07 + " fa3:" + fa3 + " fa4:" + fa4 + " f02:" + f02 + " f18:" + f18 + " f45:"
                + f45 + " f27:" + f27 +
                "  ff02:" + ff02 +
                "  ff03:" + ff03 +
                "  ff04:" + ff04 +
                "  ff05:" + ff05 +
                "  ff08:" + ff08 +
                "  ffc9:" + ffc9);
        writeRtuHour(mid, stcd, funccode, curViewDate, sendDatetime, new Double(voltage).doubleValue(), channel, f4, f5, f26, f20, f1a, f37, f39, f10, f11, f13, f0d, f06, f07, fa3, fa4, f02, f18, f45, f27, ff02, ff03, ff04, ff05, ffc9, ff08);
    }

    /**
     * 处理小时报数据
     *
     * @param body
     */
    private void processBody34(String mid, String funccode, String channel,
                               Up30Body body) throws Exception {
        if (null == body) {
            return;
        }
        String stcd = body.getStcd();

        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();

        Date curViewDate = null;
        //如果观察时间是05的小时报文，因协议原因，设置观察时间是观察时间的截止时间
        if (viewDate.substring(viewDate.length() - 2).equals("05")) {
            curViewDate = HydroParser.addMinYYMMDDHHmm(viewDate, 55);
        } else {
            curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
        }
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List<DataItem> items = body.getItems();
        // 仅为找到电压
        String voltage = processSymbol38H(items);

        boolean pptnFlag = false;
        boolean riverFlag = false;
        String[] f4 = new String[12];
        String[] f5 = new String[12];
        String f26 = "", f20 = "", f1a = "", f37 = "", f39 = "", f10 = "", f11 = "", f13 = "", f0d = "",
                f06 = "", f07 = "", fa3 = "", fa4 = "", f02 = "", f18 = "", f45 = "", f27 = "", ff02 = "", ff03 = "", ff04 = "", ff05 = "", ff08 = "", ffc9 = "";
        for (DataItem item : items) {

            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int length = item.getLength();
            int decimal = item.getDecimal();
            String extend = item.getExtend();
            // 有的公司，出现发送多次时间的现象
            if (label != null) {

                if (label[0] == (byte) 0xF0) {
                    // System.out.println("---xxxxxx--------xxxxxxxxxxxx------xxxxxxxxx---xxxxxx--------------------------");
                    viewDate = ByteUtil.bcd2Str(value);
                    curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
                } else {
                    switch (label[0]) {

                        case (byte) 0xF4:
                            decimal = 1;
                            pptnFlag = true;
                            // 5分钟时段数据，添加时间段
                            f4 = processSymbolF4H(curViewDate, items);
                            break;
                        case (byte) 0xF5:
                        case (byte) 0xF6:
                        case (byte) 0xF7:
                        case (byte) 0xF8:
                        case (byte) 0xF9:
                        case (byte) 0xFA:
                        case (byte) 0xFB:
                        case (byte) 0xFC:
                            riverFlag = true;
                            decimal = 2;
                            // 5分钟水位数据，添加时间段，这个地方得显示声明，因为有的水文站，即包括雨量数据也包括水位数据。
                            f5 = processSymbolF5H(curViewDate, items);

                            break;
                        case (byte) 0x26:
                            f26 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x20:
                            f20 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x1A:
                            f1a = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x37:
                            f37 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x39:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3C:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x3B:
                            f39 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x10:
                            f10 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x11:
                            f11 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x13:
                            f13 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x0D:
                            f0d = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x06:
                            f06 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x07:
                            f07 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA3:
                            fa3 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xA4:
                            fa4 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x45:
                            f45 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x02:
                            f02 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x18:
                            f18 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0x27:
                            f27 = processSymbolValue(value, decimal);
                            break;
                        case (byte) 0xFF:
                            byte[] b1 = new byte[value.length - 1];
                            System.arraycopy(value, 1, b1, 0, value.length - 1);
                            logger.info("extend FF" + extend);
                            switch (extend) {
                                case "02":
                                    ff02 = processSymbolValue(b1, decimal);
                                    break;
                                case "03":
                                    ff03 = processSymbolValue(b1, decimal);
                                    break;
                                case "04":
                                    ff04 = processSymbolValue(b1, decimal);
                                    break;
                                case "05":
                                    ff05 = processSymbolValue(b1, decimal);
                                    break;
                                case "C9":
                                    ffc9 = processSymbolValue(b1, decimal);
                                    break;
                                case "08":
                                    ff08 = processSymbolValue(b1, decimal);
                                    break;
                            }

                            break;
                        case (byte) 0x38:
                            // 写入电压数据到数据库
                            String voltagevalue = ByteUtil.bcd2Str(value);

                            if (decimal != 0) {
                                // 添加字符串
                                voltagevalue = HydroBuilder.buildDot(voltagevalue,
                                        decimal);
                            }

                            // this.writeRtuVoltage(stcd, sendDatetime, curViewDate,
                            // new Double(voltage).doubleValue());
                            break;
                    }
                }
            }
        }
        //判断雨量水位数据是否入临时表ST_RTSR_R。区别于江西与湖北，intofivedba是否入库临时表：1入库，0不入库
        String intofivedba = (String) Configurer.getStringProperty("intofivedba", "1");
        String intoF1aF20 = (String) Configurer.getStringProperty("intoF1aF20", "0");
        if (f1a.equals("")) {
            double temp = 0;
            for (int i = 0; i < f4.length; i++) {
                if (f4[i] != null && !f4[i].equals("FF") && !f4[i].equals("")) {
                    temp += Double.parseDouble(f4[i]);
                }
            }
            //计算时段雨量
            if (intoF1aF20.equals("1")) {

                DecimalFormat df = new DecimalFormat("0.0");
                if (temp > 0) {
                    f1a = df.format(temp);
                } else {
                    f1a = String.valueOf(temp);
                }
            }

        }
        //写入小时报
        writeRtuHour(mid, stcd, funccode, curViewDate, sendDatetime, new Double(voltage).doubleValue(), channel, f4, f5, f26, f20, f1a, f37, f39, f10, f11, f13, f0d, f06, f07, fa3, fa4, f02, f18, f45, f27, ff02, ff03, ff04, ff05, ffc9, ff08);
        ;
        //判断五分钟水位雨量时段数据是否入临时表ST_RTSR_R。
        if (intofivedba.equals("1")) {
            logger.info("----------------------开始写入五分钟数据----------------------");
            writeSL651FiveData(stcd, funccode, curViewDate, new Double(voltage).doubleValue(), channel, f4, f5);
            logger.info("----------------------结束写入五分钟数据----------------------");
        }

    }


    /**
     * 处理人工置数报，并将报文拆分处理
     *
     * @param body
     */
    private void processBody35(String mid, Up35Body body) throws Exception {
        int serial = body.getSerialId();

        // 发送时间格式YYMMDDHHmmSS
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        String rgzs = body.getRGZS();
    }

    /**
     * 处理图片报文，此方法在对于单条报文时
     *
     * @param body
     */
    private void processBody36(String mid, String funccode, String channel, Up36Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sttp = body.getSttp();
        String sendDate = body.getSendDate();
        String viewDate = body.getViewDate();
        byte[] picByte = body.getPic();

        List<DataItem> items = body.getItems();
        for (DataItem item : items) {
            picByte = item.getValue();
        }

        String fileName = stcd + "/" + viewDate + ".jpg";
        fileName = RtuConfig.getMsgImgPath() + "/" + fileName;
        if (picByte != null) {
            FileUtils.writeByteArrayToFile(new File(fileName), picByte);
        }
    }

    /*private void processBody40(String mid, Up40Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        BaseDao dao = SpringUtils.getBean("baseDao");

        StringBuffer sb = new StringBuffer("");
        List params = new ArrayList();

        params.add(sendDatetime);

        List<DataItem> items = body.getItems();
        for (DataItem item : items) {
            byte[] label = item.getLabel();
            byte[] value = item.getValue();
            int len = item.getLength();
            switch (label[0]) {
                case (byte) 0x01:
                    byte[] f01 = value;
                    int a1 = ByteUtil.bytesToUbyte(new byte[]{f01[0]});
                    int a2 = ByteUtil.bytesToUbyte(new byte[]{f01[1]});
                    int a3 = ByteUtil.bytesToUbyte(new byte[]{f01[2]});
                    int a4 = ByteUtil.bytesToUbyte(new byte[]{f01[3]});

                    params.add(a1 + "." + a2 + "." + a3 + "." + a4);
                    sb.append(" , f01 = ? ");
                    break;
                case (byte) 0x02:
                    params.add(StcdParser.parseStcd(value));
                    sb.append(" , f02 = ? ");
                    break;
                case (byte) 0x03:
                    params.add(ByteUtil.toHexString(value));
                    sb.append(" , f03 = ? ");
                    break;
                case (byte) 0x04:
                case (byte) 0x05:
                case (byte) 0x06:
                case (byte) 0x07:
                case (byte) 0x08:
                case (byte) 0x09:
                case (byte) 0x0A:
                case (byte) 0x0B:
                    String data4 = this.processD1(value);
                    params.add(data4);
                    sb.append(" , f" + ByteUtil.toHexString(label[0]) + " = ? ");
                    break;
                case (byte) 0x0C:
                    params.add(ByteUtil.bcd2Str(value));
                    sb.append(" , f0C = ? ");
                    break;
                case (byte) 0x0D:
                    params.add(ByteUtil.toBinaryString(value));
                    sb.append(" , f0D = ? ");
                    break;
                case (byte) 0x0E:
                    params.add(ByteUtil.toHexString(value));
                    sb.append(" , f0E = ? ");
                    break;
                case (byte) 0x0F:
                    byte[] cardData = value;
                    String cardType = new String(new byte[]{cardData[0]});
                    byte[] cardData2 = new byte[cardData.length - 1];
                    System.arraycopy(cardData, 1, cardData2, 0, cardData.length - 1);

                    params.add(cardType + "," + new String(cardData2));
                    sb.append(" , f0F = ? ");
                    break;
            }

        }

        params.add(stcd);

        System.out.println(">>>++++++++>>>>>>>+++++++ stcd " + stcd);

        String sql = "update rtu_station_40 set uptime = ? " + sb.toString()
                + " where stcd = ? ";

        logger.info(sql);

        dao.update(sql, params.toArray());
    }
*/

    /**
     * 6.6.4.14　中心站修改运行参数
     *
     * @param body
     */
    private void processBody42(String mid, Up42Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();
        StringBuffer sb = new StringBuffer();

        params.add(sendDatetime);

        List<DataItem> items = body.getItems();
        for (DataItem item : items) {
            byte[] value = item.getValue();
            byte[] label = item.getLabel();
            int length = item.getLength();
            int decimal = item.getDecimal();

            String itemvalue = ByteUtil.bcd2Str(value);

            logger.info("field" + ByteUtil.toHexString(label[0]) + "  value "
                    + itemvalue + " decimal " + decimal);

            if (decimal != 0) {
                // 添加字符串
                itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
            }
            // Map params = new HashMap();
            // params.put("stcd", stcd);
            // params.put("uptime", sendDate);
            // params.put("symbol", ByteUtil.toHexString(item.getLabel()));

            String field = "f" + ByteUtil.toHexString(label[0]);
            sb.append(" , " + field + " = ? ");
            params.add(itemvalue);

            // System.out.println(">> stcd " + stcd + " upteim " + sendDate
            // + " symbol " + ByteUtil.toHexString(item.getLabel())
            // + " itemvalue " + itemvalue);

        }

        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_bc set uptime = ? " + sb.toString()
                + " where stcd = ? ";

        logger.info(sql);

        dao.update(sql, params.toArray());

    }

    /**
     * 6.6.4.14　中心站修改运行参数
     *
     * @param body
     */
    private void processBody44(String mid, Up44Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();
        StringBuffer sb = new StringBuffer();

        params.add(sendDatetime);

        List<DataItem> items = body.getItems();
        for (DataItem item : items) {
            byte[] value = item.getValue();

            int len = value.length;
            int tempvalue = 0;
            if (len == 1) {
                tempvalue = ByteUtil.bytesToUbyte(value);
            } else if (len == 2) {
                tempvalue = ByteUtil.bytesToUshort(value);
            } else if (len == 4) {
                tempvalue = ByteUtil.bytesToInt(value);
            }

            int decimal = item.getDecimal();
            String itemvalue = HydroBuilder.buildDot("" + tempvalue, decimal);
            params.add(itemvalue);
            String field = "f" + ByteUtil.toHexString(item.getLabel());
            sb.append(" , " + field + " = ? ");
        }

        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_44 set uptime = ? " + sb.toString()
                + "  where stcd = ? ";
        logger.info(sql);
        dao.update(sql, params.toArray());

    }

    /**
     * 6.6.4.19　中心站查询遥测站软件版本
     *
     * @param body
     */
    private void processBody45(String mid, Up45Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();

        // params.add(sendDatetime);

        byte[] data = body.getData();
        if (data != null) {
            String version = new String(data);
            if (version.length() > 255) {
                version = version.substring(0, 254);
            }
            params.add(version);
        } else {
            params.add("");
        }

        params.add(stcd);

        try {
            BaseDao dao = (BaseDao) Application.getInstance()
                    .getBean("baseDao");

            String sql = "update rtu_station set version = ?  where stcd = ? ";
            logger.info(sql);
            dao.update(sql, params.toArray());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 6.6.4.20　中心站查询遥测站状态和报警信息
     *
     * @param body
     */
    private void processBody46(String mid, Up46Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();
        StringBuffer sb = new StringBuffer();

        params.add(sendDatetime);

        byte[] data = body.getData();
        String datastr = ByteUtil.toBinaryString(data);
        // System.out.println(">> datastr1 " + datastr);

        datastr = StringUtils.reverse(datastr);
        // System.out.println(">> datastr2 " + datastr);
        for (int i = 0, len = datastr.length(); i < len; i++) {
            String flag = datastr.substring(i, i + 1);
            String field = "f" + StringUtils.leftPad("" + i, 2, "0");
            sb.append(" , " + field + " = ? ");
            params.add(flag);
        }
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_46 set uptime = ? " + sb.toString()
                + " where stcd = ? ";
        logger.info(sql);
        dao.update(sql, params.toArray());

    }
/*
    private void processUpBaseBody(String func, UpBaseBody body)
            throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_ex set F" + func
                + " = ?  where stcd = ? ";

        dao.update(sql, params.toArray());
    }

 */

    /**
     * 6.6.4.19　中心站查询遥测站软件版本
     *
     * @param body
     */
    private void processBody47(String mid, Up47Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_ex set F47 = ?  where stcd = ? ";
        logger.info(sql);
        dao.update(sql, params.toArray());
    }

    /**
     * 6.6.4.19　中心站查询遥测站软件版本
     *
     * @param body
     */
    private void processBody48(String mid, Up48Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_ex set F48 = ?  where stcd = ? ";
        logger.info(sql);
        dao.update(sql, params.toArray());
    }

    /**
     * 6.6.4.23　修改密码
     *
     * @param body
     */
    private void processBody49(String mid, Up49Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);
        params.add(ByteUtil.toHexString(body.getPwd()));
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");
        String sql = "update rtu_station_ex set F49 = ? ,F49D = ?  where stcd = ? ";
        dao.update(sql, params.toArray());

        // server.getStationManager().updatePwd(stcd,
        // ByteUtil.toHexString(body.getPwd()));

    }

    /**
     * 6.6.4.24　设置遥测站时钟
     *
     * @param body
     */
    private void processBody4A(String mid, UpBaseBody body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_ex set F4A = ?  where stcd = ? ";

        dao.update(sql, params.toArray());
    }

    /**
     * 6.6.4.25　设置遥测站IC卡状态
     *
     * @param body
     */
    private void processBody4B(String mid, Up46Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);

        String binstr = ByteUtil.toBinaryString(body.getData());
        binstr = StringUtils.reverse(binstr);
        String status = binstr.substring(9, 10);
        params.add(status);
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_ex set F4B = ? , F4BD = ? where stcd = ? ";

        dao.update(sql, params.toArray());
    }

    /**
     * 6.6.4.26　控制水泵开关命令/水泵状态自报
     *
     * @param body
     */
    private void processBody4C(String mid, Up4CBody body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();
        StringBuffer sb = new StringBuffer();

        params.add(sendDatetime);

        byte[] data = body.getData();
        String datastr = ByteUtil.toBinaryString(data);
        sb.append(" , binstr = ? ");
        params.add(datastr);

        datastr = StringUtils.reverse(datastr);
        for (int i = 0, len = datastr.length(); i < len; i++) {
            String flag = datastr.substring(i, i + 1);
            String field = "D" + i;
            sb.append(" , " + field + " = ? ");
            params.add(flag);
        }

        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_4c set uptime = ? " + sb.toString()
                + " where stcd = ? ";

        dao.update(sql, params.toArray());

    }

    /**
     * 6.6.4.27　控制阀门开关命令/阀门状态信息自报
     *
     * @param body
     */
    private void processBody4D(String mid, Up4CBody body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();
        StringBuffer sb = new StringBuffer();

        params.add(sendDatetime);

        byte[] data = body.getData();
        String datastr = ByteUtil.toBinaryString(data);
        sb.append(" , binstr = ? ");
        params.add(datastr);

        datastr = StringUtils.reverse(datastr);
        for (int i = 0, len = datastr.length(); i < len; i++) {
            String flag = datastr.substring(i, i + 1);
            String field = "D" + i;
            sb.append(" , " + field + " = ? ");
            params.add(flag);
        }

        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_4d set uptime = ? " + sb.toString()
                + " where stcd = ? ";

        dao.update(sql, params.toArray());

    }

    /**
     * 6.6.4.28　控制闸门开关命令/闸门状态信息自报
     *
     * @param body
     */
    private void processBody4E(String mid, Up4EBody body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();
        StringBuffer sb = new StringBuffer();

        params.add(sendDatetime);

        int count = body.getCount();
        byte[] data = body.getData();
        String datastr = ByteUtil.toBinaryString(data);
        datastr = StringUtils.reverse(datastr);
        for (int i = 0, len = datastr.length(); i < len; i++) {
            String flag = datastr.substring(i, i + 1);
            String field = "D" + i;
            sb.append(" , " + field + " = ? ");
            params.add(flag);
        }

        byte[] kd = body.getKd();

        for (int i = 0, len = count; i < len; i++) {
            byte[] kddata = new byte[2];
            System.arraycopy(kd, i * 2, kddata, 0, 2);

            String field = "D" + i + "H";
            sb.append(" , " + field + " = ? ");
            params.add(ByteUtil.bcd2Str(kddata));
        }

        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_4e set uptime = ? " + sb.toString()
                + " where stcd = ? ";
        logger.info(sql);
        dao.update(sql, params.toArray());

    }

    /**
     * 6.6.4.30　中心站查询遥测站事件记录
     *
     * @param body
     */
    private void processBody50(String mid, Up50Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

        List params = new ArrayList();
        StringBuffer sb = new StringBuffer();

        params.add(sendDatetime);

        byte[] erc = body.getErc(); // 64位

        for (int i = 0; i < 32; i++) {
            byte[] ercdata = new byte[2];
            System.arraycopy(erc, i * 2, ercdata, 0, 2);
            String field = "ERC" + (i + 1);
            sb.append(" , " + field + " = ? ");
            params.add(ByteUtil.bytesToUshort(ercdata));

        }

        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_50 set uptime = ? " + sb.toString()
                + " where stcd = ? ";

        dao.update(sql, params.toArray());
    }

    /**
     * 6.6.4.25　设置遥测站IC卡状态
     *
     * @param body
     */
    private void processBody4F(String mid, Up4FBody body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);

        byte data = body.getData();
        params.add(ByteUtil.toHexString(new byte[]{data}));
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");
        String sql = "update rtu_station_ex set F4F = ? , F4FD = ? where stcd = ? ";
        dao.update(sql, params.toArray());
    }

    /**
     * 6.6.4.31　中心站查询遥测站时钟
     *
     * @param body
     */
    private void processBody51(String mid, Up51Body body) throws Exception {
        int serial = body.getSerialId();
        String stcd = body.getStcd();
        String sendDate = body.getSendDate();
        Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
        List params = new ArrayList();

        params.add(sendDatetime);
        params.add(stcd);

        BaseDao dao = SpringUtils.getBean("baseDao");

        String sql = "update rtu_station_ex set F51 = ?  where stcd = ? ";

        dao.update(sql, params.toArray());
    }

    /**
     * 解析电压值--标识符38
     *
     * @param items
     * @return
     */
    public String processSymbol38H(List<DataItem> items) {
        String voltage = "";
        for (DataItem item : items) {
            byte[] label = item.getLabel();
            byte[] value = item.getValue();

            int length = item.getLength();
            int decimal = item.getDecimal();

            if (label != null) {
                // 找到此报文中的电压值
                if (label[0] == (byte) 0x38) {

                    voltage = ByteUtil.bcd2Str(value);

                    if (decimal != 0) {
                        // 添加字符串
                        voltage = HydroBuilder.buildDot(voltage, decimal);
                    }

                }
            }
        }

        return StringUtils.isNotBlank(voltage) ? voltage : "0";
    }

    /**
     * 解析1小时的雨量数据
     *
     * @param items
     * @return
     */
    public String[] processSymbolF4H(Date startDate, List<DataItem> items) {
        String voltage = "";

        String[] d12 = new String[12];

        Date curViewDate = startDate;

        for (DataItem item : items) {
            byte[] label = item.getLabel();
            byte[] value = item.getValue();

            int length = item.getLength();
            int decimal = item.getDecimal();

            if (label != null) {
                // 找到此报文中的电压值
                if (label[0] == (byte) 0xF4) {

                    decimal = 1;
                    // 5分钟时段数据，添加时间段
                    BigDecimal total = new BigDecimal(0);

                    for (int i = 0, len = value.length; i < len; i++) {
                        int intvalue = ByteUtil
                                .bytesToUbyte(new byte[]{value[i]});

                        if (intvalue == 255) {
                            d12[i] = "FF";
                        } else {
                            String itemvalue = Integer.toString(intvalue);
                            itemvalue = HydroBuilder.buildDot(itemvalue,
                                    decimal);
                            d12[i] = itemvalue;

                            // 将每5分钟的雨量相加，得出一小时的雨量
                            BigDecimal m5 = new BigDecimal(itemvalue);
                            total = total.add(m5);
                        }

                        curViewDate = DateUtil.addMinutes(curViewDate, 5);

                    }
                }
            }
        }
        return d12;
    }

    /**
     * 解析一小时的水位数据
     *
     * @param items
     * @return
     */
    public String[] processSymbolF5H(Date startDate, List<DataItem> items) {
        String[] dh12 = new String[12];

        Date curViewDate = startDate;
        for (DataItem item : items) {
            byte[] label = item.getLabel();
            byte[] value = item.getValue();

            int length = item.getLength();
            int decimal = item.getDecimal();

            if (label != null) {
                // 找到此报文中的电压值
                if (label[0] == (byte) 0xF5) {

                    decimal = 2;
                    // 5分钟水位数据，添加时间段
                    int pos = 0;
                    for (int i = 0, len = value.length; i < len; i = i + 2) {
                        int intvalue = ByteUtil.bytesToUshort(new byte[]{
                                value[i], value[i + 1]});

                        if (intvalue == 65535) {
                            dh12[pos] = "FFFF";
                        } else {
                            String itemvalue = Integer.toString(intvalue);
                            itemvalue = HydroBuilder.buildDot(itemvalue,
                                    decimal);
                            dh12[pos] = itemvalue;
                        }
                        curViewDate = DateUtil.addMinutes(curViewDate, 5);

                        pos++;
                    }

                }
            }
        }
        return dh12;
    }

    public String processSymbolValue(byte[] value, int decimal) {
        // 判断是否有非法数据的，如果数组中全为FF，则为非法数据
        boolean flag = false;
        boolean neg = false;

        // 判断是否是正常数据，如果全为FF，则为非法数据
        for (int i = 0, len = value.length; i < len; i++) {
            int intvalue = ByteUtil.bytesToUbyte(new byte[]{value[i]});
            if (intvalue != 255) {
                flag = true;
                break;
            }
        }

        if (flag) {

            if (value[0] == (byte) 0xFF) {
                neg = true;
                value[0] = (byte) 0x00;// 第一个值为FF表示为负数
            }

            String itemvalue = ByteUtil.bcd2Str(value);

            if (decimal != 0) {
                // 添加字符串
                if (isInt(itemvalue)) {
                    itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
                } else {
                    return itemvalue;
                }
            }

            if (neg) {
                itemvalue = "-" + itemvalue;
            }

            return itemvalue;

            // this.writeRtuSymbol(mid, stcd, sttp, sendDate,
            // viewDate, ByteUtil.toHexString(label),
            // itemvalue);

        } else {
            return ByteUtil.byteToHexString(value);
        }
    }

    private static boolean isInt(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void main(String[] arg) {
        //writePicToDir("d:/a.log","ddd");
        //revAllPic(36,"D:\\rcvApp\\img\\temp","1707271442");
        byte[] value = new byte[2];
        value[0] = (byte) 0xff;
        if (value[0] == (byte) 0xFF) {
            System.out.println(">> OK1");
        }

        if (value[0] == 0xFF) {
            System.out.println(">> OK2");
        }
        // String dirName = "c:\\rcvtemp\\";
        // List<File> files = Arrays.asList(new File(dirName).listFiles());
        // Collections.sort(files, new Comparator<File>() {
        // @Override
        // public int compare(File s1, File s2) {
        // if (returnDouble(s1) < returnDouble(s2))
        // return -1;
        // else if (returnDouble(s1) > returnDouble(s2))
        // return 1;
        // else
        // return 0;
        // }
        //
        // public double returnDouble(File file) {
        // String fileName = file.getName();
        // int lastPos = fileName.lastIndexOf(".");
        // fileName = fileName.substring(0, lastPos);
        // if (fileName.indexOf("_") != -1) {
        // String[] fn = fileName.split("_");
        //
        // return Double.parseDouble(fn[fn.length - 1]);
        // } else {
        // return 0;
        // }
        //
        // }
        //
        // });
        //
        // String newFile = "c:\\aaa.gif";
        //
        // MessageConsumer xxx = new MessageConsumer(null);
        // FileOutputStream out = null;
        // try {
        // out = new FileOutputStream(new File("c:" + File.separator
        // + "rtu_recv.jpg"), false);
        //
        // for (File f : files) {
        // System.out.println("f.getname " + f.getName());
        // IMessageBody body = (IMessageBody) FileUtil.readObject(f
        // .getPath());
        // try {
        // Up30Body body30 = new HexParser().parse30Body(body);
        //
        // List<DataItem> items = body30.getItems();
        // for (DataItem item : items) {
        // out.write(item.getValue());
        // }
        //
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        //
        // // System.out.println(f.getPath() + File.separator +
        // // f.getName());
        // }
        //
        // } catch (FileNotFoundException e1) {
        // // TODO Auto-generated catch block
        // e1.printStackTrace();
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // } finally {
        // try {
        // out.close();
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // }

        // String message =
        // "7E7E0020320500FFFFFF4A800802000114061408373305ACB0";
        // message = message.substring(0, 32);
        // System.out.println(">. " + message);
        // // //+ message.substring(44);
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // String sendtime = "2014-06-17 11:00:00";
        // java.util.Date send;
        // try {
        // send = sdf.parse(sendtime);
        //
        // java.util.Date now = new Date();
        //
        // long diff = now.getTime() - send.getTime();
        // if (diff / HOUR > 6) {
        // System.out.println(">>>>>" + diff / HOUR);
        // }
        // } catch (ParseException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        String msg = "7e7e010000000002000137002a0200a6170808145700f1f100000000024bf0f0170808145720190002302619000230391a0004713812135003853f";

        CommonMessage message = Sl651MessageDecoder.decodeHex(ByteUtils
                .hexStringToBytes(msg));

        MessageSl651Consumer consumer = new MessageSl651Consumer();
        try {
            consumer.processMessage("GPRS", message, "", "", 0, 0);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String hexstr = "79 61 6e 79 75 31 39 38 38 3a 59 43 5a 2d 32 41 2d 31 30 31 21 21 21 ";
        hexstr = "7e 7e 00 00 22 23 33 16 ff ff 2f 00 08 02 00 2c 17 07 20 14 08 35 03 1b 08";
        hexstr = StringUtils.replace(hexstr, " ", "");
        System.out.println(">> 35 " + new String(ByteUtils.hexStringToBytes(hexstr)));
    }

}
