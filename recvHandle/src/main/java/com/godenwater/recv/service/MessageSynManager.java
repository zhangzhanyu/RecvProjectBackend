package com.godenwater.recv.service;

import java.util.HashMap;
import java.util.Map;

public class MessageSynManager {

	/**
	 * 存储召测消息的MAP
	 */
	private Map<String, String> stcdMap = new HashMap<String, String>();

	// Wrap this guy up so we can mock out the UserManager class.

	private static MessageSynManager instance = new MessageSynManager();

	private MessageSynManager() {

	}

	/**
	 * Returns a singleton UserManager instance.
	 * 
	 * @return a UserManager instance.
	 */
	public static MessageSynManager getInstance() {
		return instance;
	}

	public String getStcd(String stcd) {
		return stcdMap.get(stcd);
	}

	public String setStcd(String stcd, String dt) {
		return stcdMap.put(stcd, dt);
	}

	public String delStcd(String stcd) {
		return stcdMap.remove(stcd);
	}

}
