package com.godenwater.recv.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.godenwater.framework.config.SpringUtils;
import com.godenwater.recv.server.sl651.HydroConfig;
import com.godenwater.recv.utils.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.godenwater.core.spring.Application;
import com.godenwater.core.spring.BaseDao;

import cn.gov.mwr.sl330.*;
import cn.gov.mwr.sl651.HydroBuilder;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.body.*;
import cn.gov.mwr.sl651.utils.ByteUtil;
import cn.gov.mwr.sl651.utils.DateUtil;
import cn.gov.mwr.sl651.utils.StcdParser;

/**
 * RTU数据入库类
 * 
 * @ClassName: MessageConsumer
 * @Description: TODO 通过线程的方式将“报文”入库。
 * 
 *               注意：只启动一个线程
 * @author lipujun
 * @date Mar 14, 2013
 * 
 */
public class RtuDatabaseConsumer implements Runnable {

	private static Logger logger = LoggerFactory
			.getLogger(RtuDatabaseConsumer.class);

	private BaseDao dao = (BaseDao) Application.getInstance()
			.getBean("baseDao");

	public RtuDatabaseConsumer() {
	}

	@Override
	public void run() {

		while (true) {
			// System.out.println("RtuDatabaseConsumer....");
//			Serial2Body body = (Serial2Body) server.queueDatabasePoll();
//			if (body != null) {
//				try {
//					// process(body);
//				} catch (Exception e) {
//
//					logger.error("处理线程出现异常：" + e.getMessage());
//					e.printStackTrace();
//				}
//			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 处理业务逻辑
	 * 
	 * @param message
	 */
	private void process(String pid,String func,IMessageBody body) throws Exception {
		logger.info(">> 入库报文处理！\t队列容量：<" + "" + ">");

		// HydroServer.getInstance().getLogManager().addLog(
		// "" + func + "号报文开始入库.....");

		if (body instanceof Up44Body) {
			// case 0x45: // 6.6.4.19　中心站查询遥测站软件版本
			Up44Body body44 = (Up44Body) body;
			this.processBody44(pid, body44);
		} else if (body instanceof Up31Body) {
			Up31Body body30 = (Up31Body) body;
			this.processBody31(pid, body30);
		} else if (body instanceof Up30Body) {
			// case 0x30: // 测试报
			// case 0x32: // 定时报
			// case 0x33: // 加报报
			// case 0x34: // 小时报，以1小时为基本单位向中心站报送遥测站水文信息
			Up30Body body30 = (Up30Body) body;
			this.processBody30(pid, body30);

		} else if (body instanceof Up35Body) {
			// case 0x35: // 人工置数报，报文内容为ASC码
			Up35Body body35 = (Up35Body) body;
			this.processBody35(pid, body35);

		} else if (body instanceof Up36Body) {
			// case 0x36: // 图片报
			Up36Body body36 = (Up36Body) body;
			this.processBody36(pid, body36);
		} else if (body instanceof Up40Body) {
			// case 0x40: // 6.6.4.14　中心站修改遥测站基本配置表
			Up40Body body40 = (Up40Body) body;
			this.processBody40(pid, body40);
		} else if (body instanceof Up42Body) {
			// case 0x36: // 6.6.4.16　中心站修改遥测站运行参数配置表
			Up42Body body42 = (Up42Body) body;
			this.processBody42(pid, body42);
		} else if (body instanceof Up45Body) {
			// case 0x45: // 6.6.4.19　中心站查询遥测站软件版本
			Up45Body body45 = (Up45Body) body;
			this.processBody45(pid, body45);
		} else if (body instanceof Up46Body) {
			// case 0x46: //6.6.4.20　中心站查询遥测站状态和报警信息
			Up46Body body46 = (Up46Body) body;
			if (func.equalsIgnoreCase("46")) {

				this.processBody46(pid, body46);
			} else {
				this.processBody4B(pid, body46);
			}
		} else if (body instanceof Up49Body) {
			// case 0x49: //6.6.4.23　修改密码
			Up49Body body49 = (Up49Body) body;
			this.processBody49(pid, body49);
		} else if (body instanceof Up4ABody) {
			// case 0x4C: //6.6.4.26　控制水泵开关命令/水泵状态自报
			Up4ABody body4A = (Up4ABody) body;
			this.processBody4A(pid, body4A);
		} else if (body instanceof Up4BBody) {
			// case 0x4C: //6.6.4.26　控制水泵开关命令/水泵状态自报
			Up4BBody body4B = (Up4BBody) body;
			this.processBody4B(pid, body4B);
		} else if (body instanceof Up4CBody) {
			// case 0x4C: //6.6.4.26　控制水泵开关命令/水泵状态自报

			Up4CBody body4c = (Up4CBody) body;
			if (func.equalsIgnoreCase("4C")) {
				this.processBody4C(pid, body4c);
			} else {
				this.processBody4D(pid, body4c);
			}

			Up4CBody body4C = (Up4CBody) body;
			this.processBody4C(pid, body4C);
		} else if (body instanceof Up4DBody) {
			// case 0x4D: //6.6.4.27　控制阀门开关命令/阀门状态信息自报
			Up4DBody body4D = (Up4DBody) body;
			this.processBody4D(pid, body4D);
		} else if (body instanceof Up4EBody) {
			// case 0x4E: //6.6.4.28　控制闸门开关命令/闸门状态信息自报
			Up4EBody body4E = (Up4EBody) body;
			this.processBody4E(pid, body4E);
		} else if (body instanceof Up4FBody) {
			// case 0x4E: //6.6.4.28　控制闸门开关命令/闸门状态信息自报
			Up4FBody body4F = (Up4FBody) body;
			this.processBody4F(pid, body4F);
		} else if (body instanceof Up50Body) {
			// case 0x50: //6.6.4.30　中心站查询遥测站事件记录
			Up50Body body50 = (Up50Body) body;
			this.processBody50(pid, body50);
		} else if (body instanceof UpBaseBody) {
			// case 0x47: // 6.6.4.21　初始化固态存储数据
			// case 0x48: // 6.6.4.22　恢复遥测站出厂设置
			// case 0x4A: // 6.6.4.24　设置遥测站时钟
			// case 0x51: // 6.6.4.31　中心站查询遥测站时钟
			this.processUpBaseBody(func, (UpBaseBody) body);

		}

		// IMessageHeader header = message.getHeader();

		// break;
		// case 0x37: // 查询遥测站实时数据
		// case 0x39: // 查询人工置数报
		// case 0x44: // 查询水泵电机实时工作数据
		// case 0x45: // 查询遥测站软件版本
		// case 0x46: // 查询遥测站状态和报警信息
		// case 0x47: // 初始化固态存储数据
		// case 0x48: // 恢复遥测站出厂设置
		// case 0x4A: // 设置遥测站时钟
		// case 0x50: // 中心站查询遥测站事件记录
		// case 0x51: // 中心站查询遥测站时钟
		// break;
		// }
		// }

	}

	/**
	 * 6.6.4.14　中心站修改遥测站基本配置表
	 * 
	 * @param body
	 */
	private String processD1(byte[] data) {
		StringBuffer sb = new StringBuffer();
		int flag = ByteUtil.bytesToUbyte(new byte[] { data[0] });// 信道类型用1字节BCD码：1-短信，2-IPV4，3-北斗，4-海事卫星，5-PSTN，6-超短波。
		if (flag == 0) {
			sb.append("0");
		} else if (flag == 2) {
			StringBuffer sbdata = new StringBuffer();
			sbdata.append(flag).append(",");
			sbdata.append(
					ByteUtil.bcd2Str(new byte[] { data[1], data[2], data[3],
							data[4], data[5], data[6] })).append(",");
			sbdata.append(ByteUtil.bcd2Str(new byte[] { data[7], data[8],
					data[9] }));
			sb.append(sbdata.toString());
		} else {
			byte[] newvalue = new byte[data.length - 1];
			System.arraycopy(data, 1, newvalue, 0, newvalue.length);
			sb.append(ByteUtil.bcd2Str(newvalue));
		}
		return sb.toString();
	}

	private void processBody40(String mid, Up40Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		BaseDao dao = SpringUtils.getBean("baseDao");

		StringBuffer sb = new StringBuffer("");
		List params = new ArrayList();

		params.add(sendDatetime);

		List<DataItem> items = body.getItems();
		for (DataItem item : items) {
			byte[] label = item.getLabel();
			byte[] value = item.getValue();
			int len = item.getLength();
			switch (label[0]) {
			case (byte) 0x01:
				byte[] f01 = value;
				int a1 = ByteUtil.bytesToUbyte(new byte[] { f01[0] });
				int a2 = ByteUtil.bytesToUbyte(new byte[] { f01[1] });
				int a3 = ByteUtil.bytesToUbyte(new byte[] { f01[2] });
				int a4 = ByteUtil.bytesToUbyte(new byte[] { f01[3] });

				params.add(a1 + "." + a2 + "." + a3 + "." + a4);
				sb.append(" , f01 = ? ");
				break;
			case (byte) 0x02:
				params.add(StcdParser.parseStcd(value));
				sb.append(" , f02 = ? ");
				break;
			case (byte) 0x03:
				params.add(ByteUtil.toHexString(value));
				sb.append(" , f03 = ? ");
				break;
			case (byte) 0x04:
			case (byte) 0x05:
			case (byte) 0x06:
			case (byte) 0x07:
			case (byte) 0x08:
			case (byte) 0x09:
			case (byte) 0x0A:
			case (byte) 0x0B:
				String data4 = this.processD1(value);
				params.add(data4);
				sb.append(" , f" + ByteUtil.toHexString(label[0]) + " = ? ");
				break;
			case (byte) 0x0C:
				params.add(ByteUtil.bcd2Str(value));
				sb.append(" , f0C = ? ");
				break;
			case (byte) 0x0D:
				params.add(ByteUtil.toBinaryString(value));
				sb.append(" , f0D = ? ");
				break;
			case (byte) 0x0E:
				params.add(ByteUtil.toHexString(value));
				sb.append(" , f0E = ? ");
				break;
			case (byte) 0x0F:
				byte[] cardData = value;
				String cardType = new String(new byte[] { cardData[0] });
				byte[] cardData2 = new byte[cardData.length - 1];
				System.arraycopy(cardData, 1, cardData2, 0, cardData.length - 1);

				params.add(cardType + "," + new String(cardData2));
				sb.append(" , f0F = ? ");
				break;
			}

		}

		params.add(stcd);

		System.out.println(">>>++++++++>>>>>>>+++++++ stcd " + stcd);

		String sql = "update rtu_station_40 set uptime = ? " + sb.toString()
				+ " where stcd = ? ";

		logger.info(sql);

		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.14　中心站修改运行参数
	 * 
	 * @param body
	 */
	private void processBody42(String mid, Up42Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();
		StringBuffer sb = new StringBuffer();

		params.add(sendDatetime);

		List<DataItem> items = body.getItems();
		for (DataItem item : items) {
			byte[] value = item.getValue();
			byte[] label = item.getLabel();
			int length = item.getLength();
			int decimal = item.getDecimal();

			String itemvalue = ByteUtil.bcd2Str(value);

			logger.info("field" + ByteUtil.toHexString(label[0]) + "  value "
					+ itemvalue + " decimal " + decimal);

			if (decimal != 0) {
				// 添加字符串
				itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
			}
			// Map params = new HashMap();
			// params.put("stcd", stcd);
			// params.put("uptime", sendDate);
			// params.put("symbol", ByteUtil.toHexString(item.getLabel()));

			String field = "f" + ByteUtil.toHexString(label[0]);
			sb.append(" , " + field + " = ? ");
			params.add(itemvalue);

			// System.out.println(">> stcd " + stcd + " upteim " + sendDate
			// + " symbol " + ByteUtil.toHexString(item.getLabel())
			// + " itemvalue " + itemvalue);

		}

		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_42 set uptime = ? " + sb.toString()
				+ " where stcd = ? ";

		logger.info(sql);

		dao.update(sql, params.toArray());

	}

	/**
	 * 6.6.4.14　中心站修改运行参数
	 * 
	 * @param body
	 */
	private void processBody44(String mid, Up44Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();
		StringBuffer sb = new StringBuffer();

		params.add(sendDatetime);

		List<DataItem> items = body.getItems();
		for (DataItem item : items) {
			byte[] value = item.getValue();

			int len = value.length;
			int tempvalue = 0;
			if (len == 1) {
				tempvalue = ByteUtil.bytesToUbyte(value);
			} else if (len == 2) {
				tempvalue = ByteUtil.bytesToUshort(value);
			} else if (len == 4) {
				tempvalue = ByteUtil.bytesToInt(value);
			}

			int decimal = item.getDecimal();
			String itemvalue = HydroBuilder.buildDot("" + tempvalue, decimal);
			params.add(itemvalue);
			String field = "f" + ByteUtil.toHexString(item.getLabel());
			sb.append(" , " + field + " = ? ");
		}

		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_44 set uptime = ? " + sb.toString()
				+ "  where stcd = ? ";
		logger.info(sql);
		dao.update(sql, params.toArray());

	}

	/**
	 * 6.6.4.19　中心站查询遥测站软件版本
	 * 
	 * @param body
	 */
	private void processBody45(String mid, Up45Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();

		params.add(sendDatetime);

		byte[] data = body.getData();
		if (data != null) {
			String version = new String(data);
			if (version.length() > 255) {
				version = version.substring(0, 254);
			}
			params.add(version);
		} else {
			params.add("");
		}

		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_ex set F45 = ? ,F45D = ?  where stcd = ? ";
		logger.info(sql);
		dao.update(sql, params.toArray());

	}

	/**
	 * 6.6.4.20　中心站查询遥测站状态和报警信息
	 * 
	 * @param body
	 */
	private void processBody46(String mid, Up46Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();
		StringBuffer sb = new StringBuffer();

		params.add(sendDatetime);

		byte[] data = body.getData();
		String datastr = ByteUtil.toBinaryString(data);
		// System.out.println(">> datastr1 " + datastr);

		datastr = StringUtils.reverse(datastr);
		// System.out.println(">> datastr2 " + datastr);
		for (int i = 0, len = datastr.length(); i < len; i++) {
			String flag = datastr.substring(i, i + 1);
			String field = "f" + StringUtils.leftPad("" + i, 2, "0");
			sb.append(" , " + field + " = ? ");
			params.add(flag);
		}
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_46 set uptime = ? " + sb.toString()
				+ " where stcd = ? ";
		logger.info(sql);
		dao.update(sql, params.toArray());

	}

	private void processUpBaseBody(String func, UpBaseBody body)
			throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_ex set F" + func
				+ " = ?  where stcd = ? ";

		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.19　中心站查询遥测站软件版本
	 * 
	 * @param body
	 */
	private void processBody47(String mid, Up47Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_ex set F47 = ?  where stcd = ? ";
		logger.info(sql);
		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.19　中心站查询遥测站软件版本
	 * 
	 * @param body
	 */
	private void processBody48(String mid, Up48Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_ex set F48 = ?  where stcd = ? ";
		logger.info(sql);
		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.23　修改密码
	 * 
	 * @param body
	 */
	private void processBody49(String mid, Up49Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);
		params.add(ByteUtil.toHexString(body.getPwd()));
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");
		String sql = "update rtu_station_ex set F49 = ? ,F49D = ?  where stcd = ? ";
		dao.update(sql, params.toArray());

		// server.getStationManager().updatePwd(stcd,
		// ByteUtil.toHexString(body.getPwd()));

	}

	/**
	 * 6.6.4.24　设置遥测站时钟
	 * 
	 * @param body
	 */
	private void processBody4A(String mid, Up4ABody body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_ex set F4A = ?  where stcd = ? ";

		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.25　设置遥测站IC卡状态
	 * 
	 * @param body
	 */
	private void processBody4B(String mid, Up46Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);

		String binstr = ByteUtil.toBinaryString(body.getData());
		binstr = StringUtils.reverse(binstr);
		String status = binstr.substring(9, 10);
		params.add(status);
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_ex set F4B = ? , F4BD = ? where stcd = ? ";

		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.26　控制水泵开关命令/水泵状态自报
	 * 
	 * @param body
	 */
	private void processBody4C(String mid, Up4CBody body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();
		StringBuffer sb = new StringBuffer();

		params.add(sendDatetime);

		byte[] data = body.getData();
		String datastr = ByteUtil.toBinaryString(data);
		sb.append(" , binstr = ? ");
		params.add(datastr);

		datastr = StringUtils.reverse(datastr);
		for (int i = 0, len = datastr.length(); i < len; i++) {
			String flag = datastr.substring(i, i + 1);
			String field = "D" + i;
			sb.append(" , " + field + " = ? ");
			params.add(flag);
		}

		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_4c set uptime = ? " + sb.toString()
				+ " where stcd = ? ";

		dao.update(sql, params.toArray());

	}

	/**
	 * 6.6.4.27　控制阀门开关命令/阀门状态信息自报
	 * 
	 * @param body
	 */
	private void processBody4D(String mid, Up4CBody body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();
		StringBuffer sb = new StringBuffer();

		params.add(sendDatetime);

		byte[] data = body.getData();
		String datastr = ByteUtil.toBinaryString(data);
		sb.append(" , binstr = ? ");
		params.add(datastr);

		datastr = StringUtils.reverse(datastr);
		for (int i = 0, len = datastr.length(); i < len; i++) {
			String flag = datastr.substring(i, i + 1);
			String field = "D" + i;
			sb.append(" , " + field + " = ? ");
			params.add(flag);
		}

		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_4d set uptime = ? " + sb.toString()
				+ " where stcd = ? ";

		dao.update(sql, params.toArray());

	}

	/**
	 * 6.6.4.28　控制闸门开关命令/闸门状态信息自报
	 * 
	 * @param body
	 */
	private void processBody4E(String mid, Up4EBody body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();
		StringBuffer sb = new StringBuffer();

		params.add(sendDatetime);

		int count = body.getCount();
		byte[] data = body.getData();
		String datastr = ByteUtil.toBinaryString(data);
		datastr = StringUtils.reverse(datastr);
		for (int i = 0, len = datastr.length(); i < len; i++) {
			String flag = datastr.substring(i, i + 1);
			String field = "D" + i;
			sb.append(" , " + field + " = ? ");
			params.add(flag);
		}

		byte[] kd = body.getKd();

		for (int i = 0, len = count; i < len; i++) {
			byte[] kddata = new byte[2];
			System.arraycopy(kd, i * 2, kddata, 0, 2);

			String field = "D" + i + "H";
			sb.append(" , " + field + " = ? ");
			params.add(ByteUtil.bcd2Str(kddata));
		}

		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_4e set uptime = ? " + sb.toString()
				+ " where stcd = ? ";
		logger.info(sql);
		dao.update(sql, params.toArray());

	}

	/**
	 * 6.6.4.30　中心站查询遥测站事件记录
	 * 
	 * @param body
	 */
	private void processBody50(String mid, Up50Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List params = new ArrayList();
		StringBuffer sb = new StringBuffer();

		params.add(sendDatetime);

		byte[] erc = body.getErc(); // 64位

		for (int i = 0; i < 32; i++) {
			byte[] ercdata = new byte[2];
			System.arraycopy(erc, i * 2, ercdata, 0, 2);
			String field = "ERC" + (i + 1);
			sb.append(" , " + field + " = ? ");
			params.add(ByteUtil.bytesToUshort(ercdata));

		}

		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_50 set uptime = ? " + sb.toString()
				+ " where stcd = ? ";

		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.25　设置遥测站IC卡状态
	 * 
	 * @param body
	 */
	private void processBody4F(String mid, Up4FBody body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);

		byte data = body.getData();
		params.add(ByteUtil.toHexString(new byte[] { data }));
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");
		String sql = "update rtu_station_ex set F4F = ? , F4FD = ? where stcd = ? ";
		dao.update(sql, params.toArray());
	}

	/**
	 * 6.6.4.31　中心站查询遥测站时钟
	 * 
	 * @param body
	 */
	private void processBody51(String mid, Up51Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);
		List params = new ArrayList();

		params.add(sendDatetime);
		params.add(stcd);

		BaseDao dao = SpringUtils.getBean("baseDao");

		String sql = "update rtu_station_ex set F51 = ?  where stcd = ? ";

		dao.update(sql, params.toArray());
	}

	/**
	 * 处理一般报文，并将报文入库
	 * 
	 * @param body
	 */
	private void processBody30(String mid, Up30Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sttp = body.getSttp();
		String sendDate = body.getSendDate();
		String viewDate = body.getViewDate();

		Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List<DataItem> items = body.getItems();

		// System.out.println("---------------------items.length " +
		// items.size());

		for (DataItem item : items) {

			byte[] label = item.getLabel();
			byte[] value = item.getValue();
			int length = item.getLength();
			int decimal = item.getDecimal();

			// 有的公司，出现发送多次时间的现象
			if (label != null) {

				if (label[0] == (byte) 0xF0) {
					// System.out
					// .println("---xxxxxx--------xxxxxxxxxxxx------xxxxxxxxx---xxxxxx--------------------------");
					viewDate = ByteUtil.bcd2Str(value);
					curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
				} else {
					switch (label[0]) {

					case (byte) 0xF4:
						decimal = 1;
						// 5分钟时段数据，添加时间段
						BigDecimal total = new BigDecimal(0);
						for (int i = 0, len = value.length; i < len; i++) {
							int intvalue = ByteUtil
									.bytesToUbyte(new byte[] { value[i] });

							if (intvalue == 255) {
								curViewDate = DateUtil.addMinutes(curViewDate,
										5);
								continue;// 此种数据属于非法数据,协议中规定
							}

							String itemvalue = Integer.toString(intvalue);
							itemvalue = HydroBuilder.buildDot(itemvalue,
									decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDatetime,
									curViewDate, ByteUtil.toHexString(label),
									itemvalue);
							curViewDate = DateUtil.addMinutes(curViewDate, 5);

							// 将每5分钟的雨量相加，得出一小时的雨量
							BigDecimal m5 = new BigDecimal(itemvalue);
							total = total.add(m5);
						}

						curViewDate = DateUtil.addMinutes(curViewDate, -5);
						this.writeDb(mid, serial, stcd, sttp, sendDatetime,
								curViewDate, "1A", total.doubleValue());
						this.writeDbFor1H(mid, serial, stcd, sttp,
								sendDatetime, curViewDate, "1A",
								total.doubleValue());
						break;
					case (byte) 0xF5:
					case (byte) 0xF6:
					case (byte) 0xF7:
					case (byte) 0xF8:
					case (byte) 0xF9:
					case (byte) 0xFA:
					case (byte) 0xFB:
					case (byte) 0xFC:
						decimal = 2;
						// 5分钟水位数据，添加时间段
						for (int i = 0, len = value.length; i < len;) {
							int intvalue = ByteUtil.bytesToUshort(new byte[] {
									value[i], value[i + 1] });
							i = i + 2;

							if (intvalue == 65535)
								continue;// 此种数据属于非法数据,协议中规定

							String itemvalue = Integer.toString(intvalue);
							itemvalue = HydroBuilder.buildDot(itemvalue,
									decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDatetime,
									curViewDate, ByteUtil.toHexString(label),
									itemvalue);

							if (i >= len) {
								this.writeDbFor1H(mid, serial, stcd, sttp,
										sendDatetime, curViewDate,
										ByteUtil.toHexString(label), itemvalue);
							}

							curViewDate = DateUtil.addMinutes(curViewDate, 5);

						}
						break;
					default:

						// 判断是否有非法数据的，如果数组中全为FF，则为非法数据
						boolean flag = false;
						boolean neg = false;
						byte[] temp = new byte[value.length];
						for (int i = 0, len = value.length; i < len; i++) {
							int intvalue = ByteUtil
									.bytesToUbyte(new byte[] { value[i] });

							if (intvalue == 255) {
								if (i == 0)
									neg = true;
								temp[i] = (byte) 0x00;// 第一个值为FF表示为负数
								continue;// 此种数据属于非法数据,协议中规定
							} else {
								temp[i] = value[i];
								flag = true;
							}
						}

						if (flag) {

							String itemvalue = ByteUtil.bcd2Str(temp);

							if (decimal != 0) {
								// 添加字符串
								itemvalue = HydroBuilder.buildDot(itemvalue,
										decimal);
							}

							if (neg) {
								itemvalue = "-" + itemvalue;
							}

							logger.info("field" + ByteUtil.toHexString(label)
									+ "  value " + itemvalue + " decimal "
									+ decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDate,
									viewDate, ByteUtil.toHexString(label),
									itemvalue);
							// 如果是“当前降雨量（20）”，也存在临时表中
							if (ByteUtil.toHexString(label).equals("20")) {
								this.writeDbFor1H(mid, serial, stcd, sttp,
										sendDatetime, curViewDate,
										ByteUtil.toHexString(label), itemvalue);
							}
						}
					}
				}
			}

		}
	}

	/**
	 * 处理时段报文，并将报文入库
	 * 
	 * @param body
	 */
	private void processBody31(String mid, Up31Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sttp = body.getSttp();
		String sendDate = body.getSendDate();
		String viewDate = body.getViewDate();

		byte[] drxxx = body.getDrxxx();
		String dhm = ByteUtil.bcd2Str(drxxx);
		String day = dhm.substring(0, 2);
		String hour = dhm.substring(2, 4);
		String minute = dhm.substring(4, 6);
		String drx = TypeDate.N;
		String drnn = hour;
		if (!day.equals("00")) {
			drx = TypeDate.D;
			drnn = day;
		}
		if (!hour.equals("00")) {
			drx = TypeDate.H;
			drnn = hour;
		}
		if (!minute.equals("00")) {
			drx = TypeDate.N;
			drnn = minute;
		}

		Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List<DataItem> items = body.getItems();

		int dataLength = 0;

		for (DataItem item : items) {

			byte[] label = item.getLabel();
			byte[] value = item.getValue();
			int length = item.getLength();
			int decimal = item.getDecimal();
			// String itemvalue = ByteUtil.bcd2Str(value);
			// if (decimal != 0) {
			// // 添加小数点字符串
			// itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
			// }
			// this.writeDb(mid, serial, stcd, sttp, sendDatetime, curViewDate,
			// new String(label), itemvalue);
			// // 向下添加时间
			// curViewDate = HydroParser.processDate(curViewDate, drx, drnn);
			//
			// ==============================================
			// 有的公司，出现发送多次时间的现象

			if (label != null) {
				logger.info("field" + ByteUtil.toHexString(label) + "  value "
						+ ByteUtil.bcd2Str(value) + " byte "
						+ ByteUtil.bytesToUbyte(value) + " decimal " + decimal);
				if (label[0] == (byte) 0xF0) {
					viewDate = ByteUtil.bcd2Str(value);
					curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
				} else {
					switch (label[0]) {
					case (byte) 0x22:

						dataLength = value.length;

						int tempDataLen = 0;
						BigDecimal hourRain = new BigDecimal(0);

						for (int ii = 0; ii < dataLength / length; ii++) {
							byte[] temp = new byte[length];
							System.arraycopy(value, ii * length, temp, 0,
									length);

							int intvalue = ByteUtil.bytesToUbyte(temp);
							if (intvalue == 255) {
								curViewDate = HydroParser.processDate(
										curViewDate, drx, drnn);
								continue;// 此种数据属于非法数据,协议中规定
							}

							String f22alue = ByteUtil.bcd2Str(temp);
							f22alue = HydroBuilder.buildDot(f22alue, decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDatetime,
									curViewDate, ByteUtil.toHexString(label),
									f22alue);
							// 将每5分钟的雨量相加，得出一小时的雨量
							curViewDate = HydroParser.processDate(curViewDate,
									drx, drnn);
							hourRain = hourRain.add(new BigDecimal(f22alue));

						}

						// 多加了5分钟，应该给减回来
						curViewDate = DateUtil.addMinutes(curViewDate, -5);
						this.writeDb(mid, serial, stcd, sttp, sendDatetime,
								curViewDate, "1A", hourRain.doubleValue());
						this.writeDbFor1H(mid, serial, stcd, sttp,
								sendDatetime, curViewDate, "1A",
								hourRain.doubleValue());

						break;
					case (byte) 0xF4:

						// -------------------------------------
						dataLength = value.length;
						BigDecimal F4HourRain = new BigDecimal(0);

						for (int ii = 0; ii < dataLength / length; ii++) {
							byte[] temp = new byte[length];
							System.arraycopy(value, ii * length, temp, 0,
									length);

							int intvalue = ByteUtil.bytesToUbyte(temp);

							if ((value.length == 1 && intvalue == 255)
									|| (value.length == 2 && intvalue == 65535)) {
								curViewDate = HydroParser.processDate(
										curViewDate, drx, drnn);
								continue;// 此种数据属于非法数据,协议中规定
							}

							String f4Value = Integer.toString(intvalue);
							f4Value = HydroBuilder.buildDot(f4Value, decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDatetime,
									curViewDate, ByteUtil.toHexString(label),
									f4Value);
							// 将每5分钟的雨量相加，得出一小时的雨量
							curViewDate = HydroParser.processDate(curViewDate,
									drx, drnn);
							hourRain = F4HourRain.add(new BigDecimal(f4Value));

						}

						// 多加了5分钟，应该给减回来
						curViewDate = DateUtil.addMinutes(curViewDate, -5);
						this.writeDb(mid, serial, stcd, sttp, sendDatetime,
								curViewDate, "1A", F4HourRain.doubleValue());
						this.writeDbFor1H(mid, serial, stcd, sttp,
								sendDatetime, curViewDate, "1A",
								F4HourRain.doubleValue());

						break;
					case (byte) 0xF5:
					case (byte) 0xF6:
					case (byte) 0xF7:
					case (byte) 0xF8:
					case (byte) 0xF9:
					case (byte) 0xFA:
					case (byte) 0xFB:
					case (byte) 0xFC:
						decimal = 2;

						// curViewDate = HydroParser.processDate(curViewDate,
						// drx,
						// drnn);
						dataLength = value.length;

						for (int ii = 0; ii < dataLength / length; ii++) {
							byte[] temp = new byte[length];
							System.arraycopy(value, ii * length, temp, 0,
									length);

							// 5分钟水位数据，添加时间段
							int rivewintvalue = ByteUtil.bytesToUbyte(temp);

							if (rivewintvalue == 65535) {
								curViewDate = HydroParser.processDate(
										curViewDate, drx, drnn);
								continue;// 此种数据属于非法数据,协议中规定
							}
							String riveritemvalue = Integer
									.toString(rivewintvalue);
							riveritemvalue = HydroBuilder.buildDot(
									riveritemvalue, decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDatetime,
									curViewDate, ByteUtil.toHexString(label),
									riveritemvalue);

							if (ii >= dataLength / length - 1) {
								this.writeDbFor1H(mid, serial, stcd, sttp,
										sendDatetime, curViewDate,
										ByteUtil.toHexString(label),
										riveritemvalue);
							}
						}

						break;
					default:

						String ditemvalue = ByteUtil.bcd2Str(value);

						if (decimal != 0) {
							// 添加字符串
							ditemvalue = HydroBuilder.buildDot(ditemvalue,
									decimal);
						}
						this.writeDb(mid, serial, stcd, sttp, sendDate,
								viewDate, ByteUtil.toHexString(label),
								ditemvalue);
						// 如果是“当前降雨量（20）”，也存在临时表中
						if (ByteUtil.toHexString(label).equals("20")) {
							this.writeDbFor1H(mid, serial, stcd, sttp,
									sendDatetime, curViewDate,
									ByteUtil.toHexString(label), ditemvalue);
						}

					}
				}
			}
		}

	}

	/**
	 * 处理时段报文，并将报文入库
	 * 
	 * @param body
	 */
	private void processBody31BAK(String mid, Up31Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sttp = body.getSttp();
		String sendDate = body.getSendDate();
		String viewDate = body.getViewDate();

		byte[] drxxx = body.getDrxxx();
		String dhm = ByteUtil.bcd2Str(drxxx);
		String day = dhm.substring(0, 2);
		String hour = dhm.substring(2, 4);
		String minute = dhm.substring(4, 6);
		String drx = TypeDate.N;
		String drnn = hour;
		if (!day.equals("00")) {
			drx = TypeDate.D;
			drnn = day;
		}
		if (!hour.equals("00")) {
			drx = TypeDate.H;
			drnn = hour;
		}
		if (!minute.equals("00")) {
			drx = TypeDate.N;
			drnn = minute;
		}

		Date curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List<DataItem> items = body.getItems();
		for (DataItem item : items) {
			byte[] label = item.getLabel();
			byte[] value = item.getValue();
			int decimal = item.getDecimal();
			// String itemvalue = ByteUtil.bcd2Str(value);
			// if (decimal != 0) {
			// // 添加小数点字符串
			// itemvalue = HydroBuilder.buildDot(itemvalue, decimal);
			// }
			// this.writeDb(mid, serial, stcd, sttp, sendDatetime, curViewDate,
			// new String(label), itemvalue);
			// // 向下添加时间
			// curViewDate = HydroParser.processDate(curViewDate, drx, drnn);
			//
			// ==============================================
			// 有的公司，出现发送多次时间的现象
			if (label != null) {
				if (label[0] == (byte) 0xF0) {
					viewDate = ByteUtil.bcd2Str(value);
					curViewDate = HydroParser.convertYYMMDDHHmm(viewDate);
				} else {
					switch (label[0]) {
					case (byte) 0x22: // 2014-07-04修改，针对武汉联宇22号标识符出现的问题进行修改
					case (byte) 0xF4:
						decimal = 1;
						// 5分钟时段数据，添加时间段
						BigDecimal total = new BigDecimal(0);
						for (int i = 0, len = value.length; i < len; i++) {
							int intvalue = ByteUtil
									.bytesToUbyte(new byte[] { value[i] });

							// curViewDate = DateUtil.addMinutes(curViewDate,
							// 5);

							if (intvalue == 255) {
								curViewDate = HydroParser.processDate(
										curViewDate, drx, drnn);
								continue;// 此种数据属于非法数据,协议中规定
							}

							String itemvalue = Integer.toString(intvalue);
							itemvalue = HydroBuilder.buildDot(itemvalue,
									decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDatetime,
									curViewDate, ByteUtil.toHexString(label),
									itemvalue);

							curViewDate = HydroParser.processDate(curViewDate,
									drx, drnn);

							// 将每5分钟的雨量相加，得出一小时的雨量
							BigDecimal m5 = new BigDecimal(itemvalue);
							total = total.add(m5);
						}
						// 多加了5分钟，应该给减回来
						curViewDate = DateUtil.addMinutes(curViewDate, -5);
						this.writeDb(mid, serial, stcd, sttp, sendDatetime,
								curViewDate, "1A", total.doubleValue());
						break;
					case (byte) 0xF5:
					case (byte) 0xF6:
					case (byte) 0xF7:
					case (byte) 0xF8:
					case (byte) 0xF9:
					case (byte) 0xFA:
					case (byte) 0xFB:
					case (byte) 0xFC:
						decimal = 2;
						// 5分钟水位数据，添加时间段
						for (int i = 0, len = value.length; i < len;) {
							int intvalue = ByteUtil.bytesToUshort(new byte[] {
									value[i], value[i + 1] });
							i = i + 2;
							// curViewDate = DateUtil.addMinutes(curViewDate,
							// 5);

							if (intvalue == 65535) {
								curViewDate = HydroParser.processDate(
										curViewDate, drx, drnn);
								continue;// 此种数据属于非法数据,协议中规定
							}
							String itemvalue = Integer.toString(intvalue);
							itemvalue = HydroBuilder.buildDot(itemvalue,
									decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDatetime,
									curViewDate, ByteUtil.toHexString(label),
									itemvalue);

							curViewDate = HydroParser.processDate(curViewDate,
									drx, drnn);
						}
						break;
					default:

						// String itemvalue = ByteUtil.bcd2Str(value);
						//
						// logger.info("field" + ByteUtil.toHexString(label)
						// + "  value " + itemvalue + " decimal "
						// + decimal);
						//
						// if (decimal != 0) {
						// // 添加字符串
						// itemvalue = HydroBuilder.buildDot(itemvalue,
						// decimal);
						// }
						// this.writeDb(mid, serial, stcd, sttp, sendDate,
						// viewDate, ByteUtil.toHexString(label),
						// itemvalue);

						// --------------------------------
						// 判断是否有非法数据的，如果数组中全为FF，则为非法数据
						boolean flag = false;
						boolean neg = false;
						byte[] temp = new byte[value.length];
						for (int i = 0, len = value.length; i < len; i++) {
							int intvalue = ByteUtil
									.bytesToUbyte(new byte[] { value[i] });

							if (intvalue == 255) {
								if (i == 0)
									neg = true;
								temp[i] = (byte) 0x00;// 第一个值为FF表示为负数
								continue;// 此种数据属于非法数据,协议中规定
							} else {
								temp[i] = value[i];
								flag = true;
							}
						}

						if (flag) {

							String itemvalue = ByteUtil.bcd2Str(temp);

							if (decimal != 0) {
								// 添加字符串
								itemvalue = HydroBuilder.buildDot(itemvalue,
										decimal);
							}

							if (neg) {
								itemvalue = "-" + itemvalue;
							}

							logger.info("field" + ByteUtil.toHexString(label)
									+ "  value " + itemvalue + " decimal "
									+ decimal);

							this.writeDb(mid, serial, stcd, sttp, sendDate,
									viewDate, ByteUtil.toHexString(label),
									itemvalue);
						}

					}
				}
			}
		}
	}

	/**
	 * 处理人工置数报，并将报文拆分处理
	 * 
	 * @param body
	 */
	private void processBody35(String mid, Up35Body body) throws Exception {
		int serial = body.getSerialId();

		// 发送时间格式YYMMDDHHmmSS
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		List<HydroData> records = HydroParser.process(body.getRGZS());
		for (HydroData record : records) {

			String stcd = record.getStcd();
			String sttp = record.getTypeCode();
			Date viewDatetime = record.getViewDate();

			List<HydroDataItem> items = record.getItems();
			for (HydroDataItem item : items) {
				String label = item.getName();
				Object value = item.getValue();

				this.writeDb(mid, serial, stcd, sttp, sendDatetime,
						viewDatetime, label, value);
			}
		}

	}

	/**
	 * 处理图片报文
	 * 
	 * @param body
	 */
	private void processBody36(String mid, Up36Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sttp = body.getSttp();
		String sendDate = body.getSendDate();
		String viewDate = body.getViewDate();
		byte[] picByte = null;

		List<DataItem> items = body.getItems();
		for (DataItem item : items) {
			picByte = item.getValue();
		}

		String fileName = stcd + "_" + serial + ".jpg";
		fileName = HydroConfig.getPicSavePath() + fileName;
		if (picByte != null)
			FileUtil.writeFile(fileName, picByte);
	}

	/**
	 * 张义华修改，这个类恐怕是有问题
	 * 
	 * @param mid
	 * @param serial
	 * @param stcd
	 * @param sttp
	 * @param sendDate
	 * @param viewDate
	 * @param label
	 * @param value
	 * @throws Exception
	 */
	private void writeDbFor1H(String mid, int serial, String stcd, String sttp,
			Date sendDate, Date viewDate, String label, Object value)
			throws Exception {

		Map params = new HashMap();
		params.put("mid", mid);
		params.put("serial", serial);
		params.put("stcd", stcd);
		params.put("sttp", sttp);
		params.put("sendtime", sendDate);
		params.put("viewtime", viewDate);

		params.put("itemlabel", label.toUpperCase());
		params.put("itemvalue", value);
		params.put("c" + label.toLowerCase(), value);

		params.put("writetime", new Date(System.currentTimeMillis()));

		try {
			int count = dao
					.queryForCount("select * from rtu_message_r where stcd='"
							+ stcd + "'");// and
											// itemlabel='"+label.toUpperCase()+"'");
			if (count == 0) {
				dao.insert("rtu_message_r", new String[] { "mid", "serial",
						"stcd", "sttp", "sendtime", "viewtime", "itemlabel",
						"itemvalue", "c" + label.toLowerCase(), "writetime" },
						params);
			} else {
				Object[] objs = new Object[] { mid, serial, sendDate, viewDate,
						value, new Date(System.currentTimeMillis()), value,
						stcd, sttp };
				String sql = "update rtu_message_r set mid=?,serial=?,sendtime=?,viewtime=?,itemvalue=?,writetime=?,c"
						+ label.toLowerCase() + "=? where stcd=? and sttp=? ";
				dao.update(sql, objs);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// 添加写入文件的
		}
		// 八时水位需要单独记录
		int hh = viewDate.getHours();
		String ss = label.toLowerCase();
		if (label.toLowerCase().equals("f5") && viewDate.getHours() == 8) {
			writeDbFor1H(mid, serial, stcd, sttp, sendDate, viewDate, "76",
					value);
		}
	}

	private void writeDb(String mid, int serial, String stcd, String sttp,
			Date sendDate, Date viewDate, String label, Object value)
			throws Exception {

		Map params = new HashMap();
		params.put("mid", mid);
		params.put("serial", serial);
		params.put("stcd", stcd);
		params.put("sttp", sttp);
		params.put("sendtime", sendDate);
		params.put("viewtime", viewDate);

		params.put("itemlabel", label.toUpperCase());
		params.put("itemvalue", value);

		params.put("writetime", new Date(System.currentTimeMillis()));

		try {
			dao.insert("rtu_message_r", new String[] { "mid", "serial", "stcd",
					"sttp", "sendtime", "viewtime", "itemlabel", "itemvalue",
					"writetime" }, params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// 添加写入文件的
		}
	}

	private void writeDb(String mid, int serial, String stcd, String sttp,
			String sendDate, String viewDate, String label, String value)
			throws Exception {

		// 发送时间格式YYMMDDHHmmSS
		SimpleDateFormat sendtimeSdf = new SimpleDateFormat("yyMMddHHmmss");// yyyy-MM-dd
		// HH:mm:ss

		// 观测时间格式YYMMDDHHmm
		SimpleDateFormat viewtimeSdf = new SimpleDateFormat("yyMMddHHmm");// yyyy-MM-dd
		// HH:mm:ss

		Date sendDatetime = sendtimeSdf.parse(sendDate);

		Date viewDatetime = viewtimeSdf.parse(viewDate);

		this.writeDb(mid, serial, stcd, sttp, sendDatetime, viewDatetime,
				label, value);

	}

	public static void main(String[] arg) {

		// BigDecimal m1 = new BigDecimal(0.1);
		// BigDecimal m2 = new BigDecimal(0.1);
		// BigDecimal m3 = new BigDecimal(0.1);
		// BigDecimal m4 = new BigDecimal(0.1);
		// BigDecimal m5 = new BigDecimal(0.1);
		// BigDecimal m6 = new BigDecimal(0.1);
		// BigDecimal m7 = new BigDecimal(0.1);
		// BigDecimal m8 = new BigDecimal(0.1);
		// BigDecimal m9 = new BigDecimal(0.1);
		// BigDecimal m10 = new BigDecimal(0.1);
		// BigDecimal m11 = new BigDecimal(0.1);
		// BigDecimal m12 = new BigDecimal(0.1);
		// BigDecimal total = new BigDecimal(0);
		// total = total.add(m1).add(m2).add(m3).add(m4).add(m5).add(m6).add(m7)
		// .add(m8).add(m9).add(m10).add(m11).add(m12);
		// System.out.println(total.doubleValue());
		//
		// // 5分钟时段数据，添加时间段
		// BigDecimal P1 = new BigDecimal(0);
		// for (int i = 0, len = 12; i < len; i++) {
		//
		// // 将每5分钟的雨量相加，得出一小时的雨量
		// BigDecimal m55 = new BigDecimal(0.1);
		// P1 = P1.add(m55);
		// System.out.println(P1.doubleValue());
		// }
		// System.out.println(P1.doubleValue());

	}

}
