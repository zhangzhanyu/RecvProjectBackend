package com.godenwater.recv.service;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 


public class RtuDbWriteServer {


	private static Logger logger = LoggerFactory.getLogger(RtuDbWriteServer.class);
	
	private long startDate = System.currentTimeMillis();

	private static RtuDbWriteServer instance;

	private boolean RUNNING = false;

	// 创建10个生产线程，使用线程池的方式，可以保障线程的安全性。
	ExecutorService service = Executors.newFixedThreadPool(5);

	/**
	 * Returns a singleton instance of XMPPServer.
	 * 
	 * @return an instance.
	 */
	public static RtuDbWriteServer getInstance() {
		if (instance == null) {
			instance = new RtuDbWriteServer();
		}
		return instance;
	}

	/**
	 * Creates a server and starts it.
	 */
	private RtuDbWriteServer() {
		if (instance != null) {
			throw new IllegalStateException(">> RTU DB writer server is already running");
		}
		instance = this;
	}
	

	/**
	 * 启动服务实例
	 */
	public void start() {

		try {
			 
			RUNNING = true;

//			service.execute(new MessageConsumer());
			service.execute(new MessageSl651Consumer());
			service.execute(new MessageSzy206Consumer());
			service.execute(new MessageYyConsumer());
			//service.execute(new MessageFileConsumer());
//			service.execute(new CombinConsumer(this));
//			service.execute(new RtuDatabaseConsumer(this));
//			service.execute(new BizDatabaseConsumer(this));

			System.out.println("RTU SERVER >> 水文水资源数据接收平台--数据处理服务启动完成.");
 

			startDate = System.currentTimeMillis();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			System.out.println("RTU SERVER >> 水文水资源数据平台.error");
			// shutdownServer();
		}

		
	}
	

	/**
	 * 关闭服务实例
	 */
	public void stop() {

		RUNNING = false;
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		service.shutdown();
		// service.shutdownNow();
		System.out.println("正在关闭入库服务，请等待....");
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("INDB SERVER >> 入库服务关闭完成！");
	}
	
	public static void main(String args[]){
		java.util.Date sd = new Date();

		
		try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		java.util.Date ed = new Date();
		long diff = ed.getTime() - sd.getTime();

//		 判断时间如果大于2个小时，则产生下发时间召测报文
		if (Math.abs(diff) / 1000 > 1) {
		System.out.println(">> diff " + diff);
		}
	}
	
}
