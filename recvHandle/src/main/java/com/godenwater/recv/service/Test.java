package com.godenwater.recv.service;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Li on 2017/2/22.
 */
public class Test {

    public static int parseStcd(byte[] centerAddr, byte[] stationAddr) {

        if(centerAddr[0]==(byte)0xFE){//有一类测站信息，中心站为FE时，不借位
            return (stationAddr[0] & 0xff);
        }else {
            // int datalen = (int) (data >> 3 & 0x1F);// 获取前五位的数据
            int decimal = (int) (centerAddr[0] & 0x07); // 获取后三位的小数点位数
            // stationAddr[0];
            // return ByteUtil.bytesToUbyte(stationAddr);

            return ((centerAddr[0] & 0x07) << 8) | (stationAddr[0] & 0xff);// 将中心站地址的右三位进行并入计算
        }
    }

    public static DateTime writeYYDatetime(byte funccode,  Date viewDate){
        DateTime vd = new DateTime(viewDate);
        switch (funccode) {
            case 0X0B:// 表示雨量加报。
            case 0X09:// 表示水位加报。
                //加报的时间，只需要减去分钟，不需要减1小时
                vd = vd.minusMinutes(vd.getMinuteOfHour()).minusSeconds(vd.getSecondOfMinute());//将分钟减去，以整点开始
                break;
            case 0X0E:// 整点报
                //正点报的，一般情况下会在正点过几分进行发送，但也有提前几分钟发送的现象
                if(vd.getMinuteOfHour() < 5){
                    vd = vd.minusMinutes(vd.getMinuteOfHour());//将分钟减去，以整点开始
                    vd = vd.minusHours(1);//减去1小时，作为时间起点
                }
                if(vd.getMinuteOfHour() > 55 ){
                    vd = vd.minusMinutes(vd.getMinuteOfHour());//将分钟减去，以整点开始
                }
                break;
        }
        return vd;
    }

    public static void main(String[] args){

//        Float f = 100.2f;
//        IEEE754 ieee = new IEEE754();
//
//        String ieee754 = ieee.floatToIEEE754(f);
//        System.out.println(ieee754);
//        java如果有“ABCDEF12”这种字符串，
//        先每2个字符读成一个byte字节，得到一个byte[4]字节串
//        再用旧IO的ByteArrayInputStream输入byte[4],再转换成DataInputStream.readFloat()输入成float浮点数

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date testDate = sdf.parse("2017-02-17 08:04:20");
            DateTime vd = Test.writeYYDatetime((byte) 0x0E,testDate);

            System.out.println(">> " + vd.toString("yyyy-MM-dd HH:mm:ss"));

            //System.out.println(">> " +    ByteUtil.toHexString("$".getBytes()));

            System.out.println(">> " +    new String(new byte[]{(byte)0xFD,(byte)0xFF,(byte)0xFF,(byte)0xFF}));
        }catch (Exception E){
            E.printStackTrace();
        }

        System.out.println(">> " + Test.parseStcd(new byte[]{(byte)0xFE},new byte[]{(byte)0x74}));
    }

}
