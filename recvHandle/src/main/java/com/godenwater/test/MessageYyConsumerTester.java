package com.godenwater.test;

import com.godenwater.yanyu.IMessageHeader;
import com.godenwater.yanyu.Symbol;
import com.godenwater.yanyu.YYBuilder;
import com.godenwater.yanyu.YYParser;
import com.godenwater.yanyu.body.Up0E10Body;
import com.godenwater.yanyu.body.Up0E16Body;
import com.godenwater.yanyu.body.Up0E17Body;
import com.godenwater.yanyu.body.Up0EBody;
import com.godenwater.yanyu.utils.ByteUtil;
import com.godenwater.recv.decode.YanyuMessageDecoder;
import com.godenwater.recv.model.CommonMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Li on 2017/6/6.
 */
public class MessageYyConsumerTester {

    public void process(String channel, CommonMessage message) throws Exception {


        boolean crcFlag = true;

        IMessageHeader header = (IMessageHeader) message.getHeader();

        // 解析报文体
        String stcd = ""
                + YYParser.parseStcd(header.getCenterAddr(),
                header.getStationAddr());

        byte[] content = message.getContent();

        byte[] funcCode = header.getFuncCode();

        String id = UUID.randomUUID().toString();
        // 写入原始报文库
        String msgstr = YYBuilder.toHexString(
                (IMessageHeader) message.getHeader(), Symbol.UP)
                + ByteUtil.toHexString(content)
                + ByteUtil.toHexString(message.getEOF())
                + ByteUtil.toHexString(message.getCRC());

        switch (funcCode[0]) {
            case 0X1B:// 表示雨量加报。
            case 0X19:// 表示水位加报。
                SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmm");
                byte[] sendDate = new byte[5];
                System.arraycopy(content, 0, sendDate, 0, sendDate.length);
                String sendtime = ByteUtil.bcd2Str(sendDate);
                Date viewtime = sdf.parse(sendtime);

                // 更新最新数据
                // this.writeMessageNew(stcd,channel,
                // ByteUtil.toHexString(funcCode), viewtime);

                // 写入原始报文库
//                this.writeRtuMessage(id, stcd, "YY", channel,
//                        ByteUtil.toHexString(funcCode), 1, viewtime, 1, 1, msgstr,
//                        crcFlag);

                break;
            case 0X0B:// 表示雨量加报。
            case 0X09:// 表示水位加报。
            case 0X0E:// 整点报
                SimpleDateFormat sdf5 = new SimpleDateFormat("yyMMddHHmm");
                byte[] sendDate5 = new byte[5];
                System.arraycopy(content, 0, sendDate5, 0, sendDate5.length);
                String sendtime5 = ByteUtil.bcd2Str(sendDate5);
                Date viewtime5 = sdf5.parse(sendtime5);

                // 写入原始报文库
//                this.writeRtuMessage(id, stcd, "YY", channel,
//                        ByteUtil.toHexString(funcCode), 1, viewtime5, 1, 1, msgstr,
//                        crcFlag);



                    //燕禹和江西需要添加此拆分
                    //1.1、写入雨量表,测站类型为1，表示雨量站
                       Up0EBody up0EBody01 =YYParser.parse0EBody(content);
                        // 根据类型判断是写入雨量还是水位
                        int ff26r1 = ByteUtil.bytesToUshort(up0EBody01.getF1());//应该取整数//processSymbolHex(up0EBody01.getF1(), 1);
                        int ff20r1 = ByteUtil.bytesToUshort(up0EBody01.getF2());//应该取整数//processSymbolHex(up0EBody01.getF2(), 1);
                        int ff1Ar1 = ByteUtil.bytesToUshort(up0EBody01.getF3());//应该取整数//processSymbolHex(up0EBody01.getF3(), 1);
                        // 根据类型判断是写入雨量还是水位
                        byte[] ff4r1 = up0EBody01.getF4();
                        byte[] ff6r1 = up0EBody01.getF6();
                        //writeYYRainData(stcd, funcCode[0], viewtime5,  processSymbolF4H(ff4),  ff20, ff1A ,ff6 );

                    //1.2、写入水位表,测站类型为2，表示水位站
                        Up0EBody up0EBody02 =YYParser.parse0EBody(content);
                        byte[] ff5r2 = up0EBody02.getF5();
                        byte[] ff6r2 = up0EBody02.getF6();
                        //writeYYRiverData(stcd, funcCode[0], viewtime5,  processSymbolF5H(ff5) ,ff6);

                    //1.3、写入雨量表,测站类型为3，表示雨量站和水位站
                       Up0EBody up0EBody03 =YYParser.parse0EBody(content);

                        int ff20r3 = ByteUtil.bytesToUshort(up0EBody03.getF2());//应该取整数//processSymbolHex(up0EBody01.getF2(), 1);
                        int ff1Ar3 = ByteUtil.bytesToUshort(up0EBody03.getF3());//应该取整数//processSymbolHex(up0EBody01.getF3(), 1);
                        //String ff20 = processSymbolHex(up0EBody03.getF2(), 1);//时段雨量
                        //String ff1A = processSymbolHex(up0EBody03.getF3(), 1);//日雨量
                        // 根据类型判断是写入雨量还是水位
                        byte[] ff4r3 = up0EBody03.getF4();
                        byte[] ff5r3 = up0EBody03.getF5();
                        byte[] ff6r3 = up0EBody03.getF6();
                        //writeYYData(stcd, funcCode[0], viewtime5, processSymbolF4H(ff4), processSymbolF5H(ff5), ff20, ff1A ,ff6);

                    //1.4、写入闸位表,测站类型为10，表示闸位站
                       Up0E10Body up0EBody10 =YYParser.parse0E10Body(content);
                        byte[] f8r4 = up0EBody10.getF8();//闸位数据
                        byte[] ff6r4 = up0EBody10.getF6();
                        //writeYYZhaweiData(stcd, funcCode[0], viewtime5,  processSymbolF8H(f8),ff6 );


                    //1.5、写入流速表,测站类型为16，表示流速站
                        Up0E16Body up0EBody16 =YYParser.parse0E16Body(content);
                        byte[] f8r5 = up0EBody16.getF8();//流速数据
                        byte[] ff6r5 = up0EBody16.getF6();
                       // writeYYLiusuData(stcd, funcCode[0], viewtime5,  processSymbolF9H(f8),ff6 );


                    //1.5、写入流量表,测站类型为17，表示流量站
                     Up0E17Body up0EBody17 =YYParser.parse0E17Body(content);
                        byte[] f8r6 = up0EBody17.getF8();//流量仪器号
                        byte[] f9r6 = up0EBody17.getF9();//流量数据
                        byte[] ff6r6 = up0EBody17.getF6();
                        //writeYYOttData(stcd, funcCode[0], viewtime5,ByteUtil.bytesToUbyte(f8),  processSymbolF9H(f9),ff6  );


                break;
            case 0X0C:
                //写入测试数据

                break;
            case 0X0A:// 雨量固态数据
                // 3.1更新最新数据
                // this.writeMessageNew(stcd, channel,
                // ByteUtil.toHexString(funcCode),
                // new Date());

                // 3.2写入原始报文库
//                this.writeRtuMessage(id, stcd, "YY", channel,
//                        ByteUtil.toHexString(funcCode), 1, new Date(), 1, 1,
//                        msgstr, crcFlag);


                break;
            case 0X06:// 水位固态数据
                // 3.1更新最新数据
                // this.writeMessageNew(stcd, channel,
                // ByteUtil.toHexString(funcCode),
                // new Date());

                // 3.2写入原始报文库
//                this.writeRtuMessage(id, stcd, "YY", channel,
//                        ByteUtil.toHexString(funcCode), 1, new Date(), 1, 1,
//                        msgstr, crcFlag);

                // 3.3分析数据
                // Up06Body up06Body = YYParser.parse06Body(stcd, content);
                // Map<Date, String> data06Map = up06Body.getData();
                // for (Map.Entry<Date, String> entry : data06Map.entrySet()) {
                // // entry.getKey();//时间
                // // entry.getValue();//值
                // // 得將水位的數據轉化為浮點型
                // writeRtuCallerRain(id, stcd, "H", entry.getKey(), "F5",
                // Double.parseDouble(entry.getValue()));
                //
                // }

                // up06Body = null;
                break;
            default:
                break;
        }

    }

    public static void main(String[] arg) {

        String msg = "7E10630E3F1702171900000000000000000000000000000000000000000E000E000E000E000E000D000D000D000D000D000E000E1330FFFFFFFFFFFFFFFFFFFF000387A7";
        byte[] bytes = ByteUtil.HexStringToBinary(msg);
        CommonMessage message = YanyuMessageDecoder.decode(bytes);
        MessageYyConsumerTester consumer = new MessageYyConsumerTester();
        try {
            consumer.process("test", message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
