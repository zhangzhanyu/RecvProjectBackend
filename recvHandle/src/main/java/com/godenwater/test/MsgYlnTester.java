package com.godenwater.test;

import com.godenwater.yanyu.utils.ByteUtil;
import com.godenwater.recv.decode.YlnMessageDecoder;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.yln.YlnMessageHeader;
import com.godenwater.utils.ByteUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;

/**
 * Created by Li on 2017/4/7.
 */
public class MsgYlnTester {


    public void testStcd(String srcPath,String destPath){
        File f = new File(srcPath);//"D:\\rcvApp\\04"

        try {
            File[] fileArray = f.listFiles();
            if (fileArray != null) {
                for (int i = 0; i < fileArray.length; i++) {
                    //递归调用
                    System.out.println(fileArray[i]);
                    List<String> lines = FileUtils.readLines(fileArray[i], "UTF-8");
                    for (String line : lines) {
                        System.out.println("READ YLN>> " + line);

                        // 1.1：转换为报文字节数组
                        byte[] bytes = ByteUtils.hexStringToBytes(line);
//
//				// //1.2：解析报文
//				// 不能使用74做为中心站名
                        CommonMessage message = YlnMessageDecoder.decode(bytes);
                        MessageYlnConsumer2 consumer = new MessageYlnConsumer2();
                        //consumer.process("test", message);

                        YlnMessageHeader header = (YlnMessageHeader) message
                                .getHeader();
                        byte[] funcCode = header.getSTART_BIT();
                        //logger.info(">YLN DATA " + new String(funcCode) + new String(message.getContent()));

                        // 解析报文体
                        String funccode = new String(funcCode);
                        String content = new String(message.getContent(),"GB2312");

                        //实时数据
                        //AAA;61079250;61079250;yilinengshuiwen;qipaoceshi;2017-03-10 15:00:00;1.61,12.2,0,0;水位(m),供电电压(V),环境温度(℃),水势;$END
                        if (StringUtils.equalsIgnoreCase(funccode, "AAA")) {
                            //processRealData(content);
                            String[] msg = content.split(";");
                            String stcd = msg[1];
                            String viewtime = msg[5];
                            String viewdata = msg[6];
                            String viewnote = msg[7];
                            if(  stcd.equals("17031702")  ){
                                FileUtils.moveFileToDirectory(fileArray[i],new File(destPath ),true);
                            }
                        }

                        //密集数据
                        if (StringUtils.equalsIgnoreCase(funccode, "BAA")) {

                            // 测站
                            byte[] buffer = message.getContent();
                            byte[] stcd = new byte[4];
                            System.arraycopy(buffer, 0, stcd, 0, 4);

                            //ByteUtil.bytesToInt(stcd);
                            int mStcd = ByteUtil.bytesToInt(new byte[]{stcd[3], stcd[2], stcd[1], stcd[0]}); //需要进行站码转换
                            if( mStcd ==17031702 ){
                                FileUtils.moveFileToDirectory(fileArray[i],new File(destPath ),true);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void testView(String destPath){
        File f = new File(destPath);//"D:\\rcvApp\\bak"

        try {
            File[] fileArray = f.listFiles();
            if (fileArray != null) {
                for (int i = 0; i < fileArray.length; i++) {
                    //递归调用
                    System.out.println(fileArray[i]);
                    List<String> lines = FileUtils.readLines(fileArray[i], "UTF-8");
                    for (String line : lines) {
                        System.out.println("READ YLN>> " + line);

                        // 1.1：转换为报文字节数组
                        byte[] bytes = ByteUtils.hexStringToBytes(line);
//
//				// //1.2：解析报文
//				// 不能使用74做为中心站名
                        CommonMessage message = YlnMessageDecoder.decode(bytes);
                        MessageYlnConsumer2 consumer = new MessageYlnConsumer2();
                        consumer.process("test", message);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args){

        String srcPath = "D:\\rcvApp\\08";
        String destPath = "D:\\rcvApp\\bak";
        String stcd = "";
        //第一步：拆分数据到BAK目录
        MsgYlnTester tester = new MsgYlnTester();
        //tester.testStcd(srcPath,destPath);

        //第二步：查看数据显示情况
        tester.testView(destPath);;

    }

}
