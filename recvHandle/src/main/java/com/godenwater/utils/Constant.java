package com.godenwater.utils;

/**
 * 常量类
 *
 * @author zhaotianlong
 * @date: 2019-08-16
 */
public class Constant {

    /**
     * 字符串：空白
     */
    public static final String STRING_BLANK = "";
    
    /**
     * 字符串：0
     */
    public static final String STRING_ZERO = "0";

    /**
     * 字符串：1
     */
    public static final String STRING_ONE = "1";

    /**
     * 字符串：2
     */
    public static final String STRING_TWO = "2";
   
    /**
     * 字符串：4
     */
    public static final String STRING_FORE = "4";
    
    /**
     * 字符串：:
     */
    public static final String STRING_COLON = ":";
    
    /**
     * 字符串：utf-8
     */
    public static final String STRING_UTF8 = "UTF-8";
    
    /**
     * 字符串：MD5
     */
    public static final String STRING_MD5 = "MD5";

    /**
     * 数字 0
     */
    public static final int INT_ZERO = 0;
    
    /**
     * 数字：1
     */
    public static final int INT_ONE = 1;

    /**
     * 数字 2
     */
    public static final int INT_TWO = 2;
    
    /**
     * 数字：3
     */
    public static final int INT_THREE = 3;

    /**
     * 数字 6
     */
    public static final int INT_SIX = 6;

    /**
     * 数字 10
     */
    public static final int INT_TEEN = 10;
    
    /**
     * 数字 16
     */
    public static final int INT_SIXTEEN = 16;

    /**
     * 数字 18
     */
    public static final int INT_EIGHTEEN = 18;

    /**
     * 重复值错误信息
     */
    public static final String REPEAT_ERROR = "登记信息已存在,请勿重复添加!";


}