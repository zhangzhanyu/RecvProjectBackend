package com.godenwater.web.listener;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.godenwater.web.manager.StationDataManager;
import org.apache.commons.io.FileUtils;
import com.godenwater.recv.server.all.RtuConfig;
import com.godenwater.web.manager.StationManager;
public class ADFContextListener implements ServletContextListener {

    protected String RTU_RCV_KEY = "RTU_RCV_KEY";

    public ADFContextListener() {

    }

    public void contextInitialized(ServletContextEvent event) {

        System.out.println(">> " + RTU_RCV_KEY + "   "
                + event.getServletContext().getRealPath("/"));

        String homePath = event.getServletContext().getRealPath("/");
        if (homePath != null) {
            homePath = homePath.replaceAll("\\\\", "/");
        }

        //初始化数据
        StationManager.getInstance().init();
        StationDataManager.getInstance().init();



        //gprs采用内存的方式，短信接收监控文件夹
        MsgFileMonitor m;
        MsgFileMonitor mSl651;
        try {
            System.out.println(">> >> >> >>  处理短信接收文件.....");
            m = new MsgFileMonitor(800);// 文件监测时间，时间频度
            //监听上报报文目录,
            //>>此处监控的文件为目录中新增的文件，所以针对如果重启后，还存在于目录中的文件，会有不处理的现象，
            //>>故可将原目录中的文件移动到临时目录中，然后在监听启动后，重新移动到相应的监听目录
            String msgpath = RtuConfig.getMsgRcvPath();
            createDir(msgpath + "/YY/sms");
            m.monitor(msgpath + "/YY/sms", new MsgYYFileListener());
            m.start();
            System.out.println(">>>RTU<<<  Monitor SMS File Directory " + msgpath + "/YY/sms");

            System.out.println(">> >> >> >>  处理sql651短信接收文件.....");
            mSl651 = new MsgFileMonitor(800);// 文件监测时间，时间频度
            //监听上报报文目录,
            //>>此处监控的文件为目录中新增的文件，所以针对如果重启后，还存在于目录中的文件，会有不处理的现象，
            //>>故可将原目录中的文件移动到临时目录中，然后在监听启动后，重新移动到相应的监听目录

            createDir(msgpath + "/SL651/sms");
            mSl651.monitor(msgpath + "/SL651/sms", new MsgSL651FileListener());
            mSl651.start();
            System.out.println(">>>RTU<<<  Monitor SMS File Directory " + msgpath + "/SL651/sms");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void createDir(String dirPath) {
        try {
            File dir = new File(dirPath);
            if (!dir.exists()) {
                FileUtils.forceMkdir(dir);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void contextDestroyed(ServletContextEvent event) {

        // 关闭基础服务
        System.out.println(">>>RTU<<<  System RTU Msg File Monitor Destroyed");

    }

}
