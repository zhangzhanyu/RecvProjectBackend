package com.godenwater.web.listener;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import com.godenwater.core.spring.Application;
import com.godenwater.core.spring.BaseDao;

public class CmdFileListener implements FileAlterationListener {

	MsgFileMonitor monitor = null;

	private BaseDao dao = (BaseDao) Application.getInstance()
			.getBean("baseDao");
	
	@Override
	public void onStart(FileAlterationObserver observer) {
		// System.out.println("onStart");
	}

	@Override
	public void onDirectoryCreate(File directory) {
		System.out.println("onDirectoryCreate:" + directory.getName());
	}

	@Override
	public void onDirectoryChange(File directory) {
		System.out.println("onDirectoryChange:" + directory.getName());
	}

	@Override
	public void onDirectoryDelete(File directory) {
		System.out.println("onDirectoryDelete:" + directory.getName());
	}

	@Override
	public void onFileCreate(File file) {
		System.out.println("onFileCreate:" + file.getName());
		// FileUtils.copyFileToDirectory(srcFile, destDir);
		String fileName = file.getName();

		String id= fileName.substring(0,fileName.lastIndexOf("."));
		System.out.println(">>>>>>> CmdFileListener "+fileName);
		try {
			// 第一步：更新库
			String sql = "update rtu_caller set flag = '1',downtime = ? where id = ?";
			
			//dao.getJdbcTemplate().execute(sql);
			dao.getJdbcTemplate().update(sql,new Date(), id);
			// 删除文件
			//FileUtils.deleteQuietly(file);
		} catch (Exception e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onFileChange(File file) {
		System.out.println("onFileChange:" + file.getName());
	}

	@Override
	public void onFileDelete(File file) {
		System.out.println("onFileDelete:" + file.getName());
	}

	@Override
	public void onStop(FileAlterationObserver observer) {
		// System.out.println("onStop");
	}

}
