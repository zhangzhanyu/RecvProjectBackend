package com.godenwater.web.listener;

import java.io.File;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

public class MsgFileMonitor {
	
	FileAlterationMonitor monitor = null;  
    public MsgFileMonitor(long interval) throws Exception {  
        monitor = new FileAlterationMonitor(interval);  
    }  
  
    public void monitor(String path, FileAlterationListener listener) {  
        FileAlterationObserver observer = new FileAlterationObserver(new File(path));  
        monitor.addObserver(observer);  
        observer.addListener(listener);  
    }  
    
    public void stop() throws Exception{  
        monitor.stop();  
    }
    
    public void start() throws Exception {  
        monitor.start();  
    }
    
    public static void main(String[] args) throws Exception {  
    	MsgFileMonitor m;
		try {
			m = new MsgFileMonitor(800);//文件监测时间，时间频度

//			byte[] category = new byte[1];
//			category[0] = 0x48;
//			System.out.println(new String(category));
//			
//			byte[] itemFlag = ByteUtils.hexStringToBytes("F460");
//			
// 			if(Arrays.equals(itemFlag,new byte[]{(byte)0xF4,0x60})){
// 				System.out.println("okkkkkkkk");
// 			}
			
			
			String path = "d:/rcvApp/msg";

			m.monitor(path + "/SL651/rcv", new MsgSL651FileListener());
			m.monitor(path + "/YY/rcv", new MsgYYFileListener());
            m.monitor(path + "/YLN/rcv", new MsgYlnFileListener());
			m.start();

			System.out
					.println(">>>RTU<<<  System RTU Msg File Monitor Started");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }  
}
