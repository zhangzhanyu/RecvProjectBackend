package com.godenwater.web.listener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import com.godenwater.recv.decode.Sl651MessageDecoder;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.server.all.RtuConfig;
import com.godenwater.recv.service.MessageSl651Consumer;
import com.godenwater.utils.ByteUtils;

public class MsgSL651FileListener extends FileAlterationListenerAdaptor {

    MsgFileMonitor monitor = null;

    //按年月日来存储
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public void onStart(FileAlterationObserver observer) {
        // System.out.println("onStart");
        super.onStart(observer);
    }

    @Override
    public void onDirectoryCreate(File directory) {
        //System.out.println("onDirectoryCreate:" + directory.getName());
    }

    @Override
    public void onDirectoryChange(File directory) {
        //System.out.println("onDirectoryChange:" + directory.getName());
    }

    @Override
    public void onDirectoryDelete(File directory) {
        //System.out.println("onDirectoryDelete:" + directory.getName());
    }

    @Override
    public void onFileCreate(File file) {
        //System.out.println("SL651 file create:" + file.getName());
        // FileUtils.copyFileToDirectory(srcFile, destDir);
        String fileName = file.getName();
//		 	String[] fileInfo = fileName.split("_");
        // 第一步：读取报文文件
        List<String> lines;
        try {
            // lines = Files.readAllLines(Paths.get("/tmp/test.csv"),
            // Charset.forName("UTF-8"));
            lines = FileUtils.readLines(file, "UTF-8");
            for (String line : lines) {
                //System.out.println("READ SL651>> " + line);


                //1.1：转换为报文字节数组
                byte[] bytes = ByteUtils.hexStringToBytes(line);

//					//1.2：解析报文
                CommonMessage message = Sl651MessageDecoder.decodeHex(bytes);

                MessageSl651Consumer consumer = new MessageSl651Consumer();
                consumer.processMessage("sms", message, "", "", 0, 0);

            }

            //最后，将文件拷贝至成功目录
            String rcvpath = RtuConfig.getMsgRcvPath();
            FileUtils.copyFileToDirectory(file, new File(rcvpath + "/SL651/suc/" + sdf.format(new Date())), true);

            //然后清除目录文件
            FileUtils.deleteQuietly(file);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void onFileChange(File file) {
        //System.out.println("onFileChange:" + file.getName());
    }

    @Override
    public void onFileDelete(File file) {
        //System.out.println("onFileDelete:" + file.getName());
    }

    @Override
    public void onStop(FileAlterationObserver observer) {
        // System.out.println("onStop");
        super.onStop(observer);
    }

}
