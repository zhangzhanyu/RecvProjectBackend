package com.godenwater.web.listener;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import com.godenwater.recv.decode.Szy206MessageDecoder;
import com.godenwater.recv.model.CommonMessage;
import com.godenwater.recv.service.MessageSzy206Consumer;
import com.godenwater.utils.ByteUtils;

public class MsgSzy206FileListener implements FileAlterationListener {

	MsgFileMonitor monitor = null;

	@Override
	public void onStart(FileAlterationObserver observer) {
		// System.out.println("onStart");
	}

	@Override
	public void onDirectoryCreate(File directory) {
		System.out.println("onDirectoryCreate:" + directory.getName());
	}

	@Override
	public void onDirectoryChange(File directory) {
		System.out.println("onDirectoryChange:" + directory.getName());
	}

	@Override
	public void onDirectoryDelete(File directory) {
		System.out.println("onDirectoryDelete:" + directory.getName());
	}

	@Override
	public void onFileCreate(File file) {
		System.out.println("onFileCreate:" + file.getName());
		// FileUtils.copyFileToDirectory(srcFile, destDir);
		String fileName = file.getName();
		String[] fileInfo = fileName.split("_");

		String channel = fileInfo[0];
		String stcd = fileInfo[1];
		String crcflag = fileInfo[2];
		// 第一步：读取报文文件
		List<String> lines;
		try {
			// lines = Files.readAllLines(Paths.get("/tmp/test.csv"),
			// Charset.forName("UTF-8"));
			lines = FileUtils.readLines(file, "UTF-8");
			for (String line : lines) {
				System.out.println("READ SZY206>> " + line);

				// 1.1：转换为报文字节数组
				byte[] bytes = ByteUtils.hexStringToBytes(line);
				//
				// //1.2：解析报文
				// 不能使用74做为中心站名
				CommonMessage message = Szy206MessageDecoder.decode(bytes);
				
				MessageSzy206Consumer consumer = new MessageSzy206Consumer();
				consumer.process(channel, message);
				// if (bytes[0] == (byte) Symbol.SOH_ASC) {
				// // 1、表示是水文的ASC码协议
				// message = Sl651MessageDecoder.decodeAsc(bytes);
				// } else if (bytes[0] == (byte) Symbol.SOH_HEX) {
				// // 2、表示是水文的HEX码协议 表示是7474，水文的16进制报文数据
				// if (bytes[1] == (byte) Symbol.SOH_HEX) {
				// message = Sl651MessageDecoder.decodeHex(buffer);
				// } else {
				// // 不能使用74做为中心站名
				// message = YanyuMessageDecoder.decode(buffer, secondBit);
				// }
				//
				// } else if (bytes[0] == (byte) 0x68) {
				// // 水资源协议
				// message = Szy206MessageDecoder.decode(buffer);
				// }

			}

			// 最后，将文件拷贝至成功目录
			FileUtils.copyFileToDirectory(file, new File("d:\\rcvApp\\suc"),
					true);

			// 然后清除目录文件
			FileUtils.deleteQuietly(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onFileChange(File file) {
		System.out.println("onFileChange:" + file.getName());
	}

	@Override
	public void onFileDelete(File file) {
		System.out.println("onFileDelete:" + file.getName());
	}

	@Override
	public void onStop(FileAlterationObserver observer) {
		// System.out.println("onStop");
	}

}
