package com.godenwater.web.manager;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.godenwater.framework.config.SpringUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import com.godenwater.core.spring.Application;
import com.godenwater.core.spring.BaseDao;
import com.godenwater.web.rtu.model.RtuStationModel;

/**
 * 测站管理，主要通过此测站管理获取测站的与数据库的对应关系数据，如燕禹的设备只有中心站码和测站编码
 *
 * @author admin
 */
public class StationManager {

    /**
     * Lock object for synchronized session count updates.
     */
    private Object m_lock;

    @Autowired
    private BaseDao baseReadDao;
    private static Logger logger = LoggerFactory.getLogger(StationManager.class);
    private static StationManager instance = null;

    private Map<String, RtuStationModel> mStations = new HashMap<String, RtuStationModel>();

    private StationManager() {
        // create a lock object for the PropManager
        m_lock = new Object();
    }

    public static synchronized StationManager getInstance() {
        if (instance == null) {
            instance = new StationManager();
            // instance.init();
        }
        return instance;
    }

    public void init() {

        if (baseReadDao == null) {
            baseReadDao = SpringUtils.getBean("baseReadDao");
        }

        String sql = "select * from rtu_station a left join rtu_station_bc b ON a.stcd = b.stcd";
        try {
            List<RtuStationModel> stationList = baseReadDao.getJdbcTemplate().query(
                    sql, new RowMapper<RtuStationModel>() {

                        public RtuStationModel mapRow(ResultSet rs, int rowNum)
                                throws SQLException {
                            RtuStationModel station = new RtuStationModel();

                            station.setStcd(rs.getString("STCD"));
                            station.setStcd8(rs.getString("STCD8"));

                            station.setCodetype(rs.getString("CODETYPE"));
                            station.setStlc(rs.getString("STLC"));
                            station.setStnm(rs.getString("STNM"));
                            station.setSttp(rs.getString("STTP"));
                            station.setBsnm(rs.getString("BSNM"));
                            station.setRvnm(rs.getString("RVNM"));
                            station.setHnnm(rs.getString("HNNM"));

                            station.setLgtd(rs.getString("LGTD"));
                            station.setLttd(rs.getString("LTTD"));

                            station.setProtocol(rs.getString("PROTOCOL"));
                            station.setMsgmode(rs.getString("MSGMODE"));
                            station.setWorkmode(rs.getString("WORKMODE"));

                            station.setProject(rs.getString("PROJECT"));
                            station.setSupply(rs.getString("SUPPLY"));
                            station.setAddvcd(rs.getString("ADDVCD"));
                            station.setAdmauth(rs.getString("ADMAUTH"));

                            station.setRtucd(rs.getString("RTUCD"));
                            station.setDtmel(rs.getBigDecimal("DTMEL"));

                            // private String stt;
                            // private String valid;
                            // private String wth;
                            // private String dtmnm;
                            // private BigDecimal dtmel;
                            // private BigDecimal dtpr;

                            station.setRainValidDay(rs
                                    .getBigDecimal("RAIN_VALID_DAY"));
                            station.setRainValidHour(rs
                                    .getBigDecimal("RAIN_VALID_HOUR"));
                            station.setRainValidMinute(rs
                                    .getBigDecimal("RAIN_VALID_MINUTE"));
                            station.setRainWarnDay(rs
                                    .getBigDecimal("RAIN_WARN_DAY"));
                            station.setRainWarnHour(rs
                                    .getBigDecimal("RAIN_WARN_HOUR"));
                            station.setRainWarnMinute(rs
                                    .getBigDecimal("RAIN_WARN_MINUTE"));

                            station.setRiverSensorType(rs
                                    .getString("RIVER_SENSOR_TYPE"));
                            station.setRiverValidHigh(rs
                                    .getBigDecimal("RIVER_VALID_HIGH"));
                            station.setRiverValidLow(rs
                                    .getBigDecimal("RIVER_VALID_LOW"));
                            station.setRiverValidRange(rs
                                    .getBigDecimal("RIVER_VALID_RANGE"));
                            station.setRiverWarnHigh(rs
                                    .getBigDecimal("RIVER_WARN_HIGH"));
                            station.setRiverWarnLow(rs
                                    .getBigDecimal("RIVER_WARN_LOW"));
                            station.setRiverWarnRange(rs
                                    .getBigDecimal("RIVER_WARN_RANGE"));

                            station.setF01(rs.getString("F01"));
                            station.setF02(rs.getString("F02"));
                            station.setF03(rs.getString("F03"));
                            station.setF04(rs.getString("F04"));
                            station.setF05(rs.getString("F05"));
                            station.setF06(rs.getString("F06"));
                            station.setF07(rs.getString("F07"));
                            station.setF08(rs.getString("F08"));
                            station.setF09(rs.getString("F09"));
                            station.setF0a(rs.getString("F0A"));
                            station.setF0b(rs.getString("F0A"));
                            station.setF0c(rs.getString("F0A"));
                            station.setF0d(rs.getString("F0A"));
                            station.setF0e(rs.getString("F0A"));
                            station.setF0f(rs.getString("F0A"));
                            station.setTelphone(rs.getString("TELPHONE"));
                            station.setCenter(rs.getString("CENTER"));
                            station.setFlag_hd(rs.getString("FLAG_HD"));
                            station.setBorrow(rs.getString("BORROW"));
                            station.setFlag_rain(rs.getString("FLAG_RAIN"));
                            station.setFlag_water(rs.getString("FLAG_WATER"));
                            return station;
                        }
                    });


            //针对燕禹产品的编码
            for (RtuStationModel station : stationList) {
                mStations.put(station.getStcd(), station);

            }
            logger.debug(">> 数据处理模块，测站加载完成，共加载 " + stationList.size() + " 个测站信息。");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RtuStationModel getStation(String stcd) {
        return this.mStations.get(stcd);
    }


    public void store(RtuStationModel station) {
        synchronized (m_lock) {

            mStations.remove(station.getStcd());
            mStations.put(station.getStcd(), station);
            logger.info(">>handle station stcd " + station.getStcd() + "  8位码 " + station.getFlag_rain() + " 是否入雨量 " + station.getFlag_water() + " 是否入水位 " + station.getStcd8() + " 高程 " + station.getDtmel() + " 协议 " + station.getProtocol() + " 电话 " + station.getTelphone() + " 中心站地址 " + station.getCenter() + " 宏电 " + station.getFlag_hd() + " 借位站号 " + station.getBorrow());
        }
    }

    public void update(RtuStationModel station) {
        synchronized (m_lock) {

            String stcd = station.getStcd();
            if (!mStations.containsKey(stcd)) {
                this.mStations.put(stcd, station);
            }

        }
    }

    public void destroyed(RtuStationModel station) {
        synchronized (m_lock) {

            String stcd = station.getStcd();
            if (mStations.containsKey(stcd)) {
                this.mStations.remove(stcd);
            }

        }
    }

    public static void main(String[] args) {
        try {
            List<String> lines = FileUtils.readLines(new File("d:/station.txt"));
            for (String line : lines) {

                String[] data = line.split("\t");
                System.out.println(data.length + "\t" + data[0] + " = " + data[1] + " = " + data[10]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
