package com.godenwater.web.manager;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.godenwater.core.container.BasicModule;

public class TaskManager  extends BasicModule{

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public TaskManager() {
		super("Task Manager"); 
	}

	@Override
	public void start() {
		logger.info("manager_TaskManager");
		
		//定时更新测站的访问时间
		//int stationPeriod = 5 * 60 * 1000;// 5分钟
//		TaskEngine.getInstance().scheduleAtFixedRate(new StationTask(), stationPeriod,
//				stationPeriod);
		 
		//定时下发召测任务，下发任务采用测站主动下发式，在sessionManager的idleSession方法中
	/*	int stationPeriod = 5 * 60 * 1000;// 5分钟
		TaskEngine.getInstance().scheduleAtFixedRate(new StationMonitorTask(), stationPeriod, stationPeriod);*/
		 
		 
		
		//定时产生校时任务
		//int caller4APeriod = 24 * 60 * 60 * 1000;// 24个小时
		//TaskEngine.getInstance().scheduleAtFixedRate(new Rtu4ATask(), callerPeriod,
			//	caller4APeriod);
		
		//定时产生查询指定要素(日雨量)的任务(每天8:15执行)
		Date now = new Date();
		now.setHours(8);
		now.setMinutes(15);
		long rtu3APeriod = (new Date()).getTime()-now.getTime();//超过8:15的时间
		if(rtu3APeriod<0)
		{
			rtu3APeriod += 24 * 60 * 60 * 1000;
		}
		rtu3APeriod = 24 * 60 * 60 * 1000 - rtu3APeriod;
		int caller3APeriod = 24 * 60 * 60 * 1000;// 24个小时
//		TaskEngine.getInstance().scheduleAtFixedRate(new Rtu3ATask(), rtu3APeriod,
//				caller3APeriod);
		
	}
	
}
