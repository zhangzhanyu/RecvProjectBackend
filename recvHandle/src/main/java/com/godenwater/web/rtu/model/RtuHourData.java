package com.godenwater.web.rtu.model;

import java.io.Serializable;

/**
 * Created by lhc on 2019/10/28 14:03
 */
public class RtuHourData implements Serializable {
    private String id;
    private String stcd;
    private String stcd8;
    private String funccode;
    private String viewtime;
    private String sendtime;
    private String valtage;
    private String p05;
    private String p10;
    private String p15;
    private String p20;
    private String p25;
    private String p30;
    private String p35;
    private String p40;
    private String p45;
    private String p50;
    private String p55;
    private String p60;
    private String h05;
    private String h10;
    private String h15;
    private String h20;
    private String h25;
    private String h30;
    private String h35;
    private String h40;
    private String h45;
    private String h50;
    private String h55;
    private String h60;
    private String dbFlag;

    public RtuHourData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStcd() {
        return stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public String getStcd8() {
        return stcd8;
    }

    public void setStcd8(String stcd8) {
        this.stcd8 = stcd8;
    }

    public String getFunccode() {
        return funccode;
    }

    public void setFunccode(String funccode) {
        this.funccode = funccode;
    }

    public String getViewtime() {
        return viewtime;
    }

    public void setViewtime(String viewtime) {
        this.viewtime = viewtime;
    }

    public String getSendtime() {
        return sendtime;
    }

    public void setSendtime(String sendtime) {
        this.sendtime = sendtime;
    }

    public String getValtage() {
        return valtage;
    }

    public void setValtage(String valtage) {
        this.valtage = valtage;
    }

    public String getP05() {
        return p05;
    }

    public void setP05(String p05) {
        this.p05 = p05;
    }

    public String getP10() {
        return p10;
    }

    public void setP10(String p10) {
        this.p10 = p10;
    }

    public String getP15() {
        return p15;
    }

    public void setP15(String p15) {
        this.p15 = p15;
    }

    public String getP20() {
        return p20;
    }

    public void setP20(String p20) {
        this.p20 = p20;
    }

    public String getP25() {
        return p25;
    }

    public void setP25(String p25) {
        this.p25 = p25;
    }

    public String getP30() {
        return p30;
    }

    public void setP30(String p30) {
        this.p30 = p30;
    }

    public String getP35() {
        return p35;
    }

    public void setP35(String p35) {
        this.p35 = p35;
    }

    public String getP40() {
        return p40;
    }

    public void setP40(String p40) {
        this.p40 = p40;
    }

    public String getP45() {
        return p45;
    }

    public void setP45(String p45) {
        this.p45 = p45;
    }

    public String getP50() {
        return p50;
    }

    public void setP50(String p50) {
        this.p50 = p50;
    }

    public String getP55() {
        return p55;
    }

    public void setP55(String p55) {
        this.p55 = p55;
    }

    public String getP60() {
        return p60;
    }

    public void setP60(String p60) {
        this.p60 = p60;
    }

    public String getH05() {
        return h05;
    }

    public void setH05(String h05) {
        this.h05 = h05;
    }

    public String getH10() {
        return h10;
    }

    public void setH10(String h10) {
        this.h10 = h10;
    }

    public String getH15() {
        return h15;
    }

    public void setH15(String h15) {
        this.h15 = h15;
    }

    public String getH20() {
        return h20;
    }

    public void setH20(String h20) {
        this.h20 = h20;
    }

    public String getH25() {
        return h25;
    }

    public void setH25(String h25) {
        this.h25 = h25;
    }

    public String getH30() {
        return h30;
    }

    public void setH30(String h30) {
        this.h30 = h30;
    }

    public String getH35() {
        return h35;
    }

    public void setH35(String h35) {
        this.h35 = h35;
    }

    public String getH40() {
        return h40;
    }

    public void setH40(String h40) {
        this.h40 = h40;
    }

    public String getH45() {
        return h45;
    }

    public void setH45(String h45) {
        this.h45 = h45;
    }

    public String getH50() {
        return h50;
    }

    public void setH50(String h50) {
        this.h50 = h50;
    }

    public String getH55() {
        return h55;
    }

    public void setH55(String h55) {
        this.h55 = h55;
    }

    public String getH60() {
        return h60;
    }

    public void setH60(String h60) {
        this.h60 = h60;
    }

    public String getDbFlag() {
        return dbFlag;
    }

    public void setDbFlag(String dbFlag) {
        this.dbFlag = dbFlag;
    }
}

