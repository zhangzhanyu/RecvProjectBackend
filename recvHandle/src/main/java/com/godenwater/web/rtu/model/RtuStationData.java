package com.godenwater.web.rtu.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the rtu_station database table.
 * 
 */
public class RtuStationData implements Serializable {

	private String stcd;

	private String river;

	private String rain5;
	private String rain60;

	public RtuStationData() {
	}

	public String getStcd() {
		return stcd;
	}

	public void setStcd(String stcd) {
		this.stcd = stcd;
	}

	public String getRiver() {
		return river;
	}

	public void setRiver(String river) {
		this.river = river;
	}


	public String getRain5() {
		return rain5;
	}

	public void setRain5(String rain5) {
		this.rain5 = rain5;
	}

	public String getRain60() {
		return rain60;
	}

	public void setRain60(String rain60) {
		this.rain60 = rain60;
	}
}