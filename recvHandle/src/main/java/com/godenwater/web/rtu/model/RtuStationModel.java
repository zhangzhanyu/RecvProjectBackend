package com.godenwater.web.rtu.model;

import java.io.Serializable;

import java.math.BigDecimal;

/**
 * The persistent class for the rtu_station database table.
 */
public class RtuStationModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private String stcd;

    private String f01;

    private String f02;

    private String f03;

    private String f04;

    private String f05;

    private String f06;

    private String f07;

    private String f08;

    private String f09;

    private String f0a;

    private String f0b;

    private String f0c;

    private String f0d;

    private String f0e;

    private String f0f;
    private String center;
    private String telphone;
    private String flag_hd;
    private String borrow;

    private String flag_water;
    private String flag_rain;
    private String online;

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getFlag_hd() {
        return flag_hd;
    }

    public void setFlag_hd(String flag_hd) {
        this.flag_hd = flag_hd;
    }

    public String getBorrow() {
        return borrow;
    }

    public void setBorrow(String borrow) {
        this.borrow = borrow;
    }

    public RtuStationModel() {
    }

    public String getStcd() {
        return this.stcd;
    }

    public void setStcd(String stcd) {
        this.stcd = stcd;
    }

    public String getF01() {
        return this.f01;
    }

    public void setF01(String f01) {
        this.f01 = f01;
    }

    public String getF02() {
        return this.f02;
    }

    public void setF02(String f02) {
        this.f02 = f02;
    }

    public String getF03() {
        return this.f03;
    }

    public void setF03(String f03) {
        this.f03 = f03;
    }

    public String getF04() {
        return this.f04;
    }

    public void setF04(String f04) {
        this.f04 = f04;
    }

    public String getF05() {
        return this.f05;
    }

    public void setF05(String f05) {
        this.f05 = f05;
    }

    public String getF06() {
        return this.f06;
    }

    public void setF06(String f06) {
        this.f06 = f06;
    }

    public String getF07() {
        return this.f07;
    }

    public void setF07(String f07) {
        this.f07 = f07;
    }

    public String getF08() {
        return this.f08;
    }

    public void setF08(String f08) {
        this.f08 = f08;
    }

    public String getF09() {
        return this.f09;
    }

    public void setF09(String f09) {
        this.f09 = f09;
    }

    public String getF0a() {
        return this.f0a;
    }

    public void setF0a(String f0a) {
        this.f0a = f0a;
    }

    public String getF0b() {
        return this.f0b;
    }

    public void setF0b(String f0b) {
        this.f0b = f0b;
    }

    public String getF0c() {
        return this.f0c;
    }

    public void setF0c(String f0c) {
        this.f0c = f0c;
    }

    public String getF0d() {
        return this.f0d;
    }

    public void setF0d(String f0d) {
        this.f0d = f0d;
    }

    public String getF0e() {
        return this.f0e;
    }

    public void setF0e(String f0e) {
        this.f0e = f0e;
    }

    public String getF0f() {
        return this.f0f;
    }

    public void setF0f(String f0f) {
        this.f0f = f0f;
    }

    private String rtucd;

    private String stcd8;

    private String codetype;

    private String rvnm;

    private String stlc;

    private String stnm;

    private String stt;

    private String sttp;

    private String hnnm;

    private String lgtd;

    private String lttd;

    private String protocol;

    private String msgmode;

    private String workmode;

    private String project;

    private String supply;

    private String valid;

    private String wth;

    private String addvcd;

    private String admauth;

    private String bsnm;

    private String dtmnm;

    private BigDecimal dtmel;

    private BigDecimal dtpr;

    private BigDecimal rainValidDay;

    private BigDecimal rainValidHour;

    private BigDecimal rainValidMinute;

    private BigDecimal rainWarnDay;

    private BigDecimal rainWarnHour;

    private BigDecimal rainWarnMinute;

    private String riverSensorType;

    private BigDecimal riverValidHigh;

    private BigDecimal riverValidLow;

    private BigDecimal riverValidRange;

    private BigDecimal riverWarnHigh;

    private BigDecimal riverWarnLow;

    private BigDecimal riverWarnRange;

    public String getFlag_water() {
        return flag_water;
    }

    public String getFlag_rain() {
        return flag_rain;
    }

    public void setFlag_water(String flag_water) {
        this.flag_water = flag_water;
    }

    public void setFlag_rain(String flag_rain) {
        this.flag_rain = flag_rain;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getAddvcd() {
        return this.addvcd;
    }

    public void setAddvcd(String addvcd) {
        this.addvcd = addvcd;
    }

    public String getAdmauth() {
        return this.admauth;
    }

    public void setAdmauth(String admauth) {
        this.admauth = admauth;
    }

    public String getBsnm() {
        return this.bsnm;
    }

    public void setBsnm(String bsnm) {
        this.bsnm = bsnm;
    }

    public BigDecimal getDtmel() {
        return this.dtmel;
    }

    public void setDtmel(BigDecimal dtmel) {
        this.dtmel = dtmel;
    }

    public BigDecimal getDtpr() {
        return this.dtpr;
    }

    public void setDtpr(BigDecimal dtpr) {
        this.dtpr = dtpr;
    }

    public String getHnnm() {
        return this.hnnm;
    }

    public void setHnnm(String hnnm) {
        this.hnnm = hnnm;
    }

    public String getLgtd() {
        return this.lgtd;
    }

    public void setLgtd(String lgtd) {
        this.lgtd = lgtd;
    }

    public String getLttd() {
        return this.lttd;
    }

    public void setLttd(String lttd) {
        this.lttd = lttd;
    }

    public String getProject() {
        return this.project;
    }

    public void setProject(String project) {
        this.project = project;
    }


    public String getWorkmode() {
        return workmode;
    }

    public void setWorkmode(String workmode) {
        this.workmode = workmode;
    }

    public String getCodetype() {
        return codetype;
    }

    public void setCodetype(String codetype) {
        this.codetype = codetype;
    }

    public BigDecimal getRainValidDay() {
        return this.rainValidDay;
    }

    public void setRainValidDay(BigDecimal rainValidDay) {
        this.rainValidDay = rainValidDay;
    }

    public BigDecimal getRainValidHour() {
        return this.rainValidHour;
    }

    public void setRainValidHour(BigDecimal rainValidHour) {
        this.rainValidHour = rainValidHour;
    }

    public BigDecimal getRainValidMinute() {
        return this.rainValidMinute;
    }

    public void setRainValidMinute(BigDecimal rainValidMinute) {
        this.rainValidMinute = rainValidMinute;
    }


    public BigDecimal getRainWarnDay() {
        return rainWarnDay;
    }

    public void setRainWarnDay(BigDecimal rainWarnDay) {
        this.rainWarnDay = rainWarnDay;
    }

    public BigDecimal getRainWarnHour() {
        return rainWarnHour;
    }

    public void setRainWarnHour(BigDecimal rainWarnHour) {
        this.rainWarnHour = rainWarnHour;
    }

    public BigDecimal getRainWarnMinute() {
        return rainWarnMinute;
    }

    public void setRainWarnMinute(BigDecimal rainWarnMinute) {
        this.rainWarnMinute = rainWarnMinute;
    }

    public String getRiverSensorType() {
        return this.riverSensorType;
    }

    public void setRiverSensorType(String riverSensorType) {
        this.riverSensorType = riverSensorType;
    }

    public BigDecimal getRiverValidHigh() {
        return this.riverValidHigh;
    }

    public void setRiverValidHigh(BigDecimal riverValidHigh) {
        this.riverValidHigh = riverValidHigh;
    }

    public BigDecimal getRiverValidLow() {
        return this.riverValidLow;
    }

    public void setRiverValidLow(BigDecimal riverValidLow) {
        this.riverValidLow = riverValidLow;
    }

    public BigDecimal getRiverValidRange() {
        return this.riverValidRange;
    }

    public void setRiverValidRange(BigDecimal riverValidRange) {
        this.riverValidRange = riverValidRange;
    }

    public BigDecimal getRiverWarnHigh() {
        return this.riverWarnHigh;
    }

    public void setRiverWarnHigh(BigDecimal riverWarnHigh) {
        this.riverWarnHigh = riverWarnHigh;
    }

    public BigDecimal getRiverWarnLow() {
        return this.riverWarnLow;
    }

    public void setRiverWarnLow(BigDecimal riverWarnLow) {
        this.riverWarnLow = riverWarnLow;
    }

    public BigDecimal getRiverWarnRange() {
        return this.riverWarnRange;
    }

    public void setRiverWarnRange(BigDecimal riverWarnRange) {
        this.riverWarnRange = riverWarnRange;
    }

    public String getRtucd() {
        return this.rtucd;
    }

    public void setRtucd(String rtucd) {
        this.rtucd = rtucd;
    }

    public String getRvnm() {
        return this.rvnm;
    }

    public void setRvnm(String rvnm) {
        this.rvnm = rvnm;
    }

    public String getStcd8() {
        return this.stcd8;
    }

    public void setStcd8(String stcd8) {
        this.stcd8 = stcd8;
    }

    public String getStlc() {
        return this.stlc;
    }

    public void setStlc(String stlc) {
        this.stlc = stlc;
    }

    public String getStnm() {
        return this.stnm;
    }

    public void setStnm(String stnm) {
        this.stnm = stnm;
    }

    public String getStt() {
        return this.stt;
    }

    public void setStt(String stt) {
        this.stt = stt;
    }

    public String getSttp() {
        return this.sttp;
    }

    public void setSttp(String sttp) {
        this.sttp = sttp;
    }

    public String getSupply() {
        return this.supply;
    }

    public void setSupply(String supply) {
        this.supply = supply;
    }

    public String getValid() {
        return this.valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getWth() {
        return this.wth;
    }

    public void setWth(String wth) {
        this.wth = wth;
    }

    public String getMsgmode() {
        return msgmode;
    }

    public void setMsgmode(String msgmode) {
        this.msgmode = msgmode;
    }

    public String getDtmnm() {
        return dtmnm;
    }

    public void setDtmnm(String dtmnm) {
        this.dtmnm = dtmnm;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }
}