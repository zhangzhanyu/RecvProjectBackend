package cn.gov.mwr.sl330;

/**
 * 编码要素标识符及存储的数据类型、格式化要求、小数位、最大值、最小值等规约类
 * 
 * @ClassName: xxx
 * @Description: TODO
 * @author lipujun
 * @date Mar 20, 2013
 * 
 */
public class Element {

	private String code;// 编码要素标识符
	private byte type;// 存储数据类型
	private String format;// 如数据类型为时间，则可以格式化为yy-mm-dd HH:MM；如时间类型为数值，可扩展为小数点位数等
	private String max;// 最大值
	private String min;// 最小值
	private String table;// 所对应的表
	private String field;// 所对应的字段

	public Element(String code, byte type, String format) {
		super();
		this.code = code;
		this.type = type;
		this.format = format;
	}

	public Element(String code, byte type, String format, String max, String min) {
		super();
		this.code = code;
		this.type = type;
		this.format = format;
		this.max = max;
		this.min = min;
	}
}
