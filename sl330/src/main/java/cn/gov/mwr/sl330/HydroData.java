package cn.gov.mwr.sl330;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 水情信息报文数据类，针对报文将其解析为水情报文数据。
 * 
 * @ClassName: HydroData
 * @Description: TODO
 * @author lipujun
 * @date Mar 20, 2013
 * 
 */
public class HydroData {

	private String stcd;

	private String typeCode;

	private Date viewDate;

	private List<HydroDataItem> items;

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getStcd() {
		return stcd;
	}

	public void setStcd(String stcd) {
		this.stcd = stcd;
	}

	public Date getViewDate() {
		return viewDate;
	}

	public void setViewDate(Date viewDate) {
		this.viewDate = viewDate;
	}

	public List<HydroDataItem> getItems() {
		return items;
	}

	public void setItems(List<HydroDataItem> items) {
		this.items = items;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(" typeCode: " + typeCode);
		sb.append(" stcd: " + stcd);
		SimpleDateFormat sdf = new SimpleDateFormat(TypeDate.FORMAT_ALL);// "yyyy-MM-dd HH:mm:ss"
		sb.append(" viewDate: " + sdf.format(viewDate));

		if (items != null) {
			for (int i = 0, len = items.size(); i < len; i++) {
				HydroDataItem item = (HydroDataItem) items.get(i);
				sb.append("\n>> item" + i + " " + item.toString());
			}
		}
		return sb.toString();
	}

}
