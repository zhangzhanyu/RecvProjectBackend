package cn.gov.mwr.sl330;

/**
 * 水情报文数据项，针对水情信息上报的具体数据项的定义
 * 
 * @ClassName: HydroDataItem
 * @Description: TODO
 * @author lipujun
 * @date Mar 20, 2013
 * 
 */
public class HydroDataItem {

	private String name;
	private Object value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(" name: " + name);
		sb.append(" value: " + value);
		return sb.toString();
	}

}
