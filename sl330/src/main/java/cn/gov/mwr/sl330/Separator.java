package cn.gov.mwr.sl330;

/**
 * 分隔符，针对水情报文中的分隔符的定义
 * 
 * @ClassName: Separator
 * @Description: TODO
 * @author lipujun
 * @date Mar 20, 2013
 * 
 */
public class Separator {

	public final static String SEP = " ";// 数据分隔符

	public final static String TT = "TT";// 时间分隔符

	public final static String ST = "ST";// 测站分隔符

	public final static String C = "C";// 时间序列报前置符

	public final static String R = "R";// 修正报前置符

	public final static String NN = "NN";// 结束符

}
