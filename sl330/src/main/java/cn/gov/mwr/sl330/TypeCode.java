package cn.gov.mwr.sl330;

/**
 * 水情信息编码分类码，包括了11类水情分类码
 * 
 * @ClassName: TypeCode
 * @Description: TODO
 * @author lipujun
 * @date Mar 20, 2013
 * 
 */
public class TypeCode {

	public final static byte P = 0x50;// 降水
	public final static byte H = 0x48;// 河道
	public final static byte K = 0x4B;// 水库（湖泊）
	public final static byte Z = 0x5A;// 闸坝
	public final static byte D = 0x44;// 泵站
	public final static byte T = 0x54;// 潮汐
	public final static byte M = 0x4D;// 墒情
	public final static byte G = 0x47;// 地下水
	public final static byte Q = 0x51;// 水质
	public final static byte I = 0x49;// 取水口
	public final static byte O = 0x4F;// 排水口

}
