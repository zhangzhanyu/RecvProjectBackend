package cn.gov.mwr.sl330;

public class TypeDate {

	public final static String D = "D";// d 

	public final static String H = "H";// 数字类型

	public final static String N = "N";
	
	public final static String YEAR = "yyyy";//"yyyy-MM-dd HH:mm:ss"
	
	public final static String FORMAT = "yyyyMMddHHmm";//"yyyy-MM-dd HH:mm:ss"
	
	public final static String FORMAT_ALL = "yyyy-MM-dd HH:mm:ss";//"yyyy-MM-dd HH:mm:ss"
}
