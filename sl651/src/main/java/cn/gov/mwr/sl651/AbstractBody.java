package cn.gov.mwr.sl651;

import java.io.Serializable;

public abstract class AbstractBody implements IMessageBody,Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	public int serialId;

	/**
	 * 2、发报时间
	 * 
	 * 取值范围：6字节的BCD编码，YYMMDDHHmmSS
	 */
	public String sendDate;

	/**
	 * 记录原始消息体
	 */
	public byte[] content;

	public void setContents(byte[] content) {
		this.content = content;
	}

	public int getLength() {
		if (this.content != null) {
			return this.content.length;
		} else {
			return 0;
		}
	}
	
	public byte[] getContent(){
		return this.content;
	}

	public int getSerialId() {
		return serialId;
	}

	public void setSerialId(int serialId) {
		this.serialId = serialId;
	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

}
