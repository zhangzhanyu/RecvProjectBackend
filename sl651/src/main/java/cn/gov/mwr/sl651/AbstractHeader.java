package cn.gov.mwr.sl651;

import java.io.Serializable;

/**
 * 报文头的抽象实现
 * 
 * @author lipujun 
 *
 */
public abstract class AbstractHeader implements IMessageHeader,Serializable {

	public byte[] START_BIT;
	public byte[] CENTER_ADDR;
	public byte[] STATION_ADDR;
	public byte[] PASSWORD;
	public byte[] FUNC_CODE;
	public byte[] BODY_SIZE;
	public byte[] BODY_START_BIT;
	public byte[] BODY_COUNT;

	public void setStartBit(byte[] sTARTBIT) {
		START_BIT = sTARTBIT;
	}
	
	
	public void setCenterAddr(byte[] cENTERADDR) {
		CENTER_ADDR = cENTERADDR;
	}

	public void setStationAddr(byte[] sTATIONADDR) {
		STATION_ADDR = sTATIONADDR;
	}

	public void setPassword(byte[] pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public void setFuncCode(byte[] fUNCCODE) {
		FUNC_CODE = fUNCCODE;
	}

	public void setBodySize(byte[] bODYLENGTH) {
		BODY_SIZE = bODYLENGTH;
	}

	public void setBodyStartBit(byte[] bODYSTARTBIT) {
		BODY_START_BIT = bODYSTARTBIT;
	}

	public void setBodyCount(byte[] bODYCOUNT) {
		BODY_COUNT = bODYCOUNT;
	}

	public byte[] getBodyCount() {
		// TODO Auto-generated method stub
		return this.BODY_COUNT;
	}

	public byte[] getBodySize() {
		// TODO Auto-generated method stub
		return this.BODY_SIZE;
	}

	public byte[] getBodyStartBit() {
		// TODO Auto-generated method stub
		return this.BODY_START_BIT;
	}

	public byte[] getCenterAddr() {
		// TODO Auto-generated method stub
		return this.CENTER_ADDR;
	}

	public byte[] getFuncCode() {
		// TODO Auto-generated method stub
		return this.FUNC_CODE;
	}

	public byte[] getPassword() {
		// TODO Auto-generated method stub
		return this.PASSWORD;
	}

	public byte[] getStartBit() {
		// TODO Auto-generated method stub
		return this.START_BIT;
	}

	public byte[] getStationAddr() {
		// TODO Auto-generated method stub
		return this.STATION_ADDR;
	}

}
