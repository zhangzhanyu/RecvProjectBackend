package cn.gov.mwr.sl651;

import java.util.HashMap;
import java.util.Map;

/**
 * 附录C，ASC对HEX码的转换表
* @ClassName: FC 
* @Description: TODO
* @author lipujun 
* @date Jun 24, 2013 
*
 */
public class FCAsc2Hex {

	public static Map<String, String> ASC2HEX = new HashMap<String, String>();
	static {
		ASC2HEX.put("TT", "F0");
		ASC2HEX.put("ST", "F1");
		ASC2HEX.put("RGZS", "F2");
		ASC2HEX.put("PIC", "F3");
		ASC2HEX.put("TT", "F0");
		ASC2HEX.put("ST", "F1");
		ASC2HEX.put("RGZS", "F2");
		ASC2HEX.put("PIC", "F3");
		ASC2HEX.put("DRP", "F4");
		ASC2HEX.put("DRZ1", "F5");
		ASC2HEX.put("DRZ2", "F6");
		ASC2HEX.put("DRZ3", "F7");
		ASC2HEX.put("DRZ4", "F8");
		ASC2HEX.put("DRZ5", "F9");
		ASC2HEX.put("DRZ6", "FA");
		ASC2HEX.put("DRZ7", "FB");
		ASC2HEX.put("DRZ8", "FC");
		ASC2HEX.put("DATA", "FD");
		ASC2HEX.put("AC", "01");
		ASC2HEX.put("AI", "02");
		ASC2HEX.put("C", "03");
		ASC2HEX.put("DRxnn", "04");
		ASC2HEX.put("DT", "05");
		ASC2HEX.put("ED", "06");
		ASC2HEX.put("EJ", "07");
		ASC2HEX.put("FL", "08");
		ASC2HEX.put("G", "09");
		ASC2HEX.put("GN", "0A");
		ASC2HEX.put("GS", "0B");
		ASC2HEX.put("GT", "0C");
		ASC2HEX.put("GTP", "0D");
		ASC2HEX.put("", "0E");
		ASC2HEX.put("HW", "0F");
		ASC2HEX.put("M10", "10");
		ASC2HEX.put("M20", "11");
		ASC2HEX.put("M30", "12");
		ASC2HEX.put("M40", "13");
		ASC2HEX.put("M50", "14");
		ASC2HEX.put("M60", "15");
		ASC2HEX.put("M80", "16");
		ASC2HEX.put("M100", "17");
		ASC2HEX.put("MST", "18");
		ASC2HEX.put("NS", "19");
		ASC2HEX.put("P1", "1A");
		ASC2HEX.put("P2", "1B");
		ASC2HEX.put("P3", "1C");
		ASC2HEX.put("P6", "1D");
		ASC2HEX.put("P12", "1E");
		ASC2HEX.put("PD", "1F");
		ASC2HEX.put("PJ", "20");
		ASC2HEX.put("PN01", "21");
		ASC2HEX.put("PN05", "22");
		ASC2HEX.put("PN10", "23");
		ASC2HEX.put("PN30", "24");
		ASC2HEX.put("PR", "25");
		ASC2HEX.put("PT", "26");
		ASC2HEX.put("Q", "27");
		ASC2HEX.put("Q1", "28");
		ASC2HEX.put("Q2", "29");
		ASC2HEX.put("Q3", "2A");
		ASC2HEX.put("Q4", "2B");
		ASC2HEX.put("Q5", "2C");
		ASC2HEX.put("Q6", "2D");
		ASC2HEX.put("Q7", "2E");
		ASC2HEX.put("Q8", "2F");
		ASC2HEX.put("QA", "30");
		ASC2HEX.put("QZ", "31");
		ASC2HEX.put("SW", "32");
		ASC2HEX.put("UC", "33");
		ASC2HEX.put("UE", "34");
		ASC2HEX.put("US", "35");
		ASC2HEX.put("VA", "36");
		ASC2HEX.put("VJ", "37");
		ASC2HEX.put("VT", "38");
		ASC2HEX.put("Z", "39");
		ASC2HEX.put("ZB", "3A");
		ASC2HEX.put("ZU", "3B");
		ASC2HEX.put("Z1", "3C");
		ASC2HEX.put("Z2", "3D");
		ASC2HEX.put("Z3", "3E");
		ASC2HEX.put("Z4", "3F");
		ASC2HEX.put("Z5", "40");
		ASC2HEX.put("Z6", "41");
		ASC2HEX.put("Z7", "42");
		ASC2HEX.put("Z8", "43");
		ASC2HEX.put("SQ", "44");
		ASC2HEX.put("ZT", "45");
		ASC2HEX.put("p", "46");
		ASC2HEX.put("DO", "47");
		ASC2HEX.put("COND", "48");
		ASC2HEX.put("TURB", "49");
		ASC2HEX.put("CODMN", "4A");
		ASC2HEX.put("REDOX", "4B");
		ASC2HEX.put("NH4N", "4C");
		ASC2HEX.put("TP", "4D");
		ASC2HEX.put("TN", "4E");
		ASC2HEX.put("TOC", "4F");
		ASC2HEX.put("CU", "50");
		ASC2HEX.put("ZN", "51");
		ASC2HEX.put("SE", "52");
		ASC2HEX.put("AS", "53");
		ASC2HEX.put("THG", "54");
		ASC2HEX.put("CD", "55");
		ASC2HEX.put("PB", "56");
		ASC2HEX.put("CHLA", "57");
		ASC2HEX.put("WP1", "58");
		ASC2HEX.put("WP2", "59");
		ASC2HEX.put("WP3", "5A");
		ASC2HEX.put("WP4", "5B");
		ASC2HEX.put("WP5", "5C");
		ASC2HEX.put("WP6", "5D");
		ASC2HEX.put("WP7", "5E");
		ASC2HEX.put("WP8", "5F");
		ASC2HEX.put("SYL1", "60");
		ASC2HEX.put("SYL2", "61");
		ASC2HEX.put("SYL3", "62");
		ASC2HEX.put("SYL4", "63");
		ASC2HEX.put("SYL5", "64");
		ASC2HEX.put("SYL6", "65");
		ASC2HEX.put("SYL7", "66");
		ASC2HEX.put("SYL8", "67");
		ASC2HEX.put("SBL1", "68");
		ASC2HEX.put("SBL2", "69");
		ASC2HEX.put("SBL3", "6A");
		ASC2HEX.put("SBL4", "6B");
		ASC2HEX.put("SBL5", "6C");
		ASC2HEX.put("SBL6", "6D");
		ASC2HEX.put("SBL7", "6E");
		ASC2HEX.put("SBL8", "6F");
		ASC2HEX.put("VTA", "70");
		ASC2HEX.put("VTB", "71");
		ASC2HEX.put("VTC", "72");
		ASC2HEX.put("VIA", "73");
		ASC2HEX.put("VIB", "74");
		ASC2HEX.put("VIC", "75");
	}

}
