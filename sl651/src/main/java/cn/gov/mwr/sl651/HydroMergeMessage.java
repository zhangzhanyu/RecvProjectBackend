package cn.gov.mwr.sl651;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HydroMergeMessage {

	private static Map<String, String> RAIN_DATE = new ConcurrentHashMap<String, String>();
	
	private static Map<String, String> RAIN_TIME = new ConcurrentHashMap<String, String>();

	private static Map<String, Date> RIVER_DATE = new ConcurrentHashMap<String, Date>();
	
	/**
	 * 更新雨量召测的最后时间
	 * @param stcd
	 * @param newDate
	 */
	public static void updateRainDate(String stcd, String newDate) {
		RAIN_DATE.put(stcd, newDate);
	}

	public static String getRainDate(String stcd) {
		return RAIN_DATE.get(stcd);
	}
	
	public static void updateRainTime(String stcd, String newTime) {
		RAIN_TIME.put(stcd, newTime);
	}

	public static String getRainTime(String stcd) {
		return RAIN_TIME.get(stcd);
	}
	
	
	/**
	 * 更新水位召测的最后时间
	 * @param stcd
	 * @param newDate
	 */
	public static void updateRiverDate(String stcd, Date newDate) {
		RIVER_DATE.put(stcd, newDate);
	}

	public static Date getRiverDate(String stcd) {
		return RIVER_DATE.get(stcd);
	}

}
