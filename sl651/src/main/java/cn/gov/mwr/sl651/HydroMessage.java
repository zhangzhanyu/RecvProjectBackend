package cn.gov.mwr.sl651;

import java.io.Serializable;


public class HydroMessage implements IMessage,Serializable  {

	public IMessageHeader header;

	public IMessageBody body;

	public byte eof;

	public byte[] crc;

	public void setHeader(IMessageHeader header) {
		this.header = header;
	}

	public IMessageHeader getHeader() {
		// TODO Auto-generated method stub
		return this.header;
	}

	public void setBody(IMessageBody body) {
		this.body = body;
	}

	public IMessageBody getBody() {
		// TODO Auto-generated method stub
		return this.body;
	}

	public void setEOF(byte eof) {
		this.eof = eof;
	}

	public byte getEOF() {
		// TODO Auto-generated method stub
		return this.eof;
	}

	public void setCRC(byte[] crc) {
		this.crc = crc;
	}

	public byte[] getCRC() {
		// TODO Auto-generated method stub
		return this.crc;
	}
	
}
