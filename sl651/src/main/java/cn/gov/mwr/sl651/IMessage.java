package cn.gov.mwr.sl651;

/**
 * 
 * @ClassName: IMessage
 * @Description:此接口结构定义包括了文件头、文件体、文件结束符、文件CRC校验
 * 
 *               “上行报文”---UP---帧结构
 * 
 *               1、报头，取值范围：01H/7E7EH
 * 
 *               2、中心站地址，取值范围：范围为1～255
 * 
 *               3、遥测站地址
 * 
 *               4、密码
 * 
 *               5、功能码
 * 
 *               6、报文上下行标识及长度
 * 
 *               7、报文起始符，取值范围：STX/SYN
 * 
 *               8、包总数及序列号，报文起始符为SYN时编入该组，其他情况下省略
 * 
 *               9、报文正文
 * 
 *               10、报文结束符,取值：ETB/ETX。
 * 
 *               11、校验码，校验友前所有字节的CRC校验
 *               
 *               “下行报文”---DOWN---帧结构与“上行报文”类似，只需将2、3地址对换即可
 *             
 * 
 * @author lipujun
 * @date Mar 2, 2013
 * 
 */
public interface IMessage {

	public IMessageHeader getHeader();

	public void setHeader(IMessageHeader header);

	public IMessageBody getBody();

	public void setBody(IMessageBody body);

	public byte getEOF();

	public void setEOF(byte eof);

	public byte[] getCRC();

	public void setCRC(byte[] crc);

}
