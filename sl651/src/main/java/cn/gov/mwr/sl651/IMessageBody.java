package cn.gov.mwr.sl651;

/**
 * 报文体接口，使用者可根据此接口对消息体进行扩充，本协议的实现均在cn.gov.mwr.sl651.body
* @ClassName: IMessageBody 
* @Description: TODO
* @author lipujun 
* @date Mar 11, 2013 
* @see 	cn.gov.mwr.sl651.body
*
 */
public interface IMessageBody {

	public void setContents(byte[] content);
	
	public byte[] getContent();

	public int getLength();
	
//	
//	public String getStcd();
//	
//	public String setSttp();
//	
//	public String getViewDate();
//	
//	public String getSendDate();
	
	
}
