package cn.gov.mwr.sl651;

import java.util.Date;

/**
 * 解析器接口，针对ASC和HEX两类报文所共用的解析方法做接口声明
 * 
 * @ClassName: IParser
 * @Description: TODO
 * @author lipujun
 * @date Jun 9, 2013
 * 
 */
public interface IParser {

	/**
	 * 解析报文"传输模式"
	 */
	public String parseMode(IMessageHeader header);

	/**
	 * 解析报文"报文体"
	 */
	public Object parseBody(byte funcCode, byte[] body) throws Exception;
 
	/**
	 * 解析报文"遥测站名称"
	 */
	public String parseStcd(byte[] data);

	/**
	 * 解析报文"包总数及包序号"
	 */
	public int[] parseBodyCount(byte[] bytes);

	/**
	 * 解析报文"功能码"
	 */
	public byte parseFuncCode(byte[] bytes);

	/**
	 * 解析报文"报文数据超始位"
	 */
	public byte parseBodyStartBit(byte[] bytes);

	/**
	 * 解析报文"序列号"
	 */
	public int parseSerial(byte[] bytes);

	/**
	 * 解析报文发送时间
	 */
	public Date parseSendTime(byte[] data) throws Exception;

}
