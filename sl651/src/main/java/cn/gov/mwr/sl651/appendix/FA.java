package cn.gov.mwr.sl651.appendix;

import java.io.Serializable;

/**
 * 遥测站分类码，见《水文监测数据通讯规约》附录A
 * 
 * @ClassName: FA
 * @Description: 需根据遥测站分类码，编制相对应的报文信息，与“附录E”相对应
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class FA implements Serializable {

	public int id;
	public String name;
	public String hex;
	public String asc;
	public String memo;

	public FA(){
		
	}
			

	public FA(String name, String hex, String asc) {
		super();
		this.name = name;
		this.hex = hex;
		this.asc = asc;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getHex() {
		return hex;
	}



	public void setHex(String hex) {
		this.hex = hex;
	}



	public String getAsc() {
		return asc;
	}



	public void setAsc(String asc) {
		this.asc = asc;
	}



	public String getMemo() {
		return memo;
	}



	public void setMemo(String memo) {
		this.memo = memo;
	}



	public static void main(String args[]) {

		new FA("降水", "50", "P");
		new FA("河道", "48", "H");
		new FA("水库（湖泊）", "4B", "K");
		new FA("闸坝", "5A", "Z");
		new FA("泵站", "44", "D");
		new FA("潮汐", "54", "T");
		new FA("墒情", "4D", "M");
		new FA("地下水", "47", "G");
		new FA("水质", "51", "Q");
		new FA("取水口", "49", "I");
		new FA("排水口", "4F", "O");

	}

}
