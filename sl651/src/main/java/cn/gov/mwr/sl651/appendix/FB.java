package cn.gov.mwr.sl651.appendix;

import java.io.Serializable;

/**
 * 功能码，见《水文监测数据通讯规约》附录B
 * 
 * @ClassName: FB
 * @Description: TODO
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class FB implements Serializable {

	public int id;
	public String func;

	public String name;

	public String memo;

	public FB(){
		
	}

	public FB(int id, String func, String name, String memo) {
		super();
		this.id = id;
		this.func = func;
		this.name = name;
		this.memo = memo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFunc() {
		return func;
	}

	public void setFunc(String func) {
		this.func = func;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public static void main(String args[]) {
		// 遥测站 发起查询
//		new FB((byte) 0x2F, "M1", "链路维持报");
//		new FB((byte) 0x30, "M1", "测试报");
//		new FB((byte) 0x31, "M1", "均匀时段水文信息报");
//		new FB((byte) 0x32, "M1", "遥测站定时报");
//		new FB((byte) 0x33, "M1", "遥测站加报报");
//		new FB((byte) 0x34, "M1", "遥测站小时报");
//		new FB((byte) 0x35, "M1", "遥测站人工置数报");
//		new FB((byte) 0x36, "M3", "遥测站图片报");
//
//		// 中心站 发起查询
//		new FB((byte) 0x37, "M4", "中心站查询遥测站实时数据");
//		new FB((byte) 0x38, "M4", "中心站查询遥测站时段数据");
//		new FB((byte) 0x39, "M4", "中心站查询遥测站人工置数");
//		new FB((byte) 0x3A, "M4", "中心站查询遥测站指定要素实时数据");
//
//		/**
//		 * 3B-3F属于保留码，扩展功能所用
//		 */
//
//		// 中心站 发起管理
//		new FB((byte) 0x40, "M4", "中心站修改遥测站基本配置表");
//		new FB((byte) 0x41, "M4", "中心站读取遥测站基本配置表");
//		new FB((byte) 0x42, "M4", "中心站修改遥测站运行参数配置表");
//		new FB((byte) 0x43, "M4", "中心站读取遥测站运行参数配置表");
//		new FB((byte) 0x44, "M4", "中心站查询水泵电机实时工作数据");
//		new FB((byte) 0x45, "M4", "中心站查询遥测站软件版本");
//		new FB((byte) 0x46, "M4", "中心站查询遥测站状态和报警信息");
//		new FB((byte) 0x47, "M4", "初始化固态存储数据");
//		new FB((byte) 0x48, "M4", "恢复遥测站出厂设置");
//		new FB((byte) 0x49, "M4", "修改密码");
//		new FB((byte) 0x4A, "M4", "设置遥测站时钟");
//		new FB((byte) 0x4B, "M4", "设置遥测站IC卡状态");
//		new FB((byte) 0x4C, "M4", "控制水泵开关命令/水泵状态自报");
//		new FB((byte) 0x4D, "M4", "控制阀门开关命令/阀门状态信息自报");
//		new FB((byte) 0x4E, "M4", "控制闸门开关命令/闸门状态信息自报");
//		new FB((byte) 0x4F, "M4", "水量定值控制命令");
//		new FB((byte) 0x50, "M4", "中心站查询遥测站事件记录");
//		new FB((byte) 0x51, "M4", "中心站查询遥测站时钟");

		/**
		 * 52-DF属于保留码，扩展功能所用
		 */
		/**
		 * E0-FF用户自定义扩展码
		 */

	}

	/**
	 * 遥测站状态和报警信息定义表
	 */
	public enum BIT0 {

	}

}
