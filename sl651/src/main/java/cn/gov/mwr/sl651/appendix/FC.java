package cn.gov.mwr.sl651.appendix;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;

/**
 * 遥测信息编码要素及标识符汇总表，见《规范》附录C
 * 
 * @ClassName: FC
 * @Description: TODO
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class FC implements Serializable{

	private int id; // 序号
	private String hex; // 标识符引导符
	private String asc; // 标识符 ASCⅡ码
	private String title;// 编码要素
	private String unit; // 量和单位
	private String memo;
	private int len;// 数据定义
	private int decimal = 0;// 数据定义

	public FC() {

	}

	public FC(int id, String hex, String asc, String title, String unit,
			int len, int decimal) {
		super();
		this.id = id;
		this.hex = hex;
		this.asc = asc;
		this.title = title;
		this.unit = unit;
		this.len = len;
		this.decimal = decimal;
	}

	public FC(int id, String hex, String asc, String title, String unit,
			int len, int decimal, String memo) {

		this(id, hex, asc, title, unit, len, decimal);
		this.memo = memo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHex() {
		return hex;
	}

	public void setHex(String hex) {
		this.hex = hex;
	}

	public String getAsc() {
		return asc;
	}

	public void setAsc(String asc) {
		this.asc = asc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getLen() {
		return len;
	}

	public void setLen(int len) {
		this.len = len;
	}

	public int getDecimal() {
		return decimal;
	}

	public void setDecimal(int decimal) {
		this.decimal = decimal;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public static void main(String args[]) {
		
	}
}
