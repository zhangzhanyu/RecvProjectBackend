package cn.gov.mwr.sl651.appendix;

import java.io.Serializable;

/**
 * 遥测站参数配置定义，见《规范》附录D
 * 
 * @ClassName: FD
 * @Description: TODO
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class FD implements Serializable{

	public String ID; // 序号
	public String LABEL;// 编码要素
	public String HEX; // 标识符引导符
	public String TYPE;// 数据定义
	public String MEMO; // 量和单位

	public FD(String id, String label, String hex, String type,
			String memo) {
		super();
		ID = id;
		LABEL = label;
		HEX = hex;
		TYPE = type;
		MEMO = memo;
	}

	public static void main(String args[]) {

		new FD("1", "中心站地址", "01", "HEX", "");
		new FD("2", "遥测站地址", "02", "BCD", "");
		new FD("3", "密码", "03", "HEX", "");
		new FD("4", "中心站1主信道类型及地址", "04", "BCD", "");// 0-禁用，1-短信，2-IPV4，3-北斗，4-海事卫星，5-PSTN，6-超短波
		new FD("5", "中心站1备用信道类型及地址", "05", "BCD", "");
		new FD("6", "中心站2主信道类型及地址", "06", "BCD", "");// 0-禁用，1-短信，2-IPV4，3-北斗，4-海事卫星，5-PSTN，6-超短波
		new FD("7", "中心站2备用信道类型及地址", "07", "BCD", "");
		new FD("8", "中心站3主信道类型及地址", "08", "BCD", "");// 0-禁用， 1-短信，2-IPV4，3-北斗，4-海事卫星，5-PSTN，6-超短波
		new FD("9", "中心站3备用信道类型及地址", "09", "BCD", "");
		new FD("10", "中心站4主信道类型及地址", "0A", "BCD", "");// 0-禁用， 1-短信，2-IPV4，3-北斗，4-海事卫星，5-PSTN，6-超短波
		new FD("11", "中心站4备用信道类型及地址", "0B", "BCD", "");
		new FD("12", "工作方式", "0C", "BCD", "");
		new FD("13", "遥测站采集要素设置", "0D", "HEX", "");
		new FD("14", "中继站（集合转发站）服务地址范围", "0E", "BCD", "");
		new FD("15", "遥测站通信设备识别号", "0F", "ASC", "");
		
	}

}
