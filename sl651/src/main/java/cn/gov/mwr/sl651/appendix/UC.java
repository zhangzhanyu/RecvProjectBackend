package cn.gov.mwr.sl651.appendix;

/**
 * 风向类，定义了16个风位
 * 
 * @ClassName: UC
 * @Description: TODO
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class UC {

	public static int notrh = 1;
	public static int east_notrh_north = 2;
	public static int east_notrh = 3;
	public static int east_notrh_east = 4;
	public static int east = 5;
	public static int east_south_east = 6;
	public static int east_south = 7;
	public static int east_south_south = 8;
	public static int south = 9;
	public static int west_south_south = 10;
	public static int west_south = 11;
	public static int west_south_west = 12;
	public static int west = 13;
	public static int west_north_west = 14;
	public static int west_north = 15;
	public static int west_north_north = 16;

}
