package cn.gov.mwr.sl651.body;

import java.io.Serializable;

/**
 * 观测要素类，仅标识符及数据
 * 
 * @ClassName: DataItem
 * @Description: 观测要素类，仅包含标识符和数值
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class DataItem implements Serializable {

	private byte[] label;

	private byte[] value;

	private int length;

	private int decimal;
	private String extend;

	public int getLength() {
		return length;
	}

	public void setLength(int len) {
		this.length = len;
	}

	public int getDecimal() {
		return decimal;
	}

	public void setDecimal(int decimal) {
		this.decimal = decimal;
	}

	public byte[] getLabel() {
		return label;
	}

	public void setLabel(byte[] label) {
		this.label = label;
	}

	public byte[] getValue() {
		return value;
	}

	public void setValue(byte[] value) {
		this.value = value;
	}

	public String getExtend() {
		return extend;
	}

	public void setExtend(String extend) {
		this.extend = extend;
	}
}
