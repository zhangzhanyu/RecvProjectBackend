package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 中心站查询遥测站时段数据(6.6.4.11 ),功能码：38H
 * 
 * @ClassName: Up38Body
 * @Description: 中心站查询遥测站指定要素的时段数据，
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Down38Body extends AbstractBody implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public String serialId;

	/**
	 * 2、发报时间
	 * 
	 * 取值范围：6字节的BCD编码，YYMMDDHHmmSS
	 */
	// public String sendDate;

	/**
	 * 3、起始时间
	 * 
	 * 取值范围：4字节BCD码，YYMMDDHH，取值参见6.6.2.5
	 */
	private String startTime;

	/**
	 * 4、结束时间
	 * 
	 * 取值范围：4字节BCD码，YYMMDDHH，取值参见6.6.2.5
	 */
	private String endTime;

	/**
	 * 5、时间步长码
	 * 
	 *  取值范围：3字节BCD码 	DDHHMM （000000）
	 */
	private String drx;

	/**
	 * 6、要素标识符
	 */
	private List<byte[]> items = new ArrayList<byte[]>();

	public List<byte[]> getItems() {
		return items;
	}

	public void setItems(List<byte[]> items) {
		this.items = items;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}



	public String getDrx() {
		return drx;
	}

	public void setDrx(String drx) {
		this.drx = drx;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;
		
		int total = getLength();

		content = new byte[total];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		// 起始时间，4位字节
		System.arraycopy(ByteUtil.str2Bcd(startTime), 0, content, 8, 4);

		// 结束时间，4位字节
		System.arraycopy(ByteUtil.str2Bcd(endTime), 0, content, 12, 4);

		// 时间步长:ASCII为DRxnn,HEX为0418H dhm
		content[16] = 0x04;
		content[17] = 0x18;

		System.arraycopy(ByteUtil.str2Bcd(drx), 0,
				content, 18, 3);
		

		// 针对HEX，一般只用于一种标识符，见表44备注2
		int pos = 0;
		for (int i = 0; i < items.size(); i++) {
			byte[] item = (byte[]) (items.get(i));
			System.arraycopy(item, 0, content, 21 + pos, item.length);
			pos = pos + item.length;
		}

		return content;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		
		int itemCount = 0;
		for (int i = 0; i < items.size(); i++) {
			byte[] item = (byte[]) (items.get(i));
			itemCount = itemCount + item.length;
		}
		
		return 8 + 4 + 4 + 5 + itemCount;
	}

}
