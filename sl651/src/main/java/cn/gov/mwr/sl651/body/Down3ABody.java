package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 遥测端查询功能，仅需包括要素标识符
 * 
 * 功能码：3AH
 * 
 * @ClassName: Down3ABody
 * @Description: 此类描述“下行报文帧的结构”框架
 * 
 *               1、流水号
 * 
 *               2、发报时间
 * 
 *               3、要素标识符
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Down3ABody extends AbstractBody implements IMessageBody,
		Serializable {

	private List<byte[]> items = new ArrayList<byte[]>();

	public List<byte[]> getItems() {
		return items;
	}

	public void setItems(List<byte[]> items) {
		this.items = items;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();

		content = new byte[total];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		int pos = 8;
		for (int i = 0; i < items.size(); i++) {
			byte[] item = (byte[]) (items.get(i));
			System.arraycopy(item, 0, content, pos, item.length);
			pos = pos + item.length;
		}

		return content;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub

		int itemCount = 0;
		for (int i = 0; i < items.size(); i++) {
			byte[] item = (byte[]) (items.get(i));
			itemCount = itemCount + item.length;
		}

		return 8 + itemCount;
	}

}
