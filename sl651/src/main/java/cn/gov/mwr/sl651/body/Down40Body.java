package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 遥测端的修改功能，需包括参数配置标识符及数据
 * 
 * @ClassName: Down40Body
 * @Description: 此类描述“下行报文帧的结构”框架
 * 
 *               1、流水号
 * 
 *               2、发报时间
 * 
 *               3、参数1：参数配置标识符 + 数据
 * 
 *               4、参数2：参数配置标识符 + 数据
 * 
 *               5、参数3.....
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Down40Body extends AbstractBody implements IMessageBody,
		Serializable {

	private List<DataItem> items = new ArrayList<DataItem>();

	public List<DataItem> getItems() {
		return items;
	}

	public void setItems(List<DataItem> items) {
		this.items = items;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		int pos = 8;

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		for (int i = 0; i < items.size(); i++) {
			DataItem item = (DataItem) (items.get(i));
			System.arraycopy(item.getLabel(), 0, content, pos, item.getLabel().length);
			pos = pos + item.getLabel().length;
			System.arraycopy(item.getValue(), 0, content, pos, item.getValue().length);
			pos = pos + item.getValue().length;
		}

		return content;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		int pos = 0;
		for (int i = 0; i < items.size(); i++) {
			DataItem item = (DataItem) (items.get(i));
			pos = pos + item.getLabel().length + item.getValue().length;
		}

		return 8 + pos;
	}

}
