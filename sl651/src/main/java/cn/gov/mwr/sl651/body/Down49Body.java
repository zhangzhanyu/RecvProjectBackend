package cn.gov.mwr.sl651.body;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 修改遥测端密码功能
 * 
 * @ClassName: Down49Body
 * @Description: TODO
 * 
 *               1、流水号
 * 
 *               2、发报时间
 * 
 *               3、密码要素标识符 + 旧密码
 * 
 *               4、密码要素标识符 + 新密码
 * 
 * @author lipujun
 * @date Mar 1, 2013
 * 
 */
public class Down49Body extends AbstractBody implements IMessageBody,
		Serializable {

	private byte SYMBOL = 0X03;// 见附录D，遥测站基本配置表

	private String oldPwd;

	private String newPwd;

	public String getOldPwd() {
		return oldPwd;
	}

	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		content = new byte[8 + 8];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		System.arraycopy(new byte[]{SYMBOL,(byte)0x10}, 0, content, 8, 2);

		System.arraycopy(ByteUtil.HexStringToBinary(oldPwd), 0, content, 10, 2);

		System.arraycopy(new byte[]{SYMBOL,(byte)0x10}, 0, content, 12, 2);

		System.arraycopy(ByteUtil.HexStringToBinary(newPwd), 0, content, 14, 2);

		return content;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return 8 + 8;
	}

}
