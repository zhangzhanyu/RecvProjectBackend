package cn.gov.mwr.sl651.body;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
* @ClassName: Down4CBody 
* @Description: TODO
*  
 *               1、流水号
 * 
 *               2、发报时间
 *               
 *               3、水泵开关机状态数据（数据位 + 数据）
 *               
* @author lipujun 
* @date Mar 1, 2013 
*
 */
public class Down4CBody extends AbstractBody implements IMessageBody,
Serializable {

//	
//	private byte dataBit;
//	
	private byte[] data;
	
//	public byte getDataBit() {
//		return dataBit;
//	}
//
//	public void setDataBit(byte dataBit) {
//		this.dataBit = dataBit;
//	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
		
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;
		
		content = new byte[8  + data.length];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

//		System.arraycopy(dataBit, 0, content, 8, 1);
		
		System.arraycopy(data, 0, content, 8, data.length);
		
		
		return content;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return 8  + data.length;
	}
	
}
