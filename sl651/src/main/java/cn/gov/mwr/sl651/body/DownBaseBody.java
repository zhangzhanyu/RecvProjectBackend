package cn.gov.mwr.sl651.body;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 	默认下行报文的报文结构，仅包含流水号及发报时间
 * 
 * @ClassName: DownBase
 * @Description: 此类描述基本的“下行报文帧的结构”框架
 * 
 *               1、流水号
 * 
 *               2、发报时间
 *               
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class DownBaseBody extends AbstractBody implements IMessageBody,
		Serializable {

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;
		
		content = new byte[8];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		return content;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return 8;
	}

}
