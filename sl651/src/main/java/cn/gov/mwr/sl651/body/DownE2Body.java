package cn.gov.mwr.sl651.body;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
* @ClassName: Down4CBody 
* @Description: TODO
*  
 *               1、流水号
 * 
 *               2、发报时间
 *               
 *               3、水泵开关机状态数据（数据位 + 数据）
 *               
* @author lipujun 
* @date Mar 1, 2013 
*
 */
public class DownE2Body extends AbstractBody implements IMessageBody,
Serializable {
  
	
	private byte[] data;
	 
 
	
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data; 
	}

	@Override
	public byte[] getContent() { 
		if (this.content != null)
			return content;
		 
		content = new byte[ data.length];
  
		System.arraycopy(data, 0, content, 0, data.length);
		
		
		return content;
	}

	@Override
	public int getLength() { 
		return    data.length;
	}
	
}
