package cn.gov.mwr.sl651.body;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 链路维持报（6.6.4.2）,功能码：2FH
 * 
 * @ClassName: Up2FBody
 * @Description: 链路维持报，仅包含流水号和发报时间
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up2FBody extends AbstractBody implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public String serialId;

	/**
	 * 2、发报时间
	 * 
	 * 取值范围：6字节的BCD编码，YYMMDDHHmmSS
	 */
	// public String sendDate;

	
	
	@Override
	public byte[] getContent() {
		if (this.content != null)
			return content;
		
		content = new byte[8];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		return content;
	}

	@Override
	public int getLength() {
		return 8;
	}

}
