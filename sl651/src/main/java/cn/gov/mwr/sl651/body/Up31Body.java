package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 均匀时段报(6.6.4.4),功能码：31H
 * 
 * @ClassName: Up30Body
 * @Description: 均匀时段报：用于遥测站向中心站报送等间隔时段水文信息。
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up31Body extends Up30Body implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public int serialId;

	/**
	 * 2、发报时间
	 */
	// public String sendDate;

	

	/**
	 * 6、时间步长
	 */
	public byte[] drh = new byte[] { (byte) 0x04, (byte) 0x18 };

	public byte[] drxxx = new byte[3]; 
	
	/**
	 * 7、要素标识符
	 */
	public byte[] itemFlag;
	
	public byte[] getDrxxx() {
		return drxxx;
	}

	public void setDrxxx(byte[] drxxx) {
		this.drxxx = drxxx;
	}

	public byte[] getItemFlag() {
		return itemFlag;
	}

	public void setItemFlag(byte[] itemFlag) {
		this.itemFlag = itemFlag;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		//序列号
		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		//--------
		
		// 遥测站地址标识符
		System.arraycopy(StationAddrSymbol, 0, content, 8, 2);

		// 遥测站地址，转换为5字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(stcd), 0, content, 10, 5);

		// 遥测站分类码
		System.arraycopy(sttp.getBytes(), 0, content, 15, 1);

		// 观测时间标识符
		System.arraycopy(viewDateSymbol, 0, content, 16, 2);

		// 观测时间，转换为5字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(viewDate), 0, content, 18, 5);
		
		//----------------------------------
		

		// 6、时间步长码
		System.arraycopy(drh, 0, content, 23, 2);
		System.arraycopy(drxxx, 0, content, 25, 3);
		
		// 7、要素标识符
		System.arraycopy(itemFlag, 0, content, 28, itemFlag.length);
		
		int pos = 8 + 7 + 1 + 7 + 5 + itemFlag.length;

		if (items != null) {
			for (int i = 0; i < items.size(); i++) {
				DataItem item = (DataItem) (items.get(i));
				if (item != null) {
					System.arraycopy(item.getValue(), 0, content, pos, item
							.getValue().length);
					pos = pos + item.getValue().length;
				}
			}
		}
		return content;
	}

	@Override
	public int getLength() {
		int pos = 0;
		if (items != null) {
			for (int i = 0; i < items.size(); i++) {
				DataItem item = (DataItem) (items.get(i));
				pos = pos + item.getValue().length;
			}
		}

		return 8 + 7 + 1 + 7 + 5 + itemFlag.length + pos;
	}

}
