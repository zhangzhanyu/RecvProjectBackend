package cn.gov.mwr.sl651.body;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 人工置数报（6.6.4.8）,功能码：35H
 * 
 * @ClassName: Up35Body
 * @Description: 人工置数报，包含流水号、发报时间、人工置数标识符及人工置数数据
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up35Body extends AbstractBody implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public String serialId;

	/**
	 * 2、发报时间
	 * 
	 * 取值范围：6字节的BCD编码，YYMMDDHHmmSS
	 */
	// public String sendDate;

	private byte[] flag;// 有两种HEX为F2H、ASC为RGZS

	private String RGZS;

	public byte[] getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = new byte[] { flag };
	}
	
	public void setFlag(String flag) {
		this.flag = flag.getBytes();
	}

	public String getRGZS() {
		return RGZS;
	}

	public void setRGZS(String rgzs) {
		RGZS = rgzs;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		content = new byte[8 + 1 + RGZS.getBytes().length];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		// 人工置数标识符
		System.arraycopy(flag, 0, content, 8, flag.length);

		// 人工置数数据
		System.arraycopy(RGZS.getBytes(), 0, content, 8 + flag.length, RGZS
				.getBytes().length);

		return content;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return 8 + flag.length + RGZS.getBytes().length;
	}

}
