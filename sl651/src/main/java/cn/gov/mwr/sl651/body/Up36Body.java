package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 遥测站图片报（6.6.4.9　）,功能码：36H
 * 
 * @ClassName: Up36Body
 * @Description: 报送遥测站摄像头拍摄的静态图片，通常采用JPG格式，功能码为36H。
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up36Body extends Up30Body implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public String serialId;

	/**
	 * 2、发报时间
	 * 
	 * 取值范围：6字节的BCD编码，YYMMDDHHmmSS
	 */
	// public String sendDate;

	private byte[] flag = new byte[2];// 有两种HEX为F3H、ASC为PIC

	private byte[] pic;

	public byte[] getFlag() {
		return flag;
	}

	public void setFlag(byte[] mFlag){
		if(mFlag!=null && mFlag.length==2){
			flag[0] = mFlag[0];
			flag[1] = mFlag[1];
		}
	}
	
	
	public void setImg(byte[] mImg){
		if(mImg!=null){
			int n = mImg.length;
			pic = new byte[n];
			System.arraycopy(mImg, 0, pic, 0, n);
		}
	}
	
	public void addItem(byte flag, byte[] pic) {
		this.flag = new byte[] { flag,flag };
		this.pic = pic;
		this.addItem();
	}

	public void addItem(String flag, byte[] pic) {
		this.flag = flag.getBytes();
		this.pic = pic;
		this.addItem();
	}

	public byte[] getPic() {
		return pic;
	}

	private void addItem() {
		DataItem item = new DataItem();
		item.setLabel(this.flag);
		item.setValue(this.pic);
		super.addItem(item);
	}
}
