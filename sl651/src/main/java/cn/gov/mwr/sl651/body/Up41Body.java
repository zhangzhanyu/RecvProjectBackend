package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 测试报(6.6.4.3),功能码：30H
 * 
 * @ClassName: Up30Body
 * @Description: 测试报，上行报文结构
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up41Body extends Up40Body implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public int serialId;

	/**
	 * 2、发报时间
	 */
	// public String sendDate;

	/**
	 * 3、遥测站地址
	 */

	public byte[] f01 = new byte[4];
	public byte[] f02 = new byte[5];
	public byte[] f03 = new byte[2];
	public byte[] f04;//范围是10或1
	public byte[] f05;
	public byte[] f06;
	public byte[] f07;
	public byte[] f08;
	public byte[] f09;
	public byte[] f0A;
	public byte[] f0B;
	public byte[] f0C  = new byte[1];
	public byte[] f0D  = new byte[8];
	public byte[] f0E;//范围最大是255
	public byte[] f0F;//

	public byte[] getF01() {
		return f01;
	}

	public void setF01(byte[] f01) {
		this.f01 = f01;
	}

	public byte[] getF02() {
		return f02;
	}

	public void setF02(byte[] f02) {
		this.f02 = f02;
	}

	public byte[] getF03() {
		return f03;
	}

	public void setF03(byte[] f03) {
		this.f03 = f03;
	}

	public byte[] getF04() {
		return f04;
	}

	public void setF04(byte[] f04) {
		this.f04 = f04;
	}

	public byte[] getF05() {
		return f05;
	}

	public void setF05(byte[] f05) {
		this.f05 = f05;
	}

	public byte[] getF06() {
		return f06;
	}

	public void setF06(byte[] f06) {
		this.f06 = f06;
	}

	public byte[] getF07() {
		return f07;
	}

	public void setF07(byte[] f07) {
		this.f07 = f07;
	}

	public byte[] getF08() {
		return f08;
	}

	public void setF08(byte[] f08) {
		this.f08 = f08;
	}

	public byte[] getF09() {
		return f09;
	}

	public void setF09(byte[] f09) {
		this.f09 = f09;
	}

	public byte[] getF0A() {
		return f0A;
	}

	public void setF0A(byte[] f0a) {
		f0A = f0a;
	}

	public byte[] getF0B() {
		return f0B;
	}

	public void setF0B(byte[] f0b) {
		f0B = f0b;
	}

	public byte[] getF0C() {
		return f0C;
	}

	public void setF0C(byte[] f0c) {
		f0C = f0c;
	}

	public byte[] getF0D() {
		return f0D;
	}

	public void setF0D(byte[] f0d) {
		f0D = f0d;
	}

	public byte[] getF0E() {
		return f0E;
	}

	public void setF0E(byte[] f0e) {
		f0E = f0e;
	}

	public byte[] getF0F() {
		return f0F;
	}

	public void setF0F(byte[] f0f) {
		f0F = f0f;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		// 遥测站地址标识符
		System.arraycopy(stationAddrSymbol, 0, content, 8, 2);

		// 遥测站地址，转换为5字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(stcd), 0, content, 10, 5);

		
		return content;
	}

	public int getLength() {

		return 8 + 2 + 5  ;
	}

}
