package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 测试报(6.6.4.3),功能码：30H
 * 
 * @ClassName: Up30Body
 * @Description: 测试报，上行报文结构
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up45Body extends UpBaseBody implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public int serialId;

	/**
	 * 2、发报时间
	 */
	// public String sendDate;

	/**
	 * 3、遥测站地址
	 */

	/**
	 * 4、遥测站软件版本信息
	 * 
	 * 版本信息字节数、遥测站软件版本信息
	 */
	public byte datalen;
	public byte[] data;

	public byte getDatalen() {
		return datalen;
	}

	public void setDatalen(byte datalen) {
		this.datalen = datalen;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		// 遥测站地址标识符
		System.arraycopy(stationAddrSymbol, 0, content, 8, 2);

		// 遥测站地址，转换为5字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(stcd), 0, content, 10, 5);

		// 1字节 遥测站状态参数标识符
		System.arraycopy(new byte[] { datalen }, 0, content, 15, 1);

		// 新密码数据 2字节HEX码，高位字节在前
		System.arraycopy(data, 0, content, 16, data.length);

		return content;
	}

	public int getLength() {

		return 8 + 2 + 5 + 1 + data.length;
	}

}
