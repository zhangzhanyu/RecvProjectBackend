package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 测试报(6.6.4.3),功能码：30H
 * 
 * @ClassName: Up30Body
 * @Description: 测试报，上行报文结构
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up49Body extends UpBaseBody implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public int serialId;

	/**
	 * 2、发报时间
	 */
	// public String sendDate;

	/**
	 * 3、遥测站地址
	 */

	/**
	 * 4、新密码数据
	 */
	public byte[] symbol;
	public byte[] pwd;

	public byte[] getSymbol() {
		return symbol;
	}

	public void setSymbol(byte[] symbol) {
		this.symbol = symbol;
	}

	public byte[] getPwd() {
		return pwd;
	}

	public void setPwd(byte[] pwd) {
		this.pwd = pwd;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		// 遥测站地址标识符
		System.arraycopy(stationAddrSymbol, 0, content, 8, 2);

		// 遥测站地址，转换为5字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(stcd), 0, content, 10, 5);

		// 1字节的密码标识符
		System.arraycopy(symbol, 0, content, 15, 2);

		// 新密码数据 2字节HEX码，高位字节在前
		System.arraycopy(pwd, 0, content, 17, 2);

		return content;
	}

	public int getLength() {

		return 8 + 2 + 5 + 2 + 2;
	}

}
