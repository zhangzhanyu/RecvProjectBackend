package cn.gov.mwr.sl651.body;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

public class UpBaseBody extends AbstractBody implements Serializable {

	/**
	 * 1、流水号
	 * 
	 * 取值范围：2字节HEX码，范围1～65535
	 */
	// public int serialId;

	/**
	 * 2、发报时间
	 */
	// public String sendDate;
	
	/**
	 * 3、遥测站地址
	 */
	public byte[] stationAddrSymbol = new byte[] { (byte) 0xF1, (byte) 0xF1 }; // F1H
	// 遥测站地址标识符，在附录D中

	public String stcd;// 遥测站地址，占用5个字节

	public byte[] getStationAddrSymbol() {
		return stationAddrSymbol;
	}

	public void setStationAddrSymbol(byte[] _stationAddrSymbol) {
		stationAddrSymbol = _stationAddrSymbol;
	}

	public String getStcd() {
		return stcd;
	}

	public void setStcd(String stcd) {
		this.stcd = stcd;
	}

	@Override
	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		System.arraycopy(ByteUtil.ushortToBytes(serialId), 0, content, 0, 2);

		// 转换为6字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 2, 6);

		// 遥测站地址标识符
		System.arraycopy(stationAddrSymbol, 0, content, 8, 2);

		// 遥测站地址，转换为5字节的BCD码
		System.arraycopy(ByteUtil.str2Bcd(stcd), 0, content, 10, 5);

		return content;
	}

	public int getLength() {
		 if (this.content != null) {
			return this.content.length;
		} else {
			return 8 + 2 + 5 ;
		}
	}
	
}
