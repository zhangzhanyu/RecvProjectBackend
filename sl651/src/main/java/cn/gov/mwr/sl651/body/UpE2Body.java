package cn.gov.mwr.sl651.body;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.gov.mwr.sl651.AbstractBody;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * 
 * 测试报(6.6.4.3),功能码：30H
 * 
 * @ClassName: Up30Body
 * @Description: 测试报，上行报文结构
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class UpE2Body implements IMessageBody, Serializable {

	/**
	 * 记录原始消息体
	 */
	public byte[] content;

	public void setContents(byte[] content) {
		this.content = content;
	}

	public int getLength() {
		if (this.content != null) {
			return this.content.length;
		} else {
			return 0;
		}
	}
	
	public byte[] getContent(){
		return this.content;
	}



}
