package cn.gov.mwr.sl651.command;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import cn.gov.mwr.sl651.Command;
import cn.gov.mwr.sl651.IMessage;
import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.appendix.FB;
import cn.gov.mwr.sl651.body.DataItem;
import cn.gov.mwr.sl651.body.Up2FBody;
import cn.gov.mwr.sl651.body.Up30Body;
import cn.gov.mwr.sl651.body.Up35Body;
import cn.gov.mwr.sl651.body.Up36Body;
import cn.gov.mwr.sl651.utils.ByteUtil;

public class UpCommand extends Command {

	/**
	 * M1、M2、M4模式如下：
	 * 
	 * 报文起始符为：STX
	 * 
	 * 报文结束符为：ETX、ETB。 ETX表示报文结束，后续无报文；ETB表示在报文分包传输时作为结束符，表示未完成，不可退出通信；
	 * 
	 * ------------------------------------------------------------------------
	 * 
	 * M3模式如下：
	 * 
	 * 报文起始符为：SYN。表示多包发送，一次确认的传输模式中使用
	 * 
	 * 报文结束符为：ETX、ETB。 ETX表示报文结束，后续无报文；ETB表示在报文分包传输时作为结束符，表示未完成，不可退出通信；
	 * 
	 * @param funcCode
	 *            ，功能码，表示需要发送哪种报文
	 * @param eof
	 *            ，报文结束符
	 */
	public UpCommand() {
		super(Symbol.UP);
		// TODO Auto-generated constructor stub
	}

	public void sendList() {
		// new FB((byte) 0x2F, "M1", "链路维持报");
		// new FB((byte) 0x30, "M2", "测试报");
		// new FB((byte) 0x31, "M1", "均匀时段水文信息报");
		// new FB((byte) 0x32, "M1", "遥测站定时报");
		// new FB((byte) 0x33, "M1", "遥测站加报报");
		// new FB((byte) 0x34, "M1", "遥测站小时报");
		// new FB((byte) 0x35, "M1", "遥测站人工置数报");
		// new FB((byte) 0x36, "M3", "遥测站图片报");
	}



	/**
	 * 链路维持报，只用于M1模式，不需要应答
	 * 
	 * 功能码：2F
	 */

	public IMessage send2FMessage(int sequence, String date) {
		Up2FBody body = new Up2FBody();
		initBase(body, sequence, date);
		return buildMessage(body);
	}

	/**
	 * 测试报
	 * 
	 * 功能码：30
	 */
	public IMessage send30Message(int sequence, String date, String stcd,
			String sttp, String viewDate, List<DataItem> items) {
		Up30Body body = new Up30Body();
		initBase(body, sequence, date);
		body.setStcd(stcd);
		body.setSttp(sttp);
		body.setViewDate(viewDate);
		body.setItems(items);
		return buildMessage(body);
	}

	public void send31Message() {

	}

	public void send32Message() {

	}

	public void send33Message() {

	}

	public void send34Message() {

	}

	/**
	 * 人工置数报
	 * 
	 * 功能码：35
	 */
	public IMessage send35Message(int sequence, String date, String rgzs) {
		Up35Body body = new Up35Body();
		initBase(body, sequence, date);
		body.setFlag((byte) 0xF2);
		body.setRGZS(rgzs);
		return buildMessage(body);
	}

	/**
	 * 图片报
	 * 
	 * @param sequence
	 * @param date
	 * @param stcd
	 * @param sttp
	 * @param viewDate
	 * @param pic
	 * @return
	 */
	public IMessage send36Message(int sequence, String date, String stcd,
			String sttp, String viewDate, byte[] pic) {
		Up36Body body = new Up36Body();
		initBase(body, sequence, date);
		body.setStcd(stcd);
		body.setSttp(sttp);
		body.setViewDate(viewDate);
		body.addItem((byte) 0xF3, pic);
		return buildMessage(body);
	}

	private byte[] readImage(String fileName) {

		InputStream is = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			is = new FileInputStream(fileName);// pathStr 文件路径
			byte[] b = new byte[4096];
			int n;
			while ((n = is.read(b)) != -1) {
				out.write(b, 0, n);

			}// end while
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return out.toByteArray();

	}

	public static void main(String[] args) {
		UpCommand cmd = new UpCommand();
		cmd.setBodyStartBit(Symbol.STX);
		cmd.setEof(Symbol.ETX);
		cmd.setFuncCode(new byte[] { (byte) 0x2f });

		IMessage message = cmd.send2FMessage(1, null);
		String hexStr = cmd.toHexString(message.getHeader());
		System.out.print(">> hex " + hexStr.toUpperCase());
		System.out.print(""
				+ ByteUtil.toHexString(message.getBody().getContent()));
		System.out.print("" + ByteUtil.toHexString(message.getEOF()));
		System.out.print("" + ByteUtil.toHexString(message.getCRC()));
	}

}
