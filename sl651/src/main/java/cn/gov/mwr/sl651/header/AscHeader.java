package cn.gov.mwr.sl651.header;

import java.io.Serializable;



import org.apache.commons.lang3.StringUtils;

import cn.gov.mwr.sl651.AbstractHeader;
import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * ASC文件头定义
 * 
 * @ClassName: AscHeader
 * @Description: TODO
 * @author lipujun
 * @date Mar 2, 2013
 * 
 */
public class AscHeader extends AbstractHeader implements Serializable {

	public static int LEN_START_BIT = 1;
	public static int LEN_CENTER_ADDR = 2;
	public static int LEN_STATION_ADDR = 10;
	public static int LEN_PASSWORD = 4;
	public static int LEN_FUNC_CODE = 2;
	public static int LEN_BODY_SIZE = 4;
	public static int LEN_BODY_START_BIT = 1;
	public static int LEN_BODY_COUNT = 6;// --M3--长度定义

	
	public void setStartBit(String startBit) {
		START_BIT = startBit.getBytes();
	}
	
	public void setPassword(String password) {
		PASSWORD = password.getBytes();
	}
	
	public void setFuncCode(String funcCode) {
		FUNC_CODE = funcCode.getBytes();
	}
	
	public void setBodySize(String bodyLength) {
		BODY_SIZE = bodyLength.getBytes();
	}

	public void setStationAddr(String addr){
		addr = StringUtils.leftPad(addr, 10, "0");
		STATION_ADDR = ByteUtil.str2Bcd(addr);
	}
	
	public int getBodyCountLen() {
		// TODO Auto-generated method stub
		return this.LEN_BODY_COUNT;
	}

	public int getBodySizeLen() {
		// TODO Auto-generated method stub
		return this.LEN_BODY_SIZE;
	}

	public int getBodyStartBitLen() {
		// TODO Auto-generated method stub
		return this.LEN_BODY_START_BIT;
	}

	public int getCenterAddrLen() {
		// TODO Auto-generated method stub
		return this.LEN_CENTER_ADDR;
	}

	public int getFuncCodeLen() {
		// TODO Auto-generated method stub
		return this.LEN_FUNC_CODE;
	}

	public int getPasswordLen() {
		// TODO Auto-generated method stub
		return this.LEN_PASSWORD;
	}

	public int getStartBitLen() {
		// TODO Auto-generated method stub
		return this.LEN_START_BIT;
	}

	public int getStationAddrLen() {
		// TODO Auto-generated method stub
		return this.LEN_STATION_ADDR;
	}

	public int getLength() {

		int len = LEN_START_BIT + LEN_CENTER_ADDR + LEN_STATION_ADDR
				+ LEN_PASSWORD + LEN_FUNC_CODE + LEN_BODY_SIZE
				+ LEN_BODY_START_BIT;
		
		if (BODY_START_BIT[0] == Symbol.SYN) {
			return len + LEN_BODY_COUNT;
		} else {
			return len;
		}

	}
}
