package cn.gov.mwr.sl651.header;

import java.io.Serializable;

import cn.gov.mwr.sl651.AbstractHeader;
import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.utils.ByteUtil;

/**
 * HEX文件头定义
 * 
 * @ClassName: HexHeader
 * @Description: TODO
 * @author lipujun
 * @date Mar 2, 2013
 * 
 */
public class HexHeader extends AbstractHeader implements Serializable {

	public static int LEN_START_BIT = 2;
	public static int LEN_CENTER_ADDR = 1;
	public static int LEN_STATION_ADDR = 5;
	public static int LEN_PASSWORD = 2;
	public static int LEN_FUNC_CODE = 1;
	public static int LEN_BODY_SIZE = 2;
	public static int LEN_BODY_START_BIT = 1;
	public static int LEN_BODY_COUNT = 3;// --M3--长度定义

	
	public void setStartBit(String startBit) {
		START_BIT = ByteUtil.HexStringToBinary(startBit);
	}
	
	public void setPassword(String password) {
		PASSWORD = ByteUtil.HexStringToBinary(password);
	}
	
	public void setFuncCode(String funcCode) {
		FUNC_CODE = ByteUtil.HexStringToBinary(funcCode);
	}
	
	public void setBodySize(String bodyLength) {
		BODY_SIZE = ByteUtil.HexStringToBinary(bodyLength);
	}

	
	
	public int getBodyCountLen() {
		return this.LEN_BODY_COUNT;
	}

	public int getBodySizeLen() {
		return this.LEN_BODY_SIZE;
	}

	public int getBodyStartBitLen() {
		return this.LEN_BODY_START_BIT;
	}

	public int getCenterAddrLen() {
		return this.LEN_CENTER_ADDR;
	}

	public int getFuncCodeLen() {
		return this.LEN_FUNC_CODE;
	}

	public int getPasswordLen() {
		return this.LEN_PASSWORD;
	}

	public int getStartBitLen() {
		return this.LEN_START_BIT;
	}

	public int getStationAddrLen() {
		return this.LEN_STATION_ADDR;
	}

	public int getLength() {

		int len = LEN_START_BIT + LEN_CENTER_ADDR + LEN_STATION_ADDR
				+ LEN_PASSWORD + LEN_FUNC_CODE + LEN_BODY_SIZE
				+ LEN_BODY_START_BIT;

		if (BODY_START_BIT[0] == Symbol.SYN) {
			return len + LEN_BODY_COUNT;
		} else {
			return len;
		}
	}


}
