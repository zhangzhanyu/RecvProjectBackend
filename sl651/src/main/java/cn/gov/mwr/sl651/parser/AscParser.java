package cn.gov.mwr.sl651.parser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.gov.mwr.sl330.HydroData;
import cn.gov.mwr.sl330.HydroDataItem;
import cn.gov.mwr.sl330.HydroParser;
import cn.gov.mwr.sl330.Separator;
import cn.gov.mwr.sl651.AbstractParser;
import cn.gov.mwr.sl651.FCAsc2Hex;
import cn.gov.mwr.sl651.IMessage;
import cn.gov.mwr.sl651.IMessageBody;
import cn.gov.mwr.sl651.IMessageHeader;
import cn.gov.mwr.sl651.IParser;
import cn.gov.mwr.sl651.Symbol;
import cn.gov.mwr.sl651.body.DataItem;
import cn.gov.mwr.sl651.body.Up40Body;
import cn.gov.mwr.sl651.body.Up41Body;
import cn.gov.mwr.sl651.body.Up42Body;
import cn.gov.mwr.sl651.body.Up44Body;
import cn.gov.mwr.sl651.body.Up45Body;
import cn.gov.mwr.sl651.body.Up46Body;
import cn.gov.mwr.sl651.body.Up47Body;
import cn.gov.mwr.sl651.body.Up48Body;
import cn.gov.mwr.sl651.body.Up49Body;
import cn.gov.mwr.sl651.body.Up4ABody;
import cn.gov.mwr.sl651.body.Up4BBody;
import cn.gov.mwr.sl651.body.Up4CBody;
import cn.gov.mwr.sl651.body.Up4EBody;
import cn.gov.mwr.sl651.body.Up4FBody;
import cn.gov.mwr.sl651.body.Up50Body;
import cn.gov.mwr.sl651.body.Up51Body;
import cn.gov.mwr.sl651.body.UpBaseBody;
 
import cn.gov.mwr.sl651.utils.ByteUtil;
import cn.gov.mwr.sl651.utils.CollectionUtil;
import cn.gov.mwr.sl651.utils.DataType;
import cn.gov.mwr.sl651.utils.StcdParser;
 

/**
 * ASC解析类
 * 
 * @ClassName: AscParser
 * @Description: TODO
 * @author lipujun
 * @date Mar 11, 2013
 * 
 */
public class AscParser extends AbstractParser implements IParser {

	private static Logger logger = LoggerFactory.getLogger(AscParser.class);

	public final static String SEP = " ";

	private final static ConcurrentHashMap FC_DATA_MAP = new ConcurrentHashMap();

	static {
		FC_DATA_MAP.put("DRP", new DataType(0, "1小时内每5分钟时段雨量",
				"每组雨量占1字节HEX，最大值25.4毫米，数据中不含小数点；FFH表示非法数据。", 1, 3, 1));
		FC_DATA_MAP
				.put(
						"DRZ1",
						new DataType(
								1,
								"1小时内5分钟间隔相对水位1",
								"每组水位占2字节HEX，分辨力是为厘米，最大值为655.34米，数据中不含小数点；FFH表示非法数据)；对于河道、闸坝（泵）站分别表示河道水位、闸（站）上水位",
								2, 5, 2));
		FC_DATA_MAP.put("DRZ2", new DataType(2, "1小时内5分钟间隔相对水位2", "", 2, 5, 2));
		FC_DATA_MAP.put("DRZ3", new DataType(3, "1小时内5分钟间隔相对水位3", "", 2, 5, 2));
		FC_DATA_MAP.put("DRZ4", new DataType(4, "1小时内5分钟间隔相对水位4", "", 2, 5, 2));
		FC_DATA_MAP.put("DRZ5", new DataType(5, "1小时内5分钟间隔相对水位5", "", 2, 5, 2));
		FC_DATA_MAP.put("DRZ6", new DataType(6, "1小时内5分钟间隔相对水位6", "", 2, 5, 2));
		FC_DATA_MAP.put("DRZ7", new DataType(7, "1小时内5分钟间隔相对水位7", "", 2, 5, 2));
		FC_DATA_MAP.put("DRZ8", new DataType(8, "1小时内5分钟间隔相对水位8", "", 2, 5, 2));
		FC_DATA_MAP.put("ZT", new DataType(9, "遥测站状态及报警信息", "", 4, 32, 0));
	}

	private final Set<String> FC_MAP = CollectionUtil.treeSet("TT", "ST",
			"RGZS", "PIC", "DRP", "DRZ1", "DRZ2", "DRZ3", "DRZ4", "DRZ5",
			"DRZ6", "DRZ7", "DRZ8", "DATA", "AC", "AI", "C", "DRxnn", "DT",
			"ED", "EJ", "FL", "GH", "GN", "GS", "GT", "GTP", "H", "HW", "M10",
			"M20", "M30", "M40", "M50", "M60", "M80", "M100", "MST", "NS",
			"P1", "P2", "P3", "P6", "P12", "PD", "PJ", "PN01", "PN05", "PN10",
			"PN30", "PR", "PT", "Q", "Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7",
			"Q8", "QA", "QZ", "SW", "UC", "UE", "US", "VA", "VJ", "VT", "Z",
			"ZB", "ZU", "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7", "Z8", "SQ",
			"ZT", "pH", "DO", "COND", "TURB", "CODMN", "REDOX", "NH4N", "TP",
			"TN", "TOC", "CU", "ZN", "SE", "AS", "THG", "CD", "PB", "CHLA",
			"WP1", "WP2", "WP3", "WP4", "WP5", "WP6", "WP7", "WP8", "SYL1",
			"SYL2", "SYL3", "SYL4", "SYL5", "SYL6", "SYL7", "SYL8", "SBL1",
			"SBL2", "SBL3", "SBL4", "SBL5", "SBL6", "SBL7", "SBL8", "VTA",
			"VTB", "VTC", "VIA", "VIB", "VIC");

	/**
	 * 解析遥测站地址编码，参见6.2.3.2
	 * 
	 * @param stcd
	 */
	public String parseStcd(byte[] data) {

		String hexStationAddrStr = new String(data);
		byte[] hexStationAddr = ByteUtil.HexStringToBinary(hexStationAddrStr);

		return StcdParser.parseStcd(hexStationAddr);
	}

	/**
	 * 根据信息构造body体
	 * 
	 * @param serial
	 * @param sendDate
	 * @param stcd
	 * @param category
	 * @param viewDate
	 * @param data
	 * @return
	 */
	public byte[] buildBody(int serial, String sendDate, String stcd,
			String category, String viewDate, String data) {

		// 1、序列号
		byte[] serialByte = ByteUtil.shortToBytes((short) serial);
		// 2、发报时间
		byte[] sendDateByte = ByteUtil.str2Bcd(sendDate);
		// 3、遥测站地址
		stcd = StringUtils.leftPad(stcd, 10, "0");
		byte[] stcdFlagByte = "ST".getBytes();
		byte[] stcdByte = ByteUtil.str2Bcd(stcd);
		// 4、分类码
		byte[] cateByte = category.getBytes();
		// 5、观测时间
		byte[] viewDateFlagByte = "TT".getBytes();
		byte[] viewDateByte = ByteUtil.str2Bcd(viewDate);

		byte[] dataBytes = data.getBytes();

		int len = serialByte.length + sendDateByte.length + stcdFlagByte.length
				+ stcdByte.length + cateByte.length + viewDateFlagByte.length
				+ viewDateByte.length + dataBytes.length;
		byte[] body = new byte[len];
		int pos = 0;
		System.arraycopy(serialByte, 0, body, pos, serialByte.length);
		pos = pos + serialByte.length;

		System.arraycopy(sendDateByte, 0, body, pos, sendDateByte.length);
		pos = pos + sendDateByte.length;

		System.arraycopy(stcdFlagByte, 0, body, pos, stcdFlagByte.length);
		pos = pos + stcdFlagByte.length;

		System.arraycopy(stcdByte, 0, body, pos, stcdByte.length);
		pos = pos + stcdByte.length;

		System.arraycopy(cateByte, 0, body, pos, cateByte.length);
		pos = pos + cateByte.length;

		System.arraycopy(viewDateFlagByte, 0, body, pos,
				viewDateFlagByte.length);
		pos = pos + viewDateFlagByte.length;

		System.arraycopy(viewDateByte, 0, body, pos, viewDateByte.length);
		pos = pos + viewDateByte.length;

		System.arraycopy(dataBytes, 0, body, pos, dataBytes.length);

		System.out.println("----OK-----" + pos + " dataBytes "
				+ dataBytes.length);

		return body;
	}

	/**
	 * 解析ASC码的报文体
	 * 
	 * 注意：针对一份报文中包含多个遥测站的信息，还未做实现。
	 * 
	 * @param body
	 */
	public void parseBody(byte[] bytes) {// IMessageBody body
		// TODO Auto-generated method stub
		// byte[] bytes = body.getContent();

		int pos = 0;
		// 序列号
		byte[] serial = new byte[2];
		System.arraycopy(bytes, pos, serial, 0, serial.length);
		pos = pos + serial.length;

		// 发报时间
		byte[] sendDate = new byte[6];
		System.arraycopy(bytes, pos, sendDate, 0, sendDate.length);
		String sendDateStr = ByteUtil.bcd2Str(sendDate);
		pos = pos + sendDate.length;

		// 遥测站地址
		byte[] stcdFlag = new byte[2];
		byte[] stcd = new byte[5];
		System.arraycopy(bytes, pos, stcdFlag, 0, stcdFlag.length);
		pos = pos + stcdFlag.length;
		System.arraycopy(bytes, pos, stcd, 0, stcd.length);
		pos = pos + stcd.length;
		String stationAddr = parseStcd(stcd);

		// 遥测站分类码
		byte[] category = new byte[1];
		System.arraycopy(bytes, pos, category, 0, category.length);
		String cate = new String(category);
		pos = pos + category.length;

		// 观测时间
		byte[] viewDateFlag = new byte[2];
		byte[] viewDateByte = new byte[5];
		System.arraycopy(bytes, pos, viewDateFlag, 0, viewDateFlag.length);
		pos = pos + viewDateFlag.length;
		System.arraycopy(bytes, pos, viewDateByte, 0, viewDateByte.length);
		pos = pos + viewDateByte.length;
		String viewDate = ByteUtil.bcd2Str(viewDateByte);
		// 数据内容
		// int pos = serial.length + sendDate.length + stcdFlag.length
		// + stcd.length + category.length + viewDateFlag.length
		// + viewDateByte.length;
		int len = bytes.length - pos;
		byte[] data = new byte[len];
		System.arraycopy(bytes, pos, data, 0, len);

		// 开始分割字符串数据
		System.out.println("----OK2-----dataBytes " + len);

		String content = new String(data);
		System.out.println(content);

	}

	/**
	 * 转换为中心站地址，如输入FF，则地址为255（在解析的时候，需将此字符串按16进制解析获取）
	 * 
	 * @param bytes
	 * @return
	 */
	public String convertCenterAddr(String hexString) {
		byte[] bytes = ByteUtil.HexStringToBinary(hexString);

		String centerAddr = ByteUtil.toHexString(bytes);
		return centerAddr;
	}

	/**
	 * 将16进制ASC码解析10进制数据，中心站地址范围为1～255。
	 * 
	 * @param hexString
	 * @return
	 */
	public int parseCenterAddr(byte[] bytes) {
		String hexString = new String(bytes);
		byte[] centerAddr = ByteUtil.HexStringToBinary(hexString);
		return ByteUtil.bytesToUbyte(centerAddr);
	}

	/**
	 * 解析报文"功能码"
	 * 
	 * @param hexString
	 * @return
	 */
	public byte parseFuncCode(byte[] bytes) {
		String hexString = new String(bytes);
		byte[] funcCode = ByteUtil.HexStringToBinary(hexString);
		return funcCode[0];
	}

	/**
	 * 解析报文"报文数据超始位"
	 */
	public byte parseBodyStartBit(byte[] bytes) {
		String hexString = ByteUtil.toHexString(bytes);
		byte[] bodyStartBit = ByteUtil.HexStringToBinary(hexString);
		return bodyStartBit[0];
	}

	/**
	 * 构造“报文上下行标识及长度”
	 * 
	 * @param upDown
	 * @param length
	 * @return
	 */
	public String buildBodyLength(byte upDown, int length) {
		byte[] lenByte = ByteUtil.ushortToBytes(length);
		byte low4 = (byte) (lenByte[0] & 0x0f);
		byte first = (byte) (upDown | low4);

		return ByteUtil.byteToHexString(new byte[] { first, lenByte[1] });
	}

	/**
	 * 
	 * 构造“包总数及序列号”
	 * 
	 * @param count
	 * @param serial
	 * @return
	 */
	public String buildBodyCount(int count, int serial) {
		byte[] countByte = ByteUtil.ushortToBytes(count);
		byte high = (byte) ((countByte[1] << 4) & 0xF0);

		byte first = (byte) (((countByte[0] & 0x0F) << 4) | ((countByte[1] & 0xF0) >> 4));

		byte[] serialByte = ByteUtil.ushortToBytes(serial);
		byte low = (byte) (serialByte[0] & 0x0f);

		byte middle = (byte) (high | low);

		return ByteUtil.byteToHexString(new byte[] { first, middle,
				serialByte[1] });
	}

	/**
	 * 解析报文上下行标识符及长度
	 */
	public int[] parseBodyLength(byte[] bytes) {

		String hexString = new String(bytes);
		hexString = StringUtils.deleteWhitespace(hexString);
		System.out.println(">> hexString " + hexString);
		// 用2字节HEX编码转换为4个ASCⅡ字符传输。
		int upDown = 0;
		if (hexString.substring(0, 1).equals("0")) {
			// 第1个字符用作上下行标识（0表示上行，8表示下行）
			upDown = 0;
		} else {
			upDown = 8;
		}
		// 其余3个字符表示报文正长度，表示报文起始符之后、文结束符之前的报文字节数，允许长度为0001～4095
		String lenStr = hexString.substring(1, 4);
		lenStr = "0" + lenStr;
		byte[] lenBytes = ByteUtil.HexStringToBinary(lenStr.toUpperCase());// 注意：将十六进制字符串全变成大写，否则会出现问题
		int len = ByteUtil.bytesToUshort(lenBytes);

		System.out.println(">>>> lenStr " + lenStr + " centerAddr "
				+ ByteUtil.byteToHexString(lenBytes) + " len " + len);
		return new int[] { upDown, len };
	}

	/**
	 * 解析报文"包总数及序列号"
	 */
	public int[] parseBodyCount(byte[] abytes) {
		String hexString = new String(abytes);
		// header.setBodyCount(ByteUtil.HexStringToBinary(hexString));
		// hexString = StringUtils.deleteWhitespace(hexString);
		// String countStr = "0" + hexString.substring(0, 3);
		// String serialIdStr = "0" + hexString.substring(3, 6);
		// byte[] countBytes =
		// ByteUtil.HexStringToBinary(countStr.toUpperCase());//
		// 注意：将十六进制字符串全变成大写，否则会出现问题
		// int count = ByteUtil.bytesToUshort(countBytes);
		//
		// byte[] serialIdBytes = ByteUtil.HexStringToBinary(serialIdStr
		// .toUpperCase());// 注意：将十六进制字符串全变成大写，否则会出现问题
		// int serialId = ByteUtil.bytesToUshort(serialIdBytes);

		byte[] bytes = ByteUtil.HexStringToBinary(hexString);
		byte high = (byte) ((bytes[0] & 0xF0) >> 4);
		byte low = (byte) (((bytes[0] & 0x0F) << 4) | ((bytes[1] & 0xF0) >> 4));

		byte[] countBytes = new byte[] { high, low };

		byte[] serialBytes = new byte[] { (byte) (bytes[1] & 0x0F), bytes[2] };

		int count = ByteUtil.bytesToUshort(countBytes);
		int serial = ByteUtil.bytesToUshort(serialBytes);

		return new int[] { count, serial };
	}

	/**
	 * 解析发送时间
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public Date parseSendTime(byte[] data) throws Exception {

		String hexString = new String(data);
		String datestr = ByteUtil
				.bcd2Str(ByteUtil.HexStringToBinary(hexString));
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
		return sdf.parse(datestr);
	}

	/**
	 * 解析报文"序列号"
	 */
	public int parseSerial(byte[] bytes) {
		String hexString = new String(bytes);
		byte[] hexSerial = ByteUtil.HexStringToBinary(hexString);
		return ByteUtil.bytesToUbyte(hexSerial);
	}

	/**
	 * 解析报文的模式，相当于只在此解析为M1,M2,M3模式
	 * 
	 * 注意点：此方法可改到外部
	 * 
	 * @param IMessageHeader
	 */
	public String parseMode(IMessageHeader header) {
		String mode = "M2";

		// 根据功能码判定为M1模式，功能码在外层做判断，不进入本层判断
		String funcCode = new String(header.getFuncCode());
		if (funcCode.equalsIgnoreCase("2F")) {
			mode = "M1";
		}

		// 根据报文起始符判定为使用M2或M3模式
		byte[] bodyStartBit = header.getBodyStartBit();
		if (bodyStartBit[0] == Symbol.SYN) {
			mode = "M3";
		} else {
			mode = "M2";
		}

		return mode;
	}

	/**
	 * 处理“测站”报文
	 * 
	 * @param typeCode
	 * @param info
	 */
	public static List<HydroData> processST(String info) {

		List<HydroData> list = new ArrayList<HydroData>();

		String stInfo = info;
		if (StringUtils.isNotEmpty(stInfo)) {
			stInfo = StringUtils.trim(stInfo);
		}

		int st_pos = stInfo.indexOf(Separator.SEP);
		String st_token = stInfo.substring(0, st_pos);

		// String st_data = stInfo.substring(st_pos);
		// int st_data_pos = st_data.trim().indexOf(Separator.SEP);
		// String st_data_token = st_data.trim().substring(0, st_data_pos);

		String st_type = stInfo.substring(st_pos);
		int st_type_pos = st_type.trim().indexOf(Separator.SEP);
		String st_type_token = st_type.trim().substring(0, st_type_pos);

		System.out.println(">>ST INFO \t " + st_token + " --- " + " --- "
				+ " --- " + st_type_token);

		String st_data = st_type.trim().substring(st_type_pos);

		String TT[] = StringUtils.trim(st_data).split(Separator.TT);
		for (int i = 0, len = TT.length; i < len; i++) {
			if (StringUtils.isNotBlank(TT[i])) {
				System.out
						.println(">>--------------------------ADD----------------------------- ");

				HydroData dataInfo = new HydroData();
				dataInfo.setTypeCode(st_type_token);
				dataInfo.setStcd(st_token);

				dataInfo.setItems(processTT(TT[i]));

				list.add(dataInfo);
			}
		}

		return list;

	}

	/**
	 * 处理“多时间”报文
	 * 
	 * @param hydro
	 * @param info
	 */
	public static List<HydroDataItem> processTT(String info) {

		List<HydroDataItem> items = new ArrayList<HydroDataItem>();

		String ttInfo = info;
		if (StringUtils.isNotEmpty(ttInfo)) {
			ttInfo = StringUtils.trim(ttInfo);
		}

		int tt_pos = ttInfo.indexOf(Separator.SEP);
		String viewDate = ttInfo.substring(0, tt_pos);
		String tt_data = ttInfo.substring(tt_pos);

		// hydro.setViewDate(convertDate(viewDate));

		System.out.println(">>----TT  \t " + viewDate + " --- " + tt_data);
		String data[] = StringUtils.trim(tt_data).split(Separator.SEP);
		for (int i = 0, len = data.length; i < len; i++) {

			String itemName = data[i];
			if (itemName.indexOf("DRN") != -1) {
				// itemName.substring(3, 5);
				System.out.println(">>>>TT item \t DRN --- "
						+ itemName.substring(3, 5));
			} else if (FC_DATA_MAP.containsKey(itemName)) {
				i = i + 1;
				DataType dt = (DataType) FC_DATA_MAP.get(itemName);
				// dt.getDecimal();
				String values = data[i];
				System.out.println(">>>>TT item \t " + itemName + " --- "
						+ values);
				for (int pos = 0; pos < 12; pos++) {
					int decimal = dt.getDecimal();
					int dsize = dt.getLength();
					int blen = dt.getBlen();
					String hexString = values.substring(pos * 2 * blen,
							(pos + 1) * 2 * blen);
					byte[] d = ByteUtil.HexStringToBinary(hexString);

					int dval = 0;
					switch (d.length) {
					case 1:
						dval = ByteUtil.bytesToUbyte(d);
						break;
					case 2:
						dval = ByteUtil.bytesToUshort(d);
						break;
					}

					String value = "" + dval;
					value = StringUtils.leftPad(value, dsize, "0");
					String lvalue = value.substring(0, (dsize - decimal));
					String rvalue = value.substring((dsize - decimal), dsize);
					String pvalue = lvalue + "." + rvalue;
					// Double.parseDouble(pvalue)
					System.out.println(">>>>>>>>item\t " + pos + " --- "
							+ Double.parseDouble(pvalue));

				}

			} else {
				HydroDataItem item = new HydroDataItem();
				item.setName(itemName);
				i = i + 1;
				item.setValue(new Double(data[i]));
				items.add(item);

				System.out.println(">>>>TT item \t " + item.getName() + " --- "
						+ item.getValue());
			}

		}

		return items;

	}

	public Up40Body parse40Body(IMessageBody messageBody) throws Exception {

		Up40Body body = new Up40Body();

		byte[] bytes = messageBody.getContent();
		String msgstr = new String(bytes);

		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		byte[] sendDate = new byte[6];
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		sendDate = ByteUtil.HexStringToBinary(sendDateStr);
		body.setSendDate(ByteUtil.bcd2Str(sendDate));
		pos = pos + sendDateLen;

		String stinfo = msgstr.substring(pos);
		// 第一步：先进行多站分析。依据6.6.2.7　遥测站地址用标识符导引，也是多站报文的分隔符。
		System.out.println("stinfo " + stinfo);

		List<DataItem> itemList = new ArrayList<DataItem>();

		String data[] = StringUtils.trim(stinfo).split(Separator.SEP);
		for (int i = 0, len = data.length; i < len; i = i + 2) {
			// HydroDataItem item = new HydroDataItem();
			// item.setName(data[i]);
			// i = i + 1;
			// item.setValue(new Double(data[i]));
			// items.add(item);

			System.out.println(">>>>TT item \t " + data[i] + " --- "
					+ data[i + 1]);
			if (data[i].equalsIgnoreCase("ST")) {
				body.setStcd(StcdParser.parseStcd(ByteUtil
						.HexStringToBinary(data[i + 1])));
			} else {
				byte[] flag = ByteUtil.HexStringToBinary(data[i]);
				DataItem item = new DataItem();
				item.setLength(0);
				item.setDecimal(0);
				item.setLabel(new byte[] { flag[0], 0 });
				item.setValue(ByteUtil.HexStringToBinary(data[i + 1]));
				itemList.add(item);
			}

		}
		body.setItems(itemList);

		processBody40("0", body);
		return body;

	}

	/**
	 * 6.6.4.15　中心站读取遥测站基本配置表/遥测站自报基本配置表
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up41Body parse41Body(IMessageBody messageBody) throws Exception {
		return (Up41Body) parse40Body(messageBody);
	}

	/**
	 * 6.6.4.16　中心站修改遥测站运行参数配置表
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up42Body parse42Body(IMessageBody messageBody) throws Exception {
		Up42Body body = new Up42Body();

		byte[] bytes = messageBody.getContent();
		String msgstr = new String(bytes);

		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		byte[] sendDate = new byte[6];
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		sendDate = ByteUtil.HexStringToBinary(sendDateStr);
		body.setSendDate(ByteUtil.bcd2Str(sendDate));
		pos = pos + sendDateLen;

		String stinfo = msgstr.substring(pos);
		// 第一步：先进行多站分析。依据6.6.2.7　遥测站地址用标识符导引，也是多站报文的分隔符。
		System.out.println("stinfo " + stinfo);

		List<DataItem> itemList = new ArrayList<DataItem>();

		String data[] = StringUtils.trim(stinfo).split(Separator.SEP);
		for (int i = 0, len = data.length; i < len; i = i + 2) {
			// HydroDataItem item = new HydroDataItem();
			// item.setName(data[i]);
			// i = i + 1;
			// item.setValue(new Double(data[i]));
			// items.add(item);

			System.out.println(">>>>TT item \t " + data[i] + " --- "
					+ data[i + 1]);
			if (data[i].equalsIgnoreCase("ST")) {
				body.setStcd(StcdParser.parseStcd(ByteUtil
						.HexStringToBinary(data[i + 1])));
			} else {

				byte[] flag = ByteUtil.HexStringToBinary(data[i]);

				DataItem item = new DataItem();
				String value = data[i + 1];
				int dotpos = value.indexOf(".");
				if (dotpos != -1) {
					item.setDecimal(value.length() - dotpos);
				}
				value = value.replaceAll(".", "");
				item.setLength(0);
				item.setLabel(new byte[] { flag[0], 0 });
				item.setValue(ByteUtil.str2Bcd(value));
				itemList.add(item);
			}

		}
		body.setItems(itemList);

		// processBody40("0", body);
		return body;

	}

	public Up44Body parse44Body(IMessageBody messageBody) throws Exception {

		Up44Body body = new Up44Body();
		byte[] bytes = messageBody.getContent();
		String msgstr = new String(bytes);

		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		// 遥测站分类码
		int categorylen = 1;
		String category = msgstr.substring(pos + 1, pos + 1 + categorylen);
		body.setSttp(category);
		pos = pos + 1 + categorylen;

		// 加的是_TT_这4个字符长度
		// 观测时间
		int viewdatelen = 10;
		String viewDate = msgstr.substring(pos + 4, pos + 4 + viewdatelen);
		pos = pos + 4 + viewdatelen;
		body.setViewDate(viewDate);

		String stinfo = msgstr.substring(pos);
		// 第一步：先进行多站分析。依据6.6.2.7　遥测站地址用标识符导引，也是多站报文的分隔符。
		System.out.println("stinfo " + stinfo);
		List<DataItem> itemList = new ArrayList<DataItem>();
		String data[] = StringUtils.trim(stinfo).split(Separator.SEP);
		for (int i = 0, len = data.length; i < len; i = i + 2) {
			System.out.println(">>>>TT item \t " + data[i] + " --- "
					+ data[i + 1]);
			DataItem item = new DataItem();
			String value = data[i + 1];
			int dotpos = value.indexOf(".");
			if (dotpos != -1) {
				item.setDecimal(value.length() - dotpos);
			}
			value = value.replaceAll(".", "");
			item.setLength(0);
			item.setLabel(ByteUtil.HexStringToBinary(FCAsc2Hex.ASC2HEX.get(data[i])));
			// item.setValue(temp);
			itemList.add(item);
		}
		return body;
	}

	/**
	 * 6.6.4.19　中心站查询遥测站软件版本
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up45Body parse45Body(IMessageBody messageBody) throws Exception {

		Up45Body body = new Up45Body();
		byte[] bytes = messageBody.getContent();
		String msgstr = new String(bytes);
		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		// 软件版本
		String versionStr = msgstr.substring(pos + 3);
		body.setData(versionStr.getBytes());
		byte[] data = body.getData();
		if (data != null) {
			String version = new String(data);
			if (version.length() > 255) {
				version = version.substring(0, 254);
			}
		}
		return body;
	}

	/**
	 * 6.6.4.20　中心站查询遥测站状态和报警信息
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up46Body parse46Body(IMessageBody messageBody) throws Exception {
		byte[] bytes = messageBody.getContent();
		Up46Body body = new Up46Body();
		String msgstr = new String(bytes);
		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		// 遥测站状态数据
		String statusStr = msgstr.substring(pos + 3);
		body.setData(ByteUtil.HexStringToBinary(statusStr.trim()));

		return body;
	}

	/**
	 * 6.6.4.21　初始化固态存储数据
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up47Body parse47Body(IMessageBody messageBody) throws Exception {
		return (Up47Body) parseUpBaseBody(messageBody);
	}

	/**
	 *6.6.4.22　恢复遥测站出厂设置
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up48Body parse48Body(IMessageBody messageBody) throws Exception {
		return (Up48Body) parseUpBaseBody(messageBody);
	}

	/**
	 * 6.6.4.23　修改密码
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up49Body parse49Body(IMessageBody messageBody) throws Exception {

		byte[] bytes = messageBody.getContent();
		Up49Body body = new Up49Body();
		String msgstr = new String(bytes);
		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		// 新密码
		String pwd = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		body.setPwd(ByteUtil.HexStringToBinary(pwd));
		return body;
	}

	/**
	 * 6.6.4.24　设置遥测站时钟
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public UpBaseBody parse4ABody(IMessageBody messageBody) throws Exception {
		return (UpBaseBody) parseUpBaseBody(messageBody);
	}

	/**
	 * 6.6.4.25　设置遥测站IC卡状态
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up4BBody parse4BBody(IMessageBody messageBody) throws Exception {
		return (Up4BBody) parse46Body(messageBody);
	}

	/**
	 * 6.6.4.26　控制水泵开关命令/水泵状态自报
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up4CBody parse4CBody(IMessageBody messageBody) throws Exception {
		byte[] bytes = messageBody.getContent();
		Up4CBody body = new Up4CBody();

		String msgstr = new String(bytes);
		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		// 水泵开关机状态数据
		String statusStr = msgstr.substring(pos + 3);

		System.out.println(statusStr);
		body.setData(ByteUtil.HexStringToBinary(statusStr.trim()));

		return body;
	}

	/**
	 * 6.6.4.28　控制闸门开关命令/闸门状态信息自报
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up4EBody parse4EBody(IMessageBody messageBody) throws Exception {
		byte[] bytes = messageBody.getContent();
		Up4EBody body = new Up4EBody();

		return body;
	}

	/**
	 * 6.6.4.29　水量定值控制命令
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up4FBody parse4FBody(IMessageBody messageBody) throws Exception {
		byte[] bytes = messageBody.getContent();
		Up4FBody body = new Up4FBody();
		String msgstr = new String(bytes);
		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		// 控制命令数据
		String cmdStr = msgstr.substring(pos + 3);
		body.setData(ByteUtil.HexStringToBinary(cmdStr)[0]);
		return body;
	}

	/**
	 * 6.6.4.30　中心站查询遥测站事件记录
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up50Body parse50Body(IMessageBody messageBody) throws Exception {
		byte[] bytes = messageBody.getContent();
		Up50Body body = new Up50Body();
		String msgstr = new String(bytes);

		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		// 遥测站事件记录数据
		String ercStr = msgstr.substring(pos + 1);
		body.setErc(ByteUtil.HexStringToBinary(ercStr.trim()));
		return body;
	}

	/**
	 * 6.6.4.31　中心站查询遥测站时钟
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public Up51Body parse51Body(IMessageBody messageBody) throws Exception {
		return (Up51Body) parseUpBaseBody(messageBody);
	}

	/**
	 * 基础类
	 * 
	 * @param messageBody
	 * @return
	 * @throws Exception
	 */
	public UpBaseBody parseUpBaseBody(IMessageBody messageBody)
			throws Exception {
		byte[] bytes = messageBody.getContent();
		UpBaseBody body = new UpBaseBody();
		String msgstr = new String(bytes);
		// 序列号
		int pos = 0;

		byte[] serial = new byte[2];
		int serialLen = 4;
		String serialStr = msgstr.substring(pos, pos + serialLen);
		serial = ByteUtil.HexStringToBinary(serialStr);
		body.setSerialId(ByteUtil.bytesToUshort(serial));
		pos = pos + serialLen;

		// 发报时间
		int sendDateLen = 12;
		String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
		body.setSendDate(sendDateStr);
		pos = pos + sendDateLen;

		// 遥测站地址
		int stcdlen = 10;
		String stcdStr = msgstr.substring(pos + 3, pos + 3 + stcdlen);
		String stationAddr = StcdParser.parseStcd(ByteUtil
				.HexStringToBinary(stcdStr));
		pos = pos + 3 + stcdlen;
		body.setStcd(stationAddr);

		return body;

	}

	private String processD1(byte[] data) {
		StringBuffer sb = new StringBuffer();
		int flag = ByteUtil.bytesToUbyte(new byte[] { data[0] });// 信道类型用1字节BCD码：1-短信，2-IPV4，3-北斗，4-海事卫星，5-PSTN，6-超短波。
		if (flag == 0) {
			sb.append("0");
		} else if (flag == 2) {
			StringBuffer sbdata = new StringBuffer();
			sbdata.append(flag).append(",");
			sbdata.append(
					ByteUtil.bcd2Str(new byte[] { data[1], data[2], data[3],
							data[4], data[5], data[6] })).append(",");
			sbdata.append(ByteUtil.bcd2Str(new byte[] { data[7], data[8],
					data[9] }));
			sb.append(sbdata.toString());
		} else {
			byte[] newvalue = new byte[data.length - 1];
			System.arraycopy(data, 1, newvalue, 0, newvalue.length);
			sb.append(ByteUtil.bcd2Str(newvalue));
		}
		return sb.toString();
	}

	private void processBody40(String mid, Up40Body body) throws Exception {
		int serial = body.getSerialId();
		String stcd = body.getStcd();
		String sendDate = body.getSendDate();
		Date sendDatetime = HydroParser.convertYYMMDDHHmmss(sendDate);

		StringBuffer sb = new StringBuffer("");
		List params = new ArrayList();

		params.add(sendDatetime);

		List<DataItem> items = body.getItems();
		for (DataItem item : items) {
			byte[] label = item.getLabel();
			byte[] value = item.getValue();
			int len = item.getLength();
			switch (label[0]) {
			case (byte) 0x01:
				byte[] f01 = value;
				int a1 = ByteUtil.bytesToUbyte(new byte[] { f01[0] });
				int a2 = ByteUtil.bytesToUbyte(new byte[] { f01[1] });
				int a3 = ByteUtil.bytesToUbyte(new byte[] { f01[2] });
				int a4 = ByteUtil.bytesToUbyte(new byte[] { f01[3] });

				System.out.println(a1 + "." + a2 + "." + a3 + "." + a4);
				sb.append(" , f01 = ? ");
				break;
			case (byte) 0x02:
				System.out.println(StcdParser.parseStcd(value));
				sb.append(" , f02 = ? ");
				break;
			case (byte) 0x03:
				System.out.println(ByteUtil.toHexString(value));
				sb.append(" , f03 = ? ");
				break;
			case (byte) 0x04:
			case (byte) 0x05:
			case (byte) 0x06:
			case (byte) 0x07:
			case (byte) 0x08:
			case (byte) 0x09:
			case (byte) 0x0A:
			case (byte) 0x0B:
				String data4 = this.processD1(value);
				System.out.println(data4);
				sb.append(" , f" + ByteUtil.toHexString(label[0]) + " = ? ");
				break;
			case (byte) 0x0C:
				System.out.println(ByteUtil.bcd2Str(value));
				sb.append(" , f0C = ? ");
				break;
			case (byte) 0x0D:
				System.out.println(ByteUtil.toBinaryString(value));
				sb.append(" , f0D = ? ");
				break;
			case (byte) 0x0E:
				System.out.println(ByteUtil.toHexString(value));
				sb.append(" , f0E = ? ");
				break;
			case (byte) 0x0F:
				byte[] cardData = value;
				String cardType = new String(new byte[] { cardData[0] });
				byte[] cardData2 = new byte[cardData.length - 1];
				System
						.arraycopy(cardData, 1, cardData2, 0,
								cardData.length - 1);

				System.out.println(cardType + "," + new String(cardData2));
				sb.append(" , f0F = ? ");
				break;
			}

		}

		params.add(stcd);

		System.out.println(">>>++++++++>>>>>>>+++++++ stcd " + stcd);

		String sql = "update rtu_station_40 set uptime = ? " + sb.toString()
				+ " where stcd = ? ";

		logger.info(sql);

		// dao.update(sql, params.toArray());
	}

	public Object parseBody(byte funcCode, byte[] bytes) {

		try {
			System.out.println("Parse Body MSG >> "
					+ new String(bytes, "gb2312"));
			String msgstr = new String(bytes);
			// 序列号
			int pos = 0;

			byte[] serial = new byte[2];
			int serialLen = 4;
			String serialStr = msgstr.substring(pos, pos + serialLen);
			serial = ByteUtil.HexStringToBinary(serialStr);
			pos = pos + serialLen;

			// 发报时间
			byte[] sendDate = new byte[6];
			int sendDateLen = 12;
			String sendDateStr = msgstr.substring(pos, pos + sendDateLen);
			sendDate = ByteUtil.HexStringToBinary(sendDateStr);
			pos = pos + sendDateLen;

			String stinfo = msgstr.substring(pos);
			// 第一步：先进行多站分析。依据6.6.2.7　遥测站地址用标识符导引，也是多站报文的分隔符。
			System.out.println("stinfo " + stinfo);
			List<HydroData> result = new ArrayList<HydroData>();

			String ST[] = stinfo.trim().toUpperCase().split(Separator.ST);
			for (int i = 0, len = ST.length; i < len; i++) {
				// 处理多时间序列
				if (StringUtils.isNotBlank(ST[i])) {
					result.addAll(processST(ST[i]));
				}
			}

			System.out.println(">> result size >> " + result.size());

			// 遥测站地址
			// byte[] stcdFlag = new byte[2];
			// byte[] stcd = new byte[5];
			// System.arraycopy(bytes, pos, stcdFlag, 0, stcdFlag.length);
			// pos = pos + stcdFlag.length;
			// System.arraycopy(bytes, pos, stcd, 0, stcd.length);
			// pos = pos + stcd.length;
			// String stationAddr = parseStcd(stcd);
			//
			// // 遥测站分类码
			// byte[] category = new byte[1];
			// System.arraycopy(bytes, pos, category, 0, category.length);
			// String cate = new String(category);
			// pos = pos + category.length;
			//
			// // 观测时间
			// byte[] viewDateFlag = new byte[2];
			// byte[] viewDateByte = new byte[5];
			// System.arraycopy(bytes, pos, viewDateFlag, 0,
			// viewDateFlag.length);
			// pos = pos + viewDateFlag.length;
			// System.arraycopy(bytes, pos, viewDateByte, 0,
			// viewDateByte.length);
			// pos = pos + viewDateByte.length;
			// String viewDate = ByteUtil.bcd2Str(viewDateByte);
			// // 数据内容
			// // int pos = serial.length + sendDate.length + stcdFlag.length
			// // + stcd.length + category.length + viewDateFlag.length
			// // + viewDateByte.length;
			// int len = bytes.length - pos;
			// byte[] data = new byte[len];
			// System.arraycopy(bytes, pos, data, 0, len);

			// // 开始分割字符串数据
			// System.out.println("----OK2-----dataBytes " + len);
			//
			// String content = new String(data);
			// System.out.println(content);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) throws Exception {

		AscParser parser = new AscParser();

		// SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
		// String sendDate = sdf.format(new Date());
		// String stcd = "88664422";
		// String category = "P";
		// SimpleDateFormat sdf2 = new SimpleDateFormat("yyMMddHHmm");
		// String viewDate = sdf2.format(new Date());
		// String data = "DRP 10111213141516171819202122 ";
		// byte[] bytes = parser.buildBody(32100, sendDate, stcd, category,
		// viewDate, data);
		// parser.parseBody(bytes);

		// String st = "ST AAA ST BBB";
		// String[] aaa = st.split("ST");
		// System.out.println(aaa.length);

		String hexstr = "303030313133303330373036303230305354203030313233343536373820482054542031333033303730363032205A20303030352E39392056542030322E313220";
		byte[] bytes = ByteUtil.HexStringToBinary(hexstr);
		// String info = new String(bytes);
		// System.out.println(info);
		// parser.parseBody(bytes);
		//		
		String ascstr = "0004130307061830ST 0012345678 H TT 1303070618 PR 0002.0 VT 12.33";
		ascstr = "0005130307060000ST 0012345678 H TT 1303070500 DRP 0A0A14141E1E282832323C3C DRZ1 01F401FE020802130220022C0232023F024F025902660278 TT 1303070600 PT 00044.0 PJ 0044.0 Z 0005.99 VT 12.33";
		ascstr = "0006130307060000ST 0012345678 H TT 1303070500 DRN05 DRP 0A0A14141E1E282832323C3C";
		ascstr = "0007130307060000ST 0012345678 H TT 1303070500 DRN05 DRZ1 01F401FE020802130220022C0232023F024F025902660278";
		ascstr = "0008130307060000ST 0012345678 H TT 1303070500 DRP 0A0A14141E1E282832323C3C DRZ1 01F401FE020802130220022C0232023F024F025902660278 TT 1303070600 PJ 0044.0 PD 0000.0 PT 00044.0 Z 0005.99 VT 12.33 ";
		ascstr = "0008130307060000ST 0012345678 H TT 1303070500 DRP 0A0A14141E1E282832323C3C DRZ1 01F401FE020802130220022C0232023F024F025902660278 TT 1303070600 PJ 0044.0 PD 0000.0 PT 00044.0 Z 0005.99 VT 12.33 ";
		ascstr = "0009130307060000ST 0012345678 H TT 1303070500 DRP 0A0A14141E1E282832323C3C DRZ1 01F401FE020802130220022C0232023F024F025902660278 TT 1303070600 PT 00044.0 PJ 0042.0 Z 0005.99 VT 12.33";
		ascstr = "000A130307060000ST 0012345678 H TT 1303070500 DRN05 DRP 0A0A14141E1E282832323C3C";
		ascstr = "000B130307060000ST 0012345678 H TT 1303070500 DRN05 DRZ1 01F401FE020802130220022C0232023F024F025902660278";
		ascstr = "000C130307060000ST 0012345678 H TT 1303070500 DRP 0A0A14141E1E282832323C3C DRZ1 01F401FE020802130220022C0232023F024F025902660278 TT 1303070600 PJ 0042.0 PD 0000.0 PT 00044.0 Z 0005.99 VT 12.33";
		ascstr = "0015130307060654ST 0011223344 04 02218094153171009000 05 00 ";
		ascstr = "0019130307060929ST 0011223344 20 01 21 02 22 03 23 0005 24 06 25 1.0 26 1.0 27 02 28 0003.000 30 04.000 38 05.00 40 6.00 41 7.00";
		ascstr = "001A130307060944ST 0011223344 H TT 1303070609 VTA M VTB M VTC M VIA M VIB M VIC M ";
		ascstr = "001B130307060955ST 0011223344 14ZSR7120 A3.60 ASC B1";
		ascstr = "001C130307061032ST 0011223344 ZT 000004A5 ";
		ascstr = "001D130307061117ST 0011223344 03 04D2";
		ascstr = "0020130308100402ST 0011223344 0101";
		ascstr = "0026130308100901ST 0011223344 000000080000000200010000000000200000000000000001000000000000002C0038000000000000000000000000000000000000000000000000000000000000";
		// ascstr
		bytes = ascstr.getBytes();
		UpBaseBody body = new UpBaseBody();
		body.setContents(bytes);
		// parser.parseBody((byte) 0x41, body);
		parser.parse50Body(body);
		return;
		// String bodyLen = parser.buildBodyLength(Symbol.DOWN, 4094);
		// System.out.println(">>>> bodyLen " + bodyLen);
		// String bodyCount = parser.buildBodyCount(4000, 4094);
		// System.out.println(">>>> bodyCount " + bodyCount);
		//
		// int[] bc = parser.parseBodyCount(bodyCount.getBytes());
		// System.out.println(">>>> bc " + bc[0] + " " + bc[1]);
		// AscHeader header = new AscHeader();
		// header.setFuncCode("2F");
		// header.setBodyLength(bodyLen);
		//
		// // byte aa = parser.parseFuncCode(header.getFuncCode());
		// // System.out.println(ByteUtil.toHexString(aa));
		// parser.parseBodyLength(header.getBodyLength());
		//
		// byte[] centerAddr = ByteUtil.HexStringToBinary("0Fff");
		// System.out.println(">>>> centerAddr1 "
		// + ByteUtil.byteToHexString(centerAddr));
		//
		// int len = 4095;
		// byte[] bLen = ByteUtil.shortToBytes((short) len);
		// System.out.println(">>>> " + ByteUtil.byteToHexString(bLen));
		//
		// // byte[] centerAddr = ByteUtil.HexStringToBinary(lenStr);
		// int aaalen = ByteUtil.bytesToUshort(bLen);
		// System.out.println(">>>> " + aaalen);
		//
		// for (int i = 1; i < 4095; i++) {
		// String ccc = parser.buildBodyCount(i, i);
		// int[] ddd = parser.parseBodyCount(ccc.getBytes());
		// System.out.println(">> ddd: " + ddd[0] + " " + ddd[1]);
		// }
	}

}
