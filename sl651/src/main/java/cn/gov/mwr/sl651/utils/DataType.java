package cn.gov.mwr.sl651.utils;

/**
 * 数据类型
 * 
 * 针对协议中所定义的数据类型
 * 
 * 表65 遥测终端水质参数种类、上限值数据格式
 * 
 * @ClassName: DataType
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public class DataType {

	private int id; // 顺序号
	private String text; // 文本解释
	private int blen; // 字节长度

	private int length; // 定义的数据长度
	private int decimal; // 定义的数据小数位长度
	private String unit; // 定义单位

	/**
	 * 构造函数，只定义顺序号、文本名称 、字节长度
	 * @param id
	 * @param text
	 * @param blen
	 */
	public DataType(int id, String text, int blen) {
		super();
		this.id = id;
		this.text = text;
		this.blen = blen;
	}

	/**
	 * 构造函数，定义顺序号、文本名称 、字节长度、数据显示总大小、数据显示小数位数
	 * @param id
	 * @param text
	 * @param blen
	 */
	public DataType(int id, String text, int blen, int length, int decimal) {
		super();
		this.id = id;
		this.text = text;
		this.blen = blen;
		this.length = length;
		this.decimal = decimal;
	}

	public DataType(int id, String text, int blen, int length, int decimal,
			String unit) {
		this(id, text, blen, length, decimal);
		this.unit = unit;
	}

	public DataType(int id, String text, String unit, int blen, int length,
			int decimal) {
		this(id, text, blen, length, decimal);
		this.unit = unit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getBlen() {
		return blen;
	}

	public void setBlen(int blen) {
		this.blen = blen;
	}

	public int getDecimal() {
		return decimal;
	}

	public void setDecimal(int decimal) {
		this.decimal = decimal;
	}

}
