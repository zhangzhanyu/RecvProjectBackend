package cn.gov.mwr.sl651.utils;

import org.apache.commons.lang3.StringUtils;


public class StcdParser {

	public static String parseStcd(byte[] data) {
		String stcd = "";

		if (data != null && data.length == 5) {

			if (data[0] == 0x00) {
				// 按《SL 502》标准协议
//				System.out.println("stcd sl502");
				stcd = ByteUtil.bcd2Str(new byte[] { data[0],data[1], data[2], data[3] , data[4]});

			} else {
				// 前三个字节A5、A4、A3采用GB2260—2007规定的行政区划代码的前6位，
//				System.out.println("stcd GB2260—2007");
				stcd = ByteUtil
						.bcd2Str(new byte[] { data[0], data[1], data[2] });

				// 后两个字节A2、A1为遥测站地址自定义段，采用HEX码，中心站解码时还原为3个字节BCD码；
				// 遥测站地址自定义1～60000,中继站地址自定义60001～65534
				int addr = ByteUtil
						.bytesToShort(new byte[] { data[3], data[4] });

				// 因为考虑到地址小于10000的现象，则需要左补0，3字节，总共为6位
				String addrStr = StringUtils.leftPad(String.valueOf(addr), 6,
						"0");// 6为三字节最大位数，在左补0

				stcd = stcd + addrStr;
			}
		}

		return stcd;
	}


}
