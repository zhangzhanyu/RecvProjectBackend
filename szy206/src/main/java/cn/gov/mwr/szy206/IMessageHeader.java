package cn.gov.mwr.szy206;

/**
 * 报文头接口，在此定义了除报文之外协议内容
 * 
 * @ClassName: IMessageHeader
 * @Description: TODO
 * @author lipujun
 * @date Mar 11, 2013
 * @see HexHeader,AscHeader
 */
public interface IMessageHeader {

	/**
	 * 1、帧起始符
	 * 
	 * @return
	 */
	public byte[] getStartBit();

	public void setStartBit(byte[] startBit);

	public int getStartBitLen();

	/**
	 * 2、报文 长度
	 * 
	 * @return
	 */
	public byte[] getBodySize();

	public void setBodySize(byte[] bodyLength);

	public int getBodySizeLen();

	/**
	 * 3、报文起始符
	 * 
	 * @return
	 */
	public byte[] getBodyStartBit();

	public void setBodyStartBit(byte[] bodyStartBit);

	public int getBodyStartBitLen();

	/**
	 * 4、遥测站地址
	 * 
	 * @return
	 */
//	public byte[] getControlAddr();
//
//	public void setControlAddr(byte[] controlAddr);
//
//	public int getControlAddrLen();
//
//	/**
//	 * 5、遥测站地址
//	 * 
//	 * @return
//	 */
//	public byte[] getStationAddr();
//
//	public void setStationAddr(byte[] stationAddr);
//
//	public int getStationAddrLen();

	public int getLength();

}
