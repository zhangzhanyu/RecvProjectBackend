package cn.gov.mwr.szy206;

import java.util.HashMap;
import java.util.Map;

/**
 * 水资源常数类，定义协议中所有的协议说明
 * 
 * @ClassName: SzyConst
 * @Description: TODO
 * @author lipujun
 * @date Jun 7, 2013
 * 
 */
public class SzyConst {
	public static Map<Byte, String> AFN = new HashMap<Byte, String>();

	static {
		AFN.put((byte) 0x02, "链路检测");
		AFN.put((byte) 0x10, "设置遥测终端、中继站地址");
		AFN.put((byte) 0x11, "设置遥测终端、中继站时钟");
		AFN.put((byte) 0x12, "设置遥测终端工作模式");
		AFN.put((byte) 0x15, "设置遥测终端本次充值量");
		AFN.put((byte) 0x16, "设置遥测终端剩余水量报警值");
		AFN.put((byte) 0x17, "设置遥测终端的水位基值、水位上下限");
		AFN.put((byte) 0x18, "设置遥测终端水压上、下限");
		AFN.put((byte) 0x19, "设置遥测终端水质参数种类、上限值");
		AFN.put((byte) 0x1A, "设置遥测终端水质参数种类、下限值");
		AFN.put((byte) 0x1B, "设置终端站水量的表底（初始）值");
		AFN.put((byte) 0x1C, "设置遥测终端转发中继引导码长");
		AFN.put((byte) 0x1D, "设置中继站转发终端地址");
		AFN.put((byte) 0x1E, "设置中继站工作机自动切换，自报状态");
		AFN.put((byte) 0x1F, "设置遥测终端流量参数上限值");
		AFN.put((byte) 0x20, "设置遥测终端检测参数启报阈值及固态存储时间段间隔");
		AFN.put((byte) 0x30, "置遥测终端IC 卡功能有效");
		AFN.put((byte) 0x31, "取消遥测终端IC 卡功能");
		AFN.put((byte) 0x32, "定值控制投入");
		AFN.put((byte) 0x33, "定值控制退出");
		AFN.put((byte) 0x34, "定值量设定");
		AFN.put((byte) 0x50, "查询遥测终端、中继站地址");
		AFN.put((byte) 0x51, "查询遥测终端、中继站时钟");
		AFN.put((byte) 0x52, "查询遥测终端工作模式");
		AFN.put((byte) 0x53, "查询遥测终端的数据自报种类及时间间隔");
		AFN.put((byte) 0x54, "查询遥测终端需查询的实时数据种类");
		AFN.put((byte) 0x55, "查询遥测终端最近成功充值量和现有剩余水量");
		AFN.put((byte) 0x56, "查询遥测终端剩余水量和报警值");
		AFN.put((byte) 0x57, "查询遥测终端水位基值、水位上下限");
		AFN.put((byte) 0x58, "查询遥测终端水压上、下限");
		AFN.put((byte) 0x59, "查询遥测终端水质参数种类、上限值");
		AFN.put((byte) 0x5A, "查询遥测终端水质参数种类、下限值");
		AFN.put((byte) 0x5D, "查询遥测终端的事件记录");
		AFN.put((byte) 0x5E, "查询遥测终端状态和报警状态");
		AFN.put((byte) 0x5F, "查询水泵电机实时工作数据");
		AFN.put((byte) 0x60, "查询遥测终端转发中继引导码长");
		AFN.put((byte) 0x61, "查询遥测终端图像记录");
		AFN.put((byte) 0x62, "查询中继站转发终端地址");
		AFN.put((byte) 0x63, "查询中继站工作机状态和切换记录");
		AFN.put((byte) 0x64, "查询遥测终端流量参数上限值");
		AFN.put((byte) 0x81, "随机自报报警数据");
		AFN.put((byte) 0x82, "人工置数");
		AFN.put((byte) 0x90, "复位遥测终端参数和状态");
		AFN.put((byte) 0x91, "清空遥测终端历史数据单元");
		AFN.put((byte) 0x92, "遥控启动水泵或阀门/闸门");
		AFN.put((byte) 0x93, "遥控关闭水泵或阀门/闸门");
		AFN.put((byte) 0x94, "遥控终端或中继站通信机切换");
		AFN.put((byte) 0x95, "遥控中继站工作机切换");
		AFN.put((byte) 0x96, "修改遥测终端密码");
		AFN.put((byte) 0xA0, "设置遥测站需查询的实时数据种类");
		AFN.put((byte) 0xA1, "设置遥测终端的数据自报种类及时间间隔");
		AFN.put((byte) 0xB0, "查询遥测终端实时值");
		AFN.put((byte) 0xB1, "查询遥测终端固态存储数据");
		AFN.put((byte) 0xB2, "查询遥测终端内存自报数据");
		AFN.put((byte) 0xC0, "遥测终端自报实时数据");
	}

	public static String[] FUNC = new String[] { "0 确认认可", "1 ：雨量参数",
			"2： 水位参数", " 3： 流量（水量）参数", "4 ：流速参数", "5： 闸位参数", "6： 功率参数",
			"7 ：气压参数", "8 ：风速参数", "9 ：水温参数", "10： 水质参数", " 11： 土壤含水率参数",
			"12： 蒸发量参数", "13： 报警或状态参数", "14： 统计雨量", "15： 水压参数" };

	public static Map<String, DataType> DATATYPE = new HashMap<String, DataType>();
	static {
		DATATYPE.put("UP1", new DataType(1, "雨量参数", 3));
		DATATYPE.put("UP2", new DataType(2, "水位参数", 4));
		DATATYPE.put("UP3", new DataType(3, "流量（水量）参数", 5));
		DATATYPE.put("UP4", new DataType(4, "流速参数", 3));
		DATATYPE.put("UP5", new DataType(5, "闸位参数", 3));
		DATATYPE.put("UP6", new DataType(6, "功率参数", 3));
		DATATYPE.put("UP7", new DataType(7, "气压参数", 3));
		DATATYPE.put("UP8", new DataType(8, "风速参数", 3));
		DATATYPE.put("UP9", new DataType(9, "水温参数", 2));
		DATATYPE.put("UP10", new DataType(10, "水质参数", 4));
		DATATYPE.put("UP11", new DataType(11, "土壤含水率参数", 2));
		DATATYPE.put("UP12", new DataType(12, "蒸发量参数", 3));
		DATATYPE.put("UP13", new DataType(13, "报警或状态参数", 3));
		DATATYPE.put("UP14", new DataType(14, "统计雨量", 3));
		DATATYPE.put("UP15", new DataType(15, "水压参数", 4));

	}

	public static Map<String, DataType> SZ = new HashMap<String, DataType>();
	static {
		SZ.put("0", new DataType(0, "水温", "℃", 4, 3, 1));
		SZ.put("1", new DataType(1, "pH", "值", 4, 4, 2));
		SZ.put("2", new DataType(2, "溶解氧", "mg/L", 4, 4, 1));
		SZ.put("3", new DataType(3, "高锰酸盐指数", "mg/L", 4, 4, 1));
		SZ.put("4", new DataType(4, "电导率", "μs/cm", 4, 5, 0));
		SZ.put("5", new DataType(5, "氧化还原电位", "mv", 4, 5, 1));
		SZ.put("6", new DataType(6, "浊度度", 4, 3, 0));
		SZ.put("7", new DataType(7, "化学需氧量", "mg/L", 4, 7, 1));
		SZ.put("8", new DataType(8, "五日生化需氧量", "mg/L", 4, 5, 1));
		SZ.put("9", new DataType(9, "氨氮", "mg/L", 4, 6, 2));
		SZ.put("10", new DataType(10, "总氮", "mg/L", 4, 5, 2));
		SZ.put("11", new DataType(11, "铜", "mg/L", 4, 7, 4));
		SZ.put("12", new DataType(12, "锌", "mg/L", 4, 6, 4));
		SZ.put("13", new DataType(13, "氟化物", "mg/L", 4, 5, 2));
		SZ.put("14", new DataType(14, "硒", "mg/L", 4, 7, 5));
		SZ.put("15", new DataType(15, "砷", "mg/L", 4, 7, 5));
		SZ.put("16", new DataType(16, "汞", "mg/L", 4, 7, 5));
		SZ.put("17", new DataType(17, "镉", "mg/L", 4, 7, 5));
		SZ.put("18", new DataType(18, "六价铬", "mg/L", 4, 5, 3));
		SZ.put("19", new DataType(19, "铅", "mg/L", 4, 7, 5));
		SZ.put("20", new DataType(20, "氰化物", "mg/L", 4, 5, 3));
		SZ.put("21", new DataType(21, "挥发酚", "mg/L", 4, 5, 3));
		SZ.put("22", new DataType(22, "苯酚", "mg/L", 4, 5, 2));
		SZ.put("23", new DataType(23, "硫化物", "mg/L", 4, 5, 3));
		SZ.put("24", new DataType(24, "粪大肠菌群", "个／L", 5, 10, 0));
		SZ.put("25", new DataType(25, "硫酸盐", "mg/L", 4, 6, 2));
		SZ.put("26", new DataType(26, "氯化物", "mg/L", 4, 8, 2));
		SZ.put("27", new DataType(27, "硝酸盐氮", "mg/L", 4, 5, 2));
		SZ.put("28", new DataType(28, "铁", "mg/L", 4, 4, 2));
		SZ.put("29", new DataType(29, "锰", "mg/L", 4, 4, 2));
		SZ.put("30", new DataType(30, "石油类", "mg/L", 4, 4, 2));
		SZ.put("31", new DataType(31, "阴离子表面活性剂", "mg/L", 4, 4, 2));
		SZ.put("32", new DataType(32, "六六六", "mg/L", 4, 7, 6));
		SZ.put("33", new DataType(33, "滴滴涕", "mg/L", 4, 7, 6));
		SZ.put("34", new DataType(34, "有机氯农药", "mg/L", 4, 7, 6));
		// new DataType(35～39","备用");
	}

	public static String[] ALARM = new String[] { "D0—工作交流电停电告警", "D1—蓄电池电压报警",
			"D2—水位超限报警", "D3—流量超限报警", "D4—水质超限报警", "D5—流量仪表故障报警", "D6—水泵开停状态",
			"D7—水位仪表故障报警", "D8—水压超限报警", "D9—备用", "D10—终端IC 卡功能报警",
			"D11—定值控制报警", "D12—剩余水量的下限报警", "D13—终端箱门状态报警", "D14—备用", "D15—备用" };

}
