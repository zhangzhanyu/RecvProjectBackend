package cn.gov.mwr.szy206;

import java.io.Serializable;
 

/**
 * 水资源消息体，在此构造消息头、消息体、校验码、结束符
 * 
 * @ClassName: SzyMessage
 * @Description: TODO
 * @author lipujun
 * @date Jun 7, 2013
 * 
 */
public class SzyMessage implements IMessage, Serializable {
	public IMessageHeader header;

	public IMessageBody body;

	public byte eof;

	public byte[] crc;

	public void setHeader(IMessageHeader header) {
		this.header = header;
	}

	public IMessageHeader getHeader() {
		// TODO Auto-generated method stub
		return this.header;
	}

	public void setBody(IMessageBody body) {
		this.body = body;
	}

	public IMessageBody getBody() {
		return this.body;
	}

	public void setEOF(byte eof) {
		this.eof = eof;
	}

	public byte getEOF() {
		return this.eof;
	}

	public void setCRC(byte[] crc) {
		this.crc = crc;
	}

	public byte[] getCRC() {
		return this.crc;
	}
}
