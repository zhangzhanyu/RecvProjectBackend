package cn.gov.mwr.szy206;

import java.io.Serializable;

import cn.gov.mwr.szy206.body.ISzyBody;
 
/**
 * 水资源消息体，消息体中包含了控制域、地址域、数据域等信息
 * 
 * @ClassName: SzyMessageBody
 * @Description: TODO
 * @author lipujun
 * @date May 27, 2013
 * 
 */
public class SzyMessageBody implements IMessageBody, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8534178934010713220L;

	public byte control;
	public byte[] address;
	public ISzyBody userdata;

	/**
	 * 记录原始消息体
	 */
	public byte[] content;

	public void setControl(byte control) {
		this.control = control;
	}

	public byte getControl() {
		return control;
	}

	public void setUserData(ISzyBody userdata) {
		this.userdata = userdata;
	}

	public ISzyBody getUserData() {
		return userdata;
	}

	public void setAddress(byte[] address) {
		this.address = address;
	}

	public byte[] getAddress() {
		return address;
	}

	public int getLength() {
		if (this.content != null)
			return content.length;
		return 1 + 5 + (userdata != null ? userdata.getLength() : 0);
	}

	public byte[] getContent() {
		// TODO Auto-generated method stub
		if (this.content != null)
			return content;

		int total = getLength();
		byte[] content = new byte[total];

		int pos = 0;
		// 控制域
		System.arraycopy(new byte[] { control }, 0, content, 0, 1);
		pos = pos + 1;

		// 地址域
		if (address != null) {
			System.arraycopy(address, 0, content, pos, address.length);
			pos = pos + address.length;
		}

		// 附加信息域AUX
		if (userdata != null) {
			System.arraycopy(userdata.getContent(), 0, content, pos, userdata
					.getLength());
		}

		return content;
	}

	public void setContents(byte[] content) {
		this.content = content;
	}

}
