package cn.gov.mwr.szy206;

import java.io.Serializable;

import cn.gov.mwr.szy206.utils.ByteUtil;

/**
 * 水资源报头
 * 
 * @ClassName: SzyHeader
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2015
 * 
 */
public class SzyMessageHeader implements IMessageHeader, Serializable {

	public static int LEN_START_BIT = 1;
	public static int LEN_BODY_SIZE = 1;
	public static int LEN_BODY_START_BIT = 1;

	private byte[] START_BIT = new byte[] { SzySymbol.STX };
	private byte[] BODY_SIZE;
	private byte[] BODY_START_BIT = new byte[] { SzySymbol.STX };

	public void setStartBit(byte[] sTARTBIT) {
		START_BIT = sTARTBIT;
	}

	public void setBodySize(byte[] bODYLENGTH) {
		BODY_SIZE = bODYLENGTH;
	}

	public void setBodySize(int bodylen) {
		BODY_SIZE = ByteUtil.ubyteToBytes(bodylen);
	}

	public void setBodyStartBit(byte[] bODYSTARTBIT) {
		BODY_START_BIT = bODYSTARTBIT;
	}

	public byte[] getStartBit() {
		return this.START_BIT;
	}

	public byte[] getBodySize() {
		return BODY_SIZE;
	}

	public byte[] getBodyStartBit() {
		return BODY_START_BIT;
	}

	public int getStartBitLen() {
		return 1;
	}

	public int getBodySizeLen() {
		return 1;
	}

	public int getBodyStartBitLen() {
		return 1;
	}

	public int getLength() {
		return 3;
	}
}
