package cn.gov.mwr.szy206;

/**
 * 水资源公用标识符
* @ClassName: SzySymbol 
* @Description: TODO
* @author lipujun 
* @date Jun 7, 2013 
*
 */
public class SzySymbol {

	/**
	 * 68H, 传输起始字符
	 */
	public final static byte STX = 0x68;

	
	/**
	 * 16H, 传输结束字符
	 */
	public final static byte EOF = 0x16;

}
