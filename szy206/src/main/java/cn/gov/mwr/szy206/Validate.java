package cn.gov.mwr.szy206;

/**
 * 校验类
 * 
 * @ClassName: Validate
 * @Description: TODO
 * @author lipujun
 * @date Jun 6, 2013
 * 
 */
public class Validate {

	public static boolean check(byte[] bytes) {
		// 第一步：解析报文
		byte startMsgBit = bytes[0];
		byte datalen = bytes[1];
		byte startBodyBit = bytes[2];

		// 1.1：解析数据长度
		int len = SzyParser.parseDataLen(new byte[] { datalen });

		// 1.2：存放正文内容
		byte[] data = new byte[len];
		System.arraycopy(bytes, 3, data, 0, len);

		// 1.3：获取CRC校验码
		byte cs = bytes[3 + len];

		// 1.4：获取结束符
		byte endBit = bytes[bytes.length - 1];

		// 5.1.3.2 f.1)
		if ((byte) startMsgBit != (byte) 0x68
				|| (byte) startBodyBit != (byte) 0x68) {
			return false;
		}

		// 5.1.3.2 f.4)
		if ((byte) cs != (byte) SzyParser.parseCrc(data)) {
			// System.out.println(">> CRC " + ByteUtil.toHexString(cs));
			return false;
		}

		// 5.1.3.2 f.5)
		if ((byte) endBit != (byte) 0x16) {
			return false;
		}

		// 5.1.3.2 f.3)
		if (bytes.length < (len + 5)) {
			return false;
		}

		return true;
	}

	public static boolean check(SzyMessage message) {

		SzyMessageHeader header = (SzyMessageHeader) message.getHeader();
		byte startMsgBit = header.getStartBit()[0];
		byte startBodyBit = header.getBodyStartBit()[0];
		byte cs = message.getCRC()[0];
		byte endBit = message.getEOF();
		byte[] data = message.getBody().getContent();
		// 第二步：校验部分
		// 5.1.3.2 f.1)
		if ((byte) startMsgBit != (byte) 0x68
				|| (byte) startBodyBit != (byte) 0x68) {
			return false;
		}

		// 5.1.3.2 f.4)
		if ((byte) cs != (byte) SzyParser.parseCrc(data)) {
			// System.out.println(">> CRC " + ByteUtil.toHexString(cs));
			return false;
		}

		// 5.1.3.2 f.5)
		if ((byte) endBit != (byte) 0x16) {
			return false;
		}

		return true;

		// // 5.1.3.2 f.3)
		// if (bytes.length < (data.length + 5)) {
		// return false;
		// }

	}

}
