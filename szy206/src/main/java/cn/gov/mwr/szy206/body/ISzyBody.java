package cn.gov.mwr.szy206.body;

/**
 * 用户数据接口
 * 
 * @ClassName: IUserData
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public interface ISzyBody {


	public int getLength();

	public byte[] getContent();

	
}
