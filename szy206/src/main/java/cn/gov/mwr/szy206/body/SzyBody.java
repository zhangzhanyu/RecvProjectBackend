package cn.gov.mwr.szy206.body;

import java.io.Serializable;

/**
 * 水资源数据域，具体实现放在子类中实现，其主要包括 功能码、数据域、附加信息域等信息。
 * 
 * 数据域根据功能码的不同填充的数据也不一样
 * 
 * @ClassName: SzyUserData
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public abstract class SzyBody implements ISzyBody, Serializable {

	// 功能码AFN
	protected byte AFN;

	// 数据域
	protected byte[] data;

	// 报警状态
	protected byte[] alarm;
	
	// 终端状态
	protected byte[] status;

	// AUX区域
	protected byte[] pw;// = new byte[2];
	protected byte[] tp;// = new byte[5];

	public void setAFN(byte afn) {
		this.AFN = afn;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public byte getAFN() {
		return AFN;
	}

	public byte[] getData() {
		return data;
	}

	public byte[] getPw() {
		return pw;
	}

	public void setPw(byte[] pw) {
		this.pw = pw;
	}

	public byte[] getTp() {
		return tp;
	}

	public void setTp(byte[] tp) {
		this.tp = tp;
	}

	public byte[] getStatus() {
		return status;
	}

	public void setStatus(byte[] status) {
		this.status = status;
	}

	public byte[] getAlarm() {
		return alarm;
	}

	public void setAlarm(byte[] alarm) {
		this.alarm = alarm;
	}

	/**
	 * 获取数据区长度
	 */
	public int getLength() {

		int datalen = data == null ? 0 : data.length;
		int alarmlen = alarm == null ? 0 : alarm.length;
		int statuslen = status == null ? 0 : status.length;
		int pwLen = pw == null ? 0 : pw.length;
		int tpLen = tp == null ? 0 : tp.length;

		return 1 + datalen + alarmlen + statuslen + pwLen + tpLen;
	}

	/**
	 * 获取数据区内容
	 */
	public byte[] getContent() {

		int total = getLength();
		byte[] content = new byte[total];

		int pos = 0;
		// 功能码AFN
		System.arraycopy(new byte[] { AFN }, 0, content, 0, 1);
		pos = pos + 1;

		// 数据域
		if (data != null) {
			System.arraycopy(data, 0, content, pos, data.length);
			pos = pos + data.length;
		}


		// 数据域
		if (alarm != null) {
			System.arraycopy(alarm, 0, content, pos, alarm.length);
			pos = pos + alarm.length;
		}

		// 数据域
		if (status != null) {
			System.arraycopy(status, 0, content, pos, status.length);
			pos = pos + status.length;
		}

		
		// 附加信息域--密码域
		if (pw != null) {
			System.arraycopy(pw, 0, content, pos, pw.length);
			pos = pos + pw.length;
		}

		// 附加信息域--时间域
		if (tp != null) {
			System.arraycopy(tp, 0, content, pos, tp.length);
			pos = pos + tp.length;
		}

		return content;
	}

}
