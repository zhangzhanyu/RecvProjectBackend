package cn.gov.mwr.szy206.command;

import java.text.DecimalFormat;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import cn.gov.mwr.szy206.IMessage;
import cn.gov.mwr.szy206.IMessageBody;
import cn.gov.mwr.szy206.SzyBuilder;
import cn.gov.mwr.szy206.SzyMessage;
import cn.gov.mwr.szy206.SzyMessageBody;
import cn.gov.mwr.szy206.SzyMessageHeader;
import cn.gov.mwr.szy206.SzySymbol;
import cn.gov.mwr.szy206.body.BodyDown;
import cn.gov.mwr.szy206.utils.ByteUtil;
import cn.gov.mwr.szy206.utils.CrcUtil;

/**
 * 
 * 
 * 10://设置遥测终端、中继站地址
 * 
 * 11://设置遥测终端、中继站时钟
 * 
 * 12://设置遥测终端工作模式
 * 
 * 15://设置遥测终端本次充值量
 * 
 * 16://设置遥测终端剩余水量报警值
 * 
 * 
 * 17://设置遥测终端水位基值、水位上下限
 * 
 * 18://设置遥测终端水压上、下限
 * 
 * 19://设置遥测终端水质参数种类、上限值
 * 
 * 1A://设置遥测终端水质参数种类、下限值
 * 
 * 1B://设置遥测终端站水量的表底（初始）值
 * 
 * 1C://设置遥测终端转发中继引导码长
 * 
 * 1D://设置中继站转发终端地址
 * 
 * 1E://设置中继站工作机自动切换，自报状态
 * 
 * 1F://设置遥测终端流量参数上限值
 * 
 * 20://设置遥测终端检测参数启报阈值及固态存储时间段间隔
 * 
 * 30://设置遥测终端IC卡功能有效
 * 
 * 31://取消遥测终端IC卡功能
 * 
 * 32://定值控制投入
 * 
 * 33://定值控制退出
 * 
 * 34://定值量设定
 * 
 * @author lipujun
 *
 */
public class DownCommand {

	public static int DIR = 0;// 表示由中心端下发的报文

	public static int DIV = 0;// 0表示单报文，下发的命令，都是单报文

	public static int FCB = 3;// 3表示重发次数，默认为3次，最大为3次

	// 启用默认的延时时间设置
	public static int DELAY = 0;

	public static int SEC_METHOD = 1;

	public static int SEC_SECRET = 1;

	protected IMessage buildMessage(IMessageBody body) {

		SzyMessage message = new SzyMessage();

		SzyMessageHeader header = new SzyMessageHeader();
		header.setBodySize(body.getContent().length);

		message.setHeader(header);
		message.setBody(body);

		byte[] bytes = body.getContent();
		byte[] crcResult = CrcUtil.crc8Check(bytes);
		message.setCRC(crcResult);

		message.setEOF(SzySymbol.EOF);

		return message;

	}

	/**
	 * 02://******** 链路检测 ********
	 * 
	 * 原始包长度：13字节 原始包信息：68086830320101010002F06716
	 * 
	 * 传输方向位DIR：0；拆分标志位DIV：0；帧计数位FCB：3；功能码：0
	 * 
	 * 行政区划码：320101，测站地址：1 应用层功能码：02
	 * 
	 * @return
	 */
	public IMessage send02Message(String stcd, byte data) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		bodyDown.setAFN((byte) 0x02);
		bodyDown.setData(new byte[] { data });

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);

	}

	/**
	 * 50://查询遥测终端、中继站地址
	 * 
	 * @return
	 */
	public IMessage sendQueryMessage(String stcd, int FUNC, byte AFN) {

		SzyMessageBody messageBody = new SzyMessageBody();

		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		// switch (AFN) {
		// case (byte) 0x50:// 查询遥测终端、中继站地址
		// case (byte) 0x51:// 查询遥测终端、中继站时钟
		// case (byte) 0x52:// 查询遥测终端工作模式
		// case (byte) 0x53:// 查询遥测终端的数据自报种类及时间间隔
		// case (byte) 0x54:// 查询遥测终端需查询的实时数据种类
		// case (byte) 0x55:// 查询遥测终端最近成功充值量和现有剩余水量
		// case (byte) 0x56:// 查询遥测终端剩余水量和报警值
		// case (byte) 0x57:// 查询遥测终端水位基值、水位上下限
		// case (byte) 0x58:// 查询遥测终端水压上、下限
		// case (byte) 0x59:// 查询遥测终端水质参数种类、上限值
		// case (byte) 0x5A:// 查询遥测终端水质参数种类、下限值
		// case (byte) 0x5D:// 查询遥测终端的事件记录
		// case (byte) 0x5E:// 查询遥测终端状态和报警状态
		// case (byte) 0x5F:// 查询水泵电机实时工作数据
		// case (byte) 0x60:// 查询遥测终端转发中继引导码长
		// case (byte) 0x61:// 查询遥测终端图像记录
		// case (byte) 0x62:// 查询中继站转发终端地址
		// case (byte) 0x63:// 查询中继站工作机状态和切换记录
		// case (byte) 0x64:// 查询遥测终端流量参数上限值
		// case (byte) 0xB0:// 查询遥测终端实时值（AFN=B0H)
		// bodyDown.setAFN(AFN);
		// }
		bodyDown.setAFN(AFN);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 50://查询遥测终端、中继站地址
	 * 
	 * @return
	 */
	public IMessage send50Message(String stcd) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 14;
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		bodyDown.setAFN((byte) 0x50);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 50: 查询遥测终端实时值 680768313201040200B0DD16
	 * 
	 * @return
	 */
	public IMessage sendB0Message(String stcd) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 1;
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		bodyDown.setAFN((byte) 0xB0);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * B1://查询终端固态存储数据
	 * 
	 * @return
	 */
	public IMessage sendB1Message(String stcd, int value, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		byte[] newdata = SzyBuilder.newSZ(value, 10, 0);
		bodyDown.setAFN((byte) 0xB1);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 10://设置遥测终端、中继站地址
	 * 
	 * @return
	 */
	public IMessage send10Message(String stcd, String address, int method,
			int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] naddress = SzyBuilder.newStcd(address);
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x10);
		bodyDown.setData(naddress);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 11://设置遥测终端、中继站时钟
	 * 
	 * @return
	 */
	public IMessage send11Message(String stcd, String colck) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] newcolck = SzyBuilder.newClock();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(SEC_METHOD, SEC_SECRET);

		bodyDown.setAFN((byte) 0x10);
		bodyDown.setData(newcolck);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);

	}

	/**
	 * 12://设置遥测终端工作模式
	 * 
	 * @return
	 */
	public IMessage send12Message(String stcd, byte data, int method, int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x10);
		bodyDown.setData(new byte[] { data });
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);

	}

	/**
	 * 15://设置遥测终端本次充值量
	 * 
	 * @return
	 */
	public IMessage send15Message(String stcd, int data, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		if (data > 99999999 || data < 0) {
			data = 0;
		}
		String strData = StringUtils.leftPad("" + data, 8, "0");
		byte[] newdata = ByteUtil.str2Bcd(strData);
		ArrayUtils.reverse(newdata);

		bodyDown.setAFN((byte) 0x15);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 16://设置遥测终端剩余水量报警值 3个字节的数据
	 * 
	 * @return
	 */
	public IMessage send16Message(String stcd, int data, int method, int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		if (data > 999999 || data < 0) {
			data = 0;
		}
		String strData = StringUtils.leftPad("" + data, 6, "0");
		byte[] newdata = ByteUtil.str2Bcd(strData);
		ArrayUtils.reverse(newdata);

		bodyDown.setAFN((byte) 0x16);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 17://设置遥测终端水位基值、水位上下限
	 * 
	 * @return
	 */
	public IMessage send17Message(String stcd, double base, double lowlimit,
			double toplimit, int method, int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		// -------------------水位基值-----------------------
		if (base < -7999 || base > 7999.99) {
			base = 0;
		}
		byte[] basebyte = SzyBuilder.newR(base, 6);

		// -------------------水位下限-----------------------
		if (lowlimit < 0 || lowlimit > 99.99) {
			lowlimit = 0;
		}
		byte[] lowlimitbyte = SzyBuilder.newR(lowlimit, 4);

		// -------------------水位上限-----------------------
		if (toplimit < 0 || toplimit > 99.99) {
			toplimit = 0;
		}
		byte[] toplimitbyte = SzyBuilder.newR(toplimit, 4);

		// ------------------合并为一个数值对----------------------
		byte[] newdata = new byte[7];
		int pos = 0;
		// 设置水位基值
		System.arraycopy(basebyte, 0, newdata, 0, basebyte.length);
		pos = pos + basebyte.length;
		// 设置水位下限值
		System.arraycopy(lowlimitbyte, 0, newdata, pos, lowlimitbyte.length);
		pos = pos + lowlimitbyte.length;
		// 设置水位上限值
		System.arraycopy(toplimitbyte, 0, newdata, pos, toplimitbyte.length);

		bodyDown.setAFN((byte) 0x17);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);

	}

	/**
	 * 18://设置遥测终端水压上、下限 水压表上限值： 33.00Kpa，下限值： 2.00Kpa
	 * 
	 * @return
	 */
	public IMessage send18Message(String stcd, double toplimit,
			double lowlimit, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		// -------------------水位上限-----------------------
		if (toplimit < 0 || toplimit > 999999.99) {
			toplimit = 0;
		}
		byte[] toplimitbyte = SzyBuilder.newR(toplimit, 8);

		// -------------------水位下限-----------------------
		if (lowlimit < 0 || lowlimit > 999999.99) {
			lowlimit = 0;
		}
		byte[] lowlimitbyte = SzyBuilder.newR(lowlimit, 8);

		// ------------------合并为一个数值对----------------------
		byte[] newdata = new byte[8];
		int pos = 0;

		// 设置水位上限值
		System.arraycopy(toplimitbyte, 0, newdata, pos, toplimitbyte.length);
		pos = pos + toplimitbyte.length;

		// 设置水位下限值
		System.arraycopy(lowlimitbyte, 0, newdata, pos, lowlimitbyte.length);

		bodyDown.setAFN((byte) 0x18);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 19://******** 设置遥测终端水质参数种类、上限值 ******** 原始包长度：44字节
	 * 
	 * 原始包信息：
	 * 6827683032010402001957000000002002000000110000900000440800000007000000508059201322001316
	 * 
	 * 传输方向位DIR：0；拆分标志位DIV：0；帧计数位FCB：3；功能码：0
	 * 
	 * 行政区划码：320104，测站地址：2
	 * 
	 * 应用层功能码：19
	 * 
	 * 密钥算法：8；密钥：050；Tp：22日13时20分59秒 允许发送传输延时时间：0
	 * 
	 * 水温：22.0 ℃
	 * 
	 * pH值：11.00
	 * 
	 * 溶解氧： 9.0 mg/L
	 * 
	 * 电导率：8 us/cm
	 * 
	 * 浊度：7 度
	 * 
	 * @return
	 */
	public IMessage send19Message(String stcd, byte[] viewitem,
			byte[] viewvalue, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		// ------------------合并为一个数值对----------------------
		byte[] newdata = new byte[viewitem.length + viewvalue.length];
		int pos = 0;

		// 设置水位上限值
		System.arraycopy(viewitem, 0, newdata, pos, viewitem.length);
		pos = pos + viewitem.length;

		// 设置水位下限值
		System.arraycopy(viewvalue, 0, newdata, pos, viewvalue.length);

		bodyDown.setAFN((byte) 0x19);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 1A://设置遥测终端水质参数种类、下限值
	 * 
	 * @return
	 */
	public IMessage send1AMessage(String stcd, byte[] viewitem,
			byte[] viewvalue, int method, int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		// ------------------合并为一个数值对----------------------
		byte[] newdata = new byte[viewitem.length + viewvalue.length];
		int pos = 0;

		// 设置水位上限值
		System.arraycopy(viewitem, 0, newdata, pos, viewitem.length);
		pos = pos + viewitem.length;

		// 设置水位下限值
		System.arraycopy(viewvalue, 0, newdata, pos, viewvalue.length);

		bodyDown.setAFN((byte) 0x1A);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 1B://设置遥测终端站水量的表底（初始）值
	 * 
	 * @return
	 */
	public IMessage send1BMessage(String stcd, int[] values, int method,
			int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		int N = values.length;
		byte[] newdata = new byte[5 * N];
		int pos = 0;
		for (int i = 0; i < N; i++) {
			int value = values[i];
			byte[] d = SzyBuilder.newSZ(value, 10, 0);
			System.arraycopy(d, 0, newdata, pos, d.length);
			pos = pos + d.length;
		}

		bodyDown.setAFN((byte) 0x1B);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 1C://设置遥测终端转发中继引导码长
	 * 
	 * @return
	 */
	public IMessage send1CMessage(String stcd, int value, int method, int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		if (value < 0 || value > 255) {
			value = 0;
		}

		bodyDown.setAFN((byte) 0x1C);
		bodyDown.setData(ByteUtil.ubyteToBytes(value));
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);

	}

	/**
	 * 1D://设置中继站转发终端地址
	 * 
	 * @return
	 */
	public IMessage send1DMessage(String stcd, String address, int method,
			int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] naddress = SzyBuilder.newStcd(address);
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x1D);
		bodyDown.setData(naddress);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 1E://设置中继站工作机自动切换，自报状态
	 * 
	 * @return
	 */
	public IMessage send1EMessage(String stcd, byte data, int method, int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x1E);
		bodyDown.setData(new byte[] { data });
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 1F://设置遥测终端流量参数上限值
	 * 
	 * @return
	 */
	public IMessage send1FMessage(String stcd, double[] values, int method,
			int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		int N = values.length;
		byte[] newdata = new byte[5 * N];
		int pos = 0;
		for (int i = 0; i < N; i++) {
			double value = values[i];
			byte[] d = SzyBuilder.newSZ(value, 10, 3);
			if (value < 0) {
				d[0] = (byte) (d[0] | (byte) 0xF0);
			}
			System.arraycopy(d, 0, newdata, pos, d.length);
			pos = pos + d.length;
		}

		bodyDown.setAFN((byte) 0x1F);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	public IMessage send20Message(String stcd, String type, int interval,
			int method, int secret) {

		return null;
	}

	/**
	 * 30://设置遥测终端IC卡功能有效
	 * 
	 * @return
	 */
	public IMessage send30Message(String stcd, int method, int secret) {

		SzyMessageBody messageBody = new SzyMessageBody();

		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x30);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 31://取消遥测终端IC卡功能
	 * 
	 * @return
	 */
	public IMessage send31Message(String stcd, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x31);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 32://定值控制投入
	 * 
	 * @return
	 */
	public IMessage send32Message(String stcd, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x32);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 33://定值控制退出
	 * 
	 * @return
	 */
	public IMessage send33Message(String stcd, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x33);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	/**
	 * 34://定值量设定
	 * 
	 * @return
	 */
	public IMessage send34Message(String stcd, int value, int method, int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		byte[] newdata = SzyBuilder.newSZ(value, 10, 0);
		bodyDown.setAFN((byte) 0x34);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	public IMessage send61Message(String stcd,  int serial, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		if(serial>255){
			serial = 255;
		}
		
		bodyDown.setAFN((byte) 0x61);
		bodyDown.setData(ByteUtil.ubyteToBytes(serial));
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}
	
	
	public IMessage send90Message(String stcd,  int value, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		if(value!=1 && value!=2){
			value = 1;
		}
		
		bodyDown.setAFN((byte) 0x90);
		bodyDown.setData(ByteUtil.ubyteToBytes(value));
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}
	
	public IMessage send92Message(String stcd, int type, int value, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		byte[] newdata = ByteUtil.ubyteToBytes(value);// 将1到15转换为字节
		// 0000表示水泵，1111表示闸门
		if (type == 1) {
			// newdata[0] = (byte) ((newdata[0] << 4) | (byte) 0x0f);
			newdata[0] = (byte) (newdata[0] | (byte) 0xf0);
		}

		bodyDown.setAFN((byte) 0x92);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	public IMessage send93Message(String stcd, int type, int value, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		byte[] newdata = ByteUtil.ubyteToBytes(value);// 将1到15转换为字节
		// 0000表示水泵，1111表示闸门
		if (type == 1) {
			//newdata[0] = (byte) ((newdata[0] << 4) | (byte) 0x0f);
			newdata[0] = (byte) (newdata[0] | (byte) 0xf0);
		}

		bodyDown.setAFN((byte) 0x93);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}

	public IMessage send94Message(String stcd, int type, int value, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x94);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}
	
	public IMessage send95Message(String stcd,  int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		bodyDown.setAFN((byte) 0x95);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}
	
	public IMessage send96Message(String stcd, String pwd, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		if(StringUtils.isBlank(pwd)){
			pwd = "0";
		}
		pwd = StringUtils.leftPad(pwd,4,"0");
		
		byte[] pwdbyte = ByteUtil.str2Bcd(pwd);
		
		bodyDown.setAFN((byte) 0x96);
		bodyDown.setData(pwdbyte);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}
	
	
	public IMessage sendB2Message(String stcd, String startDate,String endDate, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		byte[] newdata = new byte[8];
		
		byte[] bstartDate = ByteUtil.str2Bcd(startDate);
		ArrayUtils.reverse(bstartDate);
		byte[] bendDate = ByteUtil.str2Bcd(endDate);
		ArrayUtils.reverse(bendDate);
		
		
		int pos = 0;
		// 设置水位上限值
		System.arraycopy(bstartDate, 0, newdata, pos, bstartDate.length);
		pos = pos + bstartDate.length;
		// 设置水位下限值
		System.arraycopy(bendDate, 0, newdata, pos, bendDate.length);

		
		bodyDown.setAFN((byte) 0xB2);
		bodyDown.setData(newdata);
		bodyDown.setPw(pw);
		bodyDown.setTp(tp);

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}
	
	

	public IMessage sendC0Message(String stcd, String mode, int method,
			int secret) {
		SzyMessageBody messageBody = new SzyMessageBody();
		int FUNC = 0;// 发送确认命令
		byte control = SzyBuilder.newControl(DIR, DIV, FCB, FUNC);
		messageBody.setControl(control);
		messageBody.setAddress(SzyBuilder.newStcd(stcd));

		BodyDown bodyDown = new BodyDown();
		byte[] tp = SzyBuilder.newTP(DELAY);
		byte[] pw = SzyBuilder.newPW(method, secret);

		byte[] newdata = new byte[1];
		newdata[0] = (byte)0x00;
//		int pos = 0;
//		// 设置水位上限值
//		System.arraycopy(bstartDate, 0, newdata, pos, bstartDate.length);
//		pos = pos + bstartDate.length;
//		// 设置水位下限值
//		System.arraycopy(bendDate, 0, newdata, pos, bendDate.length);

		
		bodyDown.setAFN((byte) 0xC0);
		bodyDown.setData(newdata); 

		messageBody.setUserData(bodyDown);
		return buildMessage(messageBody);
	}
	
	
	
	public static void main(String[] args) {
		DownCommand cmd = new DownCommand();
		// IMessage msg10 = cmd.send10Message("3201010100", "3201020200", 8,
		// 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg10));

		// IMessage msg50 = cmd.sendB0Message("3201040800");
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg50));

		// IMessage msg12 = cmd.send12Message("3201010100",(byte) 0x00, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg12));

		// IMessage msg15 = cmd.send15Message("3201040200", 100, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg15));

		// 68116830320104020016000200508038181322000716
		// IMessage msg16 = cmd.send16Message("3201040200", 200, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg16));

		// 水位表基值： 0.00m，上限值： 10.00m，下限值： 2.00m
		// IMessage msg17 = cmd.send17Message("3201040200", 0, 2.00, 10.00, 8,
		// 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg17));

		// 水压表上限值： 33.00Kpa，下限值： 2.00Kpa
		// IMessage msg18 = cmd.send18Message("3201040200", 33.00, 2.00, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg18));
		// -------------------------------------------------------------------------------------

		// byte[] viewItem = new byte[5];
		// viewItem[0] = (byte) 0x57;
		// byte[] d1 = SzyBuilder.newSZ(22.0, 8, 1);// 22.0
		// byte[] d2 = SzyBuilder.newSZ(11.0, 8, 2);// 22.0
		// byte[] d3 = SzyBuilder.newSZ(9, 8, 1);// 22.0
		// byte[] d4 = SzyBuilder.newSZ(8, 8, 0);// 22.0
		// byte[] d5 = SzyBuilder.newSZ(7, 8, 0);// 22.0
		//
		// byte[] newdata = new byte[d1.length + d2.length + d3.length +
		// d4.length
		// + d5.length];
		// int pos = 0;
		// // 设置水位基值
		// System.arraycopy(d1, 0, newdata, pos, d1.length);
		// pos = pos + d1.length;
		// // 设置水位下限值
		// System.arraycopy(d2, 0, newdata, pos, d2.length);
		// pos = pos + d2.length;
		// // 设置水位上限值
		// System.arraycopy(d3, 0, newdata, pos, d3.length);
		// pos = pos + d3.length;
		//
		// System.arraycopy(d4, 0, newdata, pos, d4.length);
		// pos = pos + d4.length;
		//
		// System.arraycopy(d5, 0, newdata, pos, d5.length);
		// System.out.println("d1>> " + d1.length);
		// System.out.println("d2>> " + d2.length);
		// System.out.println("d3>> " + d3.length);
		// System.out.println("d4>> " + d4.length);
		// System.out.println("d5>> " + d5.length);
		// 水温：22.0 ℃
		// pH值：11.00
		// 溶解氧： 9.0 mg/L
		// 电导率：8 us/cm
		// 浊度：7 度
		// IMessage msg19 = cmd.send19Message("3201040200", viewItem, newdata,
		// 8,
		// 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg19));
		// -------------------------------------------------------------------------------------
		// int[] data1B = new int[1];
		// data1B[0] = 2222;
		// IMessage msg1B = cmd.send1BMessage("3201040200", data1B, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg1B));

		// -------------------------------------------------------------------------------------
		// double[] data1F = new double[1];
		// data1F[0] = 22222.000;
		// IMessage msg1F = cmd.send1FMessage("3201040200", data1F, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg1F));
		//
		// System.out.println("msg>> "
		// + SzyBuilder.toHexString(cmd.sendQueryMessage("3201040200", 0,
		// (byte) 0x64)));

		// -------------------------------------------------------------------------------------
		// IMessage msg1C = cmd.send1CMessage("3201040200", 255, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg1C));
		//
		// //
		// -------------------------------------------------------------------------------------
		// IMessage msg30 = cmd.send30Message("3201040200", 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg30));

		// -------------------------------------------------------------------------------------
		// IMessage msg31 = cmd.send31Message("3201040200", 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg31));

		// //
		// -------------------------------------------------------------------------------------
		// IMessage msg34 = cmd.send34Message("3201040200", 1234, 8, 50);
		// System.out.println("msg>> " + SzyBuilder.toHexString(msg34));

		// -------------------------------------------------------------------------------------
//		IMessage msg92 = cmd.send92Message("3201040200", 1, 1, 8, 50);
//		System.out.println("msg>> " + SzyBuilder.toHexString(msg92));
//
//		
//		IMessage msgB0 = cmd.sendB0Message("3201040200" );
//		System.out.println("msg>> " + SzyBuilder.toHexString(msgB0));

		
		IMessage msgC0 = cmd.sendC0Message("3201010100" , "", 8, 50);
		System.out.println("msg>> " + SzyBuilder.toHexString(msgC0));
		
		
		System.out.println("msg>> "
				+ SzyBuilder.toHexString(cmd.sendQueryMessage("3201040200", 0,
						(byte) 0xB0)));
		// DecimalFormat df1 = new DecimalFormat("##.00");
		// System.out.println(df1.format(1234.56));
		// System.out.println(df1.format(1234.5));
		// System.out.println(df1.format(1234));
		// System.out.println(df1.format(123.4));
		// System.out.println(df1.format(1.2));
		// System.out.println(df1.format(0.12));
		int value = 3;
		if(value!=1 && value!=2){
			 System.out.println("1-------------");
		}

		if(value!=1 || value!=2){
			 System.out.println("2-------------");
		}
	}
}
