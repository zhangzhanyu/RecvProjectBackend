package cn.gov.mwr.szy206.data;

import java.util.Date;

/**
 * 
 * @ClassName: BaseData
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class BaseData {

	private String stcd;
	private String func;
	private Date viewtime;
	private Date writetime;
	private int amount;
	private int seq;
	private IData items;

	public String getStcd() {
		return stcd;
	}

	public void setStcd(String stcd) {
		this.stcd = stcd;
	}

	public String getFunc() {
		return func;
	}

	public void setFunc(String func) {
		this.func = func;
	}

	public Date getViewtime() {
		return viewtime;
	}

	public void setViewtime(Date viewtime) {
		this.viewtime = viewtime;
	}

	public Date getWritetime() {
		return writetime;
	}

	public void setWritetime(Date writetime) {
		this.writetime = writetime;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public IData getItems() {
		return items;
	}

	public void setItems(IData items) {
		this.items = items;
	}

}
