package cn.gov.mwr.szy206.data;

import java.io.Serializable;

public class Data1B implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -642122781332185032L;

	
	private int[] base;


	public int[] getBase() {
		return base;
	}


	public void setBase(int[] base) {
		this.base = base;
	}
	
	
}
