package cn.gov.mwr.szy206.data;

import java.io.Serializable;

public class Data1F implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6170579638022006870L;

	private double[] Q;

	public double[] getQ() {
		return Q;
	}

	public void setQ(double[] q) {
		Q = q;
	}

}
