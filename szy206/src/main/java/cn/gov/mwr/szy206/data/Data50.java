package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 设置遥测终端或中继站地址（AFN=10H）命令格式见表19。
 * 
 * 地址域A 为遥测终端或中继站旧地 址，数据域为新地址。
 * 
 * 数据域为5 字节
 * 
 * @ClassName: Data10H
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public class Data50 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1306370083465133767L;
	private String stcd;

	public String getStcd() {
		return stcd;
	}

	public void setStcd(String stcd) {
		this.stcd = stcd;
	}


}
