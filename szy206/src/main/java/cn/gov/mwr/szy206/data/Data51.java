package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 设置遥测终端或中继站时钟（AFN=11H）
 * 
 * 数据域为6 字节BCD码
 * 
 * @ClassName: Data10H
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public class Data51 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4771111044354250241L;

	public Data51() {
	}

	private String clock;

	public String getClock() {
		return clock;
	}

	public void setClock(String clock) {
		this.clock = clock;
	}

}
