package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 设置遥测终端或中继站时钟（AFN=12H）
 * 
 * 数据域为1 字节
 * 
 * 工作模式类 型=00B，设置遥测终端在兼容工作状态；
 * 
 * 工作模式类型=01H，设置遥测终端在自报工作状态；
 * 
 * 工作模式 类型=02H，设置遥测终端在查询/应答工作状态；
 * 
 * 工作模式类型=03H，遥测终端在调试/维修状态。
 * 
 * @ClassName: Data10H
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public class Data52 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5595619167827256273L;

	public Data52() {
	}

	private String mode;
	private String text;

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
