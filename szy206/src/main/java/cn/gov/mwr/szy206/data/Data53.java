package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * * 设置遥测终端的数据自报种类及时间间隔（AFN=A1H）命令格式见表32
 * 
 * 数据域为30 字节
 * 
 * @ClassName: DataA1H
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data53 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3357796982159009515L;

	public Data53() {
	}

	private int[] interval;

	public int[] getInterval() {
		return interval;
	}

	public void setInterval(int[] interval) {
		this.interval = interval;
	}

}
