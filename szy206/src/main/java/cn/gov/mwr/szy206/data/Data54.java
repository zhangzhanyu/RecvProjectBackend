package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 设置遥测站需查询的实时数据种类
 * 
 * 数据域为2 字节
 * 
 * @ClassName: DataA1H
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data54 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9050098443796735504L;

	public Data54() {
	}

	private int[] interval;

	public int[] getInterval() {
		return interval;
	}

	public void setInterval(int[] interval) {
		this.interval = interval;
	}

}
