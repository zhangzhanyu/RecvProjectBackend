package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 遥测终端最近成功充值量和现有剩余水量（AFN=55H）
 * 
 * @ClassName: Data55H
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data55 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4007220022880595715L;

	private int pay;
	private int remain;

	public int getPay() {
		return pay;
	}

	public void setPay(int pay) {
		this.pay = pay;
	}

	public int getRemain() {
		return remain;
	}

	public void setRemain(int remain) {
		this.remain = remain;
	}

}
