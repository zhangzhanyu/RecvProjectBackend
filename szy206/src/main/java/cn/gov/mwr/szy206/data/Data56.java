package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 查询遥测终端的剩余水量和报警值（AFN=56H）
 * 
 * @ClassName: Data56H
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data56 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2588738034495444768L;

	private int reamin;
	private int alarm;

	public int getReamin() {
		return reamin;
	}

	public void setReamin(int reamin) {
		this.reamin = reamin;
	}

	public int getAlarm() {
		return alarm;
	}

	public void setAlarm(int alarm) {
		this.alarm = alarm;
	}

}
