package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 查询遥测终端的水位基值、水位上下限（AFN=57H）
 * 
 * @ClassName: Data57H
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data57 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5108844034715349771L;

	private IDataItem[] items;

	public IDataItem[] getItems() {
		return items;
	}

	public void setItems(IDataItem[] items) {
		this.items = items;
	}

	public void addItem(IDataItem item){
		
	}
	
}
