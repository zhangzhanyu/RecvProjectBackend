package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 查询遥测终端的水位上下限（AFN=58H）
 * 
 * @ClassName: Data57H
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data58Item implements IDataItem, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5108844034715349771L;

	private double toplimit;
	private double lowlimit;

	public double getToplimit() {
		return toplimit;
	}

	public void setToplimit(double toplimit) {
		this.toplimit = toplimit;
	}

	public double getLowlimit() {
		return lowlimit;
	}

	public void setLowlimit(double lowlimit) {
		this.lowlimit = lowlimit;
	}

}
