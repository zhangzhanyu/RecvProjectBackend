package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 
 * @ClassName: Data59H
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data59 implements IData, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4247233046508595279L;

	private double[] sz;

	public double[] getSz() {
		return sz;
	}

	public void setSz(double[] sz) {
		this.sz = sz;
	}

	// private double d0;
	// private double d1;
	// private double d2;
	// private double d3;
	// private double d4;
	// private double d5;
	// private double d6;
	// private double d7;
	// private double d8;
	// private double d9;
	// private double d10;
	// private double d11;
	// private double d12;
	// private double d13;
	// private double d14;
	// private double d15;
	// private double d16;
	// private double d17;
	// private double d18;
	// private double d19;
	// private double d20;
	// private double d21;
	// private double d22;
	// private double d23;
	// private double d24;
	// private double d25;
	// private double d26;
	// private double d27;
	// private double d28;
	// private double d29;
	// private double d30;
	// private double d31;
	// private double d32;
	// private double d33;
	// private double d34;
	// private double d35;
	// private double d36;
	// private double d37;
	// private double d38;
	// private double d39;

}
