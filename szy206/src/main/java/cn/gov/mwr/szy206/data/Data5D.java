package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * ERC1 数据初始化记录 2
 * 
 * ERC2 参数变更记录 2
 * 
 * ERC3 状态量变位记录 2
 * 
 * ERC4 仪表故障记录 2
 * 
 * ERC5 密码错误记录 2
 * 
 * ERC6 终端故障记录 2
 * 
 * ERC7 交流失电记录 2
 * 
 * ERC8 蓄电池电压低告警记录 2
 * 
 * ERC9 终端箱非法打开记录 2
 * 
 * ERC10 水泵故障记录 2
 * 
 * ERC11 剩余水量越限告警记录 2
 * 
 * ERC12 水位超限告警记录 2
 * 
 * ERC13 水压超限告警记录 2
 * 
 * ERC14 水质参数超限告警记录 2
 * 
 * ERC15 数据出错记录 2
 * 
 * ERC16 发报文记录 2
 * 
 * ERC17 收报文记录 2
 * 
 * ERC18 发报文出错记录 2
 * 
 * @ClassName: Data5D
 * @Description: TODO
 * @author lipujun
 * @date Jun 8, 2013
 * 
 */
public class Data5D implements IData, Serializable {

	private int ERC1;
	private int ERC2;
	private int ERC3;
	private int ERC4;
	private int ERC5;
	private int ERC6;
	private int ERC7;
	private int ERC8;
	private int ERC9;
	private int ERC10;
	private int ERC11;
	private int ERC12;
	private int ERC13;
	private int ERC14;
	private int ERC15;
	private int ERC16;
	private int ERC17;
	private int ERC18;

	public int getERC1() {
		return ERC1;
	}

	public void setERC1(int eRC1) {
		ERC1 = eRC1;
	}

	public int getERC2() {
		return ERC2;
	}

	public void setERC2(int eRC2) {
		ERC2 = eRC2;
	}

	public int getERC3() {
		return ERC3;
	}

	public void setERC3(int eRC3) {
		ERC3 = eRC3;
	}

	public int getERC4() {
		return ERC4;
	}

	public void setERC4(int eRC4) {
		ERC4 = eRC4;
	}

	public int getERC5() {
		return ERC5;
	}

	public void setERC5(int eRC5) {
		ERC5 = eRC5;
	}

	public int getERC6() {
		return ERC6;
	}

	public void setERC6(int eRC6) {
		ERC6 = eRC6;
	}

	public int getERC7() {
		return ERC7;
	}

	public void setERC7(int eRC7) {
		ERC7 = eRC7;
	}

	public int getERC8() {
		return ERC8;
	}

	public void setERC8(int eRC8) {
		ERC8 = eRC8;
	}

	public int getERC9() {
		return ERC9;
	}

	public void setERC9(int eRC9) {
		ERC9 = eRC9;
	}

	public int getERC10() {
		return ERC10;
	}

	public void setERC10(int eRC10) {
		ERC10 = eRC10;
	}

	public int getERC11() {
		return ERC11;
	}

	public void setERC11(int eRC11) {
		ERC11 = eRC11;
	}

	public int getERC12() {
		return ERC12;
	}

	public void setERC12(int eRC12) {
		ERC12 = eRC12;
	}

	public int getERC13() {
		return ERC13;
	}

	public void setERC13(int eRC13) {
		ERC13 = eRC13;
	}

	public int getERC14() {
		return ERC14;
	}

	public void setERC14(int eRC14) {
		ERC14 = eRC14;
	}

	public int getERC15() {
		return ERC15;
	}

	public void setERC15(int eRC15) {
		ERC15 = eRC15;
	}

	public int getERC16() {
		return ERC16;
	}

	public void setERC16(int eRC16) {
		ERC16 = eRC16;
	}

	public int getERC17() {
		return ERC17;
	}

	public void setERC17(int eRC17) {
		ERC17 = eRC17;
	}

	public int getERC18() {
		return ERC18;
	}

	public void setERC18(int eRC18) {
		ERC18 = eRC18;
	}

}
