package cn.gov.mwr.szy206.data;

import java.io.Serializable;

/**
 * 修改遥测终端密码（AFN=96H）
 * 
 * 数据域为2 字节
 * 
 * @ClassName: Data10H
 * @Description: TODO
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public class Data96 implements IData, Serializable {

	private String secret;
	private String pwd;

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
