package cn.gov.mwr.szy206.data;

import java.io.Serializable;

public class DataC0 implements IData, Serializable {

	private int type;

	private double[] data;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public double[] getData() {
		return data;
	}

	public void setData(double[] data) {
		this.data = data;
	}

}
