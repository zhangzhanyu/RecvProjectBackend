package cn.gov.mwr.szy206.data;

import java.io.Serializable;

public class DataC0D1 implements IData, Serializable {

	public int type;

	public double p;

	public double getP() {
		return p;
	}

	public void setP(double p) {
		this.p = p;
	}

}
