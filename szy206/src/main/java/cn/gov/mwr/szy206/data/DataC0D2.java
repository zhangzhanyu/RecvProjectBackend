package cn.gov.mwr.szy206.data;

import java.io.Serializable;

public class DataC0D2 implements IData, Serializable {

	public int type;

	public double[] z;

	public double[] getZ() {
		return z;
	}

	public void setZ(double[] z) {
		this.z = z;
	}

}
