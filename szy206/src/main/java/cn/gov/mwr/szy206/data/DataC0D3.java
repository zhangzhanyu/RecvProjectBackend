package cn.gov.mwr.szy206.data;

import java.io.Serializable;

public class DataC0D3 implements IData, Serializable {

	public int type;

	public double[] q;

	public double[] getQ() {
		return q;
	}

	public void setQ(double[] q) {
		this.q = q;
	}

}
