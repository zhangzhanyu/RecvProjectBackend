package cn.gov.mwr.szy206.data;

import java.io.Serializable;

public class DataC0D4 implements IData, Serializable {

	public int type;

	public double[] s;

	public double[] getS() {
		return s;
	}

	public void setS(double[] s) {
		this.s = s;
	}

}
