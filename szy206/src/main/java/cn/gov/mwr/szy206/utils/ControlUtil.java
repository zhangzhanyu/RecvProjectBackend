package cn.gov.mwr.szy206.utils;


/**
 * 5.1.4.2 控制域C ，针对控制域的解析程序
 * 
 * @ClassName: ControlUtil
 * @Description: TODO
 *               ------------------------------------------------------------
 *               |___ D7____ |_____ D6_____ |_____D5～D4 ___|___D3～D0___|
 *               |_传输方向位DIR_|__拆分标志位DIV__|___帧计数位FCB____|___ 功能码___ |
 *               --------------------------------------------------
 * 
 * @author lipujun
 * @date Apr 9, 2013
 * 
 */
public class ControlUtil {

	/**
	 * 获取传输方向位DIR
	 * 
	 * 1) DIR=0，表示此帧报文是由中心站发出的下行报文；
	 * 
	 * 2) DIR=1，表示此帧报文是由终端发出的上行报文；
	 * 
	 * @return
	 */
	public static int[] getDIR(byte C) {

		String binstr = ByteUtil.toBinaryString(C);

		// String DIR = binstr.substring(0, 1);
		// String DIV = binstr.substring(1, 2);
		// String FCB = binstr.substring(2, 4);
		// String FUNC = binstr.substring(4, 8);
		byte DIR = (byte) ((C & 0x80) >> 7);
		byte DIV = (byte) ((C & 0x40) >> 6);
		byte FCB = (byte) ((C & 0x30) >> 4);
		byte FUNC = (byte) (C & 0x0F);

		System.out.println(">> binstr " + binstr + " DIR " + DIR + " DIV "
				+ DIV + " FCB " + FCB + " FUNC " + FUNC);

		return new int[] { DIR, DIV, FCB, FUNC };
	}

	/**
	 * 
	 * 获取报文的帧数，0为单帧；1为多帧
	 * 
	 * 1) DIV =1，表示此报文已被拆分为若干帧，接收后需要拼接。
	 * 
	 * 此时控制域C 后增加一个字节，为拆分帧计数DIVS，采用BIN 倒计数（255～1），1 时表示最后一帧。
	 * 
	 * 启动站发送时自动加上发送，从动站返回帧时对应加上确认；
	 * 
	 * 2) DIV =0，表示此帧报文为单帧。
	 * 
	 * @return
	 */
	public static int getDIV() {

		return 0;
	}

	/**
	 * 1) FCB 表示每个站连续的发送/确认或者请求/响应的变化位。
	 * 
	 * FCB 位用来防止信息传输的丢失和重复；
	 * 
	 * 2) 启动站向同一从动站传输新的发送/确认或请求/响应传输时，启动站将设置FCB 值，若超 时未收到从动站的报文，或接收出现差错，则启动站将FCB
	 * 减1，重复原来的发送/确认 或者请求/响应，直到FCB 值为0，表示本次传输服务失败；
	 * 
	 * 3) 从动站收到启动站FCB 值不为0 的报文并按照要求确认或响应时，返回相应的FCB 值。
	 * 
	 * @return
	 */
	public static int getFCB() {

		return 1;
	}

	/**
	 * 获取功能码，需根据上下行的标识来判定功能码
	 * 
	 * 0 确认认可
	 * 
	 * 1 ：雨量参数 ,2： 水位参数
	 * 
	 * 3： 流量（水量）参数, 4 ：流速参数
	 * 
	 * 5： 闸位参数,6： 功率参数
	 * 
	 * 7 ：气压参数,8 ：风速参数
	 * 
	 * 9 ：水温参数,10： 水质参数
	 * 
	 * 11： 土壤含水率参数,12： 蒸发量参数
	 * 
	 * 13： 报警或状态参数,14： 统计雨量
	 * 
	 * 15： 水压参数
	 * 
	 * @return
	 */
	public static int getFUNC() {

		return 0;
	}

	public static void main(String[] args) {

		for (int i = 0; i < 255; i++) {
			ControlUtil.getDIR((byte) i);
		}

	}
}
