package cn.gov.mwr.szy206.utils;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	private static final DateUtil instance = new DateUtil();
	private Calendar claendar;
	private DateFormat dateFormat;
	private DateFormat timeFormat;
	private DateFormat datetimeFormat;

	private DateUtil() {
	}

	public static DateUtil getInstance() {
		return instance;
	}

	private Calendar getCalendar() {
		if (this.claendar == null) {
			this.claendar = Calendar.getInstance();
		}
		this.claendar.clear();
		return this.claendar;
	}

	private DateFormat getDateFormat() {
		if (this.dateFormat == null) {
			this.dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			this.dateFormat.getCalendar().setLenient(false);
		}
		return this.dateFormat;
	}

	private DateFormat getTimeFormat() {
		if (this.timeFormat == null) {
			this.timeFormat = new SimpleDateFormat("HH:mm:ss");
			this.timeFormat.getCalendar().setLenient(false);
		}
		return this.timeFormat;
	}

	private DateFormat getDatetimeFormat() {
		if (this.datetimeFormat == null) {
			this.datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			this.datetimeFormat.getCalendar().setLenient(false);
		}
		return this.datetimeFormat;
	}

	public static Date addYears(Date date, int amount) {
		return add(date, 1, amount);
	}

	public static Date addMonths(Date date, int amount) {
		return add(date, 2, amount);
	}

	public static Date addWeeks(Date date, int amount) {
		return add(date, 3, amount);
	}

	public static Date addDays(Date date, int amount) {
		return add(date, 5, amount);
	}

	public static Date addHours(Date date, int amount) {
		return add(date, 11, amount);
	}

	public static Date addMinutes(Date date, int amount) {
		return add(date, 12, amount);
	}

	public static Date addSeconds(Date date, int amount) {
		return add(date, 13, amount);
	}

	public static Date addMilliseconds(Date date, int amount) {
		return add(date, 14, amount);
	}

	private static Date add(Date date, int calendarField, int amount) {
		if (date == null) {
			throw new IllegalArgumentException("The date must not be null");
		} else {
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(calendarField, amount);
			return c.getTime();
		}
	}

	public static void main(String args[]) {

		Date date = new Date();

		System.out.println(DateUtil.getInstance().year(date));
		System.out.println(DateUtil.getInstance().month(date));
		System.out.println(DateUtil.getInstance().day(date));
		System.out.println(DateUtil.getInstance().hour(date));
		System.out.println(DateUtil.getInstance().minute(date));
		System.out.println(DateUtil.getInstance().second(date));

		System.out.println(DateUtil.getInstance().monthBegin(date));

		System.out.println(DateUtil.getInstance().week(date));
		System.out.println(DateUtil.getInstance().weekBegin(date));
		System.out.println(DateUtil.getInstance().weekEnd(date));

		System.out.println(DateUtil.getInstance().daysInYear(date));
		System.out.println(DateUtil.getInstance().daysInMonth(date));
	}

	public int day(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.get(5);
	}

	public int daysInMonth(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.getActualMaximum(5);
	}

	public int daysInYear(int paramInt) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).set(6, 1);
		localCalendar.set(1, paramInt);
		return localCalendar.getActualMaximum(6);
	}

	public int daysInYear(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.getActualMaximum(6);
	}

	public String formatDate(java.util.Date paramDate) {
		return getDateFormat().format(paramDate);
	}

	public String formatTime(java.util.Date paramDate) {
		return getTimeFormat().format(paramDate);
	}

	public String formatTimestamp(java.util.Date paramDate) {
		return getDatetimeFormat().format(paramDate);
	}

	public int hour(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.get(11);
	}

	public java.util.Date preDay(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.add(5, -1);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date nextDay(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.add(5, 1);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date preMonth(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.add(2, -1);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date nextMonth(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.add(2, 1);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date preYear(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.add(1, -1);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date nextYear(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.add(1, 1);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public int minute(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.get(12);
	}

	public int month(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.get(2) + 1;
	}

	public java.util.Date monthBegin(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.set(5, 1);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date monthEnd(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.set(5, localCalendar.getActualMaximum(5));
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date parseDate(String paramString) throws ParseException {
		if (paramString == null) {
			return null;
		}
		return new java.sql.Date(getDateFormat().parse(paramString).getTime());
	}

	public java.util.Date parseTime(String paramString) throws ParseException {
		if (paramString == null) {
			return null;
		}
		return new Time(getTimeFormat().parse(paramString).getTime());
	}

	public java.util.Date parseTimestamp(String paramString)
			throws ParseException {
		if (paramString == null) {
			return null;
		}
		return new Timestamp(getDatetimeFormat().parse(paramString).getTime());
	}

	public java.util.Date quaterBegin(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		int i = localCalendar.get(2) / 3 * 3;
		localCalendar.set(5, 1);
		localCalendar.set(2, i);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date quaterEnd(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		int i = localCalendar.get(2) / 3 * 3 + 2;
		localCalendar.set(5, 1);
		localCalendar.set(2, i);
		localCalendar.set(5, localCalendar.getActualMaximum(5));
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public int second(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.get(13);
	}

	public java.util.Date toDate(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date toTime(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.set(1, 1970);
		localCalendar.set(2, 0);
		localCalendar.set(5, 1);
		localCalendar.set(14, 0);
		return new Time(localCalendar.getTimeInMillis());
	}

	public static int week(java.util.Date paramDate) {
		Calendar localCalendar = Calendar.getInstance();
		int dayForWeek;
		localCalendar.setTime(paramDate);
		if (localCalendar.get(Calendar.DAY_OF_WEEK) == 1) {
			dayForWeek = 7;
		} else {
			dayForWeek = localCalendar.get(Calendar.DAY_OF_WEEK) - 1;
		}
		return dayForWeek;
	}

	public java.util.Date weekBegin(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.set(7, localCalendar.getActualMinimum(7));
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		localCalendar.add(Calendar.DATE, 1);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public java.util.Date weekEnd(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		localCalendar.set(7, localCalendar.getActualMaximum(7));
		localCalendar.set(11, 0);
		localCalendar.set(12, 0);
		localCalendar.set(13, 0);
		localCalendar.set(14, 0);
		localCalendar.add(Calendar.DATE, 1);
		return new java.sql.Date(localCalendar.getTimeInMillis());
	}

	public int year(java.util.Date paramDate) {
		Calendar localCalendar;
		(localCalendar = getCalendar()).setTime(paramDate);
		return localCalendar.get(1);
	}

}
