package com.godenwater.yanyu;

import java.io.Serializable;

public abstract class AbstractBody implements IMessageBody, Serializable {// 

	/**
	 * 1、发报时间
	 * 
	 * 根据上行报文和下行报文，发报时间的取值范围不同
	 * 
	 * 取值范围：上行报文是5个字节，下行报文是6字节的BCD编码，YYMMDDHHmmSS
	 */
	public String sendDate;

	/**
	 * 记录原始消息体
	 */
	public byte[] content;

	public void setContents(byte[] content) {
		this.content = content;
	}

	public int getLength() {
		if (this.content != null) {
			return this.content.length;
		} else {
			return 0;
		}
	}
	
	public byte[] getContent(){
		return this.content;
	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

}
