package com.godenwater.yanyu;

/**
 * 报文头接口，在此定义了除报文之外协议内容
* @ClassName: IMessageHeader 
* @Description: TODO
* @author lipujun 
* @date Mar 11, 2013 
* @see HexHeader,AscHeader
 */
public interface IMessageHeader {

	/**
	 * 1、帧起始符
	 * 
	 * @return
	 */
	public byte[] getStartBit();

	public void setStartBit(byte[] startBit);

	public int getStartBitLen();

	/**
	 * 2、中心站地址
	 * 
	 * @return
	 */
	public byte[] getCenterAddr();

	public void setCenterAddr(byte[] centerAddr);

	public int getCenterAddrLen();

	/**
	 * 3、遥测站地址
	 * 
	 * @return
	 */
	public byte[] getStationAddr();

	public void setStationAddr(byte[] stationAddr);

	public int getStationAddrLen();


	/**
	 * 4、功能码
	 * 
	 * @return
	 */
	public byte[] getFuncCode();

	public void setFuncCode(byte[] funcCode);

	public int getFuncCodeLen();

	/**
	 * 5、报文长度
	 * 
	 * @return
	 */
	public byte[] getBodySize();

	public void setBodySize(byte[] bodyLength);

	public int getBodySizeLen();
 
	public int getLength();

}
