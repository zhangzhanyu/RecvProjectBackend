package com.godenwater.yanyu;

public class MessageType {

	public static byte H0E = 0X0E;// 特征为0Eh，表示正点发报。
	public static byte H0B = 0X0B;// 特征为0Bh，表示雨量加报。
	public static byte H09 = 0X09;// 特征为09h，表示水位加报。
	public static byte H0C = 0X0C;// 特征为0Ch，表示测试信息。
	public static byte H08 = 0X08;// 特征为08h，表示人工置数报

	public static byte H03 = 0X03;// 03H为写配置命令特征码，
	public static byte H05 = 0X05;// 05H为读配置命令特征码。
	
	public static byte H0A = 0X0A;// 0AH表示读取雨量数据，
	public static byte H06 = 0X06;// 06H表示读取水位数
	
}
