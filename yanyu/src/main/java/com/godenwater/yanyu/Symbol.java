package com.godenwater.yanyu;

public class Symbol {

	/**
	 * 帧起始 ---7EH，表示HEX编码报文帧起始
	 */
	public final static byte SOH = 0x7E;


	/**
	 * 03H,报文结束，后续无报文
	 * 
	 * 作为报文结束符，表示传输完成等待退出通信
	 */
	public final static byte ETX = 0x03;

	/**
	 * 17H 报文 结束，后续有报文
	 * 
	 * 在报文分包传输时作为结束符，表示未完成，不可退出通信
	 */
	public final static byte ETB = 0x17;

	/**
	 * 05H,询问
	 * 
	 * 作为下行查询及控制命令帧的报文结束符。
	 */
	public final static byte ENQ = 0x05;

	/**
	 * 04H,传输结束，退出
	 * 
	 * 作为传输结束确认帧报文符，表示可以退出通信。
	 */
	public final static byte EOT = 0x04;

	/**
	 * 06H,肯定确认，继续发送
	 * 
	 * 作为有后续报文帧的“确认”结束符。
	 */
	public final static byte ACK = 0x06;

	/**
	 * 15H,否定应答，反馈重发
	 * 
	 * 用于要求对方重发某数据包的报文结束符。
	 */
	public final static byte NAK = 0x15;

	/**
	 * 1BH,传输结束，终端保持在线
	 * 
	 * =在下行确认帧代替EOT 作为报文结束符，要求终端在线。
	 * 
	 * 保持在线10分钟内若没有接收到中心站命令，终端退回原先设定的工作状态
	 */
	public final static byte ESC = 0x1B;

	//表示属于上行报文
	public final static byte UP = 0X00;
	
	//表示必于下行报文
	public final static byte DOWN = 0X01;
	
}
