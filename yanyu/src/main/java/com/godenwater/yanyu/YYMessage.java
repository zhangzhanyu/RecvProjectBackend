package com.godenwater.yanyu;

import java.io.Serializable;

/**
 * 燕禹报文定义
 * @author admin
 *
 */
public class YYMessage implements IMessage, Serializable  {

	public IMessageHeader header;

	public IMessageBody body;

	public byte eof;

	public byte[] crc;

	public void setHeader(IMessageHeader header) {
		this.header = header;
	}

	public IMessageHeader getHeader() {
		return this.header;
	}

	public void setBody(IMessageBody body) {
		this.body = body;
	}

	public IMessageBody getBody() {
		return this.body;
	}

	public void setEOF(byte eof) {
		this.eof = eof;
	}

	public byte getEOF() {
		return this.eof;
	}

	public void setCRC(byte[] crc) {
		this.crc = crc;
	}

	public byte[] getCRC() {
		return this.crc;
	}
	
}
