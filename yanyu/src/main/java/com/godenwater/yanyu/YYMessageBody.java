package com.godenwater.yanyu;

public class YYMessageBody implements IMessageBody{

	private byte[] TM ;
	
	
	
	/**
	 * 记录原始消息体
	 */
	public byte[] content;
	
	public void setContents(byte[] content) {
		this.content = content;
	}

	public int getLength() {
		if (this.content != null) {
			return this.content.length;
		} else {
			return 0;
		}
	}
	
	public byte[] getContent(){
		return this.content;
	}
	
}
