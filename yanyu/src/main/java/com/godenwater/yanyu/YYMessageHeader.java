package com.godenwater.yanyu;

import java.io.Serializable;

public class YYMessageHeader implements IMessageHeader, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4419974117500328672L;

	public static int START_BIT_LENGTH = 1;
	public static int CENTER_ADDR_LENGTH = 1;
	public static int STATION_ADDR_LENGTH = 1;
	public static int FUNC_CODE_LENGTH = 1;
	public static int BODY_SIZE_LENGTH = 1;

	public byte[] startBit;// 7E
	public byte[] centerAddr;// 目的地址
	public byte[] stationAddr;// 源地址
	public byte[] funcCode;// 特征
	public byte[] bodySize; // 长度

	public byte[] getStartBit() {
		return startBit;
	}

	public void setStartBit(byte[] startBit) {
		this.startBit = startBit;
	}

	public byte[] getCenterAddr() {
		return centerAddr;
	}

	public void setCenterAddr(byte[] centerAddr) {
		this.centerAddr = centerAddr;
	}

	public byte[] getStationAddr() {
		return stationAddr;
	}

	public void setStationAddr(byte[] stationAddr) {
		this.stationAddr = stationAddr;
	}

	public byte[] getFuncCode() {
		return funcCode;
	}

	public void setFuncCode(byte[] funcCode) {
		this.funcCode = funcCode;
	}

	public byte[] getBodySize() {
		return bodySize;
	}

	public void setBodySize(byte[] bodySize) {
		this.bodySize = bodySize;
	}

	public int getStartBitLen() {
		return START_BIT_LENGTH;
	}

	public int getCenterAddrLen() {
		return CENTER_ADDR_LENGTH;
	}

	public int getStationAddrLen() {
		return STATION_ADDR_LENGTH;
	}

	public int getFuncCodeLen() {
		return FUNC_CODE_LENGTH;
	}

	public int getBodySizeLen() {
		return BODY_SIZE_LENGTH;
	}

	public int getLength() {
		return START_BIT_LENGTH + CENTER_ADDR_LENGTH + STATION_ADDR_LENGTH
				+ FUNC_CODE_LENGTH + BODY_SIZE_LENGTH;
	}

}
