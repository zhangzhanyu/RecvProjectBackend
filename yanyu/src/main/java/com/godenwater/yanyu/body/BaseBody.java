package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.IMessageBody;

public class BaseBody implements IMessageBody, Serializable {// 
	/**
	 * 记录原始消息体
	 */
	public byte[] content;

	public void setContents(byte[] content) {
		this.content = content;
	}

	public int getLength() {
		if (this.content != null) {
			return this.content.length;
		} else {
			return 0;
		}
	}
	
	public byte[] getContent(){
		return this.content;
	}
 
}
