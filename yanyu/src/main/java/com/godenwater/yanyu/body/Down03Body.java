package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.IMessageBody;
import com.godenwater.yanyu.utils.ByteUtil;

/**
 * 
 * 05H为读配置命令特征码。
 * 
 * @ClassName: Down05Body
 * @Description: 此类描述05H为读配置命令特征码。
 * 
 *               《配置参数》： 时段(1) 雨量加报阈值1(1) 日起始时间(1) 加报水位(2) 加报水位以 上变化量(1)
 *               加报水位以下变化量(1) 警戒水位(2) 警戒水位以上发报 时间间隔(1) 中心站号（2） 中心手机号（6）
 *               中心电话号码（6） 雨量加报阈值2（1）
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Down03Body implements IMessageBody, Serializable {

	private String sendDate;// 时间

	/**
	 * 各内容项
	 */
	// 时段(1) 雨量加报阈值1(1) 日起始时间(1) 加报水位(2) 加报水位以 上变化量(1)
	private byte[] F1 = new byte[1];// 时段(1)

	private byte[] F2 = new byte[1];// 雨量加报阈值1(1)

	private byte[] F3 = new byte[1];// 日起始时间(1)

	private byte[] F4 = new byte[2];// 加报水位(2)

	private byte[] F5 = new byte[1];// 加报水位以 上变化量(1)

	// 加报水位以下变化量(1) 警戒水位(2) 警戒水位以上发报 时间间隔(1) 中心站号（2） 中心手机号（6）
	private byte[] F6 = new byte[1];// 加报水位以下变化量(1)

	private byte[] F7 = new byte[2];// 警戒水位(2)

	private byte[] F8 = new byte[1];// 警戒水位以上发报 时间间隔(1)

	private byte[] F9 = new byte[2];// 中心站号（2）

	private byte[] F10 = new byte[6];// 中心手机号（6）

	// 中心电话号码（6） 雨量加报阈值2（1）
	private byte[] F11 = new byte[6];// 中心电话号码（6）

	private byte[] F12 = new byte[1];// 雨量加报阈值2（1）

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public byte[] getF1() {
		return F1;
	}

	public void setF1(byte[] f1) {
		F1 = f1;
	}

	public void setF1(String f1) {
		F1 = ByteUtil.str2Bcd(f1);
	}

	public byte[] getF2() {
		return F2;
	}

	public void setF2(byte[] f2) {
		F2 = f2;
	}

	public void setF2(String f2) {
		F2 = ByteUtil.str2Bcd(f2);
	}

	public byte[] getF3() {
		return F3;
	}

	public void setF3(byte[] f3) {
		F3 = f3;
	}

	public void setF3(String f3) {
		F3 = ByteUtil.str2Bcd(f3);
	}

	public byte[] getF4() {
		return F4;
	}

	public void setF4(byte[] f4) {
		F4 = f4;
	}

	public void setF4(String f4) {
		F4 = ByteUtil.str2Bcd(f4);
	}

	public byte[] getF5() {
		return F5;
	}

	public void setF5(byte[] f5) {
		F5 = f5;
	}

	public void setF5(String f5) {
		F5 = ByteUtil.str2Bcd(f5);
	}

	public byte[] getF6() {
		return F6;
	}

	public void setF6(byte[] f6) {
		F6 = f6;
	}

	public void setF6(String f6) {
		F6 = ByteUtil.str2Bcd(f6);
	}

	public byte[] getF7() {
		return F7;
	}

	public void setF7(byte[] f7) {
		F7 = f7;
	}

	public void setF7(String f7) {
		F7 = ByteUtil.str2Bcd(f7);
	}

	public byte[] getF8() {
		return F8;
	}

	public void setF8(byte[] f8) {
		F8 = f8;
	}

	public void setF8(String f8) {
		F8 = ByteUtil.str2Bcd(f8);
	}

	public byte[] getF9() {
		return F9;
	}

	public void setF9(byte[] f9) {
		F9 = f9;
	}

	public void setF9(String f9) {
		F9 = ByteUtil.str2Bcd(f9);
	}

	public byte[] getF10() {
		return F10;
	}

	public void setF10(byte[] f10) {
		F10 = f10;
	}

	public void setF10(String f10) {
		F10 = ByteUtil.str2Bcd(f10);
	}

	public byte[] getF11() {
		return F11;
	}

	public void setF11(byte[] f11) {
		F11 = f11;
	}

	public void setF11(String f11) {
		F11 = ByteUtil.str2Bcd(f11);
	}

	public byte[] getF12() {
		return F12;
	}

	public void setF12(byte[] f12) {
		F12 = f12;
	}

	public void setF12(String f12) {
		F12 = ByteUtil.str2Bcd(f12);
	}

	public void setContents(byte[] content) {

	}
 
	public byte[] getContent() {

		int total = getLength();
		byte[] content = new byte[total];

		System.arraycopy(ByteUtil.str2Bcd(sendDate), 0, content, 0, 6);
		// 转换为6字节的BCD码
		int pos = 6;
		System.arraycopy(F1, 0, content, pos, F1.length);
		pos = pos + F1.length;

		System.arraycopy(F2, 0, content, pos, F2.length);
		pos = pos + F2.length;

		System.arraycopy(F3, 0, content, pos, F3.length);
		pos = pos + F3.length;

		System.arraycopy(F4, 0, content, pos, F4.length);
		pos = pos + F4.length;

		System.arraycopy(F5, 0, content, pos, F5.length);
		pos = pos + F5.length;

		System.arraycopy(F6, 0, content, pos, F6.length);
		pos = pos + F6.length;

		System.arraycopy(F7, 0, content, pos, F7.length);
		pos = pos + F7.length;

		System.arraycopy(F8, 0, content, pos, F8.length);
		pos = pos + F8.length;

		System.arraycopy(F9, 0, content, pos, F9.length);
		pos = pos + F9.length;

		System.arraycopy(F10, 0, content, pos, F10.length);
		pos = pos + F10.length;

		System.arraycopy(F11, 0, content, pos, F11.length);
		pos = pos + F11.length;

		System.arraycopy(F12, 0, content, pos, F12.length);

		return content;
	}

	public int getLength() {
		return 6 + F1.length + F2.length + F3.length + F4.length + F5.length
				+ F6.length + F7.length + F8.length + F9.length + F10.length
				+ F11.length + F12.length;
	}

}
