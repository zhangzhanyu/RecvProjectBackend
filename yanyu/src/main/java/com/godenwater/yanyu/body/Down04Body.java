package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.IMessageBody;

/**
 * 
 * 清固态存储器数据
 * 
 * @ClassName: Down04Body
 * @Description: 此类描述04H为清固态存储器数据命令
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Down04Body implements IMessageBody, Serializable {

	public String CLR = "LCLR";// 清除命令

	public int flag = 1;

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public void setContents(byte[] content) {

	}

	public byte[] getContent() {
		int total = getLength();
		byte[] content = new byte[total];

		int pos = 0;
		System.arraycopy(CLR.getBytes(), pos, content, 0, 4);
		pos = pos + 4;
		if (flag == 2) {
			System.arraycopy(new byte[] { (byte) 0x02 }, 0, content, pos, 1);
		} else if (flag == 3) {
			System.arraycopy(new byte[] { (byte) 0x03 }, 0, content, pos, 1);
		} else {
			System.arraycopy(new byte[] { (byte) 0x01 }, 0, content, pos, 1);
		}

		return content;
	}

	public int getLength() {
		return 5;
	}

}
