package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.IMessageBody;
import com.godenwater.yanyu.utils.ByteUtil;

/**
 * 
 * 05H为读配置命令特征码。
 * 
 * @ClassName: DownBase
 * @Description: 此类描述05H为读配置命令特征码。
 * 
 *               1、发报时间
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Down05Body  implements IMessageBody, Serializable {
	public String startDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setContents(byte[] content) {

	}

	public byte[] getContent() {
		int total = getLength();
		byte[] content = new byte[total];
 
		System.arraycopy(ByteUtil.str2Bcd(startDate), 0, content, 0, 6); 

		return content;
	}

	public int getLength() {
		return 6;
	}

}
