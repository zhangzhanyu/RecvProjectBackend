package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.IMessageBody;
import com.godenwater.yanyu.utils.ByteUtil;

/**
 * 
 * 中心发取数(从某年某月某日到某年某月某日数据)命令
 * 
 * @ClassName: Down06Body
 * @Description: 此类描述06H为中心发取数(从某年某月某日到某年某月某日数据)命令
 * 
 *               1、开始时间：年1、月1、日1
 *               2、结束时间：年2、月2、日2  
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Down06Body implements IMessageBody, Serializable {

	public String startDay;

	public String endDay;

	public String getStartDay() {
		return startDay;
	}

	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}

	public String getEndDay() {
		return endDay;
	}

	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}

	public void setContents(byte[] content) {

	}

	public byte[] getContent() {
		int total = getLength();
		byte[] content = new byte[total];
 
		int pos = 0;
		System.arraycopy(ByteUtil.str2Bcd(startDay), 0, content, pos, 3);
		pos = pos + 3;

		System.arraycopy(ByteUtil.str2Bcd(endDay), 0, content, pos, 3);

		return content;
	}

	public int getLength() {
		return 6;
	}

}
