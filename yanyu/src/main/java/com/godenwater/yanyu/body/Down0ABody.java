package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.IMessageBody;

/**
 * 
 * 中心发取数(从某年某月某日到某年某月某日数据)命令
 * 
 * @ClassName: Down06Body
 * @Description: 此类描述06H为中心发取数(从某年某月某日到某年某月某日数据)命令
 * 
 *               1、开始时间：年1、月1、日1
 *               2、结束时间：年2、月2、日2  
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Down0ABody extends Down06Body implements IMessageBody, Serializable {
 

}
