package com.godenwater.yanyu.body;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.godenwater.yanyu.IMessageBody;
import com.godenwater.yanyu.utils.ByteUtil;

/**
 * 
 * 默认下行报文的报文结构，仅包含流水号及发报时间
 * 
 * @ClassName: DownBase
 * @Description: 此类描述基本的“下行报文帧的结构”框架
 * 
 *               1、发报时间
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class DownBody implements IMessageBody, Serializable {

	private String senddate;

	public String getSenddate() {
		if (senddate == null || senddate.equals("")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			senddate = sdf.format(new Date());
		}
		return senddate;
	}

	public void setSenddate(String senddate) {
		if (senddate == null || senddate.equals("")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			senddate = sdf.format(new Date());
		}
		this.senddate = senddate;
	}

	public void setContents(byte[] content) {
		if (content == null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			senddate = sdf.format(new Date());
		}
		this.senddate = ByteUtil.bcd2Str(content);
	}

	public byte[] getContent() {

		byte[] content = new byte[6];

		if (senddate == null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			senddate = sdf.format(new Date());
		}

		System.arraycopy(ByteUtil.str2Bcd(senddate), 0, content, 0, 6);

		return content;
	}

	public int getLength() {
		return 6;
	}

}
