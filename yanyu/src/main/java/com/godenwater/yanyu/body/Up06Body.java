package com.godenwater.yanyu.body;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.godenwater.yanyu.AbstractBody;

/**
 * 
 * 特征为06H,表示读取水位数据
 * 
 * @ClassName: Up0ABody
 * @Description: 召测报，上行报文结构
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up06Body extends AbstractBody implements Serializable {
 
	/**
	 * 各内容项
	 */
	private byte[] serial = new byte[2];// 序列号（2）,范围：0000H------0FFFH。

 
	private Map<Date, String> data = new HashMap<Date, String>();// 雨量的时间，四个字节（2）,范围：0000H------0FFFH。

	   
	public byte[] getSerial() {
		return serial;
	}

	public void setSerial(byte[] serial) {
		this.serial = serial;
	}


	public Map<Date, String> getData() {
		return data;
	}

	public void setData(Map<Date, String> data) {
		this.data = data;
	}

	@Override
	public byte[] getContent() {
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		System.arraycopy(sendDate, 0, content, 0, 5);
	 
		
//		int pos = 5 + 55;

//		if (items != null) {
//			for (int i = 0; i < items.size(); i++) {
//				DataItem item = (DataItem) (items.get(i));
//				if (item != null) {
//					System.arraycopy(item.getLabel(), 0, content, pos, item
//							.getLabel().length);
//					pos = pos + item.getLabel().length;
//					System.arraycopy(item.getValue(), 0, content, pos, item
//							.getValue().length);
//					pos = pos + item.getValue().length;
//				}
//			}
//		}
		return content;
	}

	public int getLength() {
		int pos = 0;
//		if (items != null) {
//			for (int i = 0; i < items.size(); i++) {
//				DataItem item = (DataItem) (items.get(i));
//				pos = pos + item.getLabel().length + item.getValue().length;
//			}
//		}

		return 5 + 55 + pos;
	}

}
