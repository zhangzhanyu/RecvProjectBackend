package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.AbstractBody;
import com.godenwater.yanyu.IMessageBody;

/**
 * 
 * 人工拍报（GPRS）
 * 
 * @ClassName: Up08Body
 * @Description: 此类描述基本的“下行报文帧的结构”框架
 * 
 *               1、发报时间
 * 
 * @author lipujun
 * @date Feb 19, 2013
 * 
 */
public class Up08Body extends AbstractBody implements IMessageBody,
		Serializable {

	/**
	 * 时间1为年、月、日、时、分（BCD码），终端发送时间。
	 */
	private byte[] F1 = new byte[5];// 

	// 2．人工拍报数据为06h字节一组，表示5位电报码，第一字节用‘A’代表扩号，用‘B’代表X，其它为键盘输入的原码。
	// 如键入（12345），则传输的6个字节为：41、31、32、33、34、35。
	// 键入X1234，则传输的6个字节为：42、42、31、32、33、34。
	// 键入12345，则传输的6个字节为：30、31、32、33、34、35。
	private byte[] F2 = new byte[6];

	public byte[] getF1() {
		return F1;
	}

	public void setF1(byte[] f1) {
		F1 = f1;
	}

	public byte[] getF2() {
		return F2;
	}

	public void setF2(byte[] f2) {
		F2 = f2;
	}

	@Override
	public byte[] getContent() {

		int total = getLength();
		byte[] content = new byte[total];

		// 转换为5字节的BCD码
		int pos = 0;
		System.arraycopy(F1, 0, content, pos, F1.length);
		pos = pos + F1.length;

		System.arraycopy(F2, 0, content, pos, F2.length);
		pos = pos + F2.length;

		return content;
	}

	@Override
	public int getLength() {
		return F1.length + F2.length;
	}

}
