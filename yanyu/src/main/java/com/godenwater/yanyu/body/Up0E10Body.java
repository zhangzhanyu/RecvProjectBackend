package com.godenwater.yanyu.body;

import com.godenwater.yanyu.AbstractBody;

import java.io.Serializable;

/**
 * 
 * 特征为0Eh，表示正点发报。
 * 10,表示是闸位数据
 * 
 * @ClassName: Up0E10Body
 * @Description: 正点报，上行报文结构
 * @author lipujun
 * @date Feb 20, 2013
 * 
 */
public class Up0E10Body extends AbstractBody implements Serializable {
 
	/**
	 * 各内容项
	 */
	private byte[] F1 = new byte[2];// 雨量累计值（2）,范围：0000H------0FFFH。

	private byte[] F2 = new byte[2];// 时段雨量（2）,范围：0000H------0FFFH。

	private byte[] F3 = new byte[2];// 日雨量（2）,范围：0000H------0FFFH。

	private byte[] F4 = new byte[12];// 雨量5（12）

	private byte[] F5 = new byte[24];// 水位5（24）

	private byte[] F6 = new byte[2];// 电压（2），2字节BCD码，表示XX。XX伏特。

	private byte[] F7 = new byte[10];//

	private byte[] F8 = new byte[24];//1个闸位数据3字节，闸位数据3*8=24字节。
	// 最高字节高4bit表示数据类型，1---表示闸位。最高字节低4bit表示闸位计号，其他2字节是闸位值

	public byte[] getF1() {
		return F1;
	}

	public void setF1(byte[] f1) {
		F1 = f1;
	}

	public byte[] getF2() {
		return F2;
	}

	public void setF2(byte[] f2) {
		F2 = f2;
	}

	public byte[] getF3() {
		return F3;
	}

	public void setF3(byte[] f3) {
		F3 = f3;
	}

	public byte[] getF4() {
		return F4;
	}

	public void setF4(byte[] f4) {
		F4 = f4;
	}

	public byte[] getF5() {
		return F5;
	} 

	public void setF5(byte[] f5) {
		F5 = f5;
	}

	public byte[] getF6() {
		return F6;
	}

	public void setF6(byte[] f6) {
		F6 = f6;
	}

	public byte[] getF7() {
		return F7;
	}

	public void setF7(byte[] f7) {
		F7 = f7;
	}

	public byte[] getF8() {
		return F8;
	}

	public void setF8(byte[] f8) {
		F8 = f8;
	}


	
	/**
	 * 6、观测要素
	 */
	// public List<DataItem> items = null;// new ArrayList<DataItem>();
	//
	// public List<DataItem> getItems() {
	// return items;
	// }
	//
	// public void setItems(List<DataItem> items) {
	// this.items = items;
	// }
	//
	// public void addItem(DataItem item) {
	// if (items == null) {
	// items = new ArrayList<DataItem>();
	// }
	// items.add(item);
	// }

	@Override
	public byte[] getContent() {
		if (this.content != null)
			return content;

		int total = getLength();
		content = new byte[total];

		System.arraycopy(sendDate, 0, content, 0, 5);
		System.arraycopy(F1, 0, content, 5, 2);
		System.arraycopy(F2, 0, content, 7, 2);
		System.arraycopy(F3, 0, content, 9, 2);
		System.arraycopy(F4, 0, content, 11, 12);
		System.arraycopy(F5, 0, content, 23, 24);
		System.arraycopy(F6, 0, content, 47, 2);
		System.arraycopy(F7, 0, content, 49, 10);
		System.arraycopy(F8, 0, content, 59, 24);

		
//		int pos = 5 + 55;

//		if (items != null) {
//			for (int i = 0; i < items.size(); i++) {
//				DataItem item = (DataItem) (items.get(i));
//				if (item != null) {
//					System.arraycopy(item.getLabel(), 0, content, pos, item
//							.getLabel().length);
//					pos = pos + item.getLabel().length;
//					System.arraycopy(item.getValue(), 0, content, pos, item
//							.getValue().length);
//					pos = pos + item.getValue().length;
//				}
//			}
//		}
		return content;
	}

	public int getLength() {
		int pos = 0;
//		if (items != null) {
//			for (int i = 0; i < items.size(); i++) {
//				DataItem item = (DataItem) (items.get(i));
//				pos = pos + item.getLabel().length + item.getValue().length;
//			}
//		}

		return 5 + 78 + pos;
	}

	public static void main(String[] args){
		byte[] F111 = new byte[1];// 状态（1）
		if(F111[0]==0){
			System.out.println(">> is null" );
		}else{
			System.out.println(">> not null" );
		}
	}
	
}
