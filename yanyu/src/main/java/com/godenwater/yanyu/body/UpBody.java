package com.godenwater.yanyu.body;

import java.io.Serializable;

import com.godenwater.yanyu.IMessageBody;
 

public class UpBody implements IMessageBody, Serializable {

	private byte[] content;

	public void setContents(byte[] content) {
		this.content = content;
	}
	
	public byte[] getContent() {
		if (this.content != null)
			return content;

		return content;
	}

	public int getLength() {
		if (this.content != null) {
			return this.content.length;
		} else {
			return 0;
		}
	}

}
