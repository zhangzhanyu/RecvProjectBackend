package com.godenwater.yanyu.command;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.godenwater.yanyu.Command;
import com.godenwater.yanyu.IMessage;
import com.godenwater.yanyu.Symbol;
import com.godenwater.yanyu.body.BaseBody;
import com.godenwater.yanyu.body.Down03Body;
import com.godenwater.yanyu.body.Down05Body;
import com.godenwater.yanyu.body.Down06Body;
import com.godenwater.yanyu.body.Down07Body;
import com.godenwater.yanyu.utils.ByteUtil;

/**
 * 下发命令类，用于中心站向遥测站发送请求
 *
 * @author lipujun
 * @ClassName: DownCommand
 * @Description: 下发命令类
 * @date Mar 1, 2013
 */
public class DownCommand extends Command {


    private static SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");

    /**
     * M2、M4模式如下：
     * <p>
     * 报文起始符为：STX
     * <p>
     * 报文结束符为：ENQ、ACK、EOT、ESC。 ENQ表示请求；ACK表示应答；EOT表示；ESC表示
     * <p>
     * ------------------------------------------------------------------------
     * <p>
     * M3模式如下：
     * <p>
     * 报文起始符为：SYN。表示多包发送，一次确认的传输模式中使用
     * <p>
     * 报文结束符为：ENQ、ACK、EOT、ESC。
     * ENQ表示请求；ACK表示作为有后续报文帧的“确认”结束符；EOT表示作为传输结束确认帧报文符，表示可以退出通信
     * 。；ESC表示传输结束，终端保持在线
     */
    public DownCommand() {
        super(Symbol.DOWN);
    }

    /**
     * @return
     */
    public IMessage sendReplyMessage(Boolean isSetTm) {
    	 BaseBody body = new BaseBody();
        String now = sdf.format(new Date());
        if (isSetTm) {
            body.setContents(ByteUtil.HexStringToBinary(now));
        }
        return buildMessage(body);
    }


    /**
     * 发送ACK回复报文，此报文仅在多发报文需要继续发送时出现。
     * 报文内容如：7E B4 10 0A 03 06 2F 1E
     *
     * @param isSetTm 是否设置下发时间
     * @return
     */
    public IMessage sendAckMessage(Boolean isSetTm) {
        BaseBody body = new BaseBody();
        String now = sdf.format(new Date());
        if (isSetTm) {
            body.setContents(ByteUtil.HexStringToBinary(now));
        }
        return buildMessage(body);
    }


    /**
     * @param sequence
     * @param date
     * @return
     */
    public IMessage send03Message(String sendDate, String f1, String f2,
                                  String f3, String f4, String f5, String f6, String f7, String f8,
                                  String f9, String f10, String f11, String f12) {
        Down03Body body = new Down03Body();

        body.setSendDate(sendDate);
        body.setF1(f1);
        body.setF2(f2);
        body.setF3(f3);
        body.setF4(f4);
        body.setF5(f5);
        body.setF6(f6);
        body.setF7(f7);
        body.setF8(f8);
        body.setF9(f9);
        body.setF10(f10);
        body.setF11(f11);
        body.setF12(f12);

        return buildMessage(body);
    }

    /**
     * @param sequence
     * @param date
     * @return
     */
    public IMessage send05Message(String startDate) {
        Down05Body body = new Down05Body();
        body.setStartDate(startDate);
        return buildMessage(body);
    }

    /**
     * 06H，表示下发读取水位数据的命令
     *
     * @param startDate ，时间格式：yyMMdd
     * @param endDay    ，时间格式：yyMMdd
     * @return
     */
    public IMessage send06Message(String startDay, String endDay) {
        Down06Body body = new Down06Body();
        body.setStartDay(startDay);
        body.setEndDay(endDay);
        return buildMessage(body);
    }

    /**
     * @param sequence
     * @param date
     * @return
     */
    public IMessage send07Message(String startDate) {
        Down07Body body = new Down07Body();
        body.setStartDate(startDate);
        return buildMessage(body);
    }


    /**
     * 0AH，表示下发读取雨量数据的命令，
     *
     * @param startDate ，时间格式：yyMMdd
     * @param endDay    ，时间格式：yyMMdd
     * @return
     */
    public IMessage send0AMessage(String startDay, String endDay) {
        return send06Message(startDay, endDay);
    }

    /**
     * 构造重发报文
     * <p>
     * 功能码：51
     */
    public IMessage sendRepeatMessage() {


        return null;
    }

    public static void main(String[] args) {


        String s = "CLR";//String变量

        byte[] b = s.getBytes();//String转换为byte[]

        String t = new String(b);//bytep[]转换为String
        System.out.println("byte " + ByteUtil.byteToHexString(b));

        DownCommand cmd = new DownCommand();
        cmd.setStartBit(Symbol.SOH);
        // cmd.setCenterAddr(ByteUtil.HexStringToBinary("FF"));
        // cmd.setStationAddr(ByteUtil.HexStringToBinary("0011223344"));
        cmd.setCenterAddr(new byte[]{(byte) 0x10});
        cmd.setStationAddr(new byte[]{(byte) 0x02});
        cmd.setFuncCode(new byte[]{(byte) 0x0e});
        // cmd.sendRepeatMessage(1, null);

        cmd.setEof(Symbol.ETX);
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        IMessage msg05 = cmd.send05Message(sdf.format(new Date()));
        System.out.println(">> MSG 05 " + cmd.printHexString(msg05));

        IMessage msg = cmd.sendReplyMessage(true);
        System.out.println(">> hex reply " + cmd.printHexString(msg));
        // cmd.send4EMessage(1, null, 22, "11110", "1234");
        // cmd.setEof(Symbol.ENQ);

        cmd.setEof(Symbol.ACK);
        IMessage msgAck = cmd.sendAckMessage(true);
        System.out.println(">> hex ack " + cmd.printHexString(msgAck));


        // ----------参数配置命令（GPRS）-----------------------05------------
        // 150703140450 02 02 07 3000 29 19 4000 02 FFFF FFFFFFFFFFFF
        // FFFFFFFFFFFF 30
        cmd.setFuncCode(new byte[]{(byte) 0x03});
        cmd.setEof(Symbol.ENQ);
        IMessage msg03 = cmd.send03Message("150703140450", "02", "02", "07", "3000", "29", "19", "4000", "02", "FFFF", "FFFFFFFFFFFF", "FFFFFFFFFFFF", "30");// 从150703 13:58:50
        System.out.println(">> hex 03H " + cmd.printHexString(msg03));

        cmd.setFuncCode(new byte[]{(byte) 0x05});
        cmd.setEof(Symbol.ENQ);
        IMessage msg0522 = cmd.send05Message("150703135850");// 从150703 13:58:50
        System.out.println(">> hex 05H " + cmd.printHexString(msg0522));

        // -----------远程（GPRS）或本地提取固态存储器数据------------06-0A----------------
        cmd.setFuncCode(new byte[]{(byte) 0x06});
        cmd.setEof(Symbol.ENQ);
        IMessage msg06 = cmd.send06Message("150702", "150704");// 从15年07月02 --
        // 15年07月04日
        System.out.println(">> hex 06H " + cmd.printHexString(msg06));

        cmd.setFuncCode(new byte[]{(byte) 0x0a});
        cmd.setEof(Symbol.ENQ);
        IMessage msg0A = cmd.send06Message("150702", "150704");// 从15年07月02 --
        // 15年07月04日
        System.out.println(">> hex 0AH " + cmd.printHexString(msg0A));


        System.out.println(">> ascIIHex " + ByteUtil.toHexString("12345X".getBytes()));
        // IMessage message;
        // // ------------------------------
        // cmd.setFuncCode((byte) 0x37);
        // message = cmd.send37Message(1, null);
        // cmd.printHexString(message);
        // // ------------------------------
        //
        // cmd.setFuncCode((byte) 0x38);
        // message = cmd.send38Message(1, null, "13010108", "13030108",
        // "000600",
        // "F0,1F,20");
        // cmd.printHexString(message);
        //
        // // ------------------------------
        //
        // cmd.setFuncCode((byte) 0x3A);
        // message = cmd.send3AMessage(0x3A, null, "F0,1F,20");
        // cmd.printHexString(message);

    }
}
