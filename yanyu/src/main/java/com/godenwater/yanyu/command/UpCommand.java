package com.godenwater.yanyu.command;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import com.godenwater.yanyu.Command;
import com.godenwater.yanyu.IMessage;
import com.godenwater.yanyu.Symbol;
import com.godenwater.yanyu.body.BaseBody;
import com.godenwater.yanyu.body.Up0EBody;
import com.godenwater.yanyu.utils.ByteUtil;

public class UpCommand extends Command {

	/**
	 * M1、M2、M4模式如下：
	 * 
	 * 报文起始符为：STX
	 * 
	 * 报文结束符为：ETX、ETB。 ETX表示报文结束，后续无报文；ETB表示在报文分包传输时作为结束符，表示未完成，不可退出通信；
	 * 
	 * ------------------------------------------------------------------------
	 * 
	 * M3模式如下：
	 * 
	 * 报文起始符为：SYN。表示多包发送，一次确认的传输模式中使用
	 * 
	 * 报文结束符为：ETX、ETB。 ETX表示报文结束，后续无报文；ETB表示在报文分包传输时作为结束符，表示未完成，不可退出通信；
	 * 
	 * @param funcCode
	 *            ，功能码，表示需要发送哪种报文
	 * @param eof
	 *            ，报文结束符
	 */
	public UpCommand() {
		super(Symbol.UP);
	}

	public IMessage sendMessage(byte[] content) {
		BaseBody body = new BaseBody();
		body.setContents(content);
		return buildMessage(body);
	}

	/**
	 * 特征为0Eh，表示正点发报。 特征为0Bh，表示雨量加报。 特征为09h，表示水位加报。 特征为0Ch，表示测试信息。 功能码：0Eh
	 */
	public IMessage sendH0EMessage(String date, byte[] f1, byte[] f2,
			byte[] f3, byte[] f4, byte[] f5, byte[] f6, byte[] f7, byte[] f8,
			byte[] f9, byte[] f10, byte[] f11, byte[] f12) {
		Up0EBody body = new Up0EBody();
		initBase(body, date);

		body.setF1(f1);
		body.setF2(f2);
		body.setF3(f3);
		body.setF4(f4);
		body.setF5(f5);
		body.setF6(f6);
		body.setF7(f7);
		body.setF8(f8);
		body.setF9(f9);
		body.setF10(f10);
		body.setF11(f11);
		body.setF12(f12);

		return buildMessage(body);
	}
 

	/**
	 * 人工置数报
	 * 
	 * 功能码：35
	 */
	public IMessage send35Message(String date, String rgzs) {
		return null;
	}

	/**
	 * 图片报
	 * 
	 * @param sequence
	 * @param date
	 * @param stcd
	 * @param sttp
	 * @param viewDate
	 * @param pic
	 * @return
	 */
	public IMessage send36Message(String date, String stcd, String sttp,
			String viewDate, byte[] pic) {
		// Up36Body body = new Up36Body();
		// initBase(body, sequence, date);
		// body.setStcd(stcd);
		// body.setSttp(sttp);
		// body.setViewDate(viewDate);
		// body.addItem((byte) 0xF3, pic);
		// return buildMessage(body);
		return null;
	}

	private byte[] readImage(String fileName) {

		InputStream is = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			is = new FileInputStream(fileName);// pathStr 文件路径
			byte[] b = new byte[4096];
			int n;
			while ((n = is.read(b)) != -1) {
				out.write(b, 0, n);

			}// end while
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return out.toByteArray();

	}

	public static void main(String[] args) {
		UpCommand cmd = new UpCommand();
		
		cmd.setCenterAddr(new byte[] { (byte) 0x10 });
		cmd.setStationAddr(new byte[] { (byte) 0x02 });
		cmd.setFuncCode(new byte[] { (byte) 0x0e });
		
		cmd.setEof(Symbol.ETX);

		String hexstr = "1506241000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1220FFFFFFFF02600281FF0000";
		byte[] content = ByteUtil.HexStringToBinary(hexstr);

		IMessage message = cmd.sendMessage(content);
		String hexStr = cmd.toHexString(message.getHeader());
		System.out.print(">> hex " + hexStr.toUpperCase());
		System.out.print(""
				+ ByteUtil.toHexString(message.getBody().getContent()));
		System.out.print("" + ByteUtil.toHexString(message.getEOF()));
		System.out.print("" + ByteUtil.toHexString(message.getCRC()));
	}

}
