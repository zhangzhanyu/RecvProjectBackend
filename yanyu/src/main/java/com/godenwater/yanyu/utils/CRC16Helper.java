package com.godenwater.yanyu.utils;

import java.util.Arrays;


/*************************************************************************
 *  Compilation:  javac CRC16.java
 *  Execution:    java CRC16 s
 *  
 *  Reads in a string s as a command-line argument, and prints out
 *  its 16-bit Cyclic Redundancy Check (CRC16). Uses a lookup table.
 *
 *  Reference:  http://www.gelato.unsw.edu.au/lxr/source/lib/crc16.c
 *
 *  % java CRC16 123456789
 *  CRC16 = bb3d
 *
 * Uses irreducible polynomial:  1 + x^2 + x^15 + x^16
 *
 *
 *************************************************************************/

/*************************************************************************
 *循环冗余校验码（CRC）的基本原理是： 在K位信息码后再拼接R位的校验码，整个编码长度为N位，因此，这种编码又叫（N，K）码。对
 * 于一个给定的（N，K）码，可以证明存在一个最高次幂为N-K=R的多项式G(x)。根据G(x)可以生
 * 成K位信息的校验码，而G(x)叫做这个CRC码的生成多项式。
 * 
 * 校验码的具体生成过程为：
 * 
 * 假设发送 信息用信息多项式C(X)表示，将C(x)左移R位，则可表示成C(x)*2的R次方，
 * 
 * 这样C(x)的右边就会 空出R位，这就是校验码的位置。
 * 
 * 通过C(x)*2的R次方除以生成多项式G(x)得到的余数就是校验码。
 *************************************************************************/
public class CRC16Helper {

	// CRC-16的多项式： X16+X15+X2+1 表达式的CRC码表,0x1005 // ASC 1 1000 0000 0000 0101
	private final static int[] table = { 0x0000, 0xC0C1, 0xC181, 0x0140,
			0xC301, 0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0, 0x0780, 0xC741,
			0x0500, 0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41,
			0x0F00, 0xCFC1, 0xCE81, 0x0E40, 0x0A00, 0xCAC1, 0xCB81, 0x0B40,
			0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941,
			0x1B00, 0xDBC1, 0xDA81, 0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40,
			0xDD01, 0x1DC0, 0x1C80, 0xDC41, 0x1400, 0xD4C1, 0xD581, 0x1540,
			0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341,
			0x1100, 0xD1C1, 0xD081, 0x1040, 0xF001, 0x30C0, 0x3180, 0xF141,
			0x3300, 0xF3C1, 0xF281, 0x3240, 0x3600, 0xF6C1, 0xF781, 0x3740,
			0xF501, 0x35C0, 0x3480, 0xF441, 0x3C00, 0xFCC1, 0xFD81, 0x3D40,
			0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0, 0x3B80, 0xFB41,
			0x3900, 0xF9C1, 0xF881, 0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940,
			0xEB01, 0x2BC0, 0x2A80, 0xEA41, 0xEE01, 0x2EC0, 0x2F80, 0xEF41,
			0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541,
			0x2700, 0xE7C1, 0xE681, 0x2640, 0x2200, 0xE2C1, 0xE381, 0x2340,
			0xE101, 0x21C0, 0x2080, 0xE041, 0xA001, 0x60C0, 0x6180, 0xA141,
			0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740,
			0xA501, 0x65C0, 0x6480, 0xA441, 0x6C00, 0xACC1, 0xAD81, 0x6D40,
			0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01, 0x6AC0, 0x6B80, 0xAB41,
			0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981, 0x7940,
			0xBB01, 0x7BC0, 0x7A80, 0xBA41, 0xBE01, 0x7EC0, 0x7F80, 0xBF41,
			0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580, 0xB541,
			0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340,
			0xB101, 0x71C0, 0x7080, 0xB041, 0x5000, 0x90C1, 0x9181, 0x5140,
			0x9301, 0x53C0, 0x5280, 0x9241, 0x9601, 0x56C0, 0x5780, 0x9741,
			0x5500, 0x95C1, 0x9481, 0x5440, 0x9C01, 0x5CC0, 0x5D80, 0x9D41,
			0x5F00, 0x9FC1, 0x9E81, 0x5E40, 0x5A00, 0x9AC1, 0x9B81, 0x5B40,
			0x9901, 0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980, 0x8941,
			0x4B00, 0x8BC1, 0x8A81, 0x4A40, 0x4E00, 0x8EC1, 0x8F81, 0x4F40,
			0x8D01, 0x4DC0, 0x4C80, 0x8C41, 0x4400, 0x84C1, 0x8581, 0x4540,
			0x8701, 0x47C0, 0x4680, 0x8641, 0x8201, 0x42C0, 0x4380, 0x8341,
			0x4100, 0x81C1, 0x8081, 0x4040 };

	// CRC-CCITT多项式： X16+X12+X5+1 CRC码表 0x1021 // ASC 1 0001 0000 0010 0001
	private final static int[] crc_16_12_5_1_table = { 0x0000, 0x1021, 0x2042,
			0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 0x8108, 0x9129, 0xa14a,
			0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 0x1231, 0x0210, 0x3273,
			0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 0x9339, 0x8318, 0xb37b,
			0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 0x2462, 0x3443, 0x0420,
			0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 0xa56a, 0xb54b, 0x8528,
			0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 0x3653, 0x2672, 0x1611,
			0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 0xb75b, 0xa77a, 0x9719,
			0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 0x48c4, 0x58e5, 0x6886,
			0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 0xc9cc, 0xd9ed, 0xe98e,
			0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 0x5af5, 0x4ad4, 0x7ab7,
			0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 0xdbfd, 0xcbdc, 0xfbbf,
			0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 0x6ca6, 0x7c87, 0x4ce4,
			0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 0xedae, 0xfd8f, 0xcdec,
			0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 0x7e97, 0x6eb6, 0x5ed5,
			0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 0xff9f, 0xefbe, 0xdfdd,
			0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 0x9188, 0x81a9, 0xb1ca,
			0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 0x1080, 0x00a1, 0x30c2,
			0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 0x83b9, 0x9398, 0xa3fb,
			0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 0x02b1, 0x1290, 0x22f3,
			0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 0xb5ea, 0xa5cb, 0x95a8,
			0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 0x34e2, 0x24c3, 0x14a0,
			0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 0xa7db, 0xb7fa, 0x8799,
			0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 0x26d3, 0x36f2, 0x0691,
			0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 0xd94c, 0xc96d, 0xf90e,
			0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 0x5844, 0x4865, 0x7806,
			0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 0xcb7d, 0xdb5c, 0xeb3f,
			0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 0x4a75, 0x5a54, 0x6a37,
			0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 0xfd2e, 0xed0f, 0xdd6c,
			0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 0x7c26, 0x6c07, 0x5c64,
			0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 0xef1f, 0xff3e, 0xcf5d,
			0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 0x6e17, 0x7e36, 0x4e55,
			0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0 };

	/**
	 * 生成CRC表的程序 ，若表达式不一样，请将表达式进行修改。
	 * @param crc
	 */
	public static void makeCrc16Table(int crc) {

		// 这里int型是占4个字节。0x00000000,当然值、只用到后2个字节,也可以直接用unsigned short ,
		// 用无符号是因为C语言里面没有无符号移位的功能。
		// int crc = 'a';// 计算字符a的crc16校验码
		// 右移8位，和手动计算一样，左移相当于补0，这里相当于直接补了8个0，开始计算。
		int aaa = crc;
		crc <<= 8; // <<= 相当余 crc=crc<<8;
		// 计算8次。
		for (int i = 0; i < 8; i++) {
			// 如果最高位是1的话需要计算，如果不是直接左移。（左移的操作可以想象成补0)
			if ((crc & 0x8000) != 0) {
				crc <<= 1;
				crc = crc ^ 0x1021;// 这个说明用的是 CRC16 x16+x12+x5+1.  A001H
			} else {
				crc <<= 1;
			}
		}
		// 取后16位，如果用的是crc使用的是unsigned short 就不需要这一步了。
		crc = crc & 0xffff;
		// 输出。
		System.out.println("crc " + aaa + " HEX " + Integer.toHexString(crc)
				+ " BIN " + Integer.toBinaryString(crc));
	}

	/**
	 * 这个方法暂时有问题
	 * @param data
	 */
	public static void makeCRC(byte[] data) {

		int crc, i, j;
		crc = data[0] << 8;
		int len = data.length;
		for (i = 1; i <= len; i++) {
			// crc = data[i] << 8;
			for (j = 1; j <= 8; j++) {
				if ((crc & 0x8000) == 0x8000)
					crc = (crc << 1) ^ 0x1021; // 0x1021
				else
					crc = crc << 1;
			}
			if (i != len)
				crc = crc ^ (data[i] << 8);
		}
		crc = crc & 0xffff;
		System.out.println("make " + Integer.toHexString(crc));
	}
	
	/**
	 * CRC校验码
	 * @param bytes
	 * @return
	 */
	public static byte[] crc16Check(byte[] bytes) {

		int crc = 0xffff;
		for (byte b : bytes) {
			crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
		}

		return ByteUtil.ushortToBytes(crc);

	}

	public static String crc16Check(String hexString) {
		if (hexString == null) {
			return null;
		}
//System.out.println(">> crc16Check " + hexString);
		byte[] bytes = StringCodeHelper.hexString2ByteArray(hexString);
		int crc = 0xffff;
		for (byte b : bytes) {
			crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
		}
		return StringCodeHelper.fillupString(Integer.toHexString(crc), 4, '0',
				true).toUpperCase();
	}

	public static void main(String[] args) {
		String str = "7e7e101234567890000034005302001b121220160000f1f1123456789048f0f01212201600f4f4000000000000000000000000f5f50aaa0aaaffffffffffffffffffffffffffffffffffffffff261900000020190000003922000027303812122017";
		String str1 = "7e7e101234567890000033001c020145121221164000f1f1123456789048f0f01212211640261900006017";
		String str2 = "7E7E0012345678FFFFFF37800802000013022511461705";
//		 str2 = "7E7E030100000003020234005902000B130423150000F1F1000000000348F0F01304231400F460000000000000000000000000F5C0000B000B000B000B000B000B000BFFFF000B000B000B000BF0F0130423150026190000302019000030391A00001238121207";
		 str2 = "7e7e030100000003020234005902000b130423150000f1f1000000000348f0f01304231400f460000000000000000000000000f5c0000b000b000b000b000b000b000bffff000b000b000b000bf0f0130423150026190000302019000030391a0000123812120703";

		 str2 = "1B";
		 str2="7E10020E3F1506241000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1220FFFFFFFF02600281FF000003";//EFEC
		byte[] test =  ByteUtil.HexStringToBinary(str2);
//		 ByteUtil.byteToHexString(test);
		 System.out.println( ">> str " + ByteUtil.byteToHexString(test));
			
		String hexStr = crc16Check(str2);
		byte[] ccc = new byte[]{(byte)0x7e};
		byte[] a = StringCodeHelper.hexString2ByteArray(str2);
		byte[] eee = ByteUtil.HexStringToBinary(str2);
		System.out.println( ">> length " +  StringCodeHelper.hexString2ByteArray(str2).length);
		System.out.println( ">> length " + ByteUtil.HexStringToBinary(str2).length);
		if (Arrays.equals(a, eee)) {
			System.out.println(">>>>> ok");
		} else {
			System.out.println(">>>>> false");
		}
		
		
		
		byte[] crcResult = CRC16Helper.crc16Check(ByteUtil.HexStringToBinary(str2));
		System.out.println(hexStr + " result " + ByteUtil.toHexString(crcResult));
		if (Arrays.equals(crcResult, new byte[]{(byte)0xb6,(byte)0xAa})) {
			System.out.println("...... ok");
		} else {
			System.out.println("..... false");
		}
		
		System.out.println(hexStr);

		// byte[] zz = new byte[2];
		// zz[0] = 21;
		// zz[1] = 56;
		// System.out.println(Integer.toHexString(2156));
		//		
		byte[] b = ByteUtil.HexStringToBinary(hexStr);

		byte[] c = ByteUtil.HexStringToBinary(hexStr);

		System.out.println("HEX " + ByteUtil.byteToHexString(c));

		if (Arrays.equals(b, c)) {
			System.out.println("ok");
		}

		System.out.println("result " + ByteUtil.byteToHexString(b));

		System.out.println("********************************************************");
		
		
		String ascstr = "01464630303132333435363738303030413333303034310230303034313330333037303631383330535420303031323334353637382048205454203133303330373036313820505220303030322e302056542031322e33332003";
		ascstr = "7E7E0001744100FF99994A800802000113112118014605";
		ascstr="7e0ffe0a0917060117060505";
		String ascResult = CRC16Helper.crc16Check(ascstr);
		
		System.out.println("crc :"+ascResult);
		byte[] bb = ByteUtil.HexStringToBinary("35383337");
		String bbcrc = new String(bb);//此时才为十六进制的字符串，再进行转换
		
		System.out.println(">>> ASC RESULT 35383337 \t " +ascResult  + "\t " + new String(bb));
		
//		for(int i=0;i<256;i++){
//			CRC16Helper.makeCrc16Table(i);
//		}
	}

}
